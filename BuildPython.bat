cls
@echo off

echo RUN COMPILATION 
set ProjectSln=%cd%\src\net\PragmaticQuant.sln
call dotnet build %ProjectSln% -c Release
if not "%errorlevel%"=="0" goto compilation_failure

echo RUN UNIT TESTS
set nunitpath=%cd%\src\net\packages\NUnit.ConsoleRunner.3.7.0\tools\nunit3-console.exe
set testdll=%cd%\src\net\test\bin\Release\test.dll 
call %nunitpath% %testdll%
if not "%errorlevel%"=="0" goto compilation_failure

echo COPY DLLS
set binDir=%cd%\src\net\PragmaticQuant.DslPayoff\bin\Release
set pyDir=%cd%\src\python
set pragmaticquantDir=%pyDir%\pragmaticquant
xcopy %binDir%\*.dll %pragmaticquantDir% /C /Y
xcopy %binDir%\*.pdb %pragmaticquantDir% /C /Y


set distDir=%cd%\dist
md %distDir%
cd %pyDir%

echo RUN PYTHON TEST 
pip install -r %pyDir%\requirements.txt
python setup.py develop
python -m unittest discover %pyDir% -v

echo MAKE PYTHON DIST
python setup.py sdist --formats=zip
xcopy %pyDir%\dist\*.* %distDir% /C /Y
cd ..\..

goto end
pause

:compilation_failure
echo COMPILATION FAILED !
exit %1

:end
echo BUILD SUCCEED !
pause
