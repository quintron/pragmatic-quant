cls
@echo off

echo RUN COMPILATION 
set ProjectSln=%cd%\src\net\XlPragmaticQuant.sln
set msBuildDir=%WINDIR%\Microsoft.NET\Framework\v4.0.30319
call %msBuildDir%\MSBuild.exe %ProjectSln% /t:Rebuild /p:Configuration=Release /verbosity:n
if not "%errorlevel%"=="0" goto compilation_failure

echo RUN UNIT TESTS
set nunitpath=%cd%\src\net\packages\NUnit.ConsoleRunner.3.7.0\tools\nunit3-console.exe
set testdll=%cd%\src\net\xltest\bin\Release\xltest.dll
call %nunitpath% %testdll%
if not "%errorlevel%"=="0" goto compilation_failure

echo COPY DLLS
set binDir=%cd%\src\net\PragmaticQuant.Xl\bin\Release
set targetDir=%cd%\xldist
md %targetDir%
xcopy %binDir%\*.dll %targetDir% /C /Y
xcopy %binDir%\*.pdb %targetDir% /C /Y
xcopy %binDir%\pragmatic-quant.xll %targetDir% /C /Y
xcopy %binDir%\pragmatic-quant.dna %targetDir% /C /Y

goto end
pause

:compilation_failure
echo COMPILATION FAILED !

:end

echo XL BUILD SUCCEED !
echo DLLS in %targetDir%
pause
