namespace PragmaticQuant.Core
{
    public static class PdeStepSolver
    {
        public static IPdeStepSolver<TState, double[]> ThetaScheme<TState>(IStepOperator<TState, double[]> stepOperator, double theta)
        {
            return new ThetaScheme1D<TState>(stepOperator, theta);
        }

        #region private class
        private class ThetaScheme1D<TState> : ThetaScheme<TState, double[]>
        {
            public ThetaScheme1D(IStepOperator<TState, double[]> stepOperator, double theta)
                : base(stepOperator, theta)
            {
            }
            protected override double[] ValueBuffer(double[] valueTemplate)
            {
                return new double[valueTemplate.Length];
            }
        }
        #endregion
    }

    public interface IPdeStepSolver<TState, TValue>
    {
        void Backward(ref ISlice<TState, TValue> slice, double start, double end);
        void Forward(ref ISlice<TState, TValue> slice, double start, double end);
    }

    public interface ISlice<out TState, TValue>
    {
        TState State { get; }
        TValue Value { get; }

        void Add(TValue val, double w = 1.0);
        void Mult(double w);
        void FillValue(TValue val);
    }
    
    public interface IOperator<TData>
    {
        void Apply(ref TData x);
        void Solve(ref TData y);

        /// <summary>
        /// transpose operator
        /// </summary>
        /// <returns></returns>
        void Adjoint();

        /// <summary>
        /// M = a * M + Id
        /// </summary>
        /// <param name="a"></param>
        void ScalePlusIdentity(double a);

        IOperator<TData> Copy();
    }

    public interface IStepOperator<in TState, TValue>
    {
        IOperator<TValue> Operator(double start, double end, TState grid);
        void FillSourceTerm(ref TValue source, double start, double end, TState grid);
    }

    /// <summary>
    /// Scheme for solving pde : dV/dt + L(V) = 0, with L a linear operator.
    /// 
    /// Pde is approximated on a discrete time step by :
    /// (V(end) - V(start))/dt + theta * L(V)(start) + (1-theta) * L(V)(end) = 0
    /// 
    /// theta=0 => explicit scheme
    /// theta=1 => implicit scheme
    /// theta=0.5 => crank-nicholson scheme
    ///  
    /// </summary>
    internal abstract class ThetaScheme<TState, TValue> : IPdeStepSolver<TState, TValue>
    {
        #region private fields
        private readonly IStepOperator<TState, TValue> stepOperator;
        #endregion
        #region private methods
        private void BuildParts(double start, double end, TState grid,
                                out IOperator<TValue> implicitOp,
                                out IOperator<TValue> explicitOp)
        {
            double dt = end - start;
            implicitOp = stepOperator.Operator(start, end, grid);
            explicitOp = implicitOp.Copy();
            implicitOp.ScalePlusIdentity(-Theta * dt);
            explicitOp.ScalePlusIdentity((1.0 - Theta) * dt);
        }
        protected abstract TValue ValueBuffer(TValue valueTemplate);
        #endregion
        protected ThetaScheme(IStepOperator<TState, TValue> stepOperator, double theta)
        {
            this.stepOperator = stepOperator;
            Theta = theta;
        }
        
        public void Backward(ref ISlice<TState, TValue> slice, double start, double end)
        {
            IOperator<TValue> implicitOp, explicitOp;
            BuildParts(start, end, slice.State, out implicitOp, out explicitOp);

            TValue sliceVal = slice.Value;
            explicitOp.Apply(ref sliceVal);

            TValue source = ValueBuffer(slice.Value);
            stepOperator.FillSourceTerm(ref source, start, end, slice.State);
            slice.Add(source, end - start);

            implicitOp.Solve(ref sliceVal);
        }
        public void Forward(ref ISlice<TState, TValue> slice, double start, double end)
        {
            IOperator<TValue> implicitPart, explicitPart;
            BuildParts(start, end, slice.State, out implicitPart, out explicitPart);

            explicitPart.Adjoint();
            implicitPart.Adjoint();

            var sliceVal = slice.Value;
            implicitPart.Solve(ref sliceVal);
            explicitPart.Apply(ref sliceVal);
        }

        public double Theta { get; private set; }
    }
    
    public class FiniteDiff1D : IStepOperator<RegularGrid1D, double[]>
    {
        #region private fields
        private readonly PdeCoeffSampler1D<RegularGrid1D> pdeCoeffs;
        public readonly BoundaryCondition1D infBoundaryCond;
        public readonly BoundaryCondition1D supBoundaryCond;
        #endregion
        #region private methods
        private void FiniteDifference(double[] d2X, double[] dX, double[] I, double step,
                                      ref double[] lower, ref double[] diag, ref double[] upper)
        {
            for (int i = 0; i < lower.Length; i++)
            {
                double drift = dX[i] * step;
                double diffusion = 2.0 * d2X[i];

                if (drift < -diffusion)
                {
                    diag[i] = (drift - diffusion) / (step * step) + I[i];
                    upper[i] = 0.5 * diffusion / (step * step);
                    lower[i] = (-drift + 0.5 * diffusion) / (step * step);
                }
                else if (drift > diffusion)
                {
                    diag[i] = (-drift - diffusion) / (step * step) + I[i];
                    upper[i] = (drift + 0.5 * diffusion) / (step * step);
                    lower[i] = 0.5 * diffusion / (step * step);
                }
                else
                {
                    diag[i] = -diffusion / (step * step) + I[i];
                    upper[i] = 0.5 * (drift + diffusion) / (step * step);
                    lower[i] = 0.5 * (-drift + diffusion) / (step * step);
                }
            }
        }
        #endregion
        public FiniteDiff1D(PdeCoeffSampler1D<RegularGrid1D> pdeCoeffs, BoundaryCondition1D infBoundaryCond, BoundaryCondition1D supBoundaryCond)
        {
            this.pdeCoeffs = pdeCoeffs;
            this.infBoundaryCond = infBoundaryCond;
            this.supBoundaryCond = supBoundaryCond;
        }
        public FiniteDiff1D(PdeCoeffSampler1D<RegularGrid1D> pdeCoeffs, BoundaryCondition1D boundaryCond)
            :this(pdeCoeffs, boundaryCond, boundaryCond)
        { }

        public IOperator<double[]> Operator(double start, double end, RegularGrid1D grid)
        {
            var gridSize = grid.Size;
            double[] d2X = new double[gridSize];
            double[] dX = new double[gridSize];
            double[] I = new double[gridSize];
            pdeCoeffs.FillCoeff(start, end, grid, ref d2X, ref dX, ref I);

            double[] lower = new double[gridSize];
            double[] diag = new double[gridSize];
            double[] upper = new double[gridSize];
            FiniteDifference(d2X, dX, I, grid.Step, ref lower, ref diag, ref upper);

            var op = new TridiagOperator1D(lower, diag, upper);
            infBoundaryCond.SetInf(op, grid.Step);
            supBoundaryCond.SetSup(op, grid.Step);
            return op;
        }
        public void FillSourceTerm(ref double[] source, double start, double end, RegularGrid1D grid)
        {
            pdeCoeffs.FillSourceTerm(start, end, grid, ref source);
        }
    }

    public abstract class PdeCoeffSampler1D<TState>
    {
        public abstract void FillCoeff(double start, double end, TState state,
                                       ref double[] d2X, ref double[] dX, ref double[] I);

        public abstract void FillSourceTerm(double start, double end, TState state,
                                            ref double[] sourceTerm);
    }
}