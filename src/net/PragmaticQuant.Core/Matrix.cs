using System;

namespace PragmaticQuant.Core
{
    public static class Matrix
    {
        private static unsafe void FillProd(double* y, double* a, double* x, int nbRows, int nbCols)
        {
            for (int i = 0, k = 0; i < nbRows; ++i)
            {
                double sumProd = 0.0;
                for (int j = 0; j < nbCols; ++j)
                    sumProd += a[k++] * x[j];

                y[i] = sumProd;
            }
        }

        public static unsafe void FillProd(ref double[] y, double[,] a, double[] x)
        {
            if (a.GetLength(0) != y.Length
             && a.GetLength(1) != x.Length)
                throw new Exception("Incompatible size");

            fixed (double* pa = &a[0, 0], px = &x[0], py = &y[0])
            {
                FillProd(py, pa, px, y.Length, x.Length);
            }
        }

        public static double[] Prod(this double[,] a, double[] x)
        {
            var result = new double[a.GetLength(0)];
            FillProd(ref result, a, x);
            return result;
        }
        
    }
}