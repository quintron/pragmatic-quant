using System;
using System.Diagnostics;
using System.Linq;

namespace PragmaticQuant.Core
{
    public sealed class OrnsteinUhlenbeckPath_Fast
    {
        #region private fields
        private readonly double[] slopes;
        private readonly double[] drifts;
        private readonly double[] volDecomposition;
        private readonly double[] initialValue;
        private readonly int dim;
        private readonly int nbStep;
        #endregion
        #region private methods
        private static double[] FlattenVols(double[][,] stepVolDecomposition, int dim)
        {
            var flattenVolDecomposition = new double[stepVolDecomposition.Length * dim * dim];
            for (int step = 0, k=0; step < stepVolDecomposition.Length; step++)
            {
                var volDecomp = stepVolDecomposition[step];
                for (int j = 0; j < dim; j++)
                {
                    for (int i = 0; i < dim; i++)
                    {
                        flattenVolDecomposition[k++] = volDecomp[j, i];
                    }
                }
            }
            return flattenVolDecomposition;
        }
        #endregion
        public OrnsteinUhlenbeckPath_Fast(double[] initialValue, double[][] stepSlopes, double[][] stepDrifts, double[][,] stepVolDecomposition)
        {
            this.initialValue = initialValue;
            slopes = stepSlopes.Aggregate(new double[0], (p, t) => p.Concat(t).ToArray());
            drifts = stepDrifts.Aggregate(new double[0], (p, t) => p.Concat(t).ToArray());
            ProcessDim = initialValue.Length;
            dim = ProcessDim;
            nbStep = stepSlopes.Length;

            if (stepVolDecomposition.Any(vol => vol.GetLength(1) != dim && vol.GetLength(0) != dim))
                throw new Exception("OrnsteinUhlenbeckPath : invalid stepVolDecomposition");

            volDecomposition = FlattenVols(stepVolDecomposition, dim);
        }

        public unsafe void ComputePathIncrements(ref double[][] dWs)
        {
            Debug.Assert(dWs.Any(w => w.Length == dim));

            var current = initialValue.Copy();
            var noises = new double[dim];
            fixed (double* pvolDecomposition = &volDecomposition[0], pCurrent = &current[0], pCorrelNoises = &noises[0])
            {
                for (int step = 0, k = 0, v = 0; step < nbStep; ++step)
                {
                    fixed (double* pWi = &dWs[step][0])
                    {
                        for (int i = 0; i < dim; ++i)
                        {
                            double noise = 0.0;
                            for (int j = 0; j < dim; ++j)
                                noise += pvolDecomposition[v++] * pWi[j];
                            pCorrelNoises[i] = noise;
                        }

                        for (int i = 0; i < dim; ++i)
                        {
                            var dx = pCurrent[i] * (slopes[k] - 1.0) + drifts[k] + pCorrelNoises[i];
                            pCurrent[i] += dx;
                            pWi[i] = dx;
                            ++k;
                        }
                    }
                }
            }
        }
        public int ProcessDim { get; private set; }
    }
}