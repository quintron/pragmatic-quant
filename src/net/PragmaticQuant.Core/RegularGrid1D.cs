using System;

namespace PragmaticQuant.Core
{
    public class RegularGrid1D
    {
        public RegularGrid1D(int size, double boundaryInf, double boundarySup)
        {
            if (boundarySup < boundaryInf)
                throw new Exception("RegularGrid1D: boundaryInf should be lower than boundarySup");
            if (size <= 1)
                throw new Exception("RegularGrid1D: size should be higher or equal than 2 !");

            Size = size;
            BoundaryInf = boundaryInf;
            BoundarySup = boundarySup;
            Step = (boundarySup - boundaryInf) / (size - 1.0);
        }
        public int Size { get; private set; }
        public readonly double BoundaryInf;
        public readonly double BoundarySup;
        public double Step { get; private set; }
        public double[] ToArray()
        {
            var grid = new double[Size];
            for (int i = 0; i < Size - 1; i++)
                grid[i] = BoundaryInf + i * Step;
            grid[Size - 1] = BoundarySup;
            return grid;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((RegularGrid1D) obj);
        }
        public bool Equals(RegularGrid1D other)
        {
            return Size == other.Size 
                && BoundaryInf.Equals(other.BoundaryInf) 
                && BoundarySup.Equals(other.BoundarySup);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Size;
                hashCode = (hashCode * 397) ^ BoundaryInf.GetHashCode();
                hashCode = (hashCode * 397) ^ BoundarySup.GetHashCode();
                return hashCode;
            }
        }
    }
    
    public class Slice1D : ISlice<RegularGrid1D, double[]>
    {
        public Slice1D(RegularGrid1D grid)
        {
            State = grid;
            Value = new double[grid.Size];
        }
        public RegularGrid1D State { get; private set; }
        public double[] Value { get; private set; }
        public void Add(double[] val, double w = 1.0)
        {
            Value.Add(val, w);
        }
        public void Mult(double w)
        {
            Value.Mult(w);
        }
        public void FillValue(double[] val)
        {
            Vector.SetValue(Value, val);
        }
    }

    public static class Slice1DExtensions
    {
        public static double LinearInterpolation(this ISlice<RegularGrid1D, double[]> slice, double x)
        {
            var grid = slice.State;
            double ratio = (x - grid.BoundaryInf) / grid.Step;
            int index = (int)ratio;

            var values = slice.Value;
            if (index < 0)
                return values[0];

            if (index >= values.Length - 1)
                return values[values.Length - 1];

            return values[index] + (ratio - index) * (values[index + 1] - values[index]);
        }

        public static ISlice<RegularGrid1D, double[]> Exp(this RegularGrid1D grid, double power)
        {
            var slice = new Slice1D(grid);

            double[] values = slice.Value;
            if (power < 0.0)
            {
                var stepExp = Math.Exp(power * grid.Step);

                var currentVal = Math.Exp(grid.BoundaryInf);
                values[0] = currentVal;
                for (int i = 1; i < values.Length; i++)
                {
                    currentVal *= stepExp;
                    values[i] = currentVal;
                }
            }
            else
            {
                var stepExp = Math.Exp(-power * grid.Step);

                var currentVal = Math.Exp(grid.BoundarySup);
                values[values.Length - 1] = currentVal;
                for (int i = values.Length - 2; i >= 0; i--)
                {
                    currentVal *= stepExp;
                    values[i] = currentVal;
                }
            }
            return slice;
        }
    }


    public class SliceAffineInterpol
    {
        #region private fields
        private readonly double inf;
        private readonly double step;
        private readonly double[] f;
        private readonly double[] df;
        #endregion
        public SliceAffineInterpol(ISlice<RegularGrid1D, double[]> slice)
        {
            f = slice.Value;
            inf = slice.State.BoundaryInf;
            step = slice.State.Step;
            df = new double[f.Length - 1];
            for (int i = 0; i < f.Length - 2; i++)
            {
                df[i] = f[i + 1] - f[i];
            }
        }
        public double Eval(double x)
        {
            double ratio = (x - inf) / step;
            int index = (int)ratio;

            if (index < 0)
                return f[0];

            if (index >= f.Length - 1)
                return f[f.Length - 1];

            return f[index] + (ratio - index) * df[index];
        }
    }
}