﻿using System;

namespace PragmaticQuant.Core
{

    /// <summary>
    /// Numerical recipes algorithm : http://numerical.recipes/webnotes/nr3web2.pdf
    /// </summary>
    public class Svd
    {
        #region private fields
        private readonly double eps = GetMachineEpsilon();
        private readonly double tsh; 

        private readonly int m;
        private readonly int n;
        #endregion

        #region private methods (decomposition)
        private static double GetMachineEpsilon()
        {
            double machEps = 1.0d;
            do
            {
                machEps /= 2.0d;
            } // ReSharper disable CompareOfFloatsByEqualityOperator
            while ((1.0 + machEps) != 1.0);
            // ReSharper restore CompareOfFloatsByEqualityOperator
            return machEps * 2.0;
        }
        private static double Sign(double a, double b)
        {
            return (b >= 0 ? (a >= 0 ? a : -a) : (a >= 0 ? -a : a));
        }
        private static double Pythag(double a, double b)
        {
            double absa = Math.Abs(a), absb = Math.Abs(b);
            return (absa > absb
                ? absa * Math.Sqrt(1.0 + Math.Pow(absb / absa, 2.0))
                : (absb == 0.0 ? 0.0 : absb * Math.Sqrt(1.0 + Math.Pow(absa / absb, 2.0))));
        }
        private void Decompose()
        {
            bool flag;
            int i, its, j, jj, k, l = 0, nm = 0;
            double anorm, c, f, g, h, s, scale, x, y, z;
            double[] rv1 = new double[n];
            g = scale = anorm = 0.0; //Householder reduction to bidiagonal form.
            for (i = 0; i < n; i++)
            {
                l = i + 2;
                rv1[i] = scale * g;
                g = s = scale = 0.0;
                if (i < m)
                {
                    for (k = i; k < m; k++) scale += Math.Abs(U[k, i]);
                    if (scale != 0.0)
                    {
                        for (k = i; k < m; k++)
                        {
                            U[k, i] /= scale;
                            s += U[k, i] * U[k, i];
                        }
                        f = U[i, i];
                        g = -Sign(Math.Sqrt(s), f);
                        h = f * g - s;
                        U[i, i] = f - g;
                        for (j = l - 1; j < n; j++)
                        {
                            for (s = 0.0, k = i; k < m; k++) s += U[k, i] * U[k, j];
                            f = s / h;
                            for (k = i; k < m; k++) U[k, j] += f * U[k, i];
                        }
                        for (k = i; k < m; k++) U[k, i] *= scale;
                    }
                }


                W[i] = scale * g;
                g = s = scale = 0.0;
                if (i + 1 <= m && i + 1 != n)
                {
                    for (k = l - 1; k < n; k++) scale += Math.Abs(U[i, k]);
                    if (scale != 0.0)
                    {
                        for (k = l - 1; k < n; k++)
                        {
                            U[i, k] /= scale;
                            s += U[i, k] * U[i, k];
                        }
                        f = U[i, l - 1];
                        g = -Sign(Math.Sqrt(s), f);
                        h = f * g - s;
                        U[i, l - 1] = f - g;
                        for (k = l - 1; k < n; k++) rv1[k] = U[i, k] / h;
                        for (j = l - 1; j < m; j++)
                        {
                            for (s = 0.0, k = l - 1; k < n; k++) s += U[j, k] * U[i, k];
                            for (k = l - 1; k < n; k++) U[j, k] += s * rv1[k];
                        }
                        for (k = l - 1; k < n; k++) U[i, k] *= scale;
                    }
                }
                anorm = Math.Max(anorm, (Math.Abs(W[i]) + Math.Abs(rv1[i])));
            }

            for (i = n - 1; i >= 0; i--)
            {
                //Accumulation of right-hand transformations.
                if (i < n - 1)
                {
                    if (g != 0.0)
                    {
                        for (j = l; j < n; j++) //Double division to avoid possible underflow.
                            V[j, i] = (U[i, j] / U[i, l]) / g;
                        for (j = l; j < n; j++)
                        {
                            for (s = 0.0, k = l; k < n; k++) s += U[i, k] * V[k, j];
                            for (k = l; k < n; k++) V[k, j] += s * V[k, i];
                        }
                    }
                    for (j = l; j < n; j++) V[i, j] = V[j, i] = 0.0;
                }
                V[i, i] = 1.0;
                g = rv1[i];
                l = i;
            }


            for (i = Math.Min(m, n) - 1; i >= 0; i--)
            {
                //Accumulation of left-hand transformations.
                l = i + 1;
                g = W[i];
                for (j = l; j < n; j++) U[i, j] = 0.0;
                if (g != 0.0)
                {
                    g = 1.0 / g;
                    for (j = l; j < n; j++)
                    {
                        for (s = 0.0, k = l; k < m; k++) s += U[k, i] * U[k, j];
                        f = (s / U[i, i]) * g;
                        for (k = i; k < m; k++) U[k, j] += f * U[k, i];
                    }
                    for (j = i; j < m; j++) U[j, i] *= g;
                }
                else for (j = i; j < m; j++) U[j, i] = 0.0;
                ++U[i, i];
            }

            for (k = n - 1; k >= 0; k--)
            {
                //Diagonalization of the bidiagonal form: Loop over
                for (its = 0; its < 30; its++)
                {
                    //singular values, and over allowed iterations.
                    flag = true;
                    for (l = k; l >= 0; l--)
                    {
                        //Test for splitting.
                        nm = l - 1;
                        if (l == 0 || Math.Abs(rv1[l]) <= eps * anorm)
                        {
                            flag = false;
                            break;
                        }
                        if (Math.Abs(W[nm]) <= eps * anorm) break;
                    }
                    if (flag)
                    {
                        c = 0.0; //Cancellation of rv1[l], if l > 0.
                        s = 1.0;
                        for (i = l; i < k + 1; i++)
                        {
                            f = s * rv1[i];
                            rv1[i] = c * rv1[i];
                            if (Math.Abs(f) <= eps * anorm) break;
                            g = W[i];
                            h = Pythag(f, g);
                            W[i] = h;
                            h = 1.0 / h;
                            c = g * h;
                            s = -f * h;
                            for (j = 0; j < m; j++)
                            {
                                y = U[j, nm];
                                z = U[j, i];
                                U[j, nm] = y * c + z * s;
                                U[j, i] = z * c - y * s;
                            }
                        }
                    }

                    z = W[k];
                    if (l == k)
                    {
                        //Convergence.
                        if (z < 0.0)
                        {
                            //Singular value is made nonnegative.
                            W[k] = -z;
                            for (j = 0; j < n; j++) V[j, k] = -V[j, k];
                        }
                        break;
                    }

                    if (its == 29) throw new Exception("no convergence in 30 svdcmp iterations");
                    x = W[l]; //Shift from bottom 2-by-2 minor.
                    nm = k - 1;
                    y = W[nm];
                    g = rv1[nm];
                    h = rv1[k];
                    f = ((y - z) * (y + z) + (g - h) * (g + h)) / (2.0 * h * y);
                    g = Pythag(f, 1.0);
                    f = ((x - z) * (x + z) + h * ((y / (f + Sign(g, f))) - h)) / x;
                    c = s = 1.0; //Next QR transformation:
                    for (j = l; j <= nm; j++)
                    {
                        i = j + 1;
                        g = rv1[i];
                        y = W[i];
                        h = s * g;
                        g = c * g;
                        z = Pythag(f, h);
                        rv1[j] = z;
                        c = f / z;
                        s = h / z;
                        f = x * c + g * s;
                        g = g * c - x * s;
                        h = y * s;
                        y *= c;
                        for (jj = 0; jj < n; jj++)
                        {
                            x = V[jj, j];
                            z = V[jj, i];
                            V[jj, j] = x * c + z * s;
                            V[jj, i] = z * c - x * s;
                        }
                        z = Pythag(f, h);
                        W[j] = z; //Rotation can be arbitrary if z D 0.
                        if (z != 0.0) // original nr code is : if (z)
                        {
                            z = 1.0 / z;
                            c = f * z;
                            s = h * z;
                        }
                        f = c * g + s * y;
                        x = c * y - s * g;
                        for (jj = 0; jj < m; jj++)
                        {
                            y = U[jj, j];
                            z = U[jj, i];
                            U[jj, j] = y * c + z * s;
                            U[jj, i] = z * c - y * s;
                        }
                    }
                    rv1[l] = 0.0;
                    rv1[k] = f;
                    W[k] = x;
                }
            }


        }
        private void Reorder()
        {
            // Given the output of decompose, this routine sorts the singular values, and corresponding columns
            // of u and v, by decreasing magnitude. Also, signs of corresponding columns are flipped so as to
            // maximize the number of positive elements.

            int i, j, k, s, inc = 1;
            double sw;
            double[] su = new double[m], sv = new double[n];
            do { inc *= 3; inc++; } while (inc <= n);

            do
            {
                inc /= 3;
                for (i = inc; i < n; i++)
                {
                    sw = W[i];
                    for (k = 0; k < m; k++) su[k] = U[k, i];
                    for (k = 0; k < n; k++) sv[k] = V[k, i];
                    j = i;
                    while (W[j - inc] < sw)
                    {
                        W[j] = W[j - inc];
                        for (k = 0; k < m; k++) U[k, j] = U[k, j - inc];
                        for (k = 0; k < n; k++) V[k, j] = V[k, j - inc];
                        j -= inc;
                        if (j < inc) break;
                    }
                    W[j] = sw;
                    for (k = 0; k < m; k++) U[k, j] = su[k];
                    for (k = 0; k < n; k++) V[k, j] = sv[k];
                }
            } while (inc > 1);

            for (k = 0; k < n; k++)
            {
                //Flip signs.
                s = 0;
                for (i = 0; i < m; i++) if (U[i, k] < 0.0) s++;
                for (j = 0; j < n; j++) if (V[j, k] < 0.0) s++;
                if (s > (m + n) / 2)
                {
                    for (i = 0; i < m; i++) U[i, k] = -U[i, k];
                    for (j = 0; j < n; j++) V[j, k] = -V[j, k];
                }
            }

        }
        #endregion

        /// <summary>
        /// Compute SVD decomposition s.t. :
        ///     A = U * diag(W) * transpose(V),
        ///     transpose(U) * U = Id,
        ///     transpose(V) * V = Id .
        /// </summary>
        /// <param name="a">input matrix A</param>
        public Svd(double[,] a)
        {
            m = a.GetLength(0);
            n = a.GetLength(1);

            U = a.Copy();
            V = new double[n, n];
            W = new double[n];

            Decompose();
            Reorder();
            tsh = 0.5 * Math.Sqrt(m + n + 1.0) * W[0] * eps;
        }
        
        public readonly double[,] U;
        public readonly double[,] V;
        public readonly double[] W;

        public void Solve(ref double[] x, double[] b, double thresh = double.NaN)
        {
            double s;
            if(b.Length!=m || x.Length!=n )
                throw new Exception("SVD.Solve : bad sizes");

            var tmp = new double[n];
            if (Double.IsNaN(thresh))
                thresh = tsh;

            for (int j = 0; j < n; j++)
            {
                s = 0.0;
                if (W[j] > thresh)
                {
                    for (int i = 0; i < m; i++) s += U[i, j] * b[i];
                    s /= W[j];
                }
                tmp[j] = s;
            }
            for (int j = 0; j < n; j++)
            {
                s = 0.0;
                for (int jj = 0; jj < n; jj++) s += V[j, jj] * tmp[jj];
                x[j] = s;
            }
        }
        public double[] Solve(double[] b, double thresh = double.NaN)
        {
            var x = new double[n];
            Solve(ref x, b, thresh);
            return x;
        }
    }

}
