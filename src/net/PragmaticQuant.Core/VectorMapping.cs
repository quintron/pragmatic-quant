﻿using System;

namespace PragmaticQuant.Core
{
    public static partial class Vector
    {
        #region private method
        private static unsafe void FillValue(double* x, int size, double value)
        {
            for (int i = 0; i < size; i++)
                x[i] = value;
        }
        private static unsafe void ApplyExp(double* x, int size)
        {
            for (int i = 0; i < size; i++)
                x[i] = Math.Exp(x[i]);
        }
        private static unsafe void ApplyExp(double* x, int size, double power, double mult, double add)
        {
            for (int i = 0; i < size; i++)
                x[i] = add + mult * Math.Exp(power * x[i]);
        }
        private static unsafe void ApplyLog(double* x, int size)
        {
            for (int i = 0; i < size; i++)
                x[i] = Math.Log(x[i]);
        }
        private static unsafe void Abs(double* x, int size)
        {
            for (int i = 0; i < size; i++)
                x[i] = Math.Abs(x[i]);
        }
        private static unsafe void Max(double* x, int size, double value)
        {
            for (int i = 0; i < size; i++)
                x[i] = Math.Max(x[i], value);
        }
        private static unsafe void Min(double* x, int size, double value)
        {
            for (int i = 0; i < size; i++)
                x[i] = Math.Min(x[i], value);
        }
        private static unsafe void Inv(double* x, int size)
        {
            for (int i = 0; i < size; i++)
                x[i] = 1.0 / x[i];
        }
        private static unsafe void Pow(double* x, int size, double power)
        {
            for (int i = 0; i < size; i++)
                x[i] = Math.Pow(x[i], power);
        }
        private static unsafe void RightShift(double* x, int size, double leftValue)
        {
            double prev = x[0];
            x[0] = leftValue;
            for (int i = 1; i < size; i++)
            {
                double temp = x[i];
                x[i] = prev;
                prev = temp;
            }
        }
        private static unsafe void LeftShift(double* x, int size, double rightValue)
        {
            double next = x[size - 1];
            x[size - 1] = rightValue;
            for (int i = size-2; i >= 0; i--)
            {
                double temp = x[i];
                x[i] = next;
                next = temp;
            }
        }
        #endregion

        public static unsafe void FillValue(this double[] x, double val)
        {
            fixed (double* p_x = &x[0])
            {
                FillValue(p_x, x.Length, val);
            }
        }
        public static unsafe void FillValue(this double[,] x, double val)
        {
            fixed (double* p_x = &x[0, 0])
            {
                FillValue(p_x, x.Length, val);
            }
        }

        public static void Apply(this double[] x, Func<double, double> func)
        {
            for (int i = 0; i < x.Length; i++)
                x[i] = func(x[i]);
        }
        
        /// <summary>
        /// Apply mapping : x -> exp(x)
        /// </summary>
        /// <param name="x"></param>
        public static unsafe void ApplyExp(this double[] x)
        {
            fixed (double* p_x = &x[0])
            {
                ApplyExp(p_x, x.Length);
            }
        }
        /// <summary>
        /// Apply mapping : x -> exp(x)
        /// </summary>
        /// <param name="x"></param>
        public static unsafe void ApplyExp(this double[,] x)
        {
            fixed (double* p_x = &x[0, 0])
            {
                ApplyExp(p_x, x.Length);
            }
        }

        /// <summary>
        /// Apply mapping : x -> mult * exp(power * x) + add
        /// </summary>
        /// <param name="x"></param>
        /// <param name="power"></param>
        /// <param name="mult"></param>
        /// <param name="add"></param>
        public static unsafe void ApplyExp(this double[] x, double power, double mult, double add)
        {
            fixed (double* p_x = &x[0])
            {
                ApplyExp(p_x, x.Length, power, mult, add);
            }
        }
        /// <summary>
        /// Apply mapping : x -> mult * exp(power * x) + add
        /// </summary>
        /// <param name="x"></param>
        /// <param name="power"></param>
        /// <param name="mult"></param>
        /// <param name="add"></param>
        public static unsafe void ApplyExp(this double[,] x, double power, double mult, double add)
        {
            fixed (double* p_x = &x[0,0])
            {
                ApplyExp(p_x, x.Length, power, mult, add);
            }
        }

        /// <summary>
        /// Apply mapping : x -> ln(x)
        /// </summary>
        /// <param name="x"></param>
        public static unsafe void ApplyLog(this double[] x)
        {
            fixed (double* p_x = &x[0])
            {
                ApplyLog(p_x, x.Length);
            }
        }
        /// <summary>
        /// Apply mapping : x -> ln(x)
        /// </summary>
        /// <param name="x"></param>
        public static unsafe void ApplyLog(this double[,] x)
        {
            fixed (double* p_x = &x[0, 0])
            {
                ApplyLog(p_x, x.Length);
            }
        }

        /// <summary>
        /// Apply mapping : x -> abs(x)
        /// </summary>
        /// <param name="x"></param>
        public static unsafe void ApplyAbs(this double[] x)
        {
            fixed (double* p_x = &x[0])
            {
                Abs(p_x, x.Length);
            }
        }
        /// <summary>
        /// Apply mapping : x -> abs(x)
        /// </summary>
        /// <param name="x"></param>
        public static unsafe void ApplyAbs(this double[,] x)
        {
            fixed (double* p_x = &x[0, 0])
            {
                Abs(p_x, x.Length);
            }
        }

        /// <summary>
        /// Apply mapping : x -> max(x, val)
        /// </summary>
        /// <param name="x"></param>
        /// <param name="val"></param>
        public static unsafe void MaxWith(this double[] x, double val)
        {
            fixed (double* p_x = &x[0])
            {
                Max(p_x, x.Length, val);
            }
        }
        /// <summary>
        /// Apply mapping : x -> max(x, val)
        /// </summary>
        /// <param name="x"></param>
        /// <param name="val"></param>
        public static unsafe void MaxWith(this double[,] x, double val)
        {
            fixed (double* p_x = &x[0, 0])
            {
                Max(p_x, x.Length, val);
            }
        }

        /// <summary>
        /// Apply mapping : x -> min(x, val)
        /// </summary>
        /// <param name="x"></param>
        /// <param name="val"></param>
        public static unsafe void MinWith(this double[] x, double val)
        {
            fixed (double* p_x = &x[0])
            {
                Min(p_x, x.Length, val);
            }
        }
        /// <summary>
        /// Apply mapping : x -> min(x, val)
        /// </summary>
        /// <param name="x"></param>
        /// <param name="val"></param>
        public static unsafe void MinWith(this double[,] x, double val)
        {
            fixed (double* p_x = &x[0, 0])
            {
                Min(p_x, x.Length, val);
            }
        }

        /// <summary>
        /// Apply mapping : x -> 1 / x
        /// </summary>
        /// <param name="x"></param>
        public static unsafe void ApplyInv(this double[] x)
        {
            fixed (double* p_x = &x[0])
            {
                Inv(p_x, x.Length);
            }
        }
        /// <summary>
        /// Apply mapping : x -> 1 / x
        /// </summary>
        /// <param name="x"></param>
        public static unsafe void ApplyInv(this double[,] x)
        {
            fixed (double* p_x = &x[0, 0])
            {
                Inv(p_x, x.Length);
            }
        }

        /// <summary>
        /// Apply mapping : x -> x^power
        /// </summary>
        /// <param name="x"></param>
        /// <param name="power"></param>
        public static unsafe void ApplyPow(this double[] x, double power)
        {
            fixed (double* p_x = &x[0])
            {
                Pow(p_x, x.Length, power);
            }
        }
        /// <summary>
        /// Apply mapping : x -> x^power
        /// </summary>
        /// <param name="x"></param>
        /// <param name="power"></param>
        public static unsafe void ApplyPow(this double[,] x, double power)
        {
            fixed (double* p_x = &x[0, 0])
            {
                Pow(p_x, x.Length, power);
            }
        }

        public static unsafe void RightShift(this double[] x, double leftValue)
        {
            fixed (double* p_x = &x[0])
            {
                RightShift(p_x, x.Length, leftValue);
            }
        }

        public static unsafe void LeftShift(this double[] x, double rightValue)
        {
            fixed (double* p_x = &x[0])
            {
                LeftShift(p_x, x.Length, rightValue);
            }
        }
    }
}