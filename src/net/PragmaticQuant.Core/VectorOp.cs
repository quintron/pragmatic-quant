﻿using System;

namespace PragmaticQuant.Core
{
    public static partial class Vector
    {
        #region private methods
        internal static unsafe void Copy(double* x, int size, double* value)
        {
            for (int i = 0; i < size; i++)
                x[i] = value[i];
        }
        internal static unsafe void Add(double* x, double* y, double w, int size)
        {
            for (int i = 0; i < size; i++)
                x[i] += w * y[i];
        }
        internal static unsafe void Sub(double* x, double* y, int size)
        {
            for (int i = 0; i < size; i++)
                x[i] -= y[i];
        }
        internal static unsafe void Div(double* x, double* y, int size)
        {
            for (int i = 0; i < size; i++)
                x[i] /= y[i];
        }
        internal static unsafe void Mult(double* x, double a, int size)
        {
            for (int i = 0; i < size; i++)
                x[i] *= a;
        }
        internal static unsafe void Mult(double* x, double* y, int size)
        {
            for (int i = 0; i < size; i++)
                x[i] *= y[i];
        }
        internal static unsafe void AxPlusBy(double* x, double a, double* y, double b, int size)
        {
            for (int i = 0; i < size; i++)
            {
                x[i] = a * x[i] + b * y[i];
            }
        }
        internal static unsafe void AxPlusB(double* x, double a, double b, int size)
        {
            for (int i = 0; i < size; i++)
            {
                x[i] = a * x[i] + b;
            }
        }
        internal static unsafe double DotProduct(double* x, double* y, int size)
        {
            var dotProduct = 0.0;
            for (int i = 0; i < size; i++)
            {
                dotProduct += x[i] * y[i];
            }
            return dotProduct;
        }
        #endregion

        public static void Add(this double[] x, double v)
        {
            x.AxPlusB(1.0, v);
        }
        public static void Add(this double[,] x, double v)
        {
            x.AxPlusB(1.0, v);
        }

        public static unsafe void Add(this double[] x, double[] y, double w = 1.0)
        {
            int size = Math.Min(x.Length, y.Length);
            fixed (double* p_x = &x[0], p_y = &y[0])
            {
                Add(p_x, p_y, w, size);
            }
        }
        public static unsafe void Add(this double[,] x, double[,] y, double w = 1.0)
        {
            if (x.GetLength(0) != y.GetLength(0) ||
                x.GetLength(1) != y.GetLength(1))
                throw new Exception("Incompatible size");

            fixed (double* p_x = &x[0, 0], p_y = &y[0, 0])
            {
                Add(p_x, p_y, w, x.Length);
            }
        }

        public static unsafe void Sub(this double[] x, double[] y)
        {
            int size = Math.Min(x.Length, y.Length);
            fixed (double* p_x = &x[0], p_y = &y[0])
            {
                Sub(p_x, p_y, size);
            }
        }
        public static unsafe void Sub(this double[,] x, double[,] y)
        {
            if (x.GetLength(0) != y.GetLength(0) ||
                x.GetLength(1) != y.GetLength(1))
                throw new Exception("Incompatible size");

            fixed (double* p_x = &x[0, 0], p_y = &y[0, 0])
            {
                Sub(p_x, p_y, x.Length);
            }
        }

        public static unsafe void Mult(this double[] x, double a)
        {
            fixed (double* p_x = &x[0])
            {
                Mult(p_x, a, x.Length);
            }
        }
        public static unsafe void Mult(this double[,] x, double a)
        {
            fixed (double* p_x = &x[0, 0])
            {
                Mult(p_x, a, x.Length);
            }
        }

        public static unsafe void Mult(this double[] x, double[] y)
        {
            int size = Math.Min(x.Length, y.Length);
            fixed (double* p_x = &x[0], p_y = &y[0])
            {
                Mult(p_x, p_y, size);
            }
        }
        public static unsafe void Mult(this double[,] x, double[,] y)
        {
            if (x.GetLength(0) != y.GetLength(0) ||
                x.GetLength(1) != y.GetLength(1))
                throw new Exception("Incompatible size");

            fixed (double* p_x = &x[0, 0], p_y = &y[0, 0])
            {
                Mult(p_x, p_y, x.Length);
            }
        }

        public static unsafe void Div(this double[] x, double[] y)
        {
            int size = Math.Min(x.Length, y.Length);
            fixed (double* p_x = &x[0], p_y = &y[0])
            {
                Div(p_x, p_y, size);
            }
        }
        public static unsafe void Div(this double[,] x, double[,] y)
        {
            if (x.GetLength(0) != y.GetLength(0) ||
                x.GetLength(1) != y.GetLength(1))
                throw new Exception("Incompatible size");

            fixed (double* p_x = &x[0, 0], p_y = &y[0, 0])
            {
                Div(p_x, p_y, x.Length);
            }
        }

        public static unsafe void AxPlusBy(this double[] x, double a, double[] y, double b)
        {
            int n = x.Length;
            ArrayCheck.EqualSize(n, x, y);
            fixed (double* p_x = &x[0], p_y = &y[0])
            {
                AxPlusBy(p_x, a, p_y, b, n);
            }
        }
        public static unsafe void AxPlusBy(this double[,] x, double a, double[,] y, double b)
        {
            if (x.GetLength(0) != y.GetLength(0) ||
                x.GetLength(1) != y.GetLength(1))
                throw new Exception("Incompatible size");

            fixed (double* p_x = &x[0, 0], p_y = &y[0, 0])
            {
                AxPlusBy(p_x, a, p_y, b, x.Length);
            }
        }

        public static unsafe void AxPlusB(this double[] x, double a, double b)
        {
            fixed (double* p_x = &x[0])
            {
                AxPlusB(p_x, a, b, x.Length);
            }
        }
        public static unsafe void AxPlusB(this double[,] x, double a, double b)
        {
            fixed (double* p_x = &x[0, 0])
            {
                AxPlusB(p_x, a, b, x.Length);
            }
        }

        public static unsafe void SetValue(this double[] x, double[] val)
        {
            int n = x.Length;
            ArrayCheck.EqualSize(n, x, val);
            fixed (double* px = &x[0], pv = &val[0])
            {
                Copy(px, n, pv);
            }
        }
        public static unsafe void SetValue(this double[,] x, double[,] val)
        {
            if (x.GetLength(0) != val.GetLength(0) ||
                x.GetLength(1) != val.GetLength(1))
                throw new Exception("Incompatible size");

            fixed (double* px = &x[0, 0], pv = &val[0, 0])
            {
                Copy(px, x.Length, pv);
            }
        }

        public static double[] Copy(this double[] x)
        {
            var result = new double[x.Length];
            SetValue(result, x);
            return result;
        }
        public static double[,] Copy(this double[,] x)
        {
            var result = new double[x.GetLength(0), x.GetLength(1)];
            SetValue(result, x);
            return result;
        }

        public static double Max(this double[] x)
        {
            var max = x[0];
            for (int i = 1; i < x.Length; i++)
            {
                if (x[i] > max)
                    max = x[i];
            }
            return max;
        }
        public static double Min(this double[] x)
        {
            var min = x[0];
            for (int i = 1; i < x.Length; i++)
            {
                if (x[i] < min)
                    min = x[i];
            }
            return min;
        }
        public static double Sum(this double[] x)
        {
            double sum = 0.0;
            for (int i = 0; i < x.Length; i++)
                sum += x[i];
            return sum;
        }
        public static double Average(this double[] x)
        {
            return Sum(x) / x.Length;
        }
        
        public static unsafe double DotProduct(this double[] x, double[] y)
        {
            int n = x.Length;
            ArrayCheck.EqualSize(n, x, y);
            fixed (double* px = &x[0], py = &y[0])
            {
                return DotProduct(px, py, n);
            }
        }

    }
}
