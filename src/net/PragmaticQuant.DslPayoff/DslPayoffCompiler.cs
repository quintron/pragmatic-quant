using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Text;
using Microsoft.CSharp;
using PragmaticQuant.Basic;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.DslPayoff
{
    public static class DslPayoffCompiler
    {
        #region private methods
        private static Assembly CompileAssembly(string code, Type[] typeDependencies)
        {
            CompilerParameters parameters = new CompilerParameters
            {
                GenerateInMemory = true,
                TreatWarningsAsErrors = false,
                GenerateExecutable = false,
                CompilerOptions = "/optimize",
                IncludeDebugInformation = false
            };
            parameters.ReferencedAssemblies.AddRange(DependencieLocation(typeDependencies));
            CompilerResults results = new CSharpCodeProvider()
                                        .CompileAssemblyFromSource(parameters, code);
            if (results.Errors.HasErrors)
            {
                throw new Exception("Compilation error: " + results.Errors[0].ErrorText);
            }
            return results.CompiledAssembly;
        }
        private static string[] DependencieLocation(Type[] typeDependencies)
        {
            return typeDependencies.Map(type => type.Assembly.Location);
        }
        private static Type[] TypeDependencies()
        {
            return new[]
            {
                typeof (object),
                typeof (DslPayoff)
            };
        }
        private static string PayoffClassName()
        {
            var id = Guid.NewGuid().ToString().Replace("-", "");
            return String.Format("GeneratedDslPayoff{0}", id);
        }
        private static string GenerateCode(DslPayoffExpression[] expressions, string className, string[] methodIds)
        {
            StringBuilder code = new StringBuilder();
            code.AppendLine("using System;");
            code.AppendLine(string.Format("using {0};", typeof(DslPayoff).Namespace));
            code.AppendLine(string.Format("public sealed class {0} : {1}", className, typeof (DslPayoff).Name));
            code.AppendLine("{");

            for (int i = 0; i < expressions.Length; i++)
            {
                code.AppendLine(String.Format("     public double {0}(double[] {1})", methodIds[i], expressions[i].FixingArrayId));
                code.AppendLine("   {");
                code.AppendLine("       " + expressions[i].CSharpExpression);
                code.AppendLine("   }");
            }

            code.AppendLine("}");

            string generatedCode = code.ToString();
            return generatedCode;
        }
        
        /// <summary>
        /// Code from Ben Watson's book : "Writing High-Performance .Net Code"
        /// </summary>
        private static T FastMethodCall<T>(MethodInfo methodInfo,
                                           Type extensionType,
                                           Type returnType,
                                           Type[] parametersTypes) where T : class
        {
            var dynamicMethod = new DynamicMethod("Invoke_" + methodInfo.Name, returnType, parametersTypes, true);
            var ilGenerator = dynamicMethod.GetILGenerator();
            
            // object's this parameter
            ilGenerator.Emit(OpCodes.Ldarg_0);
            // cast it to the correct type
            ilGenerator.Emit(OpCodes.Castclass, extensionType);
            // actual method argument
            ilGenerator.Emit(OpCodes.Ldarg_1);
            ilGenerator.EmitCall(OpCodes.Call, methodInfo, null);
            ilGenerator.Emit(OpCodes.Ret);

            object del = dynamicMethod.CreateDelegate(typeof(T));
            return (T)del;
        }
        #endregion

        public static IFixingFunction[] Compile(params DslPayoffExpression[] dslPayoffExpressions)
        {
            string payoffclassName = PayoffClassName();
            string[] methodIds = EnumerableUtils.For(0, dslPayoffExpressions.Length, i => String.Format("Payoff{0}", i));

            string generatedCode = GenerateCode(dslPayoffExpressions, payoffclassName, methodIds);
            Assembly compiledAssembly = CompileAssembly(generatedCode, TypeDependencies());

            Type payoffClassType = compiledAssembly.GetModules()[0].GetType(payoffclassName);
            object payoffObj = Activator.CreateInstance(payoffClassType);
            MethodInfo[] payoffMethods = methodIds.Map(methodId => payoffClassType.GetMethod(methodId));

            return EnumerableUtils.For(0, dslPayoffExpressions.Length, i =>
            {
                var fixings = dslPayoffExpressions[i].Fixings.ToArray();
                
                var payoff = FastMethodCall<Func<object, double[], double>>
                    (payoffMethods[i], payoffClassType, typeof (double), new[] {typeof (object), typeof (double[])});

                return new DslPayoffFunction(fixings, payoff, payoffObj, dslPayoffExpressions[i].CSharpExpression) as IFixingFunction;
            });
        }
    }

    [DebuggerDisplay("{Expression}")]
    internal class DslPayoffFunction : IFixingFunction
    {
        #region private fields
        private readonly Func<object, double[], double> payoff;
        private readonly object payoffObj;
        #endregion
        internal DslPayoffFunction(IObservation[] fixings, Func<object, double[], double> payoff, object payoffObj, string expression)
        {
            this.payoff = payoff;
            this.payoffObj = payoffObj;
            Observations = fixings;
            Expression = expression;
            Date = Observations.Length > 0
                       ? Observations.Select(f => f.Date).Max()
                       : DateTime.MinValue;
        }

        public string Expression { get; private set; }
        public DateTime Date { get; private set; }
        public IObservation[] Observations { get; private set; }
        public double Value(double[] fixings)
        {
            return payoff(payoffObj, fixings);
        }
    }

    public abstract class DslPayoff
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected static double Max(params double[] args)
        {
            Debug.Assert(args.Length > 0);
            double max = args[0];
            for (int i = 1; i < args.Length; i++)
            {
                double val = args[i];
                if (val > max) max = val;
            }
            return max;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected static double Min(params double[] args)
        {
            Debug.Assert(args.Length > 0);
            double min = args[0];
            for (int i = 1; i < args.Length; i++)
            {
                double val = args[i];
                if (val < min) min = val;
            }
            return min;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected static double Sum(params double[] args)
        {
            Debug.Assert(args.Length > 0);
            double sum = 0.0;
            for (int i = 0; i < args.Length; i++)
                sum += args[i];
            return sum;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected static double Average(params double[] args)
        {
            Debug.Assert(args.Length > 0);
            double avg = 0.0;
            for (int i = 0; i < args.Length; i++)
                avg += args[i];
            return avg / args.Length;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected static double Sqrt(double arg)
        {
            return Math.Sqrt(arg);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected static double Exp(double arg)
        {
            return Math.Exp(arg);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected static double Log(double arg)
        {
            return Math.Log(arg);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected static double Abs(double arg)
        {
            return Math.Abs(arg);
        }
    }

}

