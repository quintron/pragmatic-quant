using System.Collections.Generic;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.DslPayoff
{
    public class DslPayoffExpression
    {
        public DslPayoffExpression(IObservation[] fixings, string cSharpExpression, string fixingArrayId)
        {
            Fixings = fixings;
            CSharpExpression = cSharpExpression;
            FixingArrayId = fixingArrayId;
        }
        
        /// <summary>
        /// C# code that represent coupon payoff.
        /// Payoff value is given throw a return statememnt at the end of expression
        /// </summary>
        public string CSharpExpression { get; private set; }

        /// <summary>
        /// Identifier of fixing array used in Expression
        /// </summary>
        public string FixingArrayId { get; private set; }
        
        /// <summary>
        /// Fixings of the payoff
        /// </summary>
        public IReadOnlyCollection<IObservation> Fixings { get; private set; }
    }
}