using System;
using System.Collections.Generic;
using PragmaticQuant.Basic;
using PragmaticQuant.Product;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.DslPayoff
{
    public class DslPayoffFactory
    {
        #region private fields
        private readonly DslPayoffParser dslParser;
        #endregion
        public DslPayoffFactory(IFixingParser fixingParser)
        {
            dslParser = new DslPayoffParser(fixingParser);
        }
        public DslPayoffFactory() : this(new FixingParser())
        {
        }

        public IFixingFunction[] Build(params DslPayoffDescription[] payoffDescriptions)
        {
            DslPayoffExpression[] payoffExpressions = payoffDescriptions.Map(d => dslParser.Parse(d.Script, d.Parameters));
            return DslPayoffCompiler.Compile(payoffExpressions);
        }
        public Coupon[] BuildCoupons(params DslCouponDescription[] couponDescriptions)
        {
            var payoffs = Build(couponDescriptions.Map(d => d.Payoff));
            return EnumerableUtils.For(0, couponDescriptions.Length, i =>
            {
                var desc = couponDescriptions[i];                
                return new Coupon(desc.Id, desc.Payment, payoffs[i]);
            });
        }
    }
    
    public class DslPayoffDescription
    {
        public DslPayoffDescription(string script, IDictionary<string, object> payoffParameters = null)
        {
            Script = script;
            Parameters = payoffParameters ?? new Dictionary<string, object>();
        }
        public string Script { get; private set; }
        public IDictionary<string, object> Parameters { get; private set; }
    }
    
    public class DslCouponDescription
    {
        public DslCouponDescription(string id,
                                    DateTime payDate,
                                    Currency payCurrency,
                                    string payoffScript,
                                    IDictionary<string, object> payoffParameters = null)
            :this(id, new PaymentInfo(payCurrency, payDate), payoffScript, payoffParameters){}
        private DslCouponDescription(string id,
                                     PaymentInfo payment, 
                                     string payoffScript, 
                                     IDictionary<string, object> payoffParameters = null)
        {
            Id = id;
            Payment = payment;
            Payoff = new DslPayoffDescription(payoffScript, payoffParameters);
        }
        
        public string Id { get; private set; }
        public DslPayoffDescription Payoff { get; private set; }
        public PaymentInfo Payment { get; private set; }
    }
}