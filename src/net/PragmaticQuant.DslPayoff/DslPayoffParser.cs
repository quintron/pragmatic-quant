using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.DslPayoff
{
    public class DslPayoffParser
    {
        #region private fields
        private const string FixingArrayId = "f";
        private static readonly Regex fixingRegex = new Regex(@"[a-zA-Z_1-9]*@[a-zA-Z]*", RegexOptions.Compiled);
        private readonly IFixingParser fixingParser;
        #endregion
        #region private methods
        private object RetrieveParameter(string paramName, IDictionary<string, object> parameters)
        {
            object obj;
            if (!parameters.TryGetValue(paramName.Trim(), out obj))
                throw new Exception(string.Format("Missing payoff parameter : {0} ", paramName));
            return obj;
        }
        private DateTime RetrieveDateParameter(string paramName, IDictionary<string, object> parameters)
        {
            var obj = RetrieveParameter(paramName, parameters);
            return DateAndDurationConverter.ConvertDate(obj);
        }
        private IObservationProcess RetrieveObservationParameter(string paramName, IDictionary<string, object> parameters)
        {
            var obj = RetrieveParameter(paramName, parameters);

            var fixingProcess = obj as IObservationProcess;
            if (fixingProcess != null) return fixingProcess;

            return fixingParser.Parse(obj.ToString());
        }
        private IObservation ParseFixing(string fixingExpression, IDictionary<string, object> parameters)
        {
            var split = fixingExpression.Split('@');
            var processId = split[0];
            var obsDateId = split[1];

            IObservationProcess fixingProcess = RetrieveObservationParameter(processId, parameters);
            DateTime fixingDate = RetrieveDateParameter(obsDateId, parameters);
            return fixingProcess.Observation(fixingDate);
        }
        private string FixingReference(IObservation fixing, IDictionary<IObservation, string> fixingRefs)
        {
            string fixingRef;
            if (!fixingRefs.TryGetValue(fixing, out fixingRef))
            {
                fixingRef = string.Format("{0}[{1}]", FixingArrayId, fixingRefs.Count);
                fixingRefs.Add(fixing, fixingRef);
            }
            return fixingRef;
        }
        private string ParameterDeclaration(string name, object value, IDictionary<IObservation, string> fixingRefs)
        {
            if (value is double)
            {
                var valueAsString = ((double) value).ToString(CultureInfo.InvariantCulture);
                return string.Format("const double {0} = {1};", name, valueAsString);
            }

            var observation = value as IObservation;
            if (observation != null)
            {
                var fixing = observation;
                var fixingRef = FixingReference(fixing, fixingRefs);
                return string.Format("double {0} = {1};", name, fixingRef);
            }

            return null;
        }
        #endregion
        public DslPayoffParser(IFixingParser fixingParser)
        {
            this.fixingParser = fixingParser;
        }
        
        public DslPayoffExpression Parse(string dslPayoffScript, IDictionary<string, object> parameters)
        {
            var fixingRefs = new Dictionary<IObservation, string>();
            var payoffExpression = new StringBuilder();
            
            foreach (var param in parameters)
            {
                var decl = ParameterDeclaration(param.Key, param.Value, fixingRefs);
                payoffExpression.AppendLine(decl);
            }

            string payoff = fixingRegex.Replace(dslPayoffScript, match =>
            {
                IObservation fixing = ParseFixing(match.Value, parameters);
                return FixingReference(fixing, fixingRefs);
            });
            payoff = string.Format("return {0};", payoff);
            payoffExpression.AppendLine(payoff);

            return new DslPayoffExpression(fixingRefs.Keys.ToArray(), 
                                           payoffExpression.ToString(), FixingArrayId);
        }
    }

}