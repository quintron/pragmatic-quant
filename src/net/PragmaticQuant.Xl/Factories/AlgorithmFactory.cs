using System;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Maths;
using PragmaticQuant.Model;

namespace PragmaticQuant.Xl.Factories
{
    public class AlgorithmFactory
        : Singleton<AlgorithmFactory>, IFactoryFromBag<INumericalMethodConfig>
    {
        #region private fields
        private static ISimulationSchedule ScheduleBuilder(object[,] bag)
        {
            Duration maxStep = bag.Has("McStep") ? bag.ProcessScalarDateOrDuration("McStep").Duration : SimulationSchedules.DefaultMaxStep;
            if (bag.Has("McNbStepByDate"))
            {
                var nbStepByDate = bag.ProcessScalarInteger("McNbStepByDate");
                var minStep = bag.Has("McMinStep") ? bag.ProcessScalarDateOrDuration("McMinStep").Duration : SimulationSchedules.DefaultMinStep;
                return SimulationSchedules.ScheduleBuilder(maxStep, nbStepByDate, minStep);
            }

            if (bag.Has("McStep"))
                return SimulationSchedules.ScheduleBuilder(maxStep);

            return SimulationSchedules.DefaultScheduleBuilder;
        }
        private static MonteCarloConfig MonteCarloConfig(object[,] bag)
        {
            int nbPaths = bag.ProcessScalarInteger("NbPaths");
            ISimulationSchedule scheduleBuilder = ScheduleBuilder(bag);

            bool multithread = !bag.Has("Multithread") || bag.ProcessScalarBoolean("Multithread");

            //TODO investigate which generator is the best
            var randomGenerator = RandomGenerators.GaussianSobol();
            
            BmcStrategyConfig bmcStrategy = null;
            if (bag.Has("LsStrategyNbPaths"))
            {
                int strategyNbPaths = bag.ProcessScalarInteger("LsStrategyNbPaths");
                bmcStrategy = new BmcStrategyConfig(strategyNbPaths);
            }

            return new MonteCarloConfig(nbPaths, multithread, scheduleBuilder, randomGenerator, bmcStrategy);
        }
        
        #endregion
        public INumericalMethodConfig Build(object[,] bag)
        {
            var algorithm = bag.ProcessScalarString("Algorithm");
            switch (algorithm.ToLowerInvariant())
            {
                case "montecarlo":
                case "mc":
                    return MonteCarloConfig(bag);

                case "pde":
                    throw new NotImplementedException("Pde");
            }

            throw new ArgumentException(string.Format("Unknown algorithm : {0} ", algorithm));
        }
    }
}