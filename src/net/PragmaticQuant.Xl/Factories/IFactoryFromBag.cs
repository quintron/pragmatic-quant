﻿namespace PragmaticQuant.Xl.Factories
{
    interface IFactoryFromBag<out T>
    {
        T Build(object[,] bag);
    }
}