﻿using System;
using System.Collections.Generic;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets.RawDatas;

namespace PragmaticQuant.Xl.Factories
{
    using TimeMatrixDatas = LabelledMatrix<DateOrDuration, string, double>;
    
    public class MarketDatasFactory : Singleton<MarketDatasFactory>, IFactoryFromBag<MarketDatas>
    {
        #region private methods
        private static DateTime RefDate(object[,] bag)
        {
            return bag.ProcessScalarDateOrDuration("RefDate").Date;
        }
        private static DiscountCurveDatas[] DiscountCurveFromRawDatas(TimeMatrixDatas curveRawDatas)
        {
            DateOrDuration[] pillars = curveRawDatas.RowLabels;
            
            var curves = new List<DiscountCurveDatas>();
            foreach (var curveLabel in curveRawDatas.ColLabels)
            {
                var discountCurve = new DiscountCurveDatas();
                
                FinancingId financingId;
                if (!FinancingId.TryParse(curveLabel, out financingId))
                    throw new ArgumentException(string.Format("RateMarketFactory, invalid Discount Curve Id : {0}", curveLabel));

                discountCurve.Currency = financingId.Currency.Name;
                discountCurve.Financing = financingId.Id;
                
                double[] zcRates = curveRawDatas.GetColFromLabel(curveLabel);
                discountCurve.ZcRates = new RawMapDatas<DateOrDuration, double>(pillars, zcRates);

                curves.Add(discountCurve);
            }
            return curves.ToArray();
        }
        private static RawMapDatas<DateTime, double> ProcessDivCurve(object[,] bag, string assetName)
        {
            var divId = string.Format("Dividend.{0}", assetName);
            TimeMatrixDatas dividendsRawDatas = bag.ProcessTimeMatrixDatas(divId);
            double[] fwdDivs = dividendsRawDatas.GetColFromLabel("Forward");
            return new RawMapDatas<DateTime, double>(dividendsRawDatas.RowLabels.Map(dd=>dd.ToDate(DateTime.MaxValue)), fwdDivs);
        }
        private static LabelledMatrix<DateOrDuration, double, double> AssetVolMatrix(object[,] bag, string assetName)
        {
            var volId = string.Format("Vol.{0}", assetName);
            return bag.ProcessEqtyVolMatrix(volId);
        }
        private static AssetMarketDatas[] ProcessAssetMkt(object[,] bag)
        {
            if (!bag.Has("Asset"))
                return new AssetMarketDatas[0];

            LabelledMatrix<string, string, object> assetRawDatas = bag.ProcessLabelledMatrix("Asset", o => o.ToString(), o => o.ToString(), o => o);
            TimeMatrixDatas repoRawDatas = bag.ProcessTimeMatrixDatas("Repo");
            
            var assetMkts = new List<AssetMarketDatas>();
            for (int i = 0; i < assetRawDatas.RowLabels.Length; i++)
            {
                var assetName = assetRawDatas.RowLabels[i];
                var rawCurrency = assetRawDatas.GetColFromLabel("Currency")[i].ToString();
                object rawSpot = assetRawDatas.GetColFromLabel("Spot")[i];
                
                double spot;
                if (!NumberConverter.TryConvertDouble(rawSpot, out spot))
                    throw new ArgumentException(string.Format("AssetMarketFactory, invalid {0} spot : {1}", assetName, rawSpot));

                var repoCurve = new RawMapDatas<DateOrDuration, double>(repoRawDatas.RowLabels,
                                                                        repoRawDatas.GetColFromLabel(assetName));
                var divCurve = ProcessDivCurve(bag, assetName);
                var volMatrix = AssetVolMatrix(bag, assetName);
                
                var mkt = new AssetMarketDatas()
                {
                    Name = assetName,
                    Currency = rawCurrency,
                    Spot = spot,
                    RepoCurve = repoCurve,
                    DividendForward = divCurve,
                    VolatilityMatrix = volMatrix
                };
                assetMkts.Add(mkt);
            }
            return assetMkts.ToArray();
        }
        #endregion
        public MarketDatas Build(object[,] bag)
        {
            TimeMatrixDatas curveRawDatas = bag.ProcessTimeMatrixDatas("Discount");
            DiscountCurveDatas[] discountCurves = DiscountCurveFromRawDatas(curveRawDatas);
            AssetMarketDatas[] assetMarkets = ProcessAssetMkt(bag);
            return new MarketDatas
            {
                RefDate = RefDate(bag),
                DiscountCurves = discountCurves,
                Assets = assetMarkets
            };
        }
    }
}
