using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Maths;
using PragmaticQuant.Model;
using PragmaticQuant.Model.Equity.Bergomi;
using PragmaticQuant.Model.Equity.BlackScholes;
using PragmaticQuant.Model.Equity.LocalVolatility;
using PragmaticQuant.Model.Hybrid;
using PragmaticQuant.Model.Rate;
using PragmaticQuant.Model.Rate.HullWhite;

namespace PragmaticQuant.Xl.Factories
{
    using TimeMatrixDatas = LabelledMatrix<DateOrDuration, string, double>;

    public class ModelCalibrationFactory
        : Singleton<ModelCalibrationFactory>, IFactoryFromBag<ICalibrationDescription>
    {
        public ICalibrationDescription Build(object[,] bag)
        {
            string modelName = bag.ProcessScalarString("ModelName");
            switch (modelName.ToLowerInvariant())
            {
                case "blackscholes" :
                    return BlackScholesModelFactory.Instance.Build(bag);

                case "localvol":
                    return LocalVolModelFactoryFromBag.Instance.Build(bag);

                case "bergomi2f":
                    return Bergomi2FModelFactoryFromBag.Instance.Build(bag);

                case "hw1f" :
                    return HullWhiteNfModelFactoryFromBag.Instance.Build(bag);

                case "hwnf" :
                    return HullWhiteNfModelFactoryFromBag.Instance.Build(bag);
            }

            throw new ArgumentException(string.Format("Unknow model : {0}", modelName));
        }
    }

    public static class EquityModelFactory
    {
        public static RawMapDatas<DateOrDuration, double> ProcessDivYieldProportion(object[,] bag, string assetName)
        {
            if (!bag.Has("DividendYieldProportion"))
                return new RawMapDatas<DateOrDuration, double>(new[] { new DateOrDuration(0 * Duration.Year) }, new[] { 1.0 });
            TimeMatrixDatas dividendsRawDatas = bag.ProcessTimeMatrixDatas("DividendYieldProportion");
            double[] yieldProportion = dividendsRawDatas.GetColFromLabel(assetName);
            return new RawMapDatas<DateOrDuration, double>(dividendsRawDatas.RowLabels, yieldProportion);
        }
    }
    
    public class BlackScholesModelFactory
        : Singleton<BlackScholesModelFactory>, IFactoryFromBag<ICalibrationDescription>
    {
        public ICalibrationDescription Build(object[,] bag)
        {
            var assetName = bag.ProcessScalarString("Asset");
            var divYieldProportion = EquityModelFactory.ProcessDivYieldProportion(bag, assetName);
            
            var calibDatas = bag.ProcessTimeMatrixDatas("CalibDate");
            if (calibDatas.HasCol("Strike") && calibDatas.HasCol("VolTerm"))
                throw new Exception("BS Model, vol parameter missing : VolTerm or Strike !");

            double[] calibVols = null;
            if (calibDatas.HasCol("VolTerm"))
                calibVols = calibDatas.GetColFromLabel("VolTerm");

            double[] calibStrikes = null;
            if (calibDatas.HasCol("Strike"))
                calibStrikes = calibDatas.GetColFromLabel("Strike");
            
            return new BlackScholesModelCalibDesc(assetName, divYieldProportion, calibDatas.RowLabels, calibStrikes, calibVols);
        }
    }

    public class LocalVolModelFactoryFromBag
        : Singleton<LocalVolModelFactoryFromBag>, IFactoryFromBag<ICalibrationDescription>
    {
        #region private methods
        private LocalVolSampleConfig LocalVolConfig(object[,] bag)
        {
            var gridQuantile = bag.Has("GridQuantile") ? bag.ProcessScalarDouble("GridQuantile") : LocalVolSampleConfig.DefaultGridQuantile;
            var gridSize = bag.Has("GridSize") ? bag.ProcessScalarInteger("GridSize") : LocalVolSampleConfig.DefaultGridSize;
            var tryFixArbitrage = bag.Has("TryFixArbitrage") ? bag.ProcessScalarBoolean("TryFixArbitrage") : LocalVolSampleConfig.DefaultTryFixArbitrage;
            return new LocalVolSampleConfig(gridQuantile, gridSize, tryFixArbitrage);
        }
        #endregion
        public ICalibrationDescription Build(object[,] bag)
        {
            TimeMatrixDatas divYieldProportion = bag.ProcessTimeMatrixDatas("DividendYieldProportion");
            var lvSampleConfig = LocalVolConfig(bag);
            
            if (bag.Has("Asset"))
            {
                var assetName = bag.ProcessScalarString("Asset");
                var divModel = new RawMapDatas<DateOrDuration, double>(divYieldProportion.RowLabels,
                                                                       divYieldProportion.GetColFromLabel(assetName));
                return new LocalVolModelCalibDesc(assetName, divModel, lvSampleConfig);
            }
            
            var assetNames = bag.ProcessVectorString("Assets");
            var equityCorrelations = bag.ProcessMatrixWithLabel("EquityCorrelation");
            var divModels = assetNames.Map(name => new RawMapDatas<DateOrDuration, double>(divYieldProportion.RowLabels,
                                                                                           divYieldProportion.GetColFromLabel(name)));
            return new LocalVolModelCalibDesc(assetNames, divModels, equityCorrelations, lvSampleConfig);
        }
    }

    public class Bergomi2FModelFactoryFromBag  
        : Singleton<Bergomi2FModelFactoryFromBag>, IFactoryFromBag<ICalibrationDescription>
    {
        public static StochasticVolParams BuildStochaticVolParams(object[,] bag)
        {
            double k1 = bag.ProcessScalarDouble("K1");
            double k2 = bag.ProcessScalarDouble("K2");
            double theta = bag.ProcessScalarDouble("Theta");
            double nu = bag.ProcessScalarDouble("Nu");
            double rhoXY = bag.ProcessScalarDouble("RhoXY");
            double rhoSX = bag.ProcessScalarDouble("RhoSX");
            double rhoSY = bag.ProcessScalarDouble("RhoSY");
            return new StochasticVolParams(k1, k2, theta, nu, rhoXY, rhoSX, rhoSY);
        }

        public ICalibrationDescription Build(object[,] bag)
        {
            string assetName = bag.ProcessScalarString("Asset");
            var divYieldProportion = EquityModelFactory.ProcessDivYieldProportion(bag, assetName);
            var volParams = BuildStochaticVolParams(bag);

            DateOrDuration[] calibMaturities = new DateOrDuration[0]; 
            double[] vols = new double[0];
            if (bag.Has("Date"))
            {
                var calibDatas = bag.ProcessTimeMatrixDatas("Date");
                calibMaturities = calibDatas.RowLabels;
                
                if (calibDatas.HasCol("Sigma"))
                    vols = calibDatas.GetColFromLabel("Sigma");
            }
            
            return new Bergomi2FModelCalibDesc(assetName, divYieldProportion, volParams, calibMaturities, vols);
        }
    }

    public class HullWhiteNfModelFactoryFromBag 
        : Singleton<HullWhiteNfModelFactoryFromBag>, IFactoryFromBag<ICalibrationDescription>
    {
        #region private methods
        private static double[,] FactorsCorrelMatrix(object[,] bag, string[] factorsNames)
        {
            if (!bag.Has("HwnFactorCorrelation"))
                return new [,] {{1.0}};

            LabelledMatrix<string, string, double> factorsCorrelations = bag.ProcessLabelledMatrix("HwnFactorCorrelation",
                o => o.ToString(), o => o.ToString(), BagServices.DoubleValueConverter("HwnFactorCorrelation"));
            var correlMatrix = ArrayUtils.CartesianProd(factorsNames, factorsNames, (f1, f2) => factorsCorrelations.GetVal(f1, f2));
            if (!correlMatrix.IsSymmetric())
                throw new Exception("HwnFactorCorrelation : symmetric matrix expected !");
            //TODO check matrix positivity
            return correlMatrix;
        }
        private static double[] FactorVolRatios(object[,] bag, string[] factorsNames)
        {
            if (!bag.Has("HwnVolRatios"))
                return new[] {1.0};

            var factorVolRatiosMap = bag.ProcessMap("HwnVolRatios", o => o.ToString(), BagServices.DoubleValueConverter("HwnVolRatios"))
                                        .ToDictionary();
            if (factorVolRatiosMap.Count != factorsNames.Length)
                throw new Exception("Hwn Model : incompatble size beetween HwnVolRatios and HwnFactors !");
            return factorsNames.Map(f => factorVolRatiosMap[f]);
        }
        #endregion

        public ICalibrationDescription Build(object[,] bag)
        {
            var currency = Currency.Parse(bag.ProcessScalarString("Currency"));

            LabelledMatrix<double, string, double> hwnFactors;
            if (bag.Has("HwnFactors"))
            {
                hwnFactors = bag.ProcessLabelledMatrix("HwnFactors",
                   BagServices.DoubleValueConverter("HwnFactors"), o => o.ToString(), BagServices.DoubleValueConverter("HwnFactors"));
            }
            else
            {
                //HW1F factor case
                double meanReversion = bag.ProcessScalarDouble("MeanReversion");
                hwnFactors = new LabelledMatrix<double, string, double>( new []{meanReversion}, new []{"RateFactor"}, new[,]{{1.0}});
            }

            double[] meanReversions = hwnFactors.RowLabels;
            double[,] factors = hwnFactors.Values;
            string[] factorsNames = hwnFactors.ColLabels;
            double[,] factorsCorrelMatrix = FactorsCorrelMatrix(bag, factorsNames);
            double[] factorVolRatios = FactorVolRatios(bag, factorsNames);

            var calibDatas = bag.ProcessLabelledMatrix("Date", BagServices.DateOrDurationValueConverter("Date"), o => o.ToString(), o => o);
            if (calibDatas.HasCol("Sigma"))
            {
                var sigmaValues = calibDatas.GetColFromLabel("Sigma").Map(BagServices.DoubleValueConverter("Sigma"));
                var sigma = new RawMapDatas<DateOrDuration, double>(calibDatas.RowLabels, sigmaValues);
                RawMapDatas<DateOrDuration, double>[] factorsVols = factorVolRatios.Map(ratio => sigma.MapValues(v => v * ratio));
                var factorDesc = new ModelFactorDescription(factorsNames, meanReversions, factors, factorsCorrelMatrix, factorsVols);
                var hwnDescription = new HwnModelDescription(currency, factorDesc);
                return new ExplicitCalibration(hwnDescription);
            }

            var calibrationVols = calibDatas.HasCol("Vol") ? calibDatas.GetColFromLabel("Vol").Map(BagServices.DoubleValueConverter("Vol")) : null;
            var calibUnderlying = calibDatas.GetColFromLabel("Underlying").Map(BagServices.DateOrDurationValueConverter("Underlying"));
            var calibStrike = ArrayUtils.Constant(calibUnderlying.Length, 0.0); //TODO currently only atm calibration allowed
            SwaptionSmilePoint[] calibPattern = EnumerableUtils.For(0, calibDatas.RowLabels.Length,
                i => new SwaptionSmilePoint(currency, calibDatas.RowLabels[i], calibUnderlying[i], calibStrike[i], true));

            return new HwnModelCalibDesc(currency, meanReversions, factorsNames, factors, factorVolRatios, factorsCorrelMatrix, calibPattern, calibrationVols);
        }
    }

    public class BlackscholesHybridHwnFactoryFromBag 
        : Singleton<BlackscholesHybridHwnFactoryFromBag>, IFactoryFromBag<ICalibrationDescription>
    {
        public ICalibrationDescription Build(object[,] bag)
        {
            ICalibrationDescription bs = BlackScholesModelFactory.Instance.Build(bag);
            ICalibrationDescription hwn = HullWhiteNfModelFactoryFromBag.Instance.Build(bag);
            var equityRateCorrel = bag.ProcessMatrixWithLabel("EquityRateCorrelation");
            return new EquityHybridCalib(hwn, bs, equityRateCorrel);
        }
    }
}