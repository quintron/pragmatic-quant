using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;

namespace PragmaticQuant.Xl.Factories
{
    public static class ObjectConverters
    {
        public static DateTime ConvertDate(object dateObj, DateTime refDate)
        {
            var dateOrDuration = DateAndDurationConverter.ConvertDateOrDuration(dateObj);
            return dateOrDuration.ToDate(refDate);
        }
        public static DateTime[] ConvertDateArray(object[] dateObjs, DateTime refDate)
        {
            return dateObjs.Map(o => ConvertDate(o, refDate));
        }
        public static DateTime[,] ConvertDateArray(object[,] dateObjs, DateTime refDate)
        {
            return dateObjs.Map(o => ConvertDate(o, refDate));
        }
    }
}