using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets.Scenarios;
using PragmaticQuant.Pricing;

namespace PragmaticQuant.Xl.Factories
{
    public class PricingTaskFactory : Singleton<PricingTaskFactory>, IFactoryFromBag<IPricingTask<IPricingResult>>
    {
        #region private methods
        private static MarketScenario[] BuildMarketScenarios(object[,] requestBag)
        {
            var scenarios = requestBag.ProcessVectorString("Scenarios");
            return scenarios.Map(MarketScenarios.Parse);
        }
        private static IRiskAnalysis[] RiskAnalyses(object[,] requestBag)
        {
            var risks = requestBag.Has("Risks") ? requestBag.ProcessVectorString("Risks") : new string[0];
            return risks.Map(RiskAnalysis.Parse);
        }
        #endregion
        public IPricingTask<IPricingResult> Build(object[,] requestBag)
        {
            var request = requestBag.ProcessScalarString("Request").Trim().ToLowerInvariant();
            switch (request)
            {
                case "price":
                    return new PriceComputationTask();

                case "riskanalysis":
                    var riskAnalyses = RiskAnalyses(requestBag);
                    return new RiskAnalysisTask(riskAnalyses);

                case "riskprofile" :
                    var scenarios = BuildMarketScenarios(requestBag);
                    var riskAnalysis = RiskAnalyses(requestBag);
                    return new RiskProfilTask(riskAnalysis, scenarios);

                default :
                    throw new Exception("Unknow request");
            }
        }
    }
}