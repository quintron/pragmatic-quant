﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Irony.Parsing;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Product;
using PragmaticQuant.Product.Fixings;
using PragmaticQuant.DslPayoff;

namespace PragmaticQuant.Xl.Factories
{
    using Parameters = LabelledMatrix<DateTime, string, object>;

    public class ProductFactory : IFactoryFromBag<IProduct>
    {
        #region private fields
        private readonly DslPayoffFactory dslPayoffFactory;
        #endregion
        #region private methods
        private static string AdjustedDecimalScript(string script)
        {
            string decimalSeparator = CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
            if (decimalSeparator != ".")
            {
                var currentCultureDecimalRegex = RegexUtils.GetNumberRegex(decimalSeparator);
                return currentCultureDecimalRegex.Replace(script, s => s.Value.Replace(decimalSeparator, "."));
            }
            return script;
        }
        private IDictionary<string, object>[] CouponParameters(string labelOfRows, Parameters parameters)
        {
            var couponParameters = new IDictionary<string, object>[parameters.RowLabels.Length];
            for (int row = 0; row < couponParameters.Length; row++)
            {
                object[] paramVals = parameters.Values.Row(row);
                IDictionary<string, object> couponParam = parameters.ColLabels.ZipToDictionary(paramVals);
                couponParam.Add(labelOfRows.Trim(), parameters.RowLabels[row]);
                couponParameters[row] = couponParam;
            }
            return couponParameters;
        }
        private Coupon[] BuildDslCoupons(string legId, Parameters parameters, string[] dslCouponPayoffs)
        {
            DateTime[] payDates = parameters.RowLabels;
            Currency[] payCurrencies = parameters.GetColFromLabel(legId + "PayCurrency", o => Currency.Parse(o.ToString()));
            var cpnParams = CouponParameters(legId + "PayDate", parameters);
            DslCouponDescription[] dslCouponDescs = EnumerableUtils.For(0, payDates.Length,
                i => new DslCouponDescription(legId, payDates[i], payCurrencies[i], dslCouponPayoffs[i], cpnParams[i]));
            return dslPayoffFactory.BuildCoupons(dslCouponDescs);
        }

        private Coupon[] BuildRedemptionCoupons(Parameters parameters)
        {
            DateTime[] payDates = parameters.HasCol("RedemptionDate")
                                ? parameters.GetColFromLabel("RedemptionDate", DateAndDurationConverter.ConvertDate)
                                : parameters.RowLabels;
            Currency[] currencies = parameters.GetColFromLabel("RedemptionCurrency", o => Currency.Parse(o.ToString()));
            string[] redemptionScripts = parameters.GetColFromLabel("Redemption", o => AdjustedDecimalScript(o.ToString()));
            var cpnParams = CouponParameters("AutocallDate", parameters);
            DslCouponDescription[] redempCouponDescs = EnumerableUtils.For(0, payDates.Length,
                i => new DslCouponDescription("RedemtionCoupon", payDates[i], currencies[i], redemptionScripts[i], cpnParams[i]));
            return dslPayoffFactory.BuildCoupons(redempCouponDescs);
        }
        private IProduct BuilAutoCall(ICouponDecomposable underlying, object[,] bag)
        {
            Parameters parameters = bag.ProcessLabelledMatrix("AutocallDate",
                                                              DateAndDurationConverter.ConvertDate,
                                                              o => o.ToString(), o => o);
            Coupon[] redemptionCoupons = BuildRedemptionCoupons(parameters);
            
            string[] triggerScripts = parameters.GetColFromLabel("AutocallTrigger", o => AdjustedDecimalScript(o.ToString()));
            IDictionary<string, object>[] cpnParams = CouponParameters("AutocallDate", parameters);
            var triggerDescs = triggerScripts.ZipWith(cpnParams, (script, param) => new DslPayoffDescription(script, param));
            IFixingFunction[] triggers = dslPayoffFactory.Build(triggerDescs);
            
            Duration callNotice = 0 * Duration.Day;
            var triggerItems = EnumerableUtils.For(0, redemptionCoupons.Length, 
                i => new TriggerItem(parameters.RowLabels[i], callNotice, redemptionCoupons[i], triggers[i]));
            return new AutoCall(underlying, triggerItems);
        }
        private IProduct BuildCallable(ICouponDecomposable underlying, object[,] bag)
        {
            Parameters parameters = bag.ProcessLabelledMatrix("CallDate",
                                                              DateAndDurationConverter.ConvertDate,
                                                              o => o.ToString(), o => o);
            Coupon[] redemptionCoupons = BuildRedemptionCoupons(parameters);
            Duration callNotice = 7 * Duration.Day;
            
            var callItems = EnumerableUtils.For(0, parameters.RowLabels.Length, 
                i => new CallItem(parameters.RowLabels[i], callNotice, redemptionCoupons[i]));
            
            return new Callable(underlying, callItems);
        }
        private IProduct BuildCancellable(string cancellableType, ParseTreeNode decomposableNode, object[,] bag)
        {
            var decomposable = (ICouponDecomposable) BuildProduct(decomposableNode, bag);
            switch (cancellableType.ToLowerInvariant())
            {
                case "autocall" :
                    return BuilAutoCall(decomposable, bag);

                case "callable" :
                    return BuildCallable(decomposable, bag);

                case "target" :
                    throw new NotImplementedException("Target not yet implemented");
                
                default :
                    throw new Exception(string.Format("Error while parsing product, unknown cancellable type : {0}", cancellableType));
            }
        }
        private ICouponDecomposable BuildLeg(string legName, object[,] bag)
        {
            var legId = legName.ToLower().Replace("leg", "");

            var parameters = bag.ProcessLabelledMatrix(legId + "PayDate",
                DateAndDurationConverter.ConvertDate,
                o => o.ToString(), o => o);

            string couponScript = AdjustedDecimalScript(bag.ProcessScalarString(legId + "CouponScript"));
            var scripts = EnumerableUtils.ConstantArray(couponScript, parameters.RowLabels.Length);
            Coupon[] coupons = BuildDslCoupons(legId, parameters, scripts);
            return CouponLinearCombination.Create(coupons); 
        }
        private IProduct BuildWeighted(double weight, ParseTreeNode factorProductNode, object[,] bag)
        {
            var factor = BuildProduct(factorProductNode, bag) as ICouponDecomposable;
            return CouponLinearCombination.Create(new[] {weight}, new[] {factor});
        }
        private IProduct BuildCombination(ParseTreeNode leftNode, ParseTreeNode rightNode, string op, object[,] bag)
        {
            ICouponDecomposable left = BuildProduct(leftNode, bag) as ICouponDecomposable;
            ICouponDecomposable right = BuildProduct(rightNode, bag) as ICouponDecomposable;
            switch (op)
            {
                case "+" :
                    return CouponLinearCombination.Create(new[] {1.0, 1.0}, new[] {left, right});

                case "-" :
                    return CouponLinearCombination.Create(new[] {1.0, -1.0}, new[] {left, right});

                default :
                    throw new Exception("BUG should never get there !");
            }
        }
        private IProduct BuildProduct(ParseTreeNode productNode, object[,] bag)
        {
            switch (productNode.Term.Name)
            {
                case "Cancellable":

                    ParseTreeNode cancellableTypeNode = productNode.ChildNodes[0];
                    string cancellableType = cancellableTypeNode.ChildNodes[0].Token.ValueString;
                    ParseTreeNode decomposableNode = productNode.ChildNodes[2];
                    return BuildCancellable(cancellableType, decomposableNode, bag);

                case "Leg" :
                    return BuildLeg(productNode.Token.ValueString, bag);

                case "Coupon" :
                    throw new NotImplementedException("Single Coupon product not yet implemented !");
                
                case "ParDecomposable" :
                    return BuildProduct(productNode.ChildNodes[1], bag);

                case "WeightedDecomposable" :
                    double weight = double.Parse(productNode.ChildNodes[0].Token.ValueString);
                    return BuildWeighted(weight, productNode.ChildNodes[2], bag);
                
                case "DecomposableCombination" :
                    var left = productNode.ChildNodes[0];
                    var right = productNode.ChildNodes[2];
                    var op = productNode.ChildNodes[1].Token.ValueString;
                    return BuildCombination(left, right, op, bag);

                default :
                    throw new Exception("BUG, Should never get there !");
            }
        }
        #endregion
        public ProductFactory(IFixingParser fixingParser)
        {
            dslPayoffFactory = new DslPayoffFactory(fixingParser);
        }

        public IProduct Build(object[,] bag)
        {
            string product = bag.ProcessScalarString("ProductName");
            ParseTree productTree = new Parser(new ProductGrammar()).Parse(product);

            if (productTree.HasErrors())
                throw new Exception(string.Format("Unable to parse product : {0} ", product));

            return BuildProduct(productTree.Root, bag);
        }
    }

    [Language("Product DSL", "1.0", "Financial product description")]
    internal class ProductGrammar : Grammar
    {
        #region 1. Terminals
        private static readonly Terminal number = new NumberLiteral("number", NumberOptions.AllowSign);
        private static readonly Terminal leg = new RegexBasedTerminal("Leg", @"[a-z]+leg");
        private static readonly Terminal coupon = new RegexBasedTerminal("Coupon", @"[a-z]+coupon");
        #endregion
        #region 2. Non-terminals
        private readonly NonTerminal Product = new NonTerminal("Product");
        private readonly NonTerminal Cancellable = new NonTerminal("Cancellable");
        private readonly NonTerminal CancellableType = new NonTerminal("CancellableType");
        private readonly NonTerminal DecomposableProduct = new NonTerminal("DecomposableProduct");
        private readonly NonTerminal Term = new NonTerminal("Term");
        private readonly NonTerminal WeightedDecomposable = new NonTerminal("WeightedDecomposable");
        private readonly NonTerminal DecomposableCombination = new NonTerminal("DecomposableCombination");
        private readonly NonTerminal CombinationOp = new NonTerminal("CombinationOp", "operator");
        private readonly NonTerminal ParDecomposable = new NonTerminal("ParDecomposable");
        #endregion
        public ProductGrammar()
            : base(false)
        {
            #region 3. BNF rules
            Product.Rule = DecomposableProduct | Cancellable;

            Cancellable.Rule = CancellableType + PreferShiftHere() + "[" + DecomposableProduct + "]";
            CancellableType.Rule = ToTerm("AutoCall") | "Callable" | "Target";

            DecomposableProduct.Rule = Term | WeightedDecomposable | DecomposableCombination;
            Term.Rule = ParDecomposable | coupon | leg;
            WeightedDecomposable.Rule = number + "*" + Term;
            ParDecomposable.Rule = "(" + DecomposableProduct + ")";
            DecomposableCombination.Rule = DecomposableProduct + CombinationOp + DecomposableProduct;
            CombinationOp.Rule = ToTerm("+") | "-";
            #endregion
            Root = Product;

            #region 4. Operators precedence
            RegisterOperators(10, "?");
            RegisterOperators(15, "&", "&&", "|", "||");
            RegisterOperators(20, "==", "<", "<=", ">", ">=", "!=");
            RegisterOperators(30, "+", "-");
            RegisterOperators(40, "*", "/");
            RegisterOperators(50, Associativity.Right, "**");
            // For precedence to work, we need to take care of one more thing: BinOp. 
            //For BinOp which is or-combination of binary operators, we need to either 
            // 1) mark it transient or 2) set flag TermFlags.InheritPrecedence
            // We use first option, making it Transient.  

            // 5. Punctuation and transient terms
            MarkPunctuation("(", ")");
            RegisterBracePair("(", ")");
            RegisterBracePair("[", "]");
            MarkTransient(Term, DecomposableProduct, CombinationOp, ParDecomposable, Product);
            #endregion
        }
    }
    
}