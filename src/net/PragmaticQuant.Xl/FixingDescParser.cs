using System;
using System.Globalization;
using System.Linq;
using Irony.Parsing;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Model;

namespace PragmaticQuant.Xl
{
    public static class FixingDescParser
    {
        #region private fields
        private static readonly Parser grammarParser = new Parser(new LanguageData(new FixingDescGrammar()));
        #endregion
        #region private methods
        private static FixingDesc FixingExpression(ParseTreeNode node)
        {
            var childNodes = node.ChildNodes;
            if (childNodes.Count == 3)
            {
                var left = node.ChildNodes[0];
                var right = node.ChildNodes[2];
                var op = node.ChildNodes[1].Token.ValueString;
                return BuildCombination(left, right, op);
            }
            return BuildFixing(childNodes.Single());
        }
        private static FixingDesc BuildCombination(ParseTreeNode leftNode, ParseTreeNode rightNode, string op)
        {
            FixingDesc left = BuildFixing(leftNode);
            FixingDesc right = BuildFixing(rightNode);
            double[] weights;
            switch (op)
            {
                case "+":
                    weights = new[] { 1.0, 1.0 };
                    break;

                case "-":
                    weights = new[] { 1.0, -1.0 };
                    break;

                default:
                    throw new Exception("BUG should never get there !");
            }
            return LinearCombinationFixingDesc.Build(new[] { left, right }, weights);
        }
        private static FixingDesc WeightedFixingTerm(ParseTreeNode node)
        {
            if (node.Term.Name != "WeightedFixingTerm")
                throw new Exception("Unexpected node !");
            
            var weightDesc = node.ChildNodes[0].Token.Text.Replace("*", "").Replace(",", ".").Trim();
            double weight = Double.Parse(weightDesc, CultureInfo.InvariantCulture);

            var fixing = BuildFixing(node.ChildNodes[1]);
            return LinearCombinationFixingDesc.Build(new[] { fixing }, new[] { weight });
        }
        private static FixingDesc ForwardFixing(ParseTreeNode node)
        {
            var childNodes = node.ChildNodes;
            if (childNodes.Count == 2)
            {
                Duration durationNode = Duration(childNodes[0]);
                ForwardDesc basicFixing = BasicFixing(childNodes[1]);
                return basicFixing.NewMaturity(durationNode);
            }
            return BasicFixing(childNodes.Single());
        }
        private static ForwardDesc BasicFixing(ParseTreeNode node)
        {
            switch (node.Term.Name)
            {
                case "Duration":
                    return new ForwardDesc<Duration>(0 * PragmaticQuant.Basic.Dates.Duration.Day, Duration(node));

                case "assetId":
                    return new ForwardDesc<string>(0 * PragmaticQuant.Basic.Dates.Duration.Day, node.Token.Text);

                default:
                    throw new Exception("BUG should never get there !");
            }

        }
        private static Duration Duration(ParseTreeNode node)
        {
            if(node.Term.Name != "Duration")
                throw new Exception("Unexpected node !");

            var duration = node.ChildNodes[0].Token.Text + node.ChildNodes[1].Token.Text;
            return PragmaticQuant.Basic.Dates.Duration.Parse(duration);
        }

        private static FixingDesc BuildFixing(ParseTreeNode node)
        {
            switch (node.Term.Name)
            {
                case "FixingExpression":
                    return FixingExpression(node);

                case "FixingTerm":

                    var childNode = (node.ChildNodes.Count == 3) ? node.ChildNodes[1] : node.ChildNodes.Single();
                    return BuildFixing(childNode);

                case "WeightedFixingTerm":
                    return WeightedFixingTerm(node);

                case "ForwardFixing":
                    return ForwardFixing(node);

                case "BasicFixing":
                    return BasicFixing(node);

                default:
                    throw new Exception("BUG, Should never get there !");
            }
        }
        #endregion

        public static FixingDesc ParseFixing(string desc)
        {

            var tree = grammarParser.Parse(desc);

            if (tree.HasErrors())
                throw new Exception("Unable to parse fixing desc : " + desc);

            ParseTreeNode rootNode = tree.Root;
            return BuildFixing(rootNode);
        }

        #region grammar
        [Language("Fixing Desc DSL", "1.0", "Simple fixing description")]
        private class FixingDescGrammar : Grammar
        {
            public FixingDescGrammar()
                : base(false)
            {
                #region 1. Terminals
                var integer = new NumberLiteral("integer", NumberOptions.IntOnly | NumberOptions.NoDotAfterInt | NumberOptions.AllowLetterAfter);
                var durationUnit = new RegexBasedTerminal("duration unit", @"[d,m,y]");
                var weight = new RegexBasedTerminal("weight", @"[\d]{1,8}([.,][\d]{1,16})?\s*[*]");
                var assetId = new IdentifierTerminal("assetId");
                #endregion

                #region 2. Non-terminals
                var FixingExpr = new NonTerminal("FixingExpression");
                var CombinationOp = new NonTerminal("CombinationOp", "operator");
                var FixingTerm = new NonTerminal("FixingTerm");
                var WeightedFixingTerm = new NonTerminal("WeightedFixingTerm");
                var ForwardFixing = new NonTerminal("ForwardFixing");
                var BasicFixing = new NonTerminal("BasicFixing");
                var Duration = new NonTerminal("Duration");
                #endregion

                #region 3. BNF rules
                FixingExpr.Rule = FixingTerm
                                  | WeightedFixingTerm
                                  | (FixingExpr + CombinationOp + FixingExpr);
                CombinationOp.Rule = ToTerm("+") | "-";

                WeightedFixingTerm.Rule = (weight + FixingTerm);
                FixingTerm.Rule = ForwardFixing | ("(" + FixingExpr + ")");

                ForwardFixing.Rule = (Duration + BasicFixing) | BasicFixing;
                BasicFixing.Rule = Duration | assetId;
                Duration.Rule = integer + durationUnit;
                #endregion

                Root = FixingExpr;

                #region 4. Operators precedence
                RegisterOperators(10, "?");
                RegisterOperators(15, "&", "&&", "|", "||");
                RegisterOperators(20, "==", "<", "<=", ">", ">=", "!=");
                RegisterOperators(30, "+", "-");
                RegisterOperators(40, "*", "/");
                RegisterOperators(50, Associativity.Right, "**");
                // For precedence to work, we need to take care of one more thing: BinOp. 
                //For BinOp which is or-combination of binary operators, we need to either 
                // 1) mark it transient or 2) set flag TermFlags.InheritPrecedence
                // We use first option, making it Transient.  

                // 5. Punctuation and transient terms
                MarkPunctuation("(", ")");
                RegisterBracePair("(", ")");
                RegisterBracePair("[", "]");
                MarkTransient(CombinationOp, BasicFixing);
                #endregion
            }
        }
        #endregion
    }
}