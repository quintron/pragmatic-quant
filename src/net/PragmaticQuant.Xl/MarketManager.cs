using System;
using System.Collections.Generic;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets.RawDatas;
using PragmaticQuant.Markets.Scenarios;
using PragmaticQuant.Xl.Factories;
using PragmaticQuant.Pricing;

namespace PragmaticQuant.Xl
{
    public sealed class MarketManager : Singleton<MarketManager>
    {
        #region private fields
        private readonly IDictionary<string, MarketConfig> marketByIds;
        #endregion
        #region private methods
        private string FormattedId(string id)
        {
            return id.Trim().ToLowerInvariant();
        }
        public MarketManager()
        {
            marketByIds = new Dictionary<string, MarketConfig>();
        }
        #endregion

        public void SetMarket(string mktId, MarketDatas marketDatas, string mktScenario = "")
        {
            var market = new MarketConfig
            {
                MarketDatas = marketDatas,
                MarketScenario = MarketScenarios.Parse(mktScenario)
            };

            var formatedId = FormattedId(mktId);
            if (marketByIds.ContainsKey(formatedId))
            {
                marketByIds[formatedId] = market;
            }
            else
            {
                lock (this)
                {
                    marketByIds.Add(formatedId, market);
                }
            }
        }
        public void SetMarket(string mktId, object[,] marketBag, string mktScenario)
        {
            SetMarket(mktId, MarketDatasFactory.Instance.Build(marketBag), mktScenario);
        }
        public bool HasMarket(string mktId)
        {
            return marketByIds.ContainsKey(FormattedId(mktId));
        }
        public MarketConfig GetMarketDatas(object mktObj)
        {
            var mktBag = mktObj as object[,];
            if (mktBag != null)
                return new MarketConfig
                {
                    MarketDatas = MarketDatasFactory.Instance.Build(mktBag),
                    MarketScenario = MarketScenario.Identity()
                };

            if (!(mktObj is string))
                throw new ApplicationException(string.Format("Unable to build market from : {0}", mktObj));
            string mktId = (String) mktObj;

            MarketConfig mkt;
            if (!marketByIds.TryGetValue(FormattedId(mktId), out mkt))
            {
                throw new ApplicationException(string.Format("No market for id : {0}", mktId));
            }
            return mkt;
        }
    }
}