namespace PragmaticQuant.Xl.Publishers
{
    public interface IPublisher<in T>
    {
        object[,] Publish(T obj);
    }
}