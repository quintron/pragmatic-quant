using System;
using System.Collections.Generic;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Pricing;

namespace PragmaticQuant.Xl.Publishers
{
    public class PriceResultPublisher : Singleton<PriceResultPublisher>, IPublisher<IPricingResult>
    {
        #region private fields
        private static void PricingDetails(Tuple<string, PaymentInfo, Price>[] details,
            out DateTime[] payDates, out Currency[] payCurrencies, out double[,] values)
        {
            payDates = details.Select(pi => pi.Item2.Date)
                .Distinct()
                .OrderBy(d => d)
                .ToArray();
            var payDateIndexes = payDates.ZipToDictionary(Enumerable.Range(0, payDates.Length).ToArray());

            payCurrencies = details.Select(pi => pi.Item2.Currency)
                .Distinct()
                .ToArray();
            var payCurrencyIndexes = payCurrencies.ZipToDictionary(Enumerable.Range(0, payCurrencies.Length).ToArray());

            values = new double[payDates.Length, payCurrencies.Length];
            foreach (var kv in details)
            {
                var pay = kv.Item2;
                int dateIndex = payDateIndexes[pay.Date];
                int currencyIndex = payCurrencyIndexes[pay.Currency];
                values[dateIndex, currencyIndex] += kv.Item3.Value;
            }
        }
        #endregion
        public object[,] PublishPriceResult(IPricingResult pricingResult)
        {
            var price = pricingResult.Price;

            if (price == null)
                return new object[0, 0];

            var result = PublisherServices.PublishScalar(string.Format("Price ({0})",price.Currency), price.Value);

            PriceDetails priceDetails;
            if (!pricingResult.TryGetDetail(out priceDetails))
                return result;

            DateTime[] payDates;
            Currency[] payCurrencies;
            double[,] detailValues;
            PricingDetails(priceDetails.Details, out payDates, out payCurrencies, out detailValues);
            
            var details = new object[payDates.Length + 1, payCurrencies.Length + 1];
            details[0, 0] = "PricingDetails";
            ArrayUtils.SetSubArray(ref details, 1, 1, detailValues.Map(v => (object)v));
            ArrayUtils.SetSubArray(ref details, 1, 0, payDates.Cast<object>().ToArray().AsColumn());
            ArrayUtils.SetSubArray(ref details, 0, 1, payCurrencies.Map(c => (object)c.ToString()).AsRow());

            result = result.AppendUnder(details, 1);
            return result;
        }
        public object[,] PublishRiskResult(IPricingResult pricingResult)
        {
            RiskAnalysisDetails riskAnalysisDetails;
            if (!pricingResult.TryGetDetail(out riskAnalysisDetails))
                return new object[0, 0];
            
            return riskAnalysisDetails.RiskResults.Aggregate(new object[0,0], 
                (current, r) => current.AppendUnder(PublisherServices.PublishScalar(r.Item1, r.Item2)));
        }
        public object[,] PublishRiskProfileResult(IPricingResult pricingResult)
        {
            RiskProfilDetails riskProfileDetails;
            if(!pricingResult.TryGetDetail(out riskProfileDetails))
                return new object[0,0];

            var riskAnalysis = riskProfileDetails.RiskResults;
            
            List<string> allRisksNames = EnumerableUtils.Append(riskAnalysis.Map(ra => ra.RiskResults.Map(r => r.Item1))).Distinct().ToList();
            object[][] riskProfils = EnumerableUtils.For(0, riskAnalysis.Length, i =>
            {
                var riskVals = new object[allRisksNames.Count]; 
                foreach (var ra in riskAnalysis[i].RiskResults)
                {
                    var index = allRisksNames.IndexOf(ra.Item1);
                    riskVals[index] = ra.Item2;
                }
                return riskVals;
            });

            var header = new object[] {"RiskProfile", "Price"}.Concat(allRisksNames).ToArray();
            var riskValues = EnumerableUtils.For(0, riskAnalysis.Length,
                i => new object[] { riskProfileDetails.Scenarios[i], riskProfileDetails.Prices[i].Value }.Concat(riskProfils[i]).ToArray());

            var result = header.AsRow();
            result = result.AppendUnder(ArrayUtils.AppendRows(riskValues));
            return result;
        }

        public object[,] Publish(IPricingResult pricingResult)
        {
            var result = PublishPriceResult(pricingResult);
            
            var riskDetails = PublishRiskResult(pricingResult);
            result = result.AppendUnder(riskDetails, (riskDetails.GetLength(0) > 0) ? (uint)1 : 0);
            
            var riskProfilDetails = PublishRiskProfileResult(pricingResult);
            result = result.AppendUnder(riskProfilDetails, (riskProfilDetails.GetLength(0) > 0) ? (uint)1 : 0);

            return result;
        }
    }
}