using ExcelDna.Integration;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;

namespace PragmaticQuant.Xl
{
    public class XlDateFunctions
    {
        [ExcelFunction(Description = "Count time between two days.", Category = "PragmaticQuant_Date")]
        public static object DayCount(object start, object end,
            [ExcelArgument("Convention used for counting time : Act/365, Act/360...")] string convention)
        {
            var startDate = DateAndDurationConverter.ConvertDate(start);
            var endDate = DateAndDurationConverter.ConvertDate(end);
            var dayCount = DayCountFrac.Parse(convention);
            return dayCount.Count(startDate, endDate);
        }

        [ExcelFunction(Description = "Helper for add duration ('1d','5y'...) to a given date.",
                       Category = "PragmaticQuant_Date")]
        public static object AddDuration(object refDate, object period)
        {
            var date = DateAndDurationConverter.ConvertDate(refDate);
            var duration = DateAndDurationConverter.ConvertDuration(period);
            return date + duration;
        }

        [ExcelFunction(Description = "Generate swap schedule",
                       Category = "PragmaticQuant_Date")]
        public static object SwapSchedule(object start, object end, object periodicity,
            [ExcelArgument("Fixing shift from start in days, default value is 2.")] object fixingShift,
            [ExcelArgument(@"Business convention for date adjustement, default value is 'EndOfMonth'.")] object businessConvention)
        {
            return ComputationRunnerUtils.Run("SwapSchedule", () =>
            {
                var scheduleBuilder = new SwapScheduleBuilder(
                    SimpleCalendar.Instance,
                    NumberConverter.ConvertInt((fixingShift is ExcelMissing) ? 2 : fixingShift),
                    BusinessConventions.For((businessConvention is ExcelMissing) ? "EndOfMonth" : businessConvention.ToString()));

                var startDate = DateAndDurationConverter.ConvertDate(start);
                var cpnSchedules = scheduleBuilder.GetSchedule(startDate,
                    DateAndDurationConverter.ConvertDateOrDuration(end).ToDate(startDate),
                    DateAndDurationConverter.ConvertDuration(periodicity));

                var result = new object[cpnSchedules.Length + 1, 4];
                result[0, 0] = "Fixing";
                result[0, 1] = "Start";
                result[0, 2] = "End";
                result[0, 3] = "Pay";
                for (int i = 0; i < cpnSchedules.Length; i++)
                {
                    var cpnSchedule = cpnSchedules[i];
                    result[i + 1, 0] = cpnSchedule.Fixing;
                    result[i + 1, 1] = cpnSchedule.Start;
                    result[i + 1, 2] = cpnSchedule.End;
                    result[i + 1, 3] = cpnSchedule.Pay;
                }
                return result;
            });

        }
    }
}