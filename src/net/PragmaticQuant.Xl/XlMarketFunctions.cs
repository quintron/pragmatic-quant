﻿using System;
using System.Diagnostics;
using System.IO;
using ExcelDna.Integration;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets;
using PragmaticQuant.Markets.Equity;
using PragmaticQuant.Markets.RawDatas;
using PragmaticQuant.Xl.Factories;
using PragmaticQuant.Pricing;

namespace PragmaticQuant.Xl
{
    public class XlMarketFunctions
    {
        [ExcelFunction(Description = "Set market from xl range", Category = "PragmaticQuant_Market")]
        public static bool SetMarket(string mktId, object range, string marketScenario)
        {
            try
            {
                MarketManager.Instance.SetMarket(mktId, (object[,]) range, string.IsNullOrEmpty(marketScenario) ? "" : marketScenario);
                return true;
            }
            catch (Exception e)
            {
                Trace.TraceError(e.Message); 
                return false;
            }
        }

        [ExcelFunction(Description = "Save market to json file", Category = "PragmaticQuant_Market")]
        public static bool SaveMarket(string mktId, string path)
        {
            try
            {
                var mktConfig = MarketManager.Instance.GetMarketDatas(mktId);
                string mktAsJson = MarketJsonParser.ToJson(mktConfig.MarketDatas);
                File.WriteAllText(path, mktAsJson);
                return true;
            }
            catch (Exception e)
            {
                Trace.TraceError(e.Message);
                return false;
            }
        }

        [ExcelFunction(Description = "Load market from json file", Category = "PragmaticQuant_Market")]
        public static bool LoadMarket(string mktId, string path)
        {
            try
            {
                var jsonMkt = File.ReadAllText(path);
                var mkt = MarketJsonParser.ParseMarket(jsonMkt);
                MarketManager.Instance.SetMarket(mktId, mkt);
                return true;
            }
            catch (Exception e)
            {
                Trace.TraceError(e.Message);
                return false;
            }
        }

        [ExcelFunction(Description = "Get market reference date", Category = "PragmaticQuant_Market")]
        public static object MarketRefDate(object mktObj)
        {
            return ComputationRunnerUtils.Run("MarketRefDate", () =>
            {
                Market market = MarketManager.Instance.GetMarketDatas(mktObj).BuildMarket();
                return market.RefDate;
            });
        }

        [ExcelFunction(Description = "Discount zero coupon function", Category = "PragmaticQuant_Market")]
        public static object Discount(object mktObj, object[,] dates, string curveId)
        {
            return ComputationRunnerUtils.Run("Discount", () =>
            {
                Market market = MarketManager.Instance.GetMarketDatas(mktObj).BuildMarket();
                var finCurveId = FinancingId.Parse(curveId);
                DiscountCurve curve = market.DiscountCurve(finCurveId);

                var result = dates.Map(o =>
                {
                    var dateOrDuration = DateAndDurationConverter.ConvertDateOrDuration(o);
                    var date = dateOrDuration.ToDate(curve.RefDate);
                    return curve.Zc(date);
                });

                return result;
            });
        }

        [ExcelFunction(Description = "Forward bor function", Category = "PragmaticQuant_Market")]
        public static object ForwardBor(object mktObj, object[,] dates, string currency, string tenor)
        {
            return ComputationRunnerUtils.Run("ForwardBor", () =>
            {
                Market market = MarketManager.Instance.GetMarketDatas(mktObj).BuildMarket();
                var rateMarket = market.RateMarket(Currency.Parse(currency));
                var curve = rateMarket.BorForwardCurve(Duration.Parse(tenor));
                
                var result = dates.Map(o =>
                {
                    var dateOrDuration = DateAndDurationConverter.ConvertDateOrDuration(o);
                    var date = dateOrDuration.ToDate(curve.RefDate);
                    return curve.Fwd(date);
                });

                return result;
            });
        }

        [ExcelFunction(Description = "Forward swap rate function", Category = "PragmaticQuant_Market")]
        public static object ForwardSwapRate(object mktObj, object[,] dates, string currency, string tenor)
        {
            return ComputationRunnerUtils.Run("ForwardSwapRate", () =>
            {
                var cur = Currency.Parse(currency);
                var swapTenor = DateAndDurationConverter.ConvertDuration(tenor);

                Market market = MarketManager.Instance.GetMarketDatas(mktObj).BuildMarket();
                var rateMarket = market.RateMarket(cur);
                var swapRateCurve = rateMarket.SwapRateCurve();
                
                var result = dates.Map(o =>
                {
                    var dateOrDuration = DateAndDurationConverter.ConvertDateOrDuration(o);
                    var fixingDate = dateOrDuration.ToDate(market.RefDate);
                    return swapRateCurve.Fwd(fixingDate, swapTenor);
                });

                return result;
            });
        }

        [ExcelFunction(Description = "Equity Asset forward function", Category = "PragmaticQuant_Market")]
        public static object AssetForward(object mktObj, object[,] dates, string assetName)
        {
            return ComputationRunnerUtils.Run("AssetForward", () =>
            {
                Market market = MarketManager.Instance.GetMarketDatas(mktObj).BuildMarket();
                var assetMarket = market.AssetMarketFromName(assetName);

                AssetForwardCurve assetForward = assetMarket.ForwardCurve();

                var result = dates.Map(o =>
                {
                    var dateOrDuration = DateAndDurationConverter.ConvertDateOrDuration(o);
                    var date = dateOrDuration.ToDate(assetForward.RefDate);
                    return assetForward.Fwd(date);
                });

                return result;
            });
        }

        [ExcelFunction(Description = "Equity Asset vol function", Category = "PragmaticQuant_Market")]
        public static object VolSurface(object mktObj, object[] dates, double[] strikes, string assetName)
        {
            return ComputationRunnerUtils.Run("VolSurface", () =>
            {
                Market market = MarketManager.Instance.GetMarketDatas(mktObj).BuildMarket();
                AssetMarket assetMarket = market.AssetMarketFromName(assetName);
                VolatilitySurface volSurface = assetMarket.VolSurface();
                var maturities = ObjectConverters.ConvertDateArray(dates, assetMarket.RefDate);

                return ArrayUtils.CartesianProd(maturities, strikes, volSurface.Volatility);
            });
        }

        [ExcelFunction(Description = "Equity Variance LogSwap Volatility",
                       Category = "PragmaticQuant_Model")]
        public static object EquityLogSwapVol(object mktObj, object[,] dates, string assetName)
        {
            return ComputationRunnerUtils.Run("EquityLogSwapVol", () =>
            {
                Market market = MarketManager.Instance.GetMarketDatas(mktObj).BuildMarket();
                AssetMarket assetMarket = market.AssetMarketFromName(assetName);
                var logSwapVolCurve = assetMarket.LogSwapVolCurve();
                var result = dates.Map(o =>
                {
                    var dateOrDuration = DateAndDurationConverter.ConvertDateOrDuration(o);
                    var date = dateOrDuration.ToDate(assetMarket.RefDate);
                    return logSwapVolCurve.Volatility(date);
                });
                return result;
                
            });
        }

    }
}