﻿using System;
using ExcelDna.Integration;
using PragmaticQuant.Core.Sobol;
using PragmaticQuant.Basic;
using PragmaticQuant.Maths.Interpolation;

namespace PragmaticQuant.Xl
{
    public class XlMathFunctions
    {
        [ExcelFunction(Description = "Generate uniform sample with Sobol algorithm", 
                       Category = "PragmaticQuant_Math")]
        public static object Sobol(string directionType, int dimension, int nbPaths,
            int nbSkippedPaths, object startDimIndexDisplay)
        {
            return ComputationRunnerUtils.Run("Sobol", () =>
            {
                SobolDirection direction;
                if (!Enum.TryParse(directionType, true, out direction))
                    throw new Exception(string.Format("Unknow sobol direction type {0}", directionType));

                var sobolGen = new SobolRsg(dimension, direction);
                sobolGen.SkipTo(nbSkippedPaths);

                int startIndex;
                if (!NumberConverter.TryConvertInt(startDimIndexDisplay, out startIndex))
                {
                    startIndex = 0;
                }
                if (startIndex >= dimension)
                    throw new Exception(string.Format("invalid startDimIndexDisplay : {0}", startIndex));

                var result = new double[nbPaths, dimension - startIndex];
                for (int i = 0; i < nbPaths; i++)
                {
                    var sample = sobolGen.NextSequence();
                    for (int j = 0; j < dimension - startIndex; j++)
                        result[i, j] = sample[startIndex + j];
                }
                return result;
            });
        }

        [ExcelFunction(Description = "Cubic spline interpolation", 
                       Category = "PragmaticQuant_Math")]
        public static object CubicSpline(double[] abscissae, double[] values, double[] points)
        {
            return ComputationRunnerUtils.Run("CubicSpline", () =>
            {
                var interpoler = SplineInterpoler.BuildCubicSpline(abscissae, values);

                var result = new double[points.Length, 1];
                for (int i = 0; i < points.Length; i++)
                {
                    result[i, 0] = interpoler.Eval(points[i]);
                }
                return result;
            });
        }
    }
}