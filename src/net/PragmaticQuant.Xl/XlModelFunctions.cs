﻿using System;
using System.Linq;
using ExcelDna.Integration;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets;
using PragmaticQuant.Markets.Equity;
using PragmaticQuant.Maths;
using PragmaticQuant.Model;
using PragmaticQuant.Model.Equity.Bergomi;
using PragmaticQuant.Xl.Factories;
using PragmaticQuant.Model.Equity;
using PragmaticQuant.Model.Equity.Dividends;
using PragmaticQuant.Model.Equity.LocalVolatility;
using PragmaticQuant.Model.Rate.HullWhite;
using PragmaticQuant.Pricing;

namespace PragmaticQuant.Xl
{
    public class XlModelFunctions
    {
        [ExcelFunction(Description = "Equity vanilla option", Category = "PragmaticQuant_Model")]
        public static object EquityVanillaOption(object mktObj, string assetName, object maturity, double strike, double vol, string optionType)
        {
            return ComputationRunnerUtils.Run("EquityVanillaOption", () =>
            {
                Market market = MarketManager.Instance.GetMarketDatas(mktObj).BuildMarket();
                AssetMarket assetMkt = market.AssetMarketFromName(assetName);
                var vanillaPricer = EquityVanillaPricer.Build(assetMkt.Asset, market);

                OptionType type = (OptionType)Enum.Parse(typeof(OptionType), optionType.Trim(), true);
                var matAsDate = ObjectConverters.ConvertDate(maturity, assetMkt.RefDate);
                return vanillaPricer.Price(matAsDate, strike, type, vol);
            });
        }
        
        [ExcelFunction(Description = "Equity vanilla option implied volatility", Category = "PragmaticQuant_Model")]
        public static object EquityImpliedVol(object mktObj, string assetName, object maturity, double strike, double price, string optionType)
        {
            return ComputationRunnerUtils.Run("EquityImpliedVol", () =>
            {
                Market market = MarketManager.Instance.GetMarketDatas(mktObj).BuildMarket();
                AssetMarket assetMkt = market.AssetMarketFromName(assetName);
                var vanillaPricer = EquityVanillaPricer.Build(assetMkt.Asset, market);

                OptionType type = (OptionType)Enum.Parse(typeof (OptionType), optionType.Trim(), true); 
                var matAsDate = ObjectConverters.ConvertDate(maturity, assetMkt.RefDate);
                return vanillaPricer.ImpliedVol(matAsDate, strike, type, price);
            });
        }

        [ExcelFunction(Description = "Equity Variance Swap Volatility",
                       Category = "PragmaticQuant_Model")]
        public static object EquityVarianceSwapVol(object mktObj, string assetName, object[,] start, object[,] end, object[,] divModels)
        {
            return ComputationRunnerUtils.Run("EquityVarianceSwap", () =>
            {
                Market market = MarketManager.Instance.GetMarketDatas(mktObj).BuildMarket();
                AssetMarket assetMarket = market.AssetMarketFromName(assetName);
                var refDate = assetMarket.RefDate;

                var divYieldProportion = EquityModelFactory.ProcessDivYieldProportion(divModels, assetName);
                AffineDividend[] affineDivModel = EquityModelUtils.AffineDivModel(divYieldProportion, assetMarket);
                var varianceSwapCurve = VarianceSwapCurve.Build(assetMarket, affineDivModel);

                var result = start.ZipWith(end, (s, e) =>
                {
                    var startDate = DateAndDurationConverter.ConvertDateOrDuration(s).ToDate(refDate);
                    var endDate = DateAndDurationConverter.ConvertDateOrDuration(e).ToDate(refDate);
                    return varianceSwapCurve.Vol(startDate, endDate);
                });
                return result;
            });
        }

        [ExcelFunction(Description = "Equity local volatility function", Category = "PragmaticQuant_Model")]
        public static object LocalVolViewer(object mktObj, string assetName, object[] dates, double[] strikes, string request, object[,] divModels, bool fixLocalVol)
        {
            return ComputationRunnerUtils.Run("LocalVolViewer", () =>
            {
                Market market = MarketManager.Instance.GetMarketDatas(mktObj).BuildMarket();
                AssetMarket assetMarket = market.AssetMarketFromName(assetName);

                var spot = assetMarket.Spot;
                var volMatrix = assetMarket.VolMatrix;
                var time = assetMarket.VolMatrix.Time;
                
                var divYieldProportion = EquityModelFactory.ProcessDivYieldProportion(divModels, assetName);
                AffineDividend[] affineDivModel = EquityModelUtils.AffineDivModel(divYieldProportion, assetMarket);

                var divTools = new AffineDivCurveUtils(affineDivModel, assetMarket.AssetDiscountCurve, volMatrix.Time);
                var driftFreeVolMatrix = divTools.DriftFreeVolMatrix(volMatrix, spot);
                var localVariance = LocalVolUtils.LocalVariance(driftFreeVolMatrix, spot, fixLocalVol);
                
                var moneyness = MoneynessProvider.DivAdjusted(spot, divTools); 
                //TODO use correct moneyness computation for drift free underlying
                
                var result = new double[dates.Length, strikes.Length];
                for (int i = 0; i<dates.Length; i++)
                {
                    var date = ObjectConverters.ConvertDate(dates[i], assetMarket.RefDate);
                    double t = time[date];
                    var logSpots = strikes.Map(moneyness.Moneyness(t));

                    double[] slice;
                    switch (request.ToLowerInvariant().Trim())
                    {
                        case "density" :
                            slice = localVariance.DensityRatioSample(t, logSpots);
                            break;

                        case "calendar" :
                            slice = localVariance.InstantVarianceSample(t, logSpots);
                            break;

                        case "" :
                        case "localvol" :
                            slice = localVariance.VarianceSample(t, logSpots).Map(Math.Sqrt);
                            break;

                        default :
                            throw new ArgumentException("Unknown LocalVolViewer request, could be : Density, Calendar or LocalVol");
                    }
                    
                    ArrayUtils.SetRow(ref result, i, slice);
                }
                return result;
            });
        }
        
        [ExcelFunction(Description = "Equity sabr volatility matrix", Category = "PragmaticQuant_Model")]
        public static object SabrVolMatrix(double[] strikeMoneynesses, double[] atmVols, double[] skews, double[] convexities)
        {
            return ComputationRunnerUtils.Run("SabrVolMatrix", () =>
            {
                var volMatrix = new double[atmVols.Length, strikeMoneynesses.Length];
                for (int i = 0; i < atmVols.Length; i++)
                {
                    var smile = SabrFormulas.ComputeSmile(strikeMoneynesses, atmVols[i], skews[i], convexities[i]);
                    ArrayUtils.SetRow(ref volMatrix, i, smile);
                }
                return volMatrix;
            });
        }
    }

    public static class BergomiModelFunctions
    {
        [ExcelFunction(Description = "Bergomi 2F model Vol Of Vol Viewer",
            Category = "PragmaticQuant_Model")]
        public static object Bergomi2FVolOfVol(object mktObj, object[,] modelBag, object[] starts, object[] ends)
        {
            return ComputationRunnerUtils.Run("Bergomi2FVolOfVol", () =>
            {
                Market market = MarketManager.Instance.GetMarketDatas(mktObj).BuildMarket();
                ICalibrationDescription b2FCalibDesc = Bergomi2FModelFactoryFromBag.Instance.Build(modelBag);
                IModelDescription b2FDesc = ModelCalibration.Instance.Calibrate(b2FCalibDesc, market, null);
                var b2Fmodel = (Bergomi2FModel) ModelFactory.Instance.Build(b2FDesc, market).GetEquityParts().First();

                var startDates = b2Fmodel.Time[ObjectConverters.ConvertDateArray(starts, market.RefDate)];
                var endDates = b2Fmodel.Time[ObjectConverters.ConvertDateArray(ends, market.RefDate)];
                if (startDates.Length != endDates.Length)
                    throw new ArgumentException("Incompatible size between starts and ends");

                return Bergomi2FUtils.FwdVolInstantVol(b2Fmodel, startDates, endDates).AsColumn();
            });
        }

        [ExcelFunction(Description = "Bergomi 2F model Correlation Of Vol Viewer",
            Category = "PragmaticQuant_Model")]
        public static object Bergomi2FCorrelOfVol(object mktObj, object[,] modelBag, object[] starts, object[] ends)
        {
            return ComputationRunnerUtils.Run("Bergomi2FCorrelOfVol", () =>
            {
                Market market = MarketManager.Instance.GetMarketDatas(mktObj).BuildMarket();
                ICalibrationDescription b2FCalibDesc = Bergomi2FModelFactoryFromBag.Instance.Build(modelBag);
                IModelDescription b2FDesc = ModelCalibration.Instance.Calibrate(b2FCalibDesc, market, null);
                var b2Fmodel = (Bergomi2FModel)ModelFactory.Instance.Build(b2FDesc, market).GetEquityParts().First();
                
                var startDates = b2Fmodel.Time[ObjectConverters.ConvertDateArray(starts, market.RefDate)];
                var endDates = b2Fmodel.Time[ObjectConverters.ConvertDateArray(ends, market.RefDate)];
                if (startDates.Length != endDates.Length)
                    throw new ArgumentException("Incompatible size between starts and ends");

                var covar = Bergomi2FUtils.FwdVolInstantCovariance(b2Fmodel, startDates, endDates);
                var correl = covar.Correlation();
                return correl;
            });
        }

        [ExcelFunction(Description = "Bergomi 2F model Correlation Of Vol Viewer",
            Category = "PragmaticQuant_Model")]
        public static object Bergomi2FAtmfSkewApprox(object mktObj, object[,] modelBag, object[] maturities)
        {
            return ComputationRunnerUtils.Run("Bergomi2FAtmfSkewApprox", () =>
            {
                Market market = MarketManager.Instance.GetMarketDatas(mktObj).BuildMarket();
                ICalibrationDescription b2FCalibDesc = Bergomi2FModelFactoryFromBag.Instance.Build(modelBag);
                IModelDescription b2FDesc = ModelCalibration.Instance.Calibrate(b2FCalibDesc, market, null);
                var b2Fmodel = (Bergomi2FModel)ModelFactory.Instance.Build(b2FDesc, market).GetEquityParts().First();
                var mats = b2Fmodel.Time[ObjectConverters.ConvertDateArray(maturities, market.RefDate)];
                var skews = Bergomi2FUtils.AtmfSkewApprox(b2Fmodel, mats);
                return skews.AsColumn();
            });
        }
    }

    public static class HullWhitewNFactorModelFunctions
    {
        #region private methods
        private static HwnCorrelationViewer Viewer(object mktObj, object[,] modelBag)
        {
            IFactoryFromBag<ICalibrationDescription> calibFactory;
            if (modelBag.Has("Asset"))
                calibFactory = BlackscholesHybridHwnFactoryFromBag.Instance;
            else
                calibFactory = HullWhiteNfModelFactoryFromBag.Instance;
            ICalibrationDescription calib = calibFactory.Build(modelBag);

            Market market = MarketManager.Instance.GetMarketDatas(mktObj).BuildMarket();
            IModelDescription modelDesc = ModelCalibration.Instance.Calibrate(calib, market, null);
            IModel model = ModelFactory.Instance.Build(modelDesc, market);
            return HwnCorrelationViewer.Build(model);
        }
        #endregion

        [ExcelFunction(Description = "HullWhite N-Factors instantaneaous correlation viewer",
            Category = "PragmaticQuant_Model")]
        public static object HwnFInstantCorrelationViewer(object mktObj, object[,] modelBag, object[] underlying1, object[] underlying2)
        {
            return ComputationRunnerUtils.Run("HwnFInstantCorrelationViewer", () =>
            {
                var correlViewer = Viewer(mktObj, modelBag);

                if (underlying1.Length != underlying2.Length)
                    throw new ArgumentException("Incompatible size between underlying1 and underlying2");
                var underlying1Descs = underlying1.Map(o=> FixingDescParser.ParseFixing(o.ToString()));
                var underlying2Descs = underlying2.Map(o => FixingDescParser.ParseFixing(o.ToString()));

                var correls = EnumerableUtils.For(0, underlying1Descs.Length, i =>
                {
                    var var1 = correlViewer.Variance(underlying1Descs[i]);
                    var var2 = correlViewer.Variance(underlying2Descs[i]);
                    var covar = correlViewer.Covariance(underlying1Descs[i], underlying2Descs[i]);
                    return covar.Eval(0.0) / Math.Sqrt(var1.Eval(0.0) * var2.Eval(0.0));
                });

                return correls.AsColumn();
            });
        }

        [ExcelFunction(Description = "HullWhite N-Factors instantaneaous volatility viewer",
            Category = "PragmaticQuant_Model")]
        public static object HwnFInstantVolViewer(object mktObj, object[,] modelBag, object[] underlying)
        {
            return ComputationRunnerUtils.Run("HwnFInstantVolViewer", () =>
            {
                var correlViewer = Viewer(mktObj, modelBag);
                var underlyingDescs = underlying.Map(o => FixingDescParser.ParseFixing(o.ToString()));
                var vols = underlyingDescs.Map(undl =>
                {
                    var var = correlViewer.Variance(undl);
                    return Math.Sqrt(var.Eval(0.0));
                });
                return vols.AsColumn();
            });
        }
    }
}