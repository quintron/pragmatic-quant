using System;
using System.Diagnostics;
using System.Linq;
using ExcelDna.Integration;
using PragmaticQuant.Xl.Factories;
using PragmaticQuant.Xl.Publishers;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets;
using PragmaticQuant.Markets.Rate;
using PragmaticQuant.Model;
using PragmaticQuant.Model.Rate;
using PragmaticQuant.Pricing;
using PragmaticQuant.Product;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.Xl
{
    public class XlPricingFunctions
    {
        #region private methods
        private static IProduct BuildProduct(object[,] productBag)
        {
            return ComputationRunnerUtils.RunWithLog("Product preparation",
                () =>
                {
                    var productFactory = new ProductFactory(new FixingParser());
                    return productFactory.Build(productBag);
                });
        }
        private static ICalibrationDescription BuildCalibration(object[,] modelBag)
        {
            return ComputationRunnerUtils.RunWithLog("model preparation",
                () => ModelCalibrationFactory.Instance.Build(modelBag));
        }
        private static PriceableConfig BuildPricingConfig(object[,] productBag, object mktObj, object[,] modelBag, object[,] algorithmBag)
        {
            var market = MarketManager.Instance.GetMarketDatas(mktObj);
            var pricingConfig = new PricingConfig()
            {
                Market = market,
                Model = BuildCalibration(modelBag),
                NumericalMethod = AlgorithmFactory.Instance.Build(algorithmBag)
            };
            return new PriceableConfig
            {
                PricingConfig = pricingConfig,
                Product = BuildProduct(productBag)
            };
        }
        private static IPricingTask<IPricingResult> BuildPricingTask(object requestObj)
        {
            if (requestObj is string
                && requestObj.ToString().Trim().Equals("Price", StringComparison.InvariantCultureIgnoreCase))
                return new PriceComputationTask();

            var requestBag = requestObj as object[,];
            if (requestBag != null)
                return PricingTaskFactory.Instance.Build(requestBag);

            throw new Exception("Unknow request");
        }
        #endregion
        
        [ExcelFunction(Description = "Exotic product pricing function",
                       Category = "PragmaticQuant_Pricing")]
        public static object ExoticPrice(object requestObj, object[,] productBag, object mktObj, object[,] modelBag, object[,] algorithmBag)
        {
            return ComputationRunnerUtils.Run("ExoticPrice", () =>
            {
                Trace.WriteLine("");
                PriceableConfig priceableConfig = BuildPricingConfig(productBag, mktObj, modelBag, algorithmBag);
                IPricingTask<IPricingResult> pricingTask = BuildPricingTask(requestObj);
                IPricingResult priceResult = pricingTask.Price(priceableConfig);
                Trace.WriteLine("");
                return PriceResultPublisher.Instance.Publish(priceResult);
            });
        }
        
        [ExcelFunction(Description = "Equity Volatility Viewer",
                       Category = "PragmaticQuant_Pricing")]
        public static object EquityVolViewer(object[,] smileDescriptionBag, object mktObj, object[,] modelBag, object[,] algorithmBag)
        {
            return ComputationRunnerUtils.Run("EquityVolViewer", () =>
            {
                var marketConfig = MarketManager.Instance.GetMarketDatas(mktObj);
                ICalibrationDescription modelCalibDesc = ModelCalibrationFactory.Instance.Build(modelBag);
                INumericalMethodConfig algorithm = AlgorithmFactory.Instance.Build(algorithmBag);
                var pricingConfig = new PricingConfig
                {
                    Market = marketConfig,
                    Model = modelCalibDesc,
                    NumericalMethod = algorithm,
                };
                
                double[] strikes = smileDescriptionBag.ProcessVectorDouble("Strike");
                DateOrDuration[] maturities = smileDescriptionBag.ProcessVectorDateOrDuration("Maturity");
                string asset = smileDescriptionBag.ProcessScalarString("Asset");

                return VanillaViewer.EquitySmile(pricingConfig, asset, maturities, strikes);
            });
        }

        [ExcelFunction(Description = "Equity Variance Viewer",
                       Category = "PragmaticQuant_Pricing")]
        public static object EquityVarianceViewer(object[,] smileDescriptionBag, object mktObj, object[,] modelBag, object[,] algorithmBag)
        {
            return ComputationRunnerUtils.Run("EquityVarianceViewer", () =>
            {
                var marketConfig = MarketManager.Instance.GetMarketDatas(mktObj);
                ICalibrationDescription modelCalibDesc = ModelCalibrationFactory.Instance.Build(modelBag);
                INumericalMethodConfig algorithm = AlgorithmFactory.Instance.Build(algorithmBag);
                var pricingConfig = new PricingConfig
                {
                    Market = marketConfig,
                    Model = modelCalibDesc,
                    NumericalMethod = algorithm
                };
                
                DateOrDuration[] maturities = smileDescriptionBag.ProcessVectorDateOrDuration("Maturity");
                string asset = smileDescriptionBag.ProcessScalarString("Asset");

                Market market = marketConfig.BuildMarket();
                AssetId assetId = market.AssetIdFromName(asset);
                AssetMarket assetMkt = market.AssetMarket(assetId);

                var fwdCurve = assetMkt.ForwardCurve();
                var maturityDates = maturities.Map(o => o.ToDate(market.RefDate));
                var logSwaps = maturityDates.Map(mat =>
                {
                    double fwd = fwdCurve.Fwd(mat);
                    return new LogSwap(assetId, mat, assetMkt.AssetCurrency, fwd);
                });
                
                var optionPricingConfig = new PriceableConfig
                {
                    PricingConfig = pricingConfig,
                    Product = CouponLinearCombination.Create(logSwaps.Map(op => op.GetCoupon))
                };
                IPricingResult priceResult = new PriceComputationTask().Price(optionPricingConfig);
                PriceDetails priceDetails = priceResult.GetDetails<PriceDetails>();

                var discountCurve = market.OisDiscountCurve(assetMkt.AssetCurrency);
                var varSwapVols = maturityDates.Map(mat =>
                {
                    var logSwap = logSwaps.Single(op => op.Maturity == mat);
                    Price price = priceDetails.Details.Single(det => det.Item1.Equals(logSwap.Id, StringComparison.InvariantCultureIgnoreCase)).Item3;
                    var variance = price.Value / discountCurve.Zc(mat);
                    return Math.Sqrt(variance / assetMkt.Time[mat]);
                });
                return varSwapVols.AsColumn();
            });
        }

        [ExcelFunction(Description = "Swaption Volatility Viewer",
                       Category = "PragmaticQuant_Pricing")]
        public static object SwaptionVolViewer(object[,] smileDescriptionBag, object mktObj, object[,] modelBag, object[,] algorithmBag)
        {
            return ComputationRunnerUtils.Run("SwaptionVolViewer", () =>
            {
                var marketConfig = MarketManager.Instance.GetMarketDatas(mktObj);
                ICalibrationDescription modelCalibDesc = ModelCalibrationFactory.Instance.Build(modelBag);
                INumericalMethodConfig algorithm = AlgorithmFactory.Instance.Build(algorithmBag);
                var pricingConfig = new PricingConfig
                {
                    Market = marketConfig,
                    Model = modelCalibDesc,
                    NumericalMethod = algorithm
                };

                Currency currency = Currency.Parse(smileDescriptionBag.ProcessScalarString("Currency"));
                bool strikeDev = smileDescriptionBag.Has("StrikeDev");
                double[] strikeDatas = smileDescriptionBag.ProcessVectorDouble(strikeDev ? "StrikeDev" : "Strike");
                DateOrDuration[] maturities = smileDescriptionBag.ProcessVectorDateOrDuration("Maturity");
                DateOrDuration[] underlyings = smileDescriptionBag.ProcessVectorDateOrDuration("Underlying");
                if (underlyings.Length != maturities.Length)
                    throw new Exception("Incompatible size between Maturity and Underlying");

                var market = marketConfig.BuildMarket();
                RateMarket rateMarket = market.RateMarket(currency);
                ForwardSwapRateCurve fwdSwapRateCurve = rateMarket.SwapRateCurve();
                var maturityDates = maturities.Map(d => d.ToDate(market.RefDate));
                var smilePoints = (from matIndex in Enumerable.Range(0, maturities.Length)
                                   from strikeData in strikeDatas
                                     select new SwaptionSmilePoint(currency, maturities[matIndex], underlyings[matIndex], strikeData, strikeDev)
                                   ).ToArray();
                var options = smilePoints.Map(p => p.ToSwaption(market));

                var optionPricingConfig = new PriceableConfig
                {
                    PricingConfig = pricingConfig,
                    Product = CouponLinearCombination.Create(options.Map(op => op.GetCoupon()))
                };
                IPricingResult priceResult = new PriceComputationTask().Price(optionPricingConfig);
                PriceDetails priceDetails;
                priceResult.TryGetDetail(out priceDetails);

                var vanillaPricer = SwaptionPricer.Build(rateMarket);
                var optionVols = ArrayUtils.CartesianProd(Enumerable.Range(0, maturityDates.Length).ToArray(), strikeDatas, (matIndex, k) =>
                {
                    double strike = k;
                    if (strikeDev)
                        strike += fwdSwapRateCurve.Fwd(maturityDates[matIndex], underlyings[matIndex]);

                    Swaption option = options.Single(op => op.Date == maturityDates[matIndex] && DoubleUtils.Equality(strike, op.Strike, 1.0e-13));
                    Price price = priceDetails.Details.Single(det => det.Item1.Equals(option.Id, StringComparison.InvariantCultureIgnoreCase)).Item3;
                    return vanillaPricer.ImpliedVol(maturityDates[matIndex], option.Underlying, strike, option.OptionType, price.Value);
                });
                return optionVols;
            });
        }
        
    }
}