using System;

namespace PragmaticQuant.Basic
{
    public class AssetId
    {
        #region private fields
        private readonly string name;
        #endregion
        #region private method
        private bool Equals(AssetId other)
        {
            return string.Equals(name, other.name, StringComparison.InvariantCultureIgnoreCase);
        }
        #endregion

        public AssetId(string name)
        {
            this.name = name;
        }
        public string Name
        {
            get { return name; }
        }
        
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((AssetId)obj);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                return (name != null ? name.GetHashCode() : 0);
            }
        }
        public override string ToString()
        {
            return name;
        }
    }
}