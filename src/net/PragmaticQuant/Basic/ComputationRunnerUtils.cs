using System;
using System.Diagnostics;

namespace PragmaticQuant.Basic
{
    public static class ComputationRunnerUtils
    {
        #region private methods
        private static Exception[] CollectAggregate(Exception exception)
        {
            var aggregateExcept = exception as AggregateException;
            if (aggregateExcept != null)
            {
                var inners = aggregateExcept.InnerExceptions.Map(CollectAggregate);
                return EnumerableUtils.Append(inners);
            }
            return new[] {exception};
        }
        #endregion

        public static object Run<T>(string computationName, Func<T> computation)
        {
            try
            {
                return computation();
            }
            catch (Exception e)
            {
                var inners = CollectAggregate(e);
                foreach (var exception in inners)
                    Trace.TraceError(exception.Message);
                return string.Format("ERROR while running {0}", computationName);
            }
        }

        public static T RunWithLog<T>(string computationName, Func<T> computation)
        {
            var timer = new Stopwatch();
            Trace.WriteLine(string.Format("Start {0}...", computationName));
            timer.Restart();

            var result = computation();

            timer.Stop();
            Trace.WriteLine(String.Format("{3} done in {0} min {1} s {2} ms",
                timer.Elapsed.Minutes, timer.Elapsed.Seconds, timer.Elapsed.Milliseconds, computationName));
            return result;
        }
    }
}