﻿using System;
using System.Globalization;

namespace PragmaticQuant.Basic.Dates
{
    public class DateOrDuration
    {
        #region private fields
        private readonly Duration duration;
        private readonly DateTime? date;
        private readonly bool isDate;
        #endregion
        
        public DateOrDuration(DateTime date)
        {
            duration = null;
            this.date = date;
            isDate = true;
        }
        public DateOrDuration(Duration duration)
        {
            this.duration = duration;
            date = null;
            isDate = false;
        }

        public static implicit operator DateOrDuration(Duration duration)
        {
            return new DateOrDuration(duration);
        }
        public static implicit operator DateOrDuration(DateTime date)
        {
            return new DateOrDuration(date);
        }

        public Duration Duration
        {
            get
            {
                return duration;
            }
        }
        public DateTime Date
        {
            get { return date.GetValueOrDefault(); }
        }
        public bool IsDuration
        {
            get
            {
                return !isDate;
            }
        }
        public bool IsDate
        {
            get
            {
                return isDate;
            }
        }

        public DateTime ToDate(DateTime refDate)
        {
            if (isDate && date != null) return date.Value;
            return refDate + duration;
        }

        public string ToString(IFormatProvider formatProvider)
        {
            if (isDate && date.HasValue)
                return date.Value.ToString(formatProvider);
            return duration.ToString();
        }
        public override string ToString()
        {
            return ToString(CultureInfo.InvariantCulture);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((DateOrDuration) obj);
        }
        protected bool Equals(DateOrDuration other)
        {
            return Equals(duration, other.duration) 
                && date.Equals(other.date) 
                && isDate.Equals(other.isDate);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (duration != null ? duration.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ date.GetHashCode();
                hashCode = (hashCode * 397) ^ isDate.GetHashCode();
                return hashCode;
            }
        }
    }
}