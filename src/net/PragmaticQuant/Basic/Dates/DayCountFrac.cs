﻿using System;
using System.Diagnostics;

namespace PragmaticQuant.Basic.Dates
{
    public abstract class DayCountFrac
    {
        public abstract double Count(DateTime start, DateTime end);

        public static readonly DayCountFrac Thirty360 = new DayCountFrac_30_360();
        public static readonly DayCountFrac ThirtyE360 = new DayCountFrac_30E_360();
        public static readonly DayCountFrac Act365Fixed = new ActDayCountFrac(365.0);
        public static readonly DayCountFrac Act360 = new ActDayCountFrac(360.0);
        public static DayCountFrac Parse(string convention)
        {
            switch (convention.ToLowerInvariant())
            {
                case "30/360" : 
                    return Thirty360;

                case "30E/360" :
                    return ThirtyE360;
                    
                case "act/365" :
                    return Act365Fixed;

                case "actual360" :
                case "act/360" :
                    return Act360;
            }
            throw new ArgumentException(string.Format("Unknown DayCount convention {0}", convention));
        }
        
        #region private class
        [DebuggerDisplay("{ToString()}")]
        private class ActDayCountFrac : DayCountFrac
        {
            private readonly double yearBasis;
            public ActDayCountFrac(double yearBasis)
            {
                this.yearBasis = yearBasis;
            }
            public override double Count(DateTime start, DateTime end)
            {
                return (end - start).TotalDays / yearBasis;
            }
            public override string ToString()
            {
                return string.Format("Act/{0}", yearBasis);
            }
        }
        [DebuggerDisplay("30/360")]
        private class DayCountFrac_30_360 : DayCountFrac
        {
            public override double Count(DateTime start, DateTime end)
            {
                int d1 = start.Day;
                if (d1 == 31) d1 = 30;

                int d2 = end.Day;
                if (d2 == 31 && (d1 == 30 || d2 == 31)) d2 = 30;

                return (360.0 * (end.Year - start.Year) + 30.0 * (end.Month - start.Month) + (d2 - d1)) / 360.0;
            }
        }
        [DebuggerDisplay("30E/360")]
        private class DayCountFrac_30E_360 : DayCountFrac
        {
            public override double Count(DateTime start, DateTime end)
            {
                int d1 = start.Day;
                if (d1 == 31) d1 = 30;

                int d2 = end.Day;
                if (d2 == 31) d2 = 30;

                return (360.0 * (end.Year - start.Year) + 30.0 * (end.Month - start.Month) + (d2 - d1)) / 360.0;
            }
        }
        #endregion
    }
}