using System;
using System.Linq;

namespace PragmaticQuant.Basic.Dates
{
    public interface IBusinessConvention
    {
        DateTime[] Adjust(DateTime[] dates, ICalendar calendar);
    }

    public static class BusinessConventions
    {
        public static readonly IBusinessConvention Following = new FollowingConvention();
        public static readonly IBusinessConvention ModifiedFollowing = new ModifiedFollowingConvention();
        public static readonly IBusinessConvention Preceding = new PrecedingConvention();
        public static readonly IBusinessConvention EndOfMonth = new EndOfMonthConvention();
        public static IBusinessConvention For(string conv)
        {
            switch (conv.Trim().ToLowerInvariant())
            {
                case "following":
                    return Following;

                case "modifiedfollowing":
                    return ModifiedFollowing;

                case "preceding":
                    return Preceding;

                case "endofmonth":
                    return EndOfMonth;

                default:
                    throw new Exception("Unknown business convention : " + conv);
            }
        }

        #region private class
        private abstract class BusinessConvention : IBusinessConvention
        {
            protected abstract DateTime Adjust(DateTime date, ICalendar calendar);
            public DateTime[] Adjust(DateTime[] dates, ICalendar calendar)
            {
                return dates.Select(d => Adjust(d, calendar)).Distinct().ToArray();
            }
        }

        private class FollowingConvention : BusinessConvention
        {
            protected override DateTime Adjust(DateTime date, ICalendar calendar)
            {
                var result = date;
                while (!calendar.IsWorkingDay(result))
                {
                    result = result.AddDays(1);
                }
                return result;
            }
        }

        private class ModifiedFollowingConvention : BusinessConvention
        {
            protected override DateTime Adjust(DateTime date, ICalendar calendar)
            {
                var result = date;
                while (!calendar.IsWorkingDay(result))
                {
                    result = result.AddDays(1);
                }

                if (result.Month == date.Month)
                    return result;

                result = date;
                while (!calendar.IsWorkingDay(result))
                {
                    result = result.AddDays(-1);
                }
                return result;
            }
        }

        private class PrecedingConvention : BusinessConvention
        {
            protected override DateTime Adjust(DateTime date, ICalendar calendar)
            {
                var result = date;
                while (!calendar.IsWorkingDay(result))
                {
                    result = result.AddDays(-1);
                }
                return result;
            }
        }

        private class EndOfMonthConvention : IBusinessConvention
        {
            private DateTime MonthLastBusinessDay(DateTime date, ICalendar calendar)
            {
                var lastDayOfMonth = DateTime.DaysInMonth(date.Year, date.Month);
                var lastDateOfMonth = new DateTime(date.Year, date.Month, lastDayOfMonth, date.Hour, date.Minute, date.Second);
                return calendar.PreviousWorkingDayIfClosed(lastDateOfMonth);
            }
            private bool IsMonthLastBusinessDay(DateTime date, ICalendar calendar)
            {
                var lastMonthBusinessDay = MonthLastBusinessDay(date, calendar);
                return (date.Day == lastMonthBusinessDay.Day
                        && date.Month == lastMonthBusinessDay.Month
                        && date.Year == lastMonthBusinessDay.Year);
            }
            public DateTime[] Adjust(DateTime[] dates, ICalendar calendar)
            {
                var adjustedDates = ModifiedFollowing.Adjust(dates, calendar);
                for (int i = 1; i < adjustedDates.Length; i++)
                {
                    if (IsMonthLastBusinessDay(adjustedDates[i - 1], calendar))
                    {
                        adjustedDates[i] = MonthLastBusinessDay(adjustedDates[i], calendar);
                    }
                }
                return adjustedDates;
            }
        }
        #endregion
    }

}