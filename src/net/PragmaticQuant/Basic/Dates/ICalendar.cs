using System;
using PragmaticQuant.Basic.Structure;

namespace PragmaticQuant.Basic.Dates
{
    public interface ICalendar
    {
        bool IsWorkingDay(DateTime date);
    }

    public static class CalendarExtensions
    {
        public static DateTime NextWorkingDayIfClosed(this ICalendar calendar, DateTime date)
        {
            var result = date;
            while (!calendar.IsWorkingDay(result))
            {
                result = result.AddDays(1);
            }
            return result;
        }
        public static DateTime PreviousWorkingDayIfClosed(this ICalendar calendar, DateTime date)
        {
            var result = date;
            while (!calendar.IsWorkingDay(result))
            {
                result = result.AddDays(-1);
            }
            return result;
        }
        public static DateTime AddOpenDays(this ICalendar calendar, DateTime date, int nbOpenDays)
        {
            if (nbOpenDays < 0)
                throw new Exception("nbOpenDays should be positive !");
            var result = calendar.NextWorkingDayIfClosed(date);
            for (int i = 0; i < nbOpenDays; i++) result = NextWorkingDayIfClosed(calendar, result.AddDays(1.0));
            return result;
        }
        public static DateTime SubOpenDays(this ICalendar calendar, DateTime date, int nbOpenDays)
        {
            if (nbOpenDays < 0)
                throw new Exception("nbOpenDays should be positive !");
            var result = calendar.PreviousWorkingDayIfClosed(date);
            for (int i = 0; i < nbOpenDays; i++) result = PreviousWorkingDayIfClosed(calendar, result.AddDays(-1.0));
            return result;
        }
    }

    public class SimpleCalendar : Singleton<SimpleCalendar>, ICalendar
    {
        public bool IsWorkingDay(DateTime date)
        {
            if (date.DayOfWeek == DayOfWeek.Saturday || 
                date.DayOfWeek == DayOfWeek.Sunday)
                return false;
            
            if (date.Month == 1 && date.Day == 1) //First January 
                return false;

            return true;
        }
    }
    
}