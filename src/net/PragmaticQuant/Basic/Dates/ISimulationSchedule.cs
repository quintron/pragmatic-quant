using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace PragmaticQuant.Basic.Dates
{
    public interface ISimulationSchedule
    {
        /// <summary>
        /// Return dates for simulating step : ]start, end]. 
        /// </summary>
        /// <param name="start"> start date of simulation step </param>
        /// <param name="end"> end date of simualtion step </param>
        /// /// <param name="refDate"> starting date for the full simulation </param>
        /// <param name="technicalDates">Technical dates imposed on simulation schedule </param>
        /// <returns></returns>
        DateTime[] Schedule(DateTime start, DateTime end, DateTime refDate, DateTime[] technicalDates);
    }

    public static class SimulationSchedules
    {
        public static ISimulationSchedule ScheduleBuilder(Duration maxStep, int nbStepByDate = -1, Duration minStep = null)
        {
            minStep = minStep ?? DefaultMinStep;
            if (nbStepByDate <= 1)
                return new RegularSimulationSchedule(maxStep);

            return new NbStepRuleSimulSchedule(nbStepByDate, minStep, maxStep);
        }
        public const int DefaultNbStep = 100;
        public static readonly Duration DefaultMinStep = 6 * Duration.Hour;
        public static readonly Duration DefaultMaxStep = 10 * Duration.Day;
        public static readonly ISimulationSchedule DefaultScheduleBuilder = new NbStepRuleSimulSchedule(DefaultNbStep, DefaultMinStep, DefaultMaxStep);
        
    }

    public class RegularSimulationSchedule : ISimulationSchedule
    {
        #region private fields
        private readonly Duration step;
        #endregion
        public RegularSimulationSchedule(Duration step)
        {
            this.step = step;
        }
        public DateTime[] Schedule(DateTime start, DateTime end, DateTime refDate, DateTime[] technicalDates)
        {
            technicalDates = technicalDates.Where(d => d > start && d <= end).OrderBy(d => d).ToArray();

            var datesList = new List<DateTime>();
            var previous = start;
            foreach (var techDate in technicalDates)
            {
                datesList.AddRange(ScheduleUtils.RawSchedule(previous, techDate, step, true));
                previous = techDate;
            }
            if (previous < end)
                datesList.AddRange(ScheduleUtils.RawSchedule(previous, end, step, true));
            
            return datesList.ToArray();
        }
    }

    public class NbStepRuleSimulSchedule : ISimulationSchedule
    {
        #region private fields
        private readonly int nbStepByDate;
        private readonly Duration minStepDuration;
        private readonly Duration maxStepDuration;
        #endregion
        #region private methods
        private double Step(DateTime refDate, DateTime end)
        {
            var stepInDays = (end - refDate).TotalDays / nbStepByDate;
            var minStep = ((refDate + minStepDuration) - refDate).TotalDays;
            var maxStep = ((refDate + maxStepDuration) - refDate).TotalDays;
            return Math.Min(maxStep, Math.Max(minStep, stepInDays));
        }
        private double AdjustedStep(DateTime start, DateTime end, double stepInDays)
        {
            var adjustedNbStep = (end - start).TotalDays / stepInDays;
            var res = adjustedNbStep - Math.Truncate(adjustedNbStep);
            adjustedNbStep = Math.Truncate(adjustedNbStep);
            if (res > 0.0) adjustedNbStep += 1.0;

            return (end - start).TotalDays / adjustedNbStep;
        }
        #endregion
        
        public NbStepRuleSimulSchedule(int nbStepByDate, Duration minStepDuration, Duration maxStepDuration)
        {
            this.nbStepByDate = nbStepByDate;
            this.minStepDuration = minStepDuration;
            this.maxStepDuration = maxStepDuration;
        }
        public DateTime[] Schedule(DateTime start, DateTime end, DateTime refDate, DateTime[] technicalDates)
        {
            var stepInDays = Step(refDate, end);
            var adjustedStep = AdjustedStep(start, end, stepInDays);
            Debug.Assert(adjustedStep > 0.0);

            var schedule = new List<DateTime> {end};
            var current = end.AddDays(-adjustedStep);
            while (current > start.AddTicks(1))
            {
                schedule.Add(current);
                current = current.AddDays(-adjustedStep);
            }
            
            return schedule.Union(technicalDates.Where(d => d > start && d <= end)).OrderBy(d=>d).ToArray();
        }
    }
}