﻿using System;
using System.Linq;

namespace PragmaticQuant.Basic.Dates
{
    public class SwapScheduleBuilder
    {
        #region private fields
        private readonly ICalendar calendar;
        private readonly IBusinessConvention businessConv;
        private readonly int fixingShiftNbDays;
        #endregion
        public SwapScheduleBuilder(ICalendar calendar, int fixingShiftNbDays, IBusinessConvention businessConv)
        {
            this.calendar = calendar;
            this.fixingShiftNbDays = fixingShiftNbDays;
            this.businessConv = businessConv;
        }

        public CouponSchedule[] GetSchedule(DateTime start, DateTime end, Duration period)
        {
            if (!(start < end))
                throw new Exception("SwapScheduleBuilder: start date must be previous to end date");

            DateTime[] rawSchedule = ScheduleUtils.RawSchedule(start, end, period, true, false);
            DateTime[] schedule = businessConv.Adjust(rawSchedule, calendar);

            var payDates = schedule.Skip(1).ToArray();
            var startDates = schedule.Take(schedule.Length - 1).ToArray();
            var fixingDates = startDates.Map(FixingDate);
            return EnumerableUtils.For(0, startDates.Length, 
                i => new CouponSchedule(fixingDates[i], startDates[i], payDates[i], payDates[i]));
        }
        public DateTime StartDate(DateTime fixingDate)
        {
            var startDate = calendar.AddOpenDays(fixingDate, 1);
            startDate = calendar.AddOpenDays(startDate, fixingShiftNbDays);
            startDate = calendar.SubOpenDays(startDate, 1);
            return startDate;
        }
        public DateTime FixingDate(DateTime startDate)
        {
            var fixingDate = calendar.AddOpenDays(startDate, 1);
            fixingDate = calendar.SubOpenDays(fixingDate, fixingShiftNbDays);
            fixingDate = calendar.SubOpenDays(fixingDate, 1);
            return fixingDate;
        } 
    }
}