﻿using System;
using System.Linq;

namespace PragmaticQuant.Basic
{
    public static class DoubleUtils
    {
        #region private methods
        private static double GetMachineEpsilon()
        {
            double machEps = 1.0d;
            do
            {
                machEps /= 2.0d;
            } // ReSharper disable CompareOfFloatsByEqualityOperator
            while ((1.0 + machEps) != 1.0);
            // ReSharper restore CompareOfFloatsByEqualityOperator
            return machEps;
        }
        #endregion
        public static readonly double MachineEpsilon = GetMachineEpsilon();
        public static readonly double Sqrt_Epsilon = Math.Sqrt(MachineEpsilon);

        public static bool MachineEquality(double x, double y)
        {
            return Equality(x, y, MachineEpsilon);
        }
        public static bool MachineEquality(double[] x, double[] y)
        {
            return Equality(x, y, MachineEpsilon);
        }
        public static bool Equality(double x, double y, double relativePrecision)
        {
            if (double.IsInfinity(x) || double.IsInfinity(y))
            {
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                return x == y;
            }
            
            return Math.Abs(x - y) <= Math.Max(Math.Abs(x), Math.Abs(y)) * relativePrecision;
        }
        public static bool Equality(double[] x, double[] y, double relativePrecision)
        {
            if (x.Length != y.Length)
                return false;
            return !x.Where((t, i) => !Equality(t, y[i], relativePrecision)).Any();
        }
        public static bool EqualZero(double x)
        {
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            return x == 0.0;
        }
        public static bool EqualZero(double[] x)
        {
            return x.All(EqualZero);
        }
    }
}
