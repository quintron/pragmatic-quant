﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PragmaticQuant.Basic
{
    public static class EnumerableUtils
    {
        public static bool IsSorted<T>(IEnumerable<T> list) where T : IComparable<T>
        {
            if (list == null || !list.Any())
                return true;
            
            var array = list as T[] ?? list.ToArray();
            var y = array.First();
            return array.Skip(1).All(x =>
            {
                bool b = y.CompareTo(x) < 0;
                y = x;
                return b;
            });
        }
        public static T[] ConstantArray<T>(T value, int size)
        {
            var result = new T[size];
            for (int i = 0; i < result.Length; ++i)
                result[i] = value;
            return result;
        }
        public static T[] For<T>(int startIndex, int count, Func<int, T> func)
        {
            return Enumerable.Range(startIndex, count).Select(func).ToArray();
        }
        public static T[] Append<T>(params IEnumerable<T>[] lists)
        {
            return lists.Aggregate(new T[0], (result, current) => result.Concat(current).ToArray());
        }
        
        public static T[] Merge<T>(params IEnumerable<T>[] lists)
        {
            return lists.Aggregate(new T[0], (result, current) => result.Union(current).ToArray());
        }
        public static T[] MergeWith<T>(this IEnumerable<T> list, params IEnumerable<T>[] otherLists)
        {
            var mergedOthers = Merge(otherLists);
            return Merge(list, mergedOthers);
        }

        public static IDictionary<TIn, TOut> ZipToDictionary<TIn, TOut>(this IList<TIn> keys, IList<TOut> values)
        {
            return Enumerable.Range(0, keys.Count).ToDictionary(i => keys[i], i => values[i]);
        }
    }

    public static class FuncUtils
    {
        public static TOut[] Map<TIn, TOut>(this IEnumerable<TIn> list, Func<TIn, TOut> map)
        {
            if (list == null) return new TOut[0];
            return list.Select(map).ToArray();
        }
        public static TOut[] ZipWith<TIn1, TIn2, TOut>(this IList<TIn1> left, IList<TIn2> right, Func<TIn1, TIn2, TOut> zipper)
        {
            if (right.Count != left.Count)
                throw new Exception("FuncUtils.ZipWith : incompatible list size !");

            var result = new TOut[left.Count];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = zipper(left[i], right[i]);
            }
            return result;
        }
        public static TOut[,] ZipWith<TIn1, TIn2, TOut>(this TIn1[,] left, TIn2[,] right, Func<TIn1, TIn2, TOut> zipper)
        {
            if (left.GetLength(0) != right.GetLength(0)
                || left.GetLength(1) != right.GetLength(1))
                throw new Exception("FuncUtils.ZipWith : incompatible array size");
            var result = new TOut[left.GetLength(0), left.GetLength(1)];
            for (int i = 0; i < left.GetLength(0); i++)
            {
                for (int j = 0; j < left.GetLength(1); j++)
                {
                    result[i, j] = zipper(left[i, j], right[i, j]);
                }
            }
            return result;
        }
        public static TOut[] Scan<TIn, TOut>(this IEnumerable<TIn> list, TOut seed ,Func<TOut, TIn, TOut> aggregator)
        {
            var result = new List<TOut>();
            var previous = seed;
            foreach (var elem in list)
            {
                var current = aggregator(previous, elem);
                result.Add(current);
                previous = current;
            }
            return result.ToArray();
        }
        public static TOut Fold<TIn, TOut>(this IEnumerable<TIn> list, TOut seed, Func<TOut, TIn, TOut> aggregator)
        {
            return list.Aggregate(seed, aggregator);
        }
    }
}
