﻿using System;
using PragmaticQuant.Basic.Dates;

namespace PragmaticQuant.Basic
{
    public class FinancingId
    {
        #region private fields
        private const string BorId = "Bor";
        private const string OisId = "Ois";
        
        private readonly string id;
        private readonly Currency currency;
        #endregion
        #region protected fields
        protected FinancingId(string id, Currency currency)
        {
            this.id = id;
            this.currency = currency;
        }
        protected bool Equals(FinancingId other)
        {
            return string.Equals(id, other.id) && Equals(currency, other.currency);
        }
        #endregion

        public string Id
        {
            get { return id; }
        }
        public Currency Currency
        {
            get { return currency; }
        }
        
        public static FinancingId Ois(Currency currency)
        {
            return new FinancingId(OisId, currency);
        }
        public static FinancingId Bor(Currency currency, Duration tenor)
        {
            return new FinancingId(BorId + tenor, currency);
        }
        public static FinancingId AssetCollat(Currency currency, AssetId asset)
        {
            return new FinancingId(asset.Name, currency);
        }
        
        public static bool TryParse(string cur, string financing, out FinancingId result)
        {
            result = null;

            Currency currency;
            if (!Currency.TryParse(cur, out currency))
                return false;

            var financingDesc = financing.Trim().ToLowerInvariant();

            if (financingDesc.Contains(OisId.ToLowerInvariant()))
            {
                if (financingDesc.Replace(OisId.ToLowerInvariant(), "") != string.Empty)
                    return false;

                result = Ois(currency);
                return true;
            }

            if (financingDesc.Contains(BorId.ToLowerInvariant()))
            {
                var tenorDesc = financingDesc.Replace(BorId.ToLowerInvariant(), "");
                
                Duration tenor;
                if (!Duration.TryParse(tenorDesc, out tenor))
                    return false;

                result = Bor(currency, tenor);
                return true;
            }
            
            return false;
        }
        public static bool TryParse(string curveId, out FinancingId result)
        {
            var splitted = curveId.Split('.');
            if (splitted.Length > 2)
                throw new Exception(string.Format("Not a valid FinancingCurveId : {0}", curveId));
            
            if (splitted.Length==2)
                return TryParse(splitted[0], splitted[1], out result);

            return TryParse(splitted[0], OisId, out result);
        }
        public static FinancingId Parse(string cur, string financing)
        {
            FinancingId result;
            if (!TryParse(cur, financing, out result))
            {
                throw new Exception(string.Format("Unable to parse financing curve id : {0} & {1}", cur, financing));
            }
            return result;
        }
        public static FinancingId Parse(string curveId)
        {
            FinancingId result;
            if (!TryParse(curveId, out result))
            {
                throw new Exception(string.Format("Unable to parse financing curve id : {0}", curveId));
            }
            return result;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((FinancingId)obj);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                return ((id != null ? id.GetHashCode() : 0) * 397) ^ (currency != null ? currency.GetHashCode() : 0);
            }
        }
        public override string ToString()
        {
            return currency.Name + "." + id;
        }
    }
}
