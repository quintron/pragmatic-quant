namespace PragmaticQuant.Basic
{
    public interface ICloneable<out T>
    {
        T Clone();
    }
}