﻿using System;
using System.Globalization;

namespace PragmaticQuant.Basic
{
    public static class NumberConverter
    {
        public static bool TryConvertDouble(object o, out double result)
        {
            if (o is double)
            {
                result = (double)o;
                return true;
            }
            return double.TryParse(o.ToString(), NumberStyles.Float, CultureInfo.InvariantCulture, out result);
        }
        public static double ConvertDouble(object o)
        {
            double result;
            if(!TryConvertDouble(o, out result))
                throw new Exception(string.Format("Unable to convert {0} to double", o));
            return result;
        }
        public static bool TryConvertInt(object o, out int result)
        {
            if (o is int)
            {
                result = (int) o;
                return true;
            }

            return int.TryParse(o.ToString(), NumberStyles.Any, CultureInfo.InvariantCulture, out result);
        }
        public static int ConvertInt(object o)
        {
            int result;
            if (!TryConvertInt(o, out result))
                throw new Exception(string.Format("Unable to convert {0} to int", o));
            return result;
        }
    }
}
