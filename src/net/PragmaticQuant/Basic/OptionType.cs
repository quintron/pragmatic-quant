using System;

namespace PragmaticQuant.Basic
{
    public enum OptionType
    {
        Call,
        Put,
        Straddle,
        BinaryCall,
        BinaryPut,
        Fwd,
    }

    public static class OptionTypeUtils
    {
        public static double Leverage(this OptionType optionType)
        {
            switch (optionType)
            {
                case OptionType.Call:
                case OptionType.Fwd:
                    return 1.0;

                case OptionType.Put:
                    return -1.0;

                default:
                    throw new Exception(string.Format("Unhandled option type : {0}", optionType));
            }
        }
    }
    
}