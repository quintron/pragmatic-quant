﻿namespace PragmaticQuant.Basic
{
    public class Price
    {
        public Price(double value, Currency currency)
        {
            Currency = currency;
            Value = value;
        }
        public double Value { get; private set; }
        public Currency Currency { get; private set; }
        public override string ToString()
        {
            return string.Format("{0} {1}", Value, Currency);
        }
    }
}
