﻿using System.Text.RegularExpressions;

namespace PragmaticQuant.Basic
{
    public static class RegexUtils
    {
        #region private
        private const string IntegerNumber = @"(\s*)([-]?)([\d]{1,16})";
        private static string NumberRegExpr(params string[] decimalSeparators)
        {
            if (decimalSeparators.Length == 0)
                return IntegerNumber; 

            return IntegerNumber + @"([" + string.Concat(decimalSeparators) + @"][\d]{1,16})?\s*";
        }
        #endregion
        public static readonly string Number = NumberRegExpr(",", ".");
        public static readonly Regex NumberRegex = GetStrictNumberRegex(",", ".");

        public static Regex GetNumberRegex(params string[] decimalSeparators)
        {
            return new Regex(NumberRegExpr(decimalSeparators), RegexOptions.Compiled);
        }
        public static Regex GetStrictNumberRegex(params string[] decimalSeparators)
        {
            return new Regex("^" + NumberRegExpr(decimalSeparators) + "$", RegexOptions.Compiled);
        }
        public static bool IsNumber(string s)
        {
            return NumberRegex.IsMatch(s);
        }

    }
}
