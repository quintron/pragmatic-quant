using System;

namespace PragmaticQuant.Basic
{
    public class StepFunction<T>
    {
        #region private fields
        private readonly StepSearcher stepSearcher;
        private readonly T[] values;
        private readonly T leftValue;
        #endregion
        
        public StepFunction(StepSearcher stepSearcher, T[] values, T leftValue)
        {
            if (stepSearcher.Pillars.Length != values.Length)
                throw new Exception("StepFunction: pillars and values should have same size !");

            this.values = values;
            this.leftValue = leftValue;
            this.stepSearcher = stepSearcher;
        }
        
        public StepFunction(double[] pillars, T[] values, T leftValue)
            :this(new StepSearcher(pillars), values, leftValue)
        {}

        public double[] Pillars
        {
            get { return stepSearcher.Pillars; }
        }
        public T[] Values
        {
            get { return values; }
        }
        public T LeftValue
        {
            get { return leftValue;}
        }

        public T Eval(double x)
        {
            int stepIndex;
            return Eval(x, out stepIndex);
        }
        public T Eval(double x, out int stepIndex)
        {
            stepIndex = stepSearcher.LocateLeftIndex(x);

            if (stepIndex <= -1)
                return leftValue;

            return values[stepIndex];
        }

        public StepFunction<TVal> Map<TVal>(Func<T, TVal> f)
        {
            return new StepFunction<TVal>(stepSearcher, values.Map(f), f(leftValue));
        }
    }
}