using System;
using System.Linq;

namespace PragmaticQuant.Basic.Structure
{
    public class LabelledMatrix<TRow, TCol, TVal> : ICloneable<LabelledMatrix<TRow, TCol, TVal>>
    {
        public LabelledMatrix(TRow[] rowLabels, TCol[] colLabels, TVal[,] values)
        {
            if (values.GetLength(0) != rowLabels.Length || values.GetLength(1) != colLabels.Length)
                throw new Exception("LabelledMatrix : invalid format datas");

            Values = values;
            ColLabels = colLabels;
            RowLabels = rowLabels;
        }
        public TRow[] RowLabels { get; private set; }
        public TCol[] ColLabels { get; private set; }
        public TVal[,] Values { get; private set; }
        
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((LabelledMatrix<TRow, TCol, TVal>) obj);
        }
        protected bool Equals(LabelledMatrix<TRow, TCol, TVal> other)
        {
            return RowLabels.Equal(other.RowLabels)
                   && ColLabels.Equal(other.ColLabels)
                   && Values.Equal(other.Values);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (RowLabels != null ? RowLabels.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ColLabels != null ? ColLabels.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Values != null ? Values.GetHashCode() : 0);
                return hashCode;
            }
        }
        public LabelledMatrix<TRow, TCol, TVal> Clone()
        {
            return new LabelledMatrix<TRow, TCol, TVal>(
                RowLabels.ToArray(),
                ColLabels.ToArray(),
                Values.Map(v => v));
        }
    }

    public static class LabelledMatrixUtils
    {
        public static bool HasCol<TRow, TVal>(LabelledMatrix<TRow, string, TVal> matrix, string label, out int colIndex)
        {
            var labelIndexes = matrix.ColLabels
                                .Select((l, i) => new { Label = l, Index = i })
                                .Where(data => data.Label.Equals(label, StringComparison.InvariantCultureIgnoreCase))
                                .Select(data => data.Index)
                                .ToArray();

            if (!labelIndexes.Any())
            {
                colIndex = -1;
                return false;
            }

            if (labelIndexes.Count() != 1)
                throw new Exception(string.Format("Multiple {0} label !", label));

            colIndex = labelIndexes.First();
            return true;
        }
        public static bool HasRow<TCol, TVal>(LabelledMatrix<string, TCol, TVal> matrix, string label, out int rowIndex)
        {
            var labelIndexes = matrix.RowLabels
                                .Select((l, i) => new { Label = l, Index = i })
                                .Where(data => data.Label.Equals(label, StringComparison.InvariantCultureIgnoreCase))
                                .Select(data => data.Index)
                                .ToArray();

            if (!labelIndexes.Any())
            {
                rowIndex = -1;
                return false;
            }

            if (labelIndexes.Count() != 1)
                throw new Exception(string.Format("Multiple {0} label !", label));

            rowIndex = labelIndexes.First();
            return true;
        }
        public static bool HasCol<TRow, TVal>(this LabelledMatrix<TRow, string, TVal> matrix, string label)
        {
            int index;
            return HasCol(matrix, label, out index);
        }
        public static TVal[] GetColFromLabel<TRow, TVal>(this LabelledMatrix<TRow, string, TVal> matrix, string label)
        {
            int labelIndex;
            if(!HasCol(matrix, label, out labelIndex))
                throw new Exception(string.Format("Missing Label : {0}", label));

            return matrix.Values.Column(labelIndex);
        }
        public static TValOut[] GetColFromLabel<TRow, TVal, TValOut>(this LabelledMatrix<TRow, string, TVal> matrix, string label, Func<TVal,TValOut> func)
        {
            return matrix.GetColFromLabel(label).Map(func);
        }
        public static TVal GetVal<TVal>(this LabelledMatrix<string, string, TVal> matrix, string rowLabel, string colLabel)
        {
            int colIndex;
            if (!HasCol(matrix, colLabel, out colIndex))
                throw new Exception(string.Format("Missing column label : {0}", colLabel));

            int rowIndex;
            if (!HasRow(matrix, rowLabel, out rowIndex))
                throw new Exception(string.Format("Missing row label : {0}", rowLabel));

            return matrix.Values[rowIndex, colIndex];
        }
    }

}