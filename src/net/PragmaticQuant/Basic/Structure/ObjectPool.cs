﻿using System;
using System.Collections.Concurrent;
using System.Linq;

namespace PragmaticQuant.Basic.Structure
{
    public interface IPoolableObject
    {
        int Size { get; }
        void Reset();
    }

    public class ObjectPool
    {
        #region private fields
        private readonly int maxSize;
        protected readonly ConcurrentStack<IPoolableObject> stack;
        protected int PoolSize;
        #endregion
        protected ObjectPool(int maxSizeInMb)
        {
            maxSize = maxSizeInMb * (1 << 20);
            stack = new ConcurrentStack<IPoolableObject>();
        }

        public void ReturnObject(IPoolableObject value)
        {
            lock (this)
            {
                if (value.Size + PoolSize < maxSize)
                {
                    stack.Push(value);
                    PoolSize += value.Size;
                }
            }
        }
    }

    public class ObjectPool<T> : ObjectPool where T : class, IPoolableObject
    {
        #region private fields
        private readonly Func<T> objectFactory;
        #endregion
        public ObjectPool(Func<T> objectFactory, int maxSizeInMb)
            :base(maxSizeInMb)
        {
            this.objectFactory = objectFactory;
        }
        public T GetObject()
        {
            T valueToReturn;
            lock (this)
            {
                IPoolableObject val;
                if (stack.TryPop(out val))
                {
                    PoolSize -= val.Size;
                    valueToReturn = (T) val;
                    valueToReturn.Reset();
                }
                else
                {
                    valueToReturn = objectFactory();
                }
            }
            return valueToReturn;
        }
    }

    public class PoolableJaggedArray : IPoolableObject 
    {
        #region private fields
        private readonly bool resetToZero;
        #endregion
        private PoolableJaggedArray(double[][] data, bool resetToZero)
        {
            this.resetToZero = resetToZero;
            Data = data;
            Size = data.Select(a => a.Length * sizeof (double)).Sum();
        }
        public static PoolableJaggedArray Create(int nbRows, int nbCols, bool resetToZero = true)
        {
            return new PoolableJaggedArray(ArrayUtils.CreateJaggedArray<double>(nbRows, nbCols), resetToZero);
        }

        public double[][] Data { get; private set; }

        public int Size { get; private set; }
        public void Reset()
        {
            if (resetToZero)
            {
                foreach (var a in Data)
                {
                    for (int j = 0; j < a.Length; j++)
                        a[j] = 0.0;
                }
            }
        }
    }

}