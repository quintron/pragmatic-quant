using System;
using System.Collections.Generic;
using System.Linq;

namespace PragmaticQuant.Basic.Structure
{
    public class RawMapDatas<TP, TV> : ICloneable<RawMapDatas<TP, TV>>
    {
        public RawMapDatas(TP[] pillars, TV[] values)
        {
            if (pillars.Length != values.Length)
                throw new Exception("RawMapDatas : incompatible size between pillars and values !");
            
            Values = values;
            Pillars = pillars;
        }
        public TP[] Pillars { get; private set; }
        public TV[] Values { get; private set; }

        private bool Equals(RawMapDatas<TP, TV> other)
        {
            return Pillars.Equal(other.Pillars)
                   && Values.Equal(other.Values);
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((RawMapDatas<TP, TV>) obj);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                return ((Pillars != null ? Pillars.GetHashCode() : 0) * 397) ^ (Values != null ? Values.GetHashCode() : 0);
            }
        }

        public RawMapDatas<TP, TV> Clone()
        {
            return new RawMapDatas<TP, TV>(Pillars.ToArray(), Values.ToArray());
        }
    }

    public static class RawMapDatasUtils
    {
        public static RawMapDatas<TP, TV> Constant<TP, TV>(TV value, params TP[] pillars)
        {
            return new RawMapDatas<TP, TV>(pillars, ArrayUtils.Constant(pillars.Length, value));
        }
        public static IDictionary<TP, TV> ToDictionary<TP, TV>(this RawMapDatas<TP, TV> rawMap)
        {
            return rawMap.Pillars.ZipToDictionary(rawMap.Values);
        }
        public static RawMapDatas<TP, TMv> MapValues<TP, TV, TMv>(this RawMapDatas<TP, TV> rawMap, Func<TV, TMv> map)
        {
            return new RawMapDatas<TP, TMv>(rawMap.Pillars, rawMap.Values.Map(map));
        }
        public static RawMapDatas<TMp, TV> MapPillars<TP, TV, TMp>(this RawMapDatas<TP, TV> rawMap, Func<TP, TMp> map)
        {
            return new RawMapDatas<TMp, TV>(rawMap.Pillars.Map(map), rawMap.Values);
        }
        public static RawMapDatas<TP, TV> SortKeys<TP, TV>(this RawMapDatas<TP, TV> rawMap)
        {
            var orderedKeys = rawMap.ToDictionary().OrderBy(kv => kv.Key);
            return new RawMapDatas<TP, TV>(orderedKeys.Map(kv => kv.Key),
                                           orderedKeys.Map(kv => kv.Value));
        }
    }

}