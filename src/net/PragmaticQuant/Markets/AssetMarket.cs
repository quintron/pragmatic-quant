using System;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Model;
using PragmaticQuant.Markets.Equity;
using PragmaticQuant.Model.Equity.Dividends;

namespace PragmaticQuant.Markets
{
    public class AssetMarket
    {
        #region private fields
        private readonly Lazy<AffineDividend[]> fullYieldDividends; 
        #endregion

        public AssetMarket(AssetId asset, ITimeMeasure time, double spot,
                           DiscountCurve assetDiscountCurve,
                           RawMapDatas<DateTime, double> forwardDivs, 
                           VolatilityMatrix volMatrix)
        {
            if (!assetDiscountCurve.Financing.Id.Equals(asset.Name, StringComparison.InvariantCultureIgnoreCase))
                throw new Exception("AssetMarket : invalid asset discountCurve !");
            if (forwardDivs.Pillars.Any(p => p < time.RefDate))
                throw new Exception("AssetMarket : forward divs previous to ref date not allowed !");
            if (time.RefDate != assetDiscountCurve.RefDate)
                throw new Exception("AssetMarket : asset discount curve ref date is not compatible with time ");

            Time = time;
            Spot = spot;
            VolMatrix = volMatrix;
            RefDate = time.RefDate;
            Asset = asset;
            AssetDiscountCurve = assetDiscountCurve;
            AssetCurrency = assetDiscountCurve.Financing.Currency;
            ForwardDivs = forwardDivs;
            
            fullYieldDividends = new Lazy<AffineDividend[]>(() =>
            {
                var affineDivStripping = new AffineDivModelStripping(Spot, AssetDiscountCurve, time);
                return affineDivStripping.StripAffineDiv(ForwardDivs, t => 1.0);
            });
        }

        public DateTime RefDate { get; private set; }
        public ITimeMeasure Time { get; private set; }
        public AssetId Asset { get; private set; }
        public Currency AssetCurrency { get; private set; }
        
        public double Spot { get; private set; }
        public DiscountCurve AssetDiscountCurve { get; private set; }
        public RawMapDatas<DateTime, double> ForwardDivs { get; private set; }
        public VolatilityMatrix VolMatrix { get; private set; }
        
        public AssetForwardCurve ForwardCurve()
        {
            return new AssetForwardCurve(Spot, fullYieldDividends.Value, AssetDiscountCurve, Time);
        }
        public VolatilitySurface VolSurface()
        {
            var moneynessForInterpolation = MoneynessProvider.DivAdjusted(Spot, fullYieldDividends.Value, AssetDiscountCurve, Time);
            return VolatilitySurface.BuildInterpol(VolMatrix, moneynessForInterpolation);
        }
        public AssetMarket NewAssetDiscountCurve(DiscountCurve newAssetDiscountCurve)
        {
            return new AssetMarket(Asset, Time, Spot, newAssetDiscountCurve, ForwardDivs, VolMatrix);
        }
    }

    public static class AssetMarketUtils
    {
        public static LogSwapVolCurve LogSwapVolCurve(this AssetMarket market)
        {
            return Equity.LogSwapVolCurve.Build(market);
        }
        public static BlackLawQuantileProvider LawQuantile(this AssetMarket assetMkt)
        {
            AssetForwardCurve forwardCurve = assetMkt.ForwardCurve();
            return new BlackLawQuantileProvider(assetMkt.VolSurface(), forwardCurve.Fwd);
        }
    }
}