using System;

namespace PragmaticQuant.Markets.Equity
{
    public class AffineDividend
    {
        public AffineDividend(DateTime date, double cash, double yield)
        {
            Date = date;
            Yield = yield;
            Cash = cash;
        }
        
        public DateTime Date { get; private set; }
        public double Cash { get; private set; }
        public double Yield { get; private set; }
    }
}