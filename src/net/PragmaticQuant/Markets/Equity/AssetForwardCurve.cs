using System;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Model.Equity.Dividends;

namespace PragmaticQuant.Markets.Equity
{
    public class AssetForwardCurve
    {
        #region private fields
        private readonly ITimeMeasure time;
        private readonly AffineDivCurveUtils divCurveUtils;
        #endregion
        
        public AssetForwardCurve(double spot, AffineDividend[] affineDividends, DiscountCurve assetFinancingCurve, ITimeMeasure time)
        {
            this.time = time;
            Spot = spot;
            divCurveUtils = new AffineDivCurveUtils(affineDividends, assetFinancingCurve, time);
        }

        public DateTime RefDate
        {
            get { return time.RefDate; }
        }
        public double Spot { get; private set; }
        public double Fwd(DateTime d)
        {
            double t = time[d];
            return divCurveUtils.Forward(t, Spot);
        }
    }
}