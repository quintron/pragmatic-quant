using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Maths;

namespace PragmaticQuant.Markets.Equity
{
    public class EquityVanillaPricer
    {
        #region private fields
        private readonly DiscountCurve discountCurve;
        private readonly AssetForwardCurve forwardCurve;
        private readonly ITimeMeasure time;
        #endregion
        public EquityVanillaPricer(DiscountCurve discountCurve, AssetForwardCurve forwardCurve, ITimeMeasure time)
        {
            this.discountCurve = discountCurve;
            this.forwardCurve = forwardCurve;
            this.time = time;
        }

        public static EquityVanillaPricer Build(AssetMarket assetMkt, DiscountCurve discount)
        {
            return new EquityVanillaPricer(discount,
                                           assetMkt.ForwardCurve(),
                                           assetMkt.Time);
        }
        public static EquityVanillaPricer Build(AssetId asset, Market market)
        {
            AssetMarket assetMkt = market.AssetMarket(asset);
            return Build(assetMkt, market.OisDiscountCurve(assetMkt.AssetCurrency));
        }
        
        public double Price(DateTime maturity, double strike, OptionType optionType, double vol)
        {
            var fwd = forwardCurve.Fwd(maturity);
            return discountCurve.Zc(maturity) * BlackScholesOption.Price(fwd, strike, vol, time[maturity], optionType.Leverage());
        }
        public double ImpliedVol(DateTime maturity, double strike, OptionType optionType, double price)
        {
            var discount = discountCurve.Zc(maturity);
            var fwd = forwardCurve.Fwd(maturity);
            return BlackScholesOption.ImpliedVol(price / discount, fwd, strike, time[maturity], optionType.Leverage());
        }
    }

}