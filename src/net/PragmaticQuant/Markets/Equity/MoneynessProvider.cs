using System;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Model.Equity.Dividends;

namespace PragmaticQuant.Markets.Equity
{
    public abstract class MoneynessProvider
    {
        public abstract double Moneyness(double maturity, double strike);
        public abstract Func<double, double> Moneyness(double maturity);
        public abstract double Strike(double maturity, double moneyness);

        public static MoneynessProvider LogSpot(double spot)
        {
            return new LogMoneyness(spot);
        }
        public static MoneynessProvider DivAdjusted(double spot,
                                                    AffineDividend[] affineDividends,
                                                    DiscountCurve assetDiscountCurve,
                                                    ITimeMeasure time)
        {
            return DivAdjusted(spot, new AffineDivCurveUtils(affineDividends, assetDiscountCurve, time));
        }
        public static MoneynessProvider DivAdjusted(double spot, AffineDivCurveUtils divCurveUtils)
        {
            return new DivAdjustedMoneyness(spot, divCurveUtils);
        }

        #region private class
        private sealed class DivAdjustedMoneyness : MoneynessProvider
        {
            #region private fields
            private readonly double spot;
            private readonly AffineDivCurveUtils affineDivCurveUtils;
            #endregion
            public DivAdjustedMoneyness(double spot, AffineDivCurveUtils affineDivCurveUtils)
            {
                this.spot = spot;
                this.affineDivCurveUtils = affineDivCurveUtils;
            }
            
            public override double Moneyness(double maturity, double strike)
            {
                var c = affineDivCurveUtils.CashDivBpv(maturity);
                var cAverage = affineDivCurveUtils.CashBpvAverage(0.0, maturity);
                var g = affineDivCurveUtils.AssetGrowth(maturity);
                var dk = g * (c - cAverage);

                return Math.Log((strike + dk) / (g * (spot + cAverage)));
            }
            public override Func<double, double> Moneyness(double maturity)
            {
                var c = affineDivCurveUtils.CashDivBpv(maturity);
                var cAverage = affineDivCurveUtils.CashBpvAverage(0.0, maturity);
                var g = affineDivCurveUtils.AssetGrowth(maturity);
                var dk = g * (c - cAverage);
                return strike => Math.Log((strike + dk) / (g * (spot + cAverage)));
            }
            public override double Strike(double maturity, double moneyness)
            {
                throw new NotImplementedException();
            }
        }

        private sealed class LogMoneyness : MoneynessProvider
        {
            private readonly double spot;
            public LogMoneyness(double spot)
            {
                this.spot = spot;
            }
            public override double Moneyness(double maturity, double strike)
            {
                return Math.Log(strike / spot);
            }
            public override Func<double, double> Moneyness(double maturity)
            {
                return k => Math.Log(k / spot);
            }
            public override double Strike(double maturity, double moneyness)
            {
                throw new NotImplementedException();
            }
        }
        #endregion
    }
}