using System;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;

namespace PragmaticQuant.Markets.Equity
{
    using SmileDatas = RawMapDatas<double, double>;

    public class VolatilityMatrix
    {
        public VolatilityMatrix(ITimeMeasure time, DateTime[] maturities, SmileDatas[] smiles)
        {
            if (maturities.Length!=smiles.Length)
                throw new Exception("VolatilityMatrix : incompatible size !");

            Time = time;
            Maturities = maturities;
            Smiles = smiles;
        }

        public ITimeMeasure Time { get; private set; }
        public DateTime[] Maturities { get; private set; }
        public SmileDatas[] Smiles { get; private set; }
    }
}