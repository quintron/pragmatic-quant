using System;
using System.Linq;
using PragmaticQuant.Core;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Maths.Integration;
using PragmaticQuant.Maths.Interpolation;
using PragmaticQuant.Model;
using PragmaticQuant.Model.Equity.LocalVolatility;

namespace PragmaticQuant.Markets.Equity
{
    public class VolatilitySurface
    {
        #region private fields
        private readonly MoneynessProvider moneyness;
        private readonly VarianceInterpoler varianceInterpoler;
        #endregion
        #region private methods
        private VolatilitySurface(ITimeMeasure time, MoneynessProvider moneyness, VarianceInterpoler varianceInterpoler)
        {
            Time = time;
            this.varianceInterpoler = varianceInterpoler;
            this.moneyness = moneyness;
        }
        #endregion
        public static VolatilitySurface BuildInterpol(VolatilityMatrix volMatrix, MoneynessProvider moneyness)
        {
            var varianceInterpol = VarianceInterpoler.BuildInterpol(volMatrix, moneyness);
            return new VolatilitySurface(volMatrix.Time, moneyness, varianceInterpol);
        }

        public double Variance(double maturity, double strike)
        {
            var m = moneyness.Moneyness(maturity, strike);
            return varianceInterpoler.Eval(maturity, m);
        }
        public RrFunction VarianceFromMoneyness(double maturity)
        {
            return varianceInterpoler.TimeSlice(maturity);
        }
        public RrFunction VarianceFromMoneyness(DateTime maturity)
        {
            return VarianceFromMoneyness(Time[maturity]);
        }

        public double Volatility(double maturity, double strike)
        {
            return Math.Sqrt(Variance(maturity, strike) / maturity);
        }
        public double Volatility(DateTime maturity, double strike)
        {
            return Volatility(Time[maturity], strike);
        }
        
        public ITimeMeasure Time { get; private set; }
    }

    public class LogSwapVolCurve
    {
        #region private fields
        private const double LawQuantile = 1.0e-6;
        private readonly int samplingNbPoints;
        private readonly double[] quadPoints, quadWeights;
        
        private readonly VolatilitySurface volSurface;
        private readonly AssetForwardCurve forwardCurve;
        private readonly BlackLawQuantileProvider lawSupport;
        #endregion
        public LogSwapVolCurve(VolatilitySurface volSurface, AssetForwardCurve forwardCurve, 
                               int samplingNbPoints = 25, int quadratureNbPoints = 9)
        {
            this.volSurface = volSurface;
            this.forwardCurve = forwardCurve;
            this.samplingNbPoints = samplingNbPoints;
            lawSupport = new BlackLawQuantileProvider(volSurface, forwardCurve.Fwd);
            GaussHermite.GetQuadrature(quadratureNbPoints, out  quadPoints, out quadWeights);
        }
        public static LogSwapVolCurve Build(AssetMarket mkt)
        {
            return new LogSwapVolCurve(mkt.VolSurface(), mkt.ForwardCurve());
        }
            
        public double Volatility(DateTime maturity)
        {
            var variance = volSurface.VarianceFromMoneyness(maturity);
            var logSpotSupport = lawSupport.LogSpotSupport(maturity, LawQuantile, 1.0 - LawQuantile);
            var logFwd = Math.Log(forwardCurve.Fwd(maturity));
            
            // m := ln(K/F) 
            // y(m) := - m / sqrt(v(m)) - sqrt(v(m)) / 2
            var mSample = GridUtils.RegularGrid(logSpotSupport.Inf - logFwd, logSpotSupport.Sup - logFwd, samplingNbPoints);
            var mFromYMap = mSample.Select(m =>
            {
                var stdev = Math.Sqrt(variance.Eval(m));
                var y = -m / stdev - 0.5 * stdev;
                return Tuple.Create(y, m);
            }).OrderBy(kv => kv.Item1)
            .ToArray();
            var mFromY = SplineInterpoler.BuildCubicSpline(mFromYMap.Map(kv => kv.Item1), mFromYMap.Map(kv => kv.Item2));

            var logSwapVariance = quadPoints.ZipWith(quadWeights, (y, w) =>
            {
                var m = mFromY.Eval(y);
                return w * variance.Eval(m);
            }).Sum();
            var logSwapVol = Math.Sqrt(logSwapVariance / volSurface.Time[maturity]);
            return logSwapVol;
        }
    }
}