using System;
using System.Collections.Generic;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.Markets
{
    public class HistoricalFixings
    {
        #region private fields
        private readonly IDictionary<IFixing, double> fixingValues;
        #endregion
        public HistoricalFixings(IDictionary<IFixing, double> fixingValues)
        {
            this.fixingValues = fixingValues;
        }

        public double GetValue(IFixing fixing)
        {
            double value;
            if (fixingValues.TryGetValue(fixing, out value))
                return value;

            throw new Exception("HistoricalFixing : missing historical fixing " + fixing);
        }
    }
}