﻿using System;
using System.Collections.Generic;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets.Rate;

namespace PragmaticQuant.Markets
{
    public class Market
    {
        #region private fields
        private readonly IDictionary<FinancingId, DiscountCurve> discountCurves;
        private readonly IDictionary<Currency, RateVolCube> rateVolCubes;
        private readonly IDictionary<AssetId, AssetMarket> assetMkts;
        #endregion
        public Market(DiscountCurve[] discountCurves, 
                      AssetMarket[] assetMkts, 
                      IMarketConventions conventions,
                      HistoricalFixings historicalFixings = null,
                      RateVolCube[] rateVolCubes = null)
            : this(discountCurves.ToDictionary(disc => disc.Financing, disc => disc),
                   rateVolCubes == null ? null : rateVolCubes.ToDictionary(c=> c.Currency, c=>c),
                   assetMkts.ToDictionary(asset => asset.Asset, asset => asset),
                   conventions, historicalFixings)
        {
        }
        private Market(IDictionary<FinancingId, DiscountCurve> discountCurves, 
                       IDictionary<Currency, RateVolCube> rateVolCubes,
                       IDictionary<AssetId, AssetMarket> assetMkts,
                       IMarketConventions conventions,                      
                       HistoricalFixings historicalFixings)
        {
            if (discountCurves.Any(kv => !kv.Key.Equals(kv.Value.Financing)))
                throw new ArgumentException("Market : Bad Discount curve input incomptible financing id !");
            if (assetMkts.Any(kv => !kv.Key.Equals(kv.Value.Asset)))
                throw new ArgumentException("Market : Bad asset market input incomptible asset id !");
            
            this.discountCurves = discountCurves;
            this.rateVolCubes = rateVolCubes;
            this.assetMkts = assetMkts;
            
            RefDate = discountCurves.First().Value.RefDate;
            Conventions = conventions;
            HistoricalFixings = historicalFixings;

            if (discountCurves.Any(c => c.Value.RefDate != RefDate))
                throw new Exception("RateMarket : many curve refDate's !");

            if (assetMkts.Any(m => m.Value.RefDate != RefDate))
                throw new Exception("AssetMarket : many curve refDate's !");
        }

        public DateTime RefDate { get; private set; }
        public IMarketConventions Conventions { get; private set; }
        public HistoricalFixings HistoricalFixings { get; private set; }

        public FinancingId[] DiscountCurveIds
        {
            get { return discountCurves.Keys.ToArray(); }
        }
        public RateMarket RateMarket(Currency currency)
        {
            var riskFreeCurve = OisDiscountCurve(currency);
            var borCurves = discountCurves.Where(kv =>
            {
                var fin = kv.Key;
                return (fin.Currency.Equals(currency) && fin.Id.Contains("Bor"));
            }).ToDictionary(kv =>
            {
                var tenor = kv.Key.Id.Replace("Bor", "");
                return Duration.Parse(tenor);
            }, kv => kv.Value);

            RateVolCube rateVolCube = null;
            if (rateVolCubes != null)
                rateVolCubes.TryGetValue(currency, out rateVolCube);

            return new RateMarket(currency, TimeMeasure.Act365(riskFreeCurve.RefDate),
                                  riskFreeCurve, borCurves, rateVolCube);
        }
        public DiscountCurve OisDiscountCurve(Currency currency)
        {
            return DiscountCurve(FinancingId.Ois(currency));
        }
        public DiscountCurve DiscountCurve(FinancingId financingId)
        {
            DiscountCurve curve;
            if (!discountCurves.TryGetValue(financingId, out curve))
                throw new Exception(string.Format("Missing discount curve : {0}", financingId));
            return curve;
        }
        public Market NewDiscountCurve(DiscountCurve curve)
        {
            var newDiscountCurves = new Dictionary<FinancingId, DiscountCurve>(discountCurves);
            if (newDiscountCurves.ContainsKey(curve.Financing))
                newDiscountCurves.Remove(curve.Financing);
            newDiscountCurves.Add(curve.Financing, curve);

            return new Market(newDiscountCurves, rateVolCubes, assetMkts, Conventions, HistoricalFixings);
        }

        public AssetId[] AssetIds
        {
            get { return assetMkts.Keys.ToArray(); }
        }
        public AssetMarket AssetMarket(AssetId asset)
        {
            AssetMarket assetMkt;
            if (!assetMkts.TryGetValue(asset, out assetMkt))
                throw new Exception(string.Format("Missing market asset : {0}", asset));
            return assetMkt;
        }
        public Market NewAssetMarket(AssetMarket newAssetMarket)
        {
            var newAssetMarkets = new Dictionary<AssetId, AssetMarket>(assetMkts); 
            if (newAssetMarkets.ContainsKey(newAssetMarket.Asset))
                newAssetMarkets.Remove(newAssetMarket.Asset);
            newAssetMarkets.Add(newAssetMarket.Asset, newAssetMarket);

            return new Market(discountCurves, rateVolCubes, newAssetMarkets, Conventions, HistoricalFixings);
        }
    }

    public static class MarketUtils
    {
        public static Currency AssetCurrency(this Market mkt, string assetName)
        {
            return mkt.Conventions.AssetCurrency(assetName);
        }
        public static AssetId AssetIdFromName(this Market mkt, string assetName)
        {
            AssetId id;
            if (!mkt.Conventions.TryGetAssetId(assetName, out id))
                throw new Exception("Missing asset in market : " + assetName);

            return id;
        }
        public static AssetMarket AssetMarketFromName(this Market mkt, string assetName)
        {
            return mkt.AssetMarket(mkt.AssetIdFromName(assetName));
        }
    }
    
    public interface IMarketConventions
    {
        SwapConvention SwapConvention(Currency currency);
        Currency AssetCurrency(string assetName);
        bool TryGetAssetCurrency(string assetName, out Currency currency);
        bool TryGetAssetId(string assetName, out AssetId id);
    }

    public class DefaultMarketConventions : IMarketConventions
    {
        #region private fields
        private readonly IDictionary<AssetId, Currency> assetCurrencies;
        #endregion

        public DefaultMarketConventions(IDictionary<AssetId, Currency> assetCurrencies = null)
        {
            this.assetCurrencies = assetCurrencies ?? new Dictionary<AssetId, Currency>();
        }
        
        public SwapConvention SwapConvention(Currency currency)
        {
            //TODO use a dictionary
            return SwapConventions.For(currency);
        }
        public Currency AssetCurrency(string assetName)
        {
            Currency cur;
            if (!TryGetAssetCurrency(assetName.ToLowerInvariant(), out cur))
                throw new Exception(string.Format("Missing asset for {0}", assetName));
            return cur;
        }
        public bool TryGetAssetCurrency(string assetName, out Currency currency)
        {
            AssetId id;
            if (!TryGetAssetId(assetName, out id))
            {
                currency = null;
                return false;
            }

            currency = assetCurrencies[id];
            return true;
        }
        public bool TryGetAssetId(string assetName, out AssetId assetId)
        {
            var ids = assetCurrencies.Keys.Where(id => id.Name.Equals(assetName, StringComparison.InvariantCultureIgnoreCase)).ToArray();
            if (ids.Any())
            {
                assetId = ids.Single();
                return true;
            }

            assetId = null;
            return false;
        }
    }
}
