using System;
using System.Collections.Generic;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets.Equity;
using PragmaticQuant.Markets.Rate;
using PragmaticQuant.Markets.RawDatas;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.Markets
{
    public static class MarketDatasInterpolation
    {
        #region private methods
        private static RawMapDatas<DateTime, double> ConvertRawMap(RawMapDatas<DateOrDuration, double> map, DateTime refDate)
        {
            return map.MapPillars(dateOrDur => dateOrDur.ToDate(refDate))
                      .SortKeys();
        }
        private static DiscountCurve InterpolateCurve(FinancingId financingId, RawMapDatas<DateOrDuration, double> rateDatas, ITimeMeasure time)
        {
            var rates = rateDatas ?? RawMapDatasUtils.Constant(0.0, (DateOrDuration)(0 * Duration.Day));
            RawMapDatas<DateTime, double> curve = ConvertRawMap(rates, time.RefDate);
            return DiscountCurve.LinearRateInterpol(financingId, curve, time);
        }
        private static DiscountCurve AssetFinancingCurve(FinancingId assetFinancing,
                                                         RawMapDatas<DateOrDuration, double> repoCurve,
                                                         DiscountCurve riskFreeCurve,
                                                         ITimeMeasure time)
        {
            var repoDiscountRates = repoCurve == null ? null : repoCurve.MapValues(repo => -repo);
            var repoDiscountCurve = InterpolateCurve(null, repoDiscountRates, time);
            if (time.RefDate != repoDiscountCurve.RefDate || time.RefDate != riskFreeCurve.RefDate)
                throw new Exception("AssetMarket : incompatible ref date !");
            return DiscountCurve.Product(repoDiscountCurve, riskFreeCurve, assetFinancing);
        }
        private static HistoricalFixings Histo(LabelledMatrix<DateTime, string, double> rawHisto)
        {
            IDictionary<IFixing, double> fixingValues = new Dictionary<IFixing, double>();
            if (rawHisto != null)
            {
                var fixingParser = new FixingParser();
                var fixingProcesses = rawHisto.ColLabels.Map(f => fixingParser.Parse(f));

                for (int row = 0; row < rawHisto.RowLabels.Count(); row++)
                {
                    var date = rawHisto.RowLabels[row];
                    var fixings = fixingProcesses.Map(fp => (IFixing)fp.Observation(date));
                    var values = rawHisto.Values.Row(row);
                    foreach (var kv in fixings.ZipToDictionary(values))
                        fixingValues.Add(kv);
                }
            }
            return new HistoricalFixings(fixingValues);
        }
        private static RateVolCube ToRateVolCube(this RateVolCubeDatas rawVolCube, ITimeMeasure time)
        {
            var rowVolMatrix = rawVolCube.AtmVolMatrix;
            var tenors = rowVolMatrix.ColLabels.Map(Duration.Parse);
            var atmVolMatrix = new LabelledMatrix<DateOrDuration, Duration, double>(rowVolMatrix.RowLabels,
                                                                                    tenors, 
                                                                                    rowVolMatrix.Values);
            return RateVolCube.Create(Currency.Parse(rawVolCube.Currency), atmVolMatrix, time);
        }
        #endregion

        public static DiscountCurve ToDiscountCurve(this DiscountCurveDatas curveDatas, ITimeMeasure time)
        {
            var financingId = FinancingId.Parse(curveDatas.Currency, curveDatas.Financing ?? "ois");
            return InterpolateCurve(financingId, curveDatas.ZcRates, time);
        }
        public static VolatilityMatrix ToVolMatrix(this LabelledMatrix<DateOrDuration, double, double> volDatas, ITimeMeasure time)
        {
            var volMaturities = volDatas.RowLabels.Map(d => d.ToDate(time.RefDate));
            var smilePillars = EnumerableUtils.For(0, volMaturities.Length,
                i => new RawMapDatas<double, double>(volDatas.ColLabels, volDatas.Values.Row(i)));

            var orderedSmileByDates = volMaturities.ZipToDictionary(smilePillars)
                                                   .OrderBy(kv => kv.Key);
            return new VolatilityMatrix(time, orderedSmileByDates.Map(kv => kv.Key), 
                                              orderedSmileByDates.Map(kv => kv.Value));
        }
        public static AssetMarket ToAssetMkt(this AssetMarketDatas assetMktDatas, ITimeMeasure time, DiscountCurve riskFreeCurve)
        {
            var assetId = assetMktDatas.Id();
            var assetFinancing = FinancingId.AssetCollat(Currency.Parse(assetMktDatas.Currency), assetId);
            if (!assetFinancing.Currency.Equals(riskFreeCurve.Financing.Currency))
                throw new Exception("Incompatible asset currency with discount curve");
            
            var repoCurve = InterpolateCurve(null, assetMktDatas.RepoCurve, time);
            if (time.RefDate != repoCurve.RefDate || time.RefDate != riskFreeCurve.RefDate)
                throw new Exception("AssetMarket : incompatible ref date !");
            var assetDiscountCurve = AssetFinancingCurve(assetFinancing, assetMktDatas.RepoCurve, riskFreeCurve, time);
            
            var fwdDivs = assetMktDatas.DividendForward ?? new RawMapDatas<DateTime, double>(new DateTime[0], new double[0]);
            fwdDivs = fwdDivs.SortKeys();
            if (fwdDivs.Pillars.Any(d=> d < time.RefDate))
                throw new Exception("Dividend date previous to market ref date not allowed !");

            LabelledMatrix<DateOrDuration, double, double> rawVolMatrix = assetMktDatas.VolatilityMatrix;
            VolatilityMatrix volMatrix = (rawVolMatrix != null) ? rawVolMatrix.ToVolMatrix(time) : null;

            return new AssetMarket(assetId, time, assetMktDatas.Spot, assetDiscountCurve, fwdDivs, volMatrix);
        }
        public static Market ToMarket(this MarketDatas marketDatas)
        {
            ITimeMeasure mktTime = TimeMeasure.Act365(marketDatas.RefDate);
            var discountCurves = marketDatas.DiscountCurves
                                            .Map(rawCurve => rawCurve.ToDiscountCurve(mktTime));

            var rateVolCubes = marketDatas.RateVolCubes == null ? null
                                : marketDatas.RateVolCubes
                                             .Map(rawRateVolCube => rawRateVolCube.ToRateVolCube(mktTime));
            var assetMkts = marketDatas.Assets
                                        .Map(assetMktDatas =>
                                        {
                                            var oisId = FinancingId.Ois(Currency.Parse(assetMktDatas.Currency));
                                            var riskFreeCurve = discountCurves.FirstOrDefault(c => oisId.Equals(c.Financing));                                                                              
                                            if (riskFreeCurve == null)
                                                throw new Exception("Missing rate curve for " + assetMktDatas.Currency);                                        
                                            return assetMktDatas.ToAssetMkt(mktTime, riskFreeCurve);
                                        });
            IMarketConventions conventions = marketDatas.MktConventions();
            HistoricalFixings historicalFixings = Histo(marketDatas.HistoricalFixings);
            return new Market(discountCurves, assetMkts, conventions, historicalFixings, rateVolCubes);
        }
    }
}
