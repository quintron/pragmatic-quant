using System;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.Markets.Rate
{
    public class BorForwardCurve
    {
        #region private fields
        private readonly SwapScheduleBuilder scheduleBuilder;
        private readonly DayCountFrac dcf;
        private readonly DiscountCurve borDiscountCurve;
        #endregion
        public BorForwardCurve(Duration tenor, DiscountCurve borDiscountCurve, SwapScheduleBuilder scheduleBuilder, DayCountFrac dcf)
        {
            this.borDiscountCurve = borDiscountCurve;
            this.scheduleBuilder = scheduleBuilder;
            this.dcf = dcf;
            Tenor = tenor;
            Currency = borDiscountCurve.Financing.Currency;
        }
        public static BorForwardCurve Build(Duration tenor, Currency currency, DiscountCurve borDiscountCurve)
        {
            var conventions = SwapConventions.For(currency).FloatLegConvention;
            return new BorForwardCurve(tenor, borDiscountCurve, conventions.ScheduleBuilder, conventions.Dcf);
        }

        public DateTime RefDate
        {
            get { return borDiscountCurve.RefDate; }
        }
        public Currency Currency { get; private set; }
        public readonly Duration Tenor;
        public double Fwd(DateTime fixingDate)
        {
            var schedule = RateFixingUtils.LiborSchedule(fixingDate, Tenor, scheduleBuilder);
            var theta = dcf.Count(schedule.Start, schedule.End);
            return (borDiscountCurve.Zc(schedule.Start) / borDiscountCurve.Zc(schedule.End) - 1.0) / theta;
        }
    }

    public static class RateFixingUtils
    {
        public static CouponSchedule[] FloatSchedule(DateTime fixingDate, Duration tenor, Duration perio, SwapScheduleBuilder scheduleBuilder)
        {
            var start = scheduleBuilder.StartDate(fixingDate);
            return scheduleBuilder.GetSchedule(start, start + tenor, perio);
        }
        public static CouponSchedule LiborSchedule(DateTime fixingDate, Duration tenor, SwapScheduleBuilder scheduleBuilder)
        {
            return FloatSchedule(fixingDate, tenor, tenor, scheduleBuilder).Single();
        }
        public static CouponSchedule Schedule(this Libor libor, SwapScheduleBuilder scheduleBuilder)
        {
            return LiborSchedule(libor.Date, libor.Tenor, scheduleBuilder);
        }
        public static CouponSchedule Schedule(this Libor libor)
        {
            return Schedule(libor, SwapScheduleConventions.For(libor.Currency));
        }
    }

}