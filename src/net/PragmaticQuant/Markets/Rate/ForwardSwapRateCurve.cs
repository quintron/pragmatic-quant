using System;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;

namespace PragmaticQuant.Markets.Rate
{
    public class ForwardSwapRateCurve
    {
        #region private fields
        private readonly SwapDataBuilder swapDatas;
        private readonly SwapScheduleBuilder scheduleBuilder;
        #endregion
        private ForwardSwapRateCurve(SwapDataBuilder swapDatas, SwapScheduleBuilder scheduleBuilder)
        {
            this.swapDatas = swapDatas;
            this.scheduleBuilder = scheduleBuilder;
            Currency = swapDatas.Currency;
        }
        public static ForwardSwapRateCurve Build(RateMarket rateMarket)
        {
            var convention = SwapConventions.For(rateMarket.Currency);
            return new ForwardSwapRateCurve(SwapDataBuilder.Build(convention, rateMarket), convention.FloatLegConvention.ScheduleBuilder);
        }

        public Currency Currency { get; private set; }
        public SwapRateDatas SwapDatas(DateTime fixingDate, DateOrDuration underlying)
        {
            var start = scheduleBuilder.StartDate(fixingDate);
            return swapDatas.SwapRate(start, underlying.ToDate(start));
        }
        public double Fwd(DateTime fixingDate, DateOrDuration underlying)
        {
            return SwapDatas(fixingDate, underlying).SwapRate();
        }
        public double Bpv(DateTime fixingDate, DateOrDuration underlying)
        {
            return SwapDatas(fixingDate, underlying).Bpv.Mtm();
        }
    }

    public class SwapRateDatas
    {
        public readonly FloatLegDatas FloatLeg;
        public readonly BpvDatas Bpv;
        public SwapRateDatas(FloatLegDatas floatLeg, BpvDatas bpv)
        {
            FloatLeg = floatLeg;
            Bpv = bpv;
        }
    }

    public static class SwapDatasUtils
    {
        public static double Mtm(this BpvDatas bpv)
        {
            return bpv.Leg.DayFracs.ZipWith(bpv.Discounts, (theta, zc) => theta * zc).Sum();
        }
        public static double Mtm(this FloatLegDatas floatLeg)
        {
            var thetas = floatLeg.Bpv.Leg.DayFracs;
            var zcs = floatLeg.Bpv.Discounts;
            var fwds = floatLeg.ForwardLibors;
            return thetas.Select((t, i) => t * fwds[i] * zcs[i]).Sum();
        }
        public static double SwapRate(this SwapRateDatas swapRate)
        {
            return swapRate.FloatLeg.Mtm() / swapRate.Bpv.Mtm();
        }
    }

    public class LegDatas
    {
        public readonly CouponSchedule[] Schedule;
        public readonly double[] DayFracs;
        public LegDatas(CouponSchedule[] schedule, double[] dayFracs)
        {
            if (schedule.Length!=dayFracs.Length)
                throw new Exception("LegDatas : incompatible size !");

            Schedule = schedule;
            DayFracs = dayFracs;
        }
    }

    public class BpvDatas
    {
        public BpvDatas(CouponSchedule[] schedule, double[] discounts, double[] dayFracs)
            : this(new LegDatas(schedule, dayFracs), discounts)
        {
        }
        public BpvDatas(LegDatas leg, double[] discounts)
        {
            if (leg.Schedule.Length != discounts.Length)
                throw new Exception("BpvDatas : incompatible datas !");
            Leg = leg;
            Discounts = discounts;
        }

        public readonly double[] Discounts;
        public readonly LegDatas Leg;
    }

    public class FloatLegDatas
    {
        public readonly BpvDatas Bpv;
        public readonly double[] ForwardLibors;
        public FloatLegDatas(BpvDatas bpv, double[] forwardLibors)
        {
            if (bpv.Leg.Schedule.Length != forwardLibors.Length)
                throw new Exception("FloatLegDatas : incompatible datas !");

            Bpv = bpv;
            ForwardLibors = forwardLibors;
        }
    }

    public class SwapDataBuilder
    {
        #region private fields
        private readonly SwapConvention defaultconvention;
        private readonly DiscountCurve discountCurve;
        private readonly BorForwardCurve borForwardCurve;
        #endregion
        public SwapDataBuilder(SwapConvention defaultconvention, DiscountCurve discountCurve, BorForwardCurve borForwardCurve)
        {
            if (!defaultconvention.FloatLegConvention.Period.Equals(borForwardCurve.Tenor))
                throw new Exception("SwapDataBuilder : incompatible forward curve");

            if (!discountCurve.Financing.Currency.Equals(borForwardCurve.Currency))
                throw new Exception("SwapDataBuilder : incompatible currencies");

            this.defaultconvention = defaultconvention;
            this.discountCurve = discountCurve;
            this.borForwardCurve = borForwardCurve;
            Currency = borForwardCurve.Currency;
        }

        public static SwapDataBuilder Build(SwapConvention convention, RateMarket rateMarket)
        {
            return new SwapDataBuilder(convention, rateMarket.OisDiscount, rateMarket.BorForwardCurve(convention.FloatLegConvention.Period));
        }
        public static SwapDataBuilder Build(RateMarket rateMarket)
        {
            return Build(SwapConventions.For(rateMarket.Currency), rateMarket);
        }

        public Currency Currency { get; private set; }
        
        public BpvDatas Bpv(DateTime start, DateTime end, SwapLegConvention legConvention)
        {
            var leg = legConvention.BuildLeg(start, end);
            var discounts = leg.Schedule.Map(cs => discountCurve.Zc(cs.Pay));
            return new BpvDatas(leg, discounts);
        }
        public BpvDatas Bpv(DateTime start, DateTime end)
        {
            return Bpv(start, end, defaultconvention.FixedLegConvention);
        }
        public BpvDatas Bpv(DateTime start, Duration tenor)
        {
            return Bpv(start, start + tenor);
        }
        
        public FloatLegDatas FloatLeg(DateTime start, DateTime end, SwapLegConvention legConvention)
        {
            var bpv = Bpv(start, end, legConvention);
            var fwds = bpv.Leg.Schedule.Map(cs => borForwardCurve.Fwd(cs.Fixing));
            return new FloatLegDatas(bpv, fwds);
        }
        public FloatLegDatas FloatLeg(DateTime start, DateTime end)
        {
            return FloatLeg(start, end, defaultconvention.FloatLegConvention);
        }
        public FloatLegDatas FloatLeg(DateTime start, Duration tenor)
        {
            return FloatLeg(start, start + tenor);
        }

        public SwapRateDatas SwapRate(DateTime start, DateTime end)
        {
            return new SwapRateDatas(FloatLeg(start, end), Bpv(start, end));
        }
        public SwapRateDatas SwapRate(DateTime start, Duration tenor)
        {
            return new SwapRateDatas(FloatLeg(start, tenor), Bpv(start, tenor));
        }
    }
    
}