﻿using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Maths.Function;

namespace PragmaticQuant.Markets.Rate
{
    public class RateVolCube
    {
        #region private fields
        private readonly StepSearcher maturityStepSearcher;
        private readonly DateTime[] maturityDatePillars;
        private readonly double[] maturityPillars;
        private readonly RrFunction[] pillarVols;
        #endregion

        #region private methods
        private double AtmVol(int maturityPillar, DateTime rateEnd)
        {
            var end = Time[rateEnd];
            return pillarVols[maturityPillar].Eval(end); 
        }
        private double AtmVariance(int maturityPillar, DateTime rateEnd)
        {
            var vol = AtmVol(maturityPillar, rateEnd);
            return vol * vol * maturityPillars[maturityPillar];
        }
        private double AtmVol(double t, DateTime rateEnd)
        {
            var maturityPillar = maturityStepSearcher.LocateLeftIndex(t);

            if (maturityPillar < 0)
            {
                return Math.Sqrt(AtmVariance(0, rateEnd) / maturityPillars[0]);                
            }
            if (maturityPillar == pillarVols.Length - 1)
            {
                return Math.Sqrt(AtmVariance(maturityPillars.Length - 1, rateEnd) / maturityPillars[maturityPillars.Length - 1]);
            }

            var prevVariance = AtmVariance(maturityPillar, rateEnd);
            var nextVariance = AtmVariance(maturityPillar + 1, rateEnd);
            double w = (t - maturityPillars[maturityPillar]) / (maturityPillars[maturityPillar + 1] - maturityPillars[maturityPillar]);
            return Math.Sqrt(((1.0 - w) * prevVariance + w * nextVariance) / t);
        }
        #endregion
        public RateVolCube(Currency currency, DateTime[] maturityDatePillars, Duration[] tenorPillars, double[,] atmVols, ITimeMeasure time)
        {
            if (maturityDatePillars.Length != atmVols.GetLength(0))
                throw new Exception("RateVolCube : maturityPillars and atmVols are incompatible");
            if (tenorPillars.Length != atmVols.GetLength(1))
                throw new Exception("RateVolCube : tenorPillars and atmVols are incompatible");

            this.maturityDatePillars = maturityDatePillars;
            Currency = currency;
            Time = time;
            maturityPillars = Time[maturityDatePillars];
            maturityStepSearcher = new StepSearcher(maturityPillars);
            pillarVols = EnumerableUtils.For(0, maturityDatePillars.Length, i =>
            {
                var pillarMat = maturityDatePillars[i];
                var pillarEnds = Time[tenorPillars.Map(tenor => pillarMat + tenor)];
                return RrFunctions.LinearInterpolation(pillarEnds, atmVols.Row(i));
             });
        }
        public static RateVolCube Create(Currency currency, LabelledMatrix<DateOrDuration, Duration, double> atmVolMatrix, ITimeMeasure time)
        {
            return new RateVolCube(currency, 
                                   atmVolMatrix.RowLabels.Map(d => d.ToDate(time.RefDate)),
                                   atmVolMatrix.ColLabels,
                                   atmVolMatrix.Values,
                                   time);
        }

        public double Volatility(DateTime maturity, DateOrDuration rateEnd, double strike)
        {            
            return AtmVol(Time[maturity], rateEnd.ToDate(maturity));
        }
        public double Volatility(DateTime maturity, Duration tenorOrEnd, double strike)
        {
            return Volatility(maturity, (DateOrDuration)tenorOrEnd, strike);
        }
        
        public Currency Currency { get; private set; }
        public ITimeMeasure Time { get; private set; }
    }

}
