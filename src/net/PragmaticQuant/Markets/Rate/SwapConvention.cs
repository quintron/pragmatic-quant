using System;
using System.Collections.Generic;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;

namespace PragmaticQuant.Markets.Rate
{
    public class SwapConvention
    {
        public SwapConvention(SwapScheduleBuilder scheduleBuilder, 
                              DayCountFrac floatDcf, Duration floatPeriod, 
                              DayCountFrac fixedDcf, Duration fixedPeriod)
        {
            FloatLegConvention = new SwapLegConvention(scheduleBuilder, floatDcf , floatPeriod);
            FixedLegConvention = new SwapLegConvention(scheduleBuilder, fixedDcf, fixedPeriod);
        }

        public SwapLegConvention FloatLegConvention { get; private set; }
        public SwapLegConvention FixedLegConvention { get; private set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((SwapConvention) obj);
        }
        private bool Equals(SwapConvention other)
        {
            return Equals(FloatLegConvention, other.FloatLegConvention) && Equals(FixedLegConvention, other.FixedLegConvention);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                return ((FloatLegConvention != null ? FloatLegConvention.GetHashCode() : 0) * 397) ^ (FixedLegConvention != null ? FixedLegConvention.GetHashCode() : 0);
            }
        }
    }

    public class SwapLegConvention
    {
        #region protected methods
        private bool Equals(SwapLegConvention other)
        {
            return Equals(ScheduleBuilder, other.ScheduleBuilder) && Equals(Dcf, other.Dcf) && Equals(Period, other.Period);
        }
        #endregion
        public SwapLegConvention(SwapScheduleBuilder scheduleBuilder, DayCountFrac dcf, Duration period)
        {
            Dcf = dcf;
            Period = period;
            ScheduleBuilder = scheduleBuilder;
        }
        
        public SwapScheduleBuilder ScheduleBuilder { get; private set; }
        public DayCountFrac Dcf { get; private set; }
        public Duration Period { get; private set; }

        public LegDatas BuildLeg(DateTime start, DateTime end)
        {
            CouponSchedule[] schedule = ScheduleBuilder.GetSchedule(start, end, Period);
            double[] dayFracs = schedule.Map(cs => cs.Theta(Dcf));
            return new LegDatas(schedule, dayFracs);
        }
        
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((SwapLegConvention) obj);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (ScheduleBuilder != null ? ScheduleBuilder.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Dcf != null ? Dcf.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Period != null ? Period.GetHashCode() : 0);
                return hashCode;
            }
        }
    }

    public static class SwapConventions
    {
        private static readonly SwapConvention Eur = new SwapConvention(SwapScheduleConventions.Eur, DayCountFrac.Act360, 6 * Duration.Month, DayCountFrac.Thirty360, Duration.Year);
        private static readonly SwapConvention Usd = new SwapConvention(SwapScheduleConventions.Usd, DayCountFrac.Act360, 3 * Duration.Month, DayCountFrac.Thirty360, 6 * Duration.Month);
        private static readonly SwapConvention Jpy = new SwapConvention(SwapScheduleConventions.Jpy, DayCountFrac.Act360, 6 * Duration.Month, DayCountFrac.Act365Fixed, 6 * Duration.Month);
        private static readonly SwapConvention Gbp = new SwapConvention(SwapScheduleConventions.Gbp, DayCountFrac.Act365Fixed, 6 * Duration.Month, DayCountFrac.Act365Fixed, 6 * Duration.Month);
        private static readonly SwapConvention Chf = new SwapConvention(SwapScheduleConventions.Chf, DayCountFrac.Act360, 6 * Duration.Month, DayCountFrac.Thirty360, Duration.Year);
        private static readonly SwapConvention Hkd = new SwapConvention(SwapScheduleConventions.Hkd, DayCountFrac.Act365Fixed, 3 * Duration.Month, DayCountFrac.Act365Fixed, 3 * Duration.Month);
        private static readonly SwapConvention Krw = new SwapConvention(SwapScheduleConventions.Krw, DayCountFrac.Act365Fixed, 6 * Duration.Month, DayCountFrac.Act365Fixed, 6 * Duration.Month);

        private static readonly IDictionary<string, SwapConvention> allConventions = new Dictionary<string, SwapConvention>
        {
            {"EUR", Eur},
            {"USD", Usd},
            {"JPY", Jpy},
            {"GBP", Gbp},
            {"CHF", Chf},
            {"HKD", Hkd},
            {"KRW", Krw}
        }; 

        public static SwapConvention For(Currency currency)
        {
            SwapConvention convention;
            if(!allConventions.TryGetValue(currency.Name.ToUpperInvariant(), out convention))
                throw new Exception(string.Format("Swap convention not handled for : {0} ", currency.Name));

            return convention;
        }
    }
}