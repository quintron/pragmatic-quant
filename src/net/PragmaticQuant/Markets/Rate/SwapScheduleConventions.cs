using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;

namespace PragmaticQuant.Markets.Rate
{
    public static class SwapScheduleConventions
    {
        public static readonly SwapScheduleBuilder Eur = new SwapScheduleBuilder(SimpleCalendar.Instance, 2, BusinessConventions.EndOfMonth);
        public static readonly SwapScheduleBuilder Usd = new SwapScheduleBuilder(SimpleCalendar.Instance, 2, BusinessConventions.EndOfMonth);
        public static readonly SwapScheduleBuilder Jpy = new SwapScheduleBuilder(SimpleCalendar.Instance, 2, BusinessConventions.EndOfMonth);
        public static readonly SwapScheduleBuilder Gbp = new SwapScheduleBuilder(SimpleCalendar.Instance, 0, BusinessConventions.EndOfMonth);
        public static readonly SwapScheduleBuilder Chf = new SwapScheduleBuilder(SimpleCalendar.Instance, 2, BusinessConventions.EndOfMonth);
        public static readonly SwapScheduleBuilder Hkd = new SwapScheduleBuilder(SimpleCalendar.Instance, 0, BusinessConventions.EndOfMonth);
        public static readonly SwapScheduleBuilder Krw = new SwapScheduleBuilder(SimpleCalendar.Instance, 2, BusinessConventions.EndOfMonth);
        
        public static SwapScheduleBuilder For(Currency currency)
        {
            switch (currency.Name.ToUpperInvariant())
            {
                case "EUR":
                    return Eur;
                case "USD":
                    return Usd;
                case "JPY":
                    return Jpy;
                case "GBP":
                    return Gbp;
                case "CHF":
                    return Chf;
                case "HKD":
                    return Hkd;
                case "KRW":
                    return Krw;
                default:
                    throw new Exception(string.Format("Swap convention not handled for : {0} ", currency.Name));
            }
        }
    }
}