using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Maths;

namespace PragmaticQuant.Markets.Rate
{
    public class SwaptionPricer
    {
        #region private fields
        private readonly ForwardSwapRateCurve swapRateCurve;
        private readonly ITimeMeasure time;
        #endregion
        internal SwaptionPricer(ForwardSwapRateCurve swapRateCurve, ITimeMeasure time)
        {
            this.swapRateCurve = swapRateCurve;
            this.time = time;
            Currency = swapRateCurve.Currency;
        }
        public static SwaptionPricer Build(RateMarket rateMarket)
        {
            return new SwaptionPricer(ForwardSwapRateCurve.Build(rateMarket), rateMarket.Time);
        }

        public Currency Currency { get; private set; }
        public double Price(DateTime maturity, DateOrDuration underlying, double strike, OptionType optionType, double vol)
        {
            var swap = swapRateCurve.SwapDatas(maturity, underlying);
            return swap.Bpv.Mtm() * BachelierOption.Price(swap.SwapRate(), strike, vol, time[maturity], optionType.Leverage());
        }
        public double ImpliedVol(DateTime maturity, DateOrDuration underlying, double strike, OptionType optionType, double price)
        {
            var swap = swapRateCurve.SwapDatas(maturity, underlying);
            var bpv = swap.Bpv.Mtm();
            return BachelierOption.ImpliedVol(price / bpv, swap.SwapRate(), strike, time[maturity], optionType.Leverage());
        }
    }
}