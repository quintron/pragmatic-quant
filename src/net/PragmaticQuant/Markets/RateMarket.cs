﻿using System;
using System.Collections.Generic;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets.Rate;

namespace PragmaticQuant.Markets
{
    public class RateMarket
    {
        #region private fields
        private readonly IDictionary<Duration, DiscountCurve> borCurves;
        private readonly RateVolCube volCube;
        #endregion
        public RateMarket(Currency currency, ITimeMeasure time, 
                          DiscountCurve oisCurve, 
                          IDictionary<Duration, DiscountCurve> borCurves,
                          RateVolCube volCube = null)
        {
            this.borCurves = borCurves ?? new Dictionary<Duration, DiscountCurve>();
            this.volCube = volCube;
            OisDiscount = oisCurve;
            Currency = currency;
            RefDate = oisCurve.RefDate;
            Time = time;            
        }

        public DateTime RefDate { get; private set; }
        public ITimeMeasure Time { get; private set; }
        public Currency Currency { get; private set; }
        public DiscountCurve OisDiscount { get; private set; }
        public BorForwardCurve BorForwardCurve(Duration tenor)
        {
            DiscountCurve borDiscountCurve;
            if (borCurves.TryGetValue(tenor, out borDiscountCurve))
                return Rate.BorForwardCurve.Build(tenor, Currency, borDiscountCurve);

            throw new Exception(string.Format("Missing bor curve : {0}{1}", Currency, tenor));
        }
        public ForwardSwapRateCurve SwapRateCurve()
        {
            return ForwardSwapRateCurve.Build(this);
        }
        public RateVolCube VolCube()
        {
            return volCube;
        }
    }

}
