﻿using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;

namespace PragmaticQuant.Markets.RawDatas
{
    public class AssetMarketDatas : ICloneable<AssetMarketDatas>
    {
        public string Name { get; set; }
        public string Currency { get; set; }
        public double Spot { get; set; }
        public RawMapDatas<DateOrDuration, double> RepoCurve { get; set; }
        public RawMapDatas<DateTime, double> DividendForward { get; set; }
        public LabelledMatrix<DateOrDuration, double, double> VolatilityMatrix { get; set; }
        
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((AssetMarketDatas) obj);
        }
        private bool Equals(AssetMarketDatas other)
        {
            return string.Equals(Name, other.Name) 
                && string.Equals(Currency, other.Currency) 
                && Spot.Equals(other.Spot) 
                && Equals(RepoCurve, other.RepoCurve) 
                && Equals(DividendForward, other.DividendForward)
                && Equals(VolatilityMatrix, other.VolatilityMatrix);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Currency != null ? Currency.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Spot.GetHashCode();
                hashCode = (hashCode * 397) ^ (RepoCurve != null ? RepoCurve.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (DividendForward != null ? DividendForward.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (VolatilityMatrix != null ? VolatilityMatrix.GetHashCode() : 0);
                return hashCode;
            }
        }

        public AssetMarketDatas Clone()
        {
            return new AssetMarketDatas
            {
                Name = (string)Name.Clone(),
                Currency = (string)Currency.Clone(),
                Spot = Spot,
                RepoCurve = (RepoCurve==null) ? null : RepoCurve.Clone(),
                DividendForward = (DividendForward==null) ? null : DividendForward.Clone(),
                VolatilityMatrix = (VolatilityMatrix==null) ? null : VolatilityMatrix.Clone()
            };
        }
    }

    public static class AssetMarketDatasUtils
    {
        public static AssetId Id(this AssetMarketDatas assetMktDatas)
        {
            return new AssetId(assetMktDatas.Name);
        }
    }
}
