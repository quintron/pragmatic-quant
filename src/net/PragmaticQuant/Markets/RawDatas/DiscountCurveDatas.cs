using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;

namespace PragmaticQuant.Markets.RawDatas
{
    public class DiscountCurveDatas : ICloneable<DiscountCurveDatas>
    {
        #region protected methods
        protected bool Equals(DiscountCurveDatas other)
        {
            return string.Equals(Currency, other.Currency) 
                   && string.Equals(Financing, other.Financing) 
                   && Equals(ZcRates, other.ZcRates);
        }
        #endregion

        public static DiscountCurveDatas Create(string currency, string financing, 
                                                DateOrDuration[] maturities, double[] rates)
        {
            if (maturities.Length!=rates.Length)
                throw new Exception("DiscountCurveDatas : incompatible size maturities vs rates");

            return new DiscountCurveDatas
            {
                Currency = currency,
                Financing = financing,
                ZcRates = new RawMapDatas<DateOrDuration, double>(maturities, rates)
            };
        }

        public string Currency { get; set; }
        public string Financing { get; set; }
        public RawMapDatas<DateOrDuration, double> ZcRates { get; set; }

        public DiscountCurveDatas Clone()
        {
            return new DiscountCurveDatas
            {
                Currency = (string) Currency.Clone(),
                Financing = (string) Financing.Clone(),
                ZcRates = ZcRates.Clone()
            };
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((DiscountCurveDatas) obj);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Currency != null ? Currency.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Financing != null ? Financing.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ZcRates != null ? ZcRates.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}