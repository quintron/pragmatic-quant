using System;
using System.Linq;
using System.Collections.Generic;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;

namespace PragmaticQuant.Markets.RawDatas
{
    public class MarketDatas : ICloneable<MarketDatas>
    {
        public DateTime RefDate { get; set; }
        public IList<AssetMarketDatas> Assets { get; set; }
        public IList<DiscountCurveDatas> DiscountCurves { get; set; }
        public IList<RateVolCubeDatas> RateVolCubes { get; set; }
        public LabelledMatrix<DateTime, string, double> HistoricalFixings { get; set; }

        public void AddDiscountCurve(DiscountCurveDatas curve)
        {
            if (DiscountCurves==null)
                DiscountCurves = new List<DiscountCurveDatas>();
            DiscountCurves.Add(curve);
        }
        public void AddAsset(AssetMarketDatas assetMarket)
        {
            if (Assets==null)
                Assets = new List<AssetMarketDatas>();
            Assets.Add(assetMarket);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((MarketDatas) obj);
        }
        protected bool Equals(MarketDatas other)
        {
            return RefDate.Equals(other.RefDate)
                   && Assets.Equal(other.Assets)
                   && DiscountCurves.Equal(other.DiscountCurves)
                   && RateVolCubes.Equal(other.RateVolCubes);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = RefDate.GetHashCode();
                hashCode = (hashCode * 397) ^ (Assets != null ? Assets.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (DiscountCurves != null ? DiscountCurves.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (RateVolCubes != null ? RateVolCubes.GetHashCode() : 0);
                return hashCode;
            }
        }

        public MarketDatas Clone()
        {
            return new MarketDatas
            {
                RefDate = RefDate,
                Assets = Assets.Select(assetMkt => assetMkt.Clone()).ToList(),
                DiscountCurves = DiscountCurves.Select(curve => curve.Clone()).ToList(),
                RateVolCubes = RateVolCubes == null ? null : RateVolCubes.Select(cube => cube.Clone()).ToList()
            };
        }
    }

    public static class MarketDatasUtils
    {
        public static AssetMarketDatas GetEquityMarket(this MarketDatas marketDatas, string asset)
        {
            var assetMkts = marketDatas.Assets.Where(mkt => mkt.Name.Equals(asset, StringComparison.InvariantCultureIgnoreCase)).ToArray();
            if (!assetMkts.Any())
                throw new Exception("Missing equity market for " + asset);
            if (assetMkts.Count() > 1)
                throw new Exception("Redundant equity market for" + asset);
            return assetMkts.Single();
        }
        public static RateVolCubeDatas GetRateVolCube(this MarketDatas marketDatas, string currency)
        {
            var rateVolCubes = marketDatas.RateVolCubes
                                          .Where(cube => cube.Currency.Equals(currency, StringComparison.InvariantCultureIgnoreCase))
                                          .ToArray();
            if (!rateVolCubes.Any())
                throw new Exception("Missing rate vol cube for " + currency);
            if (rateVolCubes.Count() > 1)
                throw new Exception("Redundant rate vol cube for" + currency);
            return rateVolCubes.Single();
        }
        public static IMarketConventions MktConventions(this MarketDatas marketDatas)
        {
            var assetCurrencies = marketDatas.Assets.ToDictionary(assetMkt => assetMkt.Id(), assetMkt => Currency.Parse(assetMkt.Currency));
            return new DefaultMarketConventions(assetCurrencies);
        }
        public static string ToJson(this MarketDatas marketDatas)
        {
            return MarketJsonParser.ToJson(marketDatas);
        }

        public static AssetMarketDatas FlatAssetMarket(string name, string currency, double spot, double vol)
        {
            var assetMkt = new AssetMarketDatas
            {
                Name = name,
                Currency = currency,
                Spot = spot,
                VolatilityMatrix = new LabelledMatrix<DateOrDuration, double, double>(
                                            new[] {(DateOrDuration) (50 * Duration.Year)},
                                            new[] {spot},
                                            new[,] {{vol}})
            };
            return assetMkt;
        }
        public static DiscountCurveDatas FlatCurve(string currency, string financing, double rate)
        {
            return DiscountCurveDatas.Create(currency, financing, new[] {(DateOrDuration) (50 * Duration.Year)}, new [] {rate});
        }
    }

}