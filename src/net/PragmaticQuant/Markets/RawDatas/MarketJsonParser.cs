﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;

namespace PragmaticQuant.Markets.RawDatas
{
    public static class MarketJsonParser
    {
        #region private fields
        private static readonly JsonSerializer serializer = new JsonSerializer
        {
            Converters =
            {
                new JsonRawFunctionConverter<string, DateOrDuration>(DateAndDurationConverter.ConvertDateOrDuration),
                new JsonRawFunctionConverter<DateTime, DateTime>(o => o),
                new JsonVolMatrixConverter(),
                new JsonAtmRateVolMatrixConverter()
            }
        };
        #endregion 
        #region private methods
        private static JObject AsJObject(object obj)
        {
            return (obj as JObject) ?? JObject.Parse(obj.ToString());
        }
        #endregion

        public static MarketDatas ParseMarket(object jsonMkt)
        {
            JObject mktObj = AsJObject(jsonMkt);
            return mktObj.ToObject<MarketDatas>(serializer);
        }
        public static AssetMarketDatas ParseAssetMarket(object jsonAssetMkt)
        {
            JObject mktObj = AsJObject(jsonAssetMkt);
            return mktObj.ToObject<AssetMarketDatas>(serializer);
        }
        public static DiscountCurveDatas ParseDiscountCurve(object jsonCurve)
        {
            JObject mktObj = AsJObject(jsonCurve);
            return mktObj.ToObject<DiscountCurveDatas>(serializer);
        }

        public static string ToJson(object datas)
        {
            return JsonConvert.SerializeObject(datas,
                Formatting.Indented,
                new JsonRawFunctionConverter<string, DateOrDuration>(DateAndDurationConverter.ConvertDateOrDuration),
                new JsonRawFunctionConverter<DateTime, DateTime>(o => o),
                new JsonVolMatrixConverter(),
                new JsonAtmRateVolMatrixConverter(),
                new JsonDateOrDurationConverter());
        }
    }

    public class JsonRawFunctionConverter<TO,T> : JsonConverter
    {
        #region private fields
        private readonly Func<TO, T> pillarConverter;
        #endregion
        public JsonRawFunctionConverter(Func<TO, T> pillarConverter)
        {
            this.pillarConverter = pillarConverter;
        }
        public override bool CanConvert(Type objectType)
        {
            return typeof(RawMapDatas<T, double>).IsAssignableFrom(objectType);
        }
        public override object ReadJson(JsonReader reader,
                                        Type objectType,
                                        object existingValue,
                                        JsonSerializer serializer)
        {
            JObject jObject = JObject.Load(reader);
            var dico = jObject.ToObject<Dictionary<TO, double>>();

            var pillars = dico.Keys.Map(pillarConverter);
            return new RawMapDatas<T, double>(pillars, dico.Values.ToArray());
        }

        public override void WriteJson(JsonWriter writer,
                                       object value,
                                       JsonSerializer serializer)
        {
            var rawMapDatas = (RawMapDatas<T, double>) value;
            var dico = rawMapDatas.Pillars.ZipToDictionary(rawMapDatas.Values);
            serializer.Serialize(writer, dico);
        }
    }

    public class JsonVolMatrixConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(LabelledMatrix<DateOrDuration, double, double>).IsAssignableFrom(objectType);
        }
        public override object ReadJson(JsonReader reader,
                                        Type objectType,
                                        object existingValue,
                                        JsonSerializer serializer)
        {
            JObject jObject = JObject.Load(reader);
            var strikes = jObject["Strikes"].ToObject<double[]>();
            var maturities = jObject["Maturities"].ToObject<string[]>()
                                                  .Map(DateAndDurationConverter.ConvertDateOrDuration);
            var vols = ArrayUtils.AppendRows(jObject["Values"].ToObject<double[][]>());
            return new LabelledMatrix<DateOrDuration, double, double>(maturities, strikes, vols);
        }

        public override void WriteJson(JsonWriter writer,
                                       object value,
                                       JsonSerializer serializer)
        {
            var labelledMatrix = (LabelledMatrix<DateOrDuration, double, double>) value;
            
            var strikes = labelledMatrix.ColLabels;
            var maturities = labelledMatrix.RowLabels;
            var values = labelledMatrix.Values;
            serializer.Serialize(writer, new { Strikes = strikes, Maturities = maturities, Values = values });
        }
    }

    public class JsonAtmRateVolMatrixConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(LabelledMatrix<DateOrDuration, string, double>).IsAssignableFrom(objectType);
        }
        public override object ReadJson(JsonReader reader,
                                        Type objectType,
                                        object existingValue,
                                        JsonSerializer serializer)
        {
            JObject jObject = JObject.Load(reader);
            var tenors = jObject["Tenors"].ToObject<string[]>();
            var maturities = jObject["Maturities"].ToObject<string[]>()
                                                  .Map(DateAndDurationConverter.ConvertDateOrDuration);
            var vols = ArrayUtils.AppendRows(jObject["Values"]
                                 .ToObject<double[][]>());
            return new LabelledMatrix<DateOrDuration, string, double>(maturities, tenors, vols);
        }

        public override void WriteJson(JsonWriter writer,
                                       object value,
                                       JsonSerializer serializer)
        {
            var labelledMatrix = (LabelledMatrix<DateOrDuration, string, double>)value;

            var tenors = labelledMatrix.ColLabels;
            var maturities = labelledMatrix.RowLabels;
            var values = labelledMatrix.Values;
            serializer.Serialize(writer, new { Tenors = tenors, Maturities = maturities, Values = values });
        }
    }

    public class JsonDateOrDurationConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof (DateOrDuration).IsAssignableFrom(objectType);
        }
        public override object ReadJson(JsonReader reader,
                                        Type objectType,
                                        object existingValue,
                                        JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer,
                                       object value,
                                       JsonSerializer serializer)
        {
            var dateOrDuration = (DateOrDuration) value;
            if (dateOrDuration.IsDuration)
            {
                serializer.Serialize(writer, dateOrDuration.Duration.ToString());
                return;
            }
            
            serializer.Serialize(writer, dateOrDuration.Date);
        }
    }
    
}
