﻿using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;

namespace PragmaticQuant.Markets.RawDatas
{
    public class RateVolCubeDatas : ICloneable<RateVolCubeDatas>
    {
        #region protected methods
        protected bool Equals(RateVolCubeDatas other)
        {
            return string.Equals(Currency, other.Currency)
                   && Equals(AtmVolMatrix, other.AtmVolMatrix);
        }
        #endregion

        public string Currency { get; set; }
        public LabelledMatrix<DateOrDuration, string, double> AtmVolMatrix { get; set; }
        
        public RateVolCubeDatas Clone()
        {
            return new RateVolCubeDatas
            {
                Currency = (string)Currency.Clone(),
                AtmVolMatrix = AtmVolMatrix.Clone()
            };
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((RateVolCubeDatas)obj);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Currency != null ? Currency.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Currency != null ? Currency.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (AtmVolMatrix != null ? AtmVolMatrix.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}