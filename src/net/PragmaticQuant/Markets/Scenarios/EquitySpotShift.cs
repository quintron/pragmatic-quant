using System.Linq;
using PragmaticQuant.Markets.RawDatas;

namespace PragmaticQuant.Markets.Scenarios
{
    public class EquitySpotShift : IMarketDatasScenario
    {
        public EquitySpotShift(string[] assets, double relativeShift, bool stickyStrikeVol)
        {
            Assets = assets;
            RelativeShift = relativeShift;
            StickyStrikeVol = stickyStrikeVol;
        }
        public EquitySpotShift(string asset, double relativeShift, bool stickyStrikeVol)
            : this(new[] {asset}, relativeShift, stickyStrikeVol)
        {
        }
        public MarketDatas Apply(MarketDatas marketDatas)
        {
            MarketDatas shiftedMarketDatas = marketDatas.Clone();

            foreach (var asset in Assets)
            {
                AssetMarketDatas shiftedAssetMkt = shiftedMarketDatas.GetEquityMarket(asset);
                shiftedAssetMkt.Spot *= (1.0 + RelativeShift);

                double[] dividendFwds = shiftedAssetMkt.DividendForward.Values;
                for (int i = 0; i < dividendFwds.Length; i++)
                    dividendFwds[i] *= (1.0 + RelativeShift);

                if (!StickyStrikeVol)
                {
                    var volMatrix = shiftedAssetMkt.VolatilityMatrix;
                    double[] volStrikes = volMatrix.ColLabels;
                    for (int i = 0; i < volStrikes.Length; i++)
                    {
                        volStrikes[i] *= (1.0 + RelativeShift);
                    }
                }
            }

            return shiftedMarketDatas;
        }
        public string Name
        {
            get
            {
                var assetDecs = Assets.Count() > 1
                    ? "[" + string.Join(",", Assets) + "]"
                    : Assets.First();
                return string.Format("SpotShift({0},{1})", assetDecs, RelativeShift.ToString("P"));
            }
        }
        public string[] Assets { get; private set; }
        public double RelativeShift { get; private set; }
        public bool StickyStrikeVol { get; private set; }
        public override string ToString()
        {
            return Name;
        }
    }

    public class EquitySpotParametricScenario : IParametricMktScenario
    {
        public EquitySpotParametricScenario(string[] assets, bool stickyStrikeVol)
        {
            Assets = assets;
            StickyStrikeVol = stickyStrikeVol;
        }
        public EquitySpotParametricScenario(string asset, bool stickyStrikeVol)
            : this(new[] {asset}, stickyStrikeVol)
        {
        }
        public MarketScenario Scenario(double shock)
        {
            return MarketScenario.FromMarketDatasScenario(new EquitySpotShift(Assets, shock, StickyStrikeVol));
        }
        public string[] Assets { get; private set; }
        public bool StickyStrikeVol { get; private set; }
    }

}