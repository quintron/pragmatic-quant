using System;
using System.Globalization;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Core;
using PragmaticQuant.Markets.Equity;
using PragmaticQuant.Markets.RawDatas;

namespace PragmaticQuant.Markets.Scenarios
{
    public class EquityVolShift : IMarketDatasScenario
    {
        public EquityVolShift(string asset, double volShift)
        {
            Asset = asset;
            VolShift = volShift;
        }
        public MarketDatas Apply(MarketDatas marketDatas)
        {
            var shiftedMarket = marketDatas.Clone();
            var shiftedAssetMkt = shiftedMarket.GetEquityMarket(Asset);
            shiftedAssetMkt.VolatilityMatrix.Values.Add(VolShift);
            return shiftedMarket;
        }
        public string Name
        {
            get { return string.Format("VolShift({0},{1})", Asset, VolShift.ToString(CultureInfo.InvariantCulture)); }
        }
        public string Asset { get; private set; }
        public double VolShift { get; private set; }
        public override string ToString()
        {
            return Name;
        }
    }

    public class EquityVolParametricScenario : IParametricMktScenario
    {
        public EquityVolParametricScenario(string asset)
        {
            Asset = asset;
        }
        public MarketScenario Scenario(double shock)
        {
            return MarketScenario.FromMarketDatasScenario(new EquityVolShift(Asset, shock));
        }
        public string Asset { get; private set; }
    }

    public class EquityVolWaveShift : IMarketDatasScenario
    {
        public EquityVolWaveShift(string asset, double volShift, DateOrDuration waveStart, double waveSmoothing = 0.1)
        {
            Asset = asset;
            VolShift = volShift;
            WaveStart = waveStart;
            WaveSmoothing = waveSmoothing;
        }
        public MarketDatas Apply(MarketDatas marketDatas)
        {
            var refDate = marketDatas.RefDate;
            var waveStartDate = WaveStart.ToDate(refDate);
            if (waveStartDate <= refDate)
                return new EquityVolShift(Asset, VolShift).Apply(marketDatas);

            var waveSmoothingNbDays = (waveStartDate - refDate).TotalDays * WaveSmoothing;
            var waveSmoothingStart = waveStartDate.AddDays(-waveSmoothingNbDays);

            var shiftedMarket = marketDatas.Clone();
            var shiftedAssetMkt = shiftedMarket.GetEquityMarket(Asset);
            
            ITimeMeasure time = TimeMeasure.Act365(refDate);
            var volDatas = shiftedAssetMkt.VolatilityMatrix;
            VolatilityMatrix volMatrix = volDatas.ToVolMatrix(time);
            var volSurface = VolatilitySurface.BuildInterpol(volMatrix, MoneynessProvider.LogSpot(shiftedAssetMkt.Spot));

            var shiftedPillars = volMatrix.Maturities.Where(m => m < waveSmoothingStart || m > waveStartDate)
                                                     .Union(new[] {waveSmoothingStart, waveStartDate})
                                                     .OrderBy(m => m).ToArray();

            var strikes = volDatas.ColLabels;
            var shiftedVols = new double[shiftedPillars.Length, strikes.Length];
            for (int i = 0; i < shiftedPillars.Length; i++)
            {
                DateTime pillar = shiftedPillars[i];
                bool isShifted = pillar > waveSmoothingStart;
                for (int j = 0; j < strikes.Length; j++)
                {
                    var vol = volSurface.Volatility(pillar, strikes[j]);
                    if (isShifted) vol += VolShift;
                    shiftedVols[i, j] = vol;
                }
            }

            shiftedAssetMkt.VolatilityMatrix = new LabelledMatrix<DateOrDuration, double, double>(
                                    shiftedPillars.Map(d => new DateOrDuration(d)), strikes, shiftedVols);
            return shiftedMarket;
        }

        public string Name
        {
            get { return string.Format("VolShift({0},{1})", Asset, VolShift.ToString(CultureInfo.InvariantCulture)); }
        }
        public string Asset { get; private set; }
        public double VolShift { get; private set; }
        public DateOrDuration WaveStart { get; private set; }
        public double WaveSmoothing { get; private set; }
        public override string ToString()
        {
            return Name;
        }
    }

    public class EquityVolWaveParametricScenario : IParametricMktScenario
    {
        public EquityVolWaveParametricScenario(string asset, DateOrDuration waveStart, double waveSmoothing = 0.1)
        {
            Asset = asset;
            WaveStart = waveStart;
            WaveSmoothing = waveSmoothing;
        }
        public MarketScenario Scenario(double shock)
        {
            return MarketScenario.FromMarketDatasScenario(new EquityVolWaveShift(Asset, shock, WaveStart, WaveSmoothing));
        }
        public string Asset { get; private set; }
        public DateOrDuration WaveStart { get; private set; }
        public double WaveSmoothing { get; private set; }
    }
    
}