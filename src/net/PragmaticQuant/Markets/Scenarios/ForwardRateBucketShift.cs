using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Maths.Function;

namespace PragmaticQuant.Markets.Scenarios
{
    public class ForwardRateBucketShift : IInterpolatedMarketscenario
    {
        public ForwardRateBucketShift(string currency, DateOrDuration bucketStart, DateOrDuration bucketEnd, double fwdRateShift)
        {
            Currency = currency;
            BucketStart = bucketStart;
            BucketEnd = bucketEnd;
            FwdRateShift = fwdRateShift;
        }
        
        public Market Apply(Market market)
        {
            var time = TimeMeasure.Act365(market.RefDate);
            var fwdShift = new StepFunction(time[new[] { BucketStart, BucketEnd }], new[] { FwdRateShift, 0.0 }, 0.0);
            var fwdShiftIntegral = fwdShift.Integral(0.0);
            Func<double, double> rateShift = t =>
            {
                t = Math.Max(DoubleUtils.MachineEpsilon, t);
                return fwdShiftIntegral.Eval(t) / t;
            };
            DiscountCurve shiftCurve = new DiscountCurveFromRate(null, time, rateShift);

            var shiftedMarket = market;
            foreach (var financingId in market.DiscountCurveIds)
            {
                if (financingId.Currency.Name.Equals(Currency, StringComparison.InvariantCultureIgnoreCase))
                {
                    var baseCurve = market.DiscountCurve(financingId);
                    var shiftedCurve = DiscountCurve.Product(baseCurve, shiftCurve, financingId);
                    shiftedMarket = shiftedMarket.NewDiscountCurve(shiftedCurve);
                }
            }

            foreach (var assetId in shiftedMarket.AssetIds)
            {
                var assetCurrency = market.AssetCurrency(assetId.Name);
                if (assetCurrency.Name.Equals(Currency, StringComparison.InvariantCultureIgnoreCase))
                {
                    AssetMarket assetMkt = shiftedMarket.AssetMarket(assetId);
                    var shiftedCurve = DiscountCurve.Product(assetMkt.AssetDiscountCurve, shiftCurve, assetMkt.AssetDiscountCurve.Financing);
                    var shiftedAssetMkt = assetMkt.NewAssetDiscountCurve(shiftedCurve);
                    shiftedMarket = shiftedMarket.NewAssetMarket(shiftedAssetMkt);
                }
            }

            return shiftedMarket;
        }
        public string Name
        {
            get
            {
                return string.Format("FwdRateShift({0},{1},{2},{3})", Currency, BucketStart, BucketEnd, FwdRateShift.ToString("P"));
            }
        }

        public string Currency { get; private set; }
        public DateOrDuration BucketStart { get; private set; }
        public DateOrDuration BucketEnd { get; private set; }
        public double FwdRateShift { get; private set; }
    }

    public class ForwardRateBucketParamScenario : IParametricMktScenario
    {
        public ForwardRateBucketParamScenario(string currency, DateOrDuration bucketStart, DateOrDuration bucketEnd)
        {
            Currency = currency;
            BucketStart = bucketStart;
            BucketEnd = bucketEnd;
        }
        public MarketScenario Scenario(double shock)
        {
            var bucketShift = new ForwardRateBucketShift(Currency, BucketStart, BucketEnd, shock);
            return MarketScenario.FromInterpolatedMarketScenario(bucketShift);
        }
        public string Currency { get; private set; }
        public DateOrDuration BucketStart { get; private set; }
        public DateOrDuration BucketEnd { get; private set; }
    }
}