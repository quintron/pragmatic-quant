using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Maths.Function;

namespace PragmaticQuant.Markets.Scenarios
{
    public class ForwardRepoBucketShift : IInterpolatedMarketscenario
    {
        public ForwardRepoBucketShift(string asset, DateOrDuration bucketStart, DateOrDuration bucketEnd, double fwdRepoShift)
        {
            Asset = asset;
            BucketStart = bucketStart;
            BucketEnd = bucketEnd;
            FwdRepoShift = fwdRepoShift;
        }

        public Market Apply(Market market)
        {
            var time = TimeMeasure.Act365(market.RefDate);
            var fwdShift = new StepFunction(time[new[] { BucketStart, BucketEnd }], new[] { FwdRepoShift, 0.0 }, 0.0);
            var fwdShiftIntegral = fwdShift.Integral(0.0);
            Func<double, double> rateShift = t =>
            {
                t = Math.Max(DoubleUtils.MachineEpsilon, t);
                return fwdShiftIntegral.Eval(t) / t;
            };
            DiscountCurve shiftCurve = new DiscountCurveFromRate(null, time, rateShift);

            var shiftedMarket = market;
            foreach (var assetId in shiftedMarket.AssetIds)
            {
                if (assetId.Name.Equals(Asset, StringComparison.InvariantCultureIgnoreCase))
                {
                    AssetMarket assetMkt = shiftedMarket.AssetMarket(assetId);
                    var shiftedCurve = DiscountCurve.Product(assetMkt.AssetDiscountCurve, shiftCurve, assetMkt.AssetDiscountCurve.Financing);
                    var shiftedAssetMkt = assetMkt.NewAssetDiscountCurve(shiftedCurve);
                    shiftedMarket = shiftedMarket.NewAssetMarket(shiftedAssetMkt);
                }
            }

            return shiftedMarket;
        }
        public string Name
        {
            get
            {
                return string.Format("FwdRepoShift({0},{1},{2},{3})", Asset, BucketStart, BucketEnd, FwdRepoShift.ToString("P"));
            }
        }

        public string Asset { get; private set; }
        public DateOrDuration BucketStart { get; private set; }
        public DateOrDuration BucketEnd { get; private set; }
        public double FwdRepoShift { get; private set; }
    }

    public class ForwardRepoBucketParamScenario : IParametricMktScenario
    {
        public ForwardRepoBucketParamScenario(string asset, DateOrDuration bucketStart, DateOrDuration bucketEnd)
        {
            Asset = asset;
            BucketStart = bucketStart;
            BucketEnd = bucketEnd;
        }
        public MarketScenario Scenario(double shock)
        {
            var bucketShift = new ForwardRepoBucketShift(Asset, BucketStart, BucketEnd, shock);
            return MarketScenario.FromInterpolatedMarketScenario(bucketShift);
        }
        public string Asset { get; private set; }
        public DateOrDuration BucketStart { get; private set; }
        public DateOrDuration BucketEnd { get; private set; }
    }

}