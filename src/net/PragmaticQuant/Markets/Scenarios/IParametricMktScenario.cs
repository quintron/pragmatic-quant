using System;
using PragmaticQuant.Basic.Dates;

namespace PragmaticQuant.Markets.Scenarios
{
    public interface IParametricMktScenario
    {
        MarketScenario Scenario(double shock);
    }

    public static class ParametricScenarios
    {
        public static IParametricMktScenario Parse(string risk, string underlying)
        {
            switch (risk.ToLowerInvariant())
            {
                case "vol":
                    return EquityVolShift(underlying);

                case "spot":
                    return EquitySpotShift(underlying, false);

                case "repo":
                    return EquityRepoShift(underlying);

                case "rate":
                    return RateShift(underlying);

                default:
                    throw new Exception("Unkown risk scenario : " + risk);
            }
        }

        public static IParametricMktScenario EquitySpotShift(string[] assets, bool stickyStrikeVol)
        {
            return new EquitySpotParametricScenario(assets, stickyStrikeVol);
        }
        public static IParametricMktScenario EquitySpotShift(string asset, bool stickyStrikeVol)
        {
            return new EquitySpotParametricScenario(asset, stickyStrikeVol);
        }
        public static IParametricMktScenario EquityVolShift(string asset)
        {
            return new EquityVolParametricScenario(asset);
        }
        public static IParametricMktScenario EquityVolWaveShift(string asset, DateOrDuration waveStart, double waveSmoothing = 0.1)
        {
            return new EquityVolWaveParametricScenario(asset, waveStart, waveSmoothing);
        }
        public static IParametricMktScenario EquityRepoShift(string asset)
        {
            return new RepoShiftParametricScenario(asset);
        }
        public static IParametricMktScenario EquityForwardRepoBucketShift(string asset, DateOrDuration bucketStart, DateOrDuration bucketEnd)
        {
            return new ForwardRepoBucketParamScenario(asset, bucketStart, bucketEnd);
        }
        
        public static IParametricMktScenario RateShift(string currency)
        {
            return new RateShiftParametricScenario(currency);
        }
        public static IParametricMktScenario ForwardRateBucketShift(string currency, DateOrDuration bucketStart, DateOrDuration bucketEnd)
        {
            return new ForwardRateBucketParamScenario(currency, bucketStart, bucketEnd);
        }
        public static IParametricMktScenario RateVolShift(string currency)
        {
            return new RateVolParametricScenario(currency);
        }
    }
    
}