using System;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Markets.RawDatas;

namespace PragmaticQuant.Markets.Scenarios
{
    public static class MarketScenarios
    {
        public static MarketScenario Parse(string scenarioDesc)
        {
            scenarioDesc = scenarioDesc.Trim();

            if (scenarioDesc == "" || scenarioDesc.Equals("identity", StringComparison.InvariantCultureIgnoreCase))
                return MarketScenario.Identity();

            var bracketSplit = scenarioDesc.Split('(', ')');
            if (bracketSplit.Length != 3)
                throw new Exception(string.Format("Invalid risk description {0}", scenarioDesc));

            string risk = bracketSplit[0].Trim();

            var riskArgs = bracketSplit[1].Split(';').Map(a => a.Trim().ToLowerInvariant());
            if (riskArgs.Length != 2)
                throw new Exception(string.Format("Invalid arguments fo scenario '{0}' : '{1}' ", risk, bracketSplit[1]));

            string underlying = riskArgs[0];
            double shock =  NumberConverter.ConvertDouble(riskArgs[1]);

            var paramScenario = ParametricScenarios.Parse(risk, underlying);
            return paramScenario.Scenario(shock);
        }
    }

    public class MarketScenario
    {
        #region private fields
        private readonly IMarketDatasScenario[] datasScenarios;
        private readonly IInterpolatedMarketscenario[] interpolScenarios;
        #endregion

        public MarketScenario(IMarketDatasScenario[] datasScenarios, IInterpolatedMarketscenario[] interpolScenarios)
        {
            this.datasScenarios = datasScenarios;
            this.interpolScenarios = interpolScenarios;
        }
        public static MarketScenario FromMarketDatasScenario(params IMarketDatasScenario[] datasScenarios)
        {
            return new MarketScenario(datasScenarios, new IInterpolatedMarketscenario[0]);
        }
        public static MarketScenario FromInterpolatedMarketScenario(params IInterpolatedMarketscenario[] scenarios)
        {
            return new MarketScenario(new IMarketDatasScenario[0], scenarios);
        }
        public static MarketScenario Identity()
        {
            return FromMarketDatasScenario(new IdentityMarketDatasScenario());
        }

        public Market BuildMarket(MarketDatas marketDatas, Func<MarketDatas, Market> marketInterpolation)
        {
            var scenarizedRawMarket = datasScenarios.Aggregate(marketDatas, (mkt, sc) => sc.Apply(mkt));
            var scenarizedMkt = marketInterpolation(scenarizedRawMarket);
            scenarizedMkt = interpolScenarios.Aggregate(scenarizedMkt, (mkt, sc) => sc.Apply(mkt));
            return scenarizedMkt;
        }
        public MarketScenario CompoundLeft(MarketScenario left)
        {
            if (left.datasScenarios.Any())
            {
                if (interpolScenarios.Any())
                    throw new Exception("Market Scenario is not left compoundable !");

                return new MarketScenario(datasScenarios.Concat(left.datasScenarios).ToArray(), left.interpolScenarios);
            }

            return new MarketScenario(datasScenarios, interpolScenarios.Concat(left.interpolScenarios).ToArray());
        }

        public string Name
        {
            get
            {
                var rawScenarioName = string.Join("|", datasScenarios.Map(sc => sc.Name));
                var interpolScenarioName = string.Join("|", interpolScenarios.Map(sc => sc.Name));

                if (rawScenarioName.Equals(""))
                    return interpolScenarioName;

                if (interpolScenarioName.Equals(""))
                    return rawScenarioName;

                return string.Format("{0}|{1}", rawScenarioName, interpolScenarioName);
            }
        }
        public override string ToString()
        {
            return Name;
        }
    }
    
    public interface IMarketDatasScenario
    {
        MarketDatas Apply(MarketDatas marketDatas);
        string Name { get; }
    }

    public interface IInterpolatedMarketscenario
    {
        Market Apply(Market market);
        string Name { get; }
    }
    
    public class IdentityMarketDatasScenario : IMarketDatasScenario
    {
        public MarketDatas Apply(MarketDatas marketDatas)
        {
            return marketDatas;
        }
        public string Name
        {
            get { return "Identity"; }
        }
        public override string ToString()
        {
            return Name;
        }
    }
}