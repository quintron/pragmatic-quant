using System;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets.RawDatas;

namespace PragmaticQuant.Markets.Scenarios
{
    public class RateShift : IMarketDatasScenario
    {
        public RateShift(string currency, double rateShift)
        {
            Currency = currency;
            Shift = rateShift;
        }
        public MarketDatas Apply(MarketDatas marketDatas)
        {
            var shiftedMarket = marketDatas.Clone();
            foreach (var curve in shiftedMarket.DiscountCurves)
            {
                if (curve.Currency.Equals(Currency, StringComparison.InvariantCultureIgnoreCase))
                {
                    curve.ZcRates = curve.ZcRates.MapValues(rate => rate + Shift);
                }
            }
            return shiftedMarket;
        }
        public string Name
        {
            get { return string.Format("RateShift({0},{1})", Currency, Shift.ToString("P")); }
        }
        public string Currency { get; private set; }
        public double Shift { get; private set; }
        public override string ToString()
        {
            return Name;
        }
    }

    public class RateShiftParametricScenario : IParametricMktScenario
    {
        public RateShiftParametricScenario(string currency)
        {
            Currency = currency;
        }
        public MarketScenario Scenario(double shock)
        {
            return MarketScenario.FromMarketDatasScenario(new RateShift(Currency, shock));
        }
        public string Currency { get; private set; }
    }
}