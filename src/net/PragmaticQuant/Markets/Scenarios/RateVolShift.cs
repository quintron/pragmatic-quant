﻿using System.Globalization;
using PragmaticQuant.Core;
using PragmaticQuant.Markets.RawDatas;

namespace PragmaticQuant.Markets.Scenarios
{

    public class RateVolShift : IMarketDatasScenario
    {
        public RateVolShift(string currency, double volShift)
        {
            Currency = currency;
            VolShift = volShift;
        }
        public MarketDatas Apply(MarketDatas marketDatas)
        {
            var shiftedMarket = marketDatas.Clone();
            var shiftedVolCube = shiftedMarket.GetRateVolCube(Currency);
            shiftedVolCube.AtmVolMatrix.Values.Add(VolShift);
            return shiftedMarket;
        }
        public string Name
        {
            get { return string.Format("VolShift({0},{1})", Currency, VolShift.ToString(CultureInfo.InvariantCulture)); }
        }
        public string Currency { get; private set; }
        public double VolShift { get; private set; }

        public override string ToString()
        {
            return Name;
        }
    }

    public class RateVolParametricScenario : IParametricMktScenario
    {
        public RateVolParametricScenario(string currency)
        {
            Currency = currency;
        }
        public MarketScenario Scenario(double shock)
        {
            return MarketScenario.FromMarketDatasScenario(new RateVolShift(Currency, shock));
        }
        public string Currency { get; private set; }
    }

}