using PragmaticQuant.Markets.RawDatas;

namespace PragmaticQuant.Markets.Scenarios
{
    public class RepoShift : IMarketDatasScenario
    {
        public RepoShift(string asset, double rateShift)
        {
            Asset = asset;
            Shift = rateShift;
        }
        public MarketDatas Apply(MarketDatas marketDatas)
        {
            var shiftedMarket = marketDatas.Clone();
            var shiftedAssetMkt = shiftedMarket.GetEquityMarket(Asset);

            var repos = shiftedAssetMkt.RepoCurve.Values;
            for (int i = 0; i < repos.Length; i++)
                repos[i] += Shift;
            
            return shiftedMarket;
        }
        public string Name
        {
            get { return string.Format("RepoShift({0},{1})", Asset, Shift.ToString("P")); }
        }
        public string Asset { get; private set; }
        public double Shift { get; private set; }
        public override string ToString()
        {
            return Name;
        }
    }

    public class RepoShiftParametricScenario : IParametricMktScenario
    {
        public RepoShiftParametricScenario(string asset)
        {
            Asset = asset;
        }
        public MarketScenario Scenario(double shock)
        {
            return MarketScenario.FromMarketDatasScenario(new RepoShift(Asset, shock));
        }
        public string Asset { get; private set; }
    }
}