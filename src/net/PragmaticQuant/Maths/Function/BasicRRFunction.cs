﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Maths.Interpolation;

namespace PragmaticQuant.Maths.Function
{
    [DebuggerDisplay("{Value}")]
    public class ConstantRrFunction : RrFunction, IAlgebraTerm
    {
        public ConstantRrFunction(double value)
        {
            Value = value;
        }
        public readonly double Value;
        public override double Eval(double x)
        {
            return Value;
        }
        public override RrFunction Add(RrFunction other)
        {
            var cst = other as ConstantRrFunction;
            if (cst != null)
                return RrFunctions.Constant(Value + cst.Value);

            var step = other as StepFunction;
            if (step != null)
                return StepFunction.Add(step, this);

            var spline = other as SplineInterpoler;
            if (spline != null)
                return SplineInterpoler.Add(spline, this);

            var lc = other as LinearCombinationRrFunction;
            if (lc != null)
                return lc.Add(this);

            return base.Add(other);
        }
        public override RrFunction Mult(RrFunction other)
        {
            var cst = other as ConstantRrFunction;
            if (cst != null)
                return ProductRrFunctions.Mult(this, cst);

            var exp = other as ExpRrFunction;
            if (exp != null)
                return ProductRrFunctions.Mult(exp, this);

            var step = other as StepFunction;
            if (step != null)
                return StepFunction.Mult(step, this);

            var spline = other as SplineInterpoler;
            if (spline != null)
                return SplineInterpoler.Mult(spline, this);

            var lc = other as LinearCombinationRrFunction;
            if (lc != null)
                return ProductRrFunctions.Mult(lc, this);

            return base.Mult(other);
        }
        public override RrFunction Integral(double basePoint)
        {
            return RrFunctions.Affine(Value, -basePoint * Value);
        }
        public override RrFunction Derivative()
        {
            return RrFunctions.Zero;
        }
        public override RrFunction Inverse()
        {
            return RrFunctions.Constant(1.0 / Value);
        }
        public bool IsColinearTo(IAlgebraTerm term, out double coeff)
        {
            var cstTerm = term as ConstantRrFunction;
            if (cstTerm != null)
            {
                if (!DoubleUtils.EqualZero(cstTerm.Value))
                {
                    coeff =  Value / cstTerm.Value;
                    return true;
                }
                if (DoubleUtils.EqualZero(Value))
                {
                    coeff = 0.0;
                    return true;
                }
            }

            var expTerm = term as ExpRrFunction;
            if (expTerm != null 
                && DoubleUtils.EqualZero(expTerm.Slope)
                && DoubleUtils.EqualZero(expTerm.Power))
            {
                coeff = Value / expTerm.Weight;
                return true;
            }

            coeff = double.NaN;
            return false;
        }
        public IAlgebraTerm Mult(IAlgebraTerm term)
        {
            throw new NotImplementedException();
        }
    }

    public class ExpRrFunction : RrFunction, IAlgebraTerm
    {
        #region private fields
        private readonly double weight;
        private readonly double slope;
        private readonly double power;
        #endregion
        private ExpRrFunction(double weight, double slope, double power)
        {
            this.weight = weight;
            this.slope = slope;
            this.power = power;
        }
        public static RrFunction Create(double weight, double slope, double power = 0.0)
        {
            if (DoubleUtils.EqualZero(weight))
                    return RrFunctions.Zero;

            if (DoubleUtils.EqualZero(slope) && DoubleUtils.EqualZero(power))
                return RrFunctions.Constant(weight);

            return new ExpRrFunction(weight, slope, power);
        }

        public double Weight
        {
            get { return weight; }
        }
        public double Slope
        {
            get { return slope; }
        }
        public double Power
        {
            get { return power; }
        }

        public override double Eval(double x)
        {
            return weight * Math.Pow(x, power) *  Math.Exp(slope * x);
        }

        public override RrFunction Add(RrFunction other)
        {
            var algebraTerm = other as IAlgebraTerm;
            if (algebraTerm != null)
            {
                double coeff;
                if (algebraTerm.IsColinearTo(this, out coeff))
                {
                    return Create(weight * (1.0 + coeff), slope, power);
                }
            }

            return base.Add(other);
        }
        public override RrFunction Mult(RrFunction other)
        {
            var cst = other as ConstantRrFunction;
            if (cst != null)
                return ProductRrFunctions.Mult(this, cst);

            var exp = other as ExpRrFunction;
            if (exp != null)
                return ProductRrFunctions.Mult(this, exp);

            var lc = other as LinearCombinationRrFunction;
            if (lc != null)
                return lc.Mult(this);
            
            return base.Mult(other);
        }
        public override RrFunction Integral(double basePoint)
        {
            RrFunction zeroBaseIntegral;

            if (DoubleUtils.EqualZero(slope))
            {
                if (DoubleUtils.MachineEquality(-1.0, power))
                    throw new NotImplementedException("ExpRrFunction.Integral : log function not implemented !");

                zeroBaseIntegral = Create(weight / (power + 1.0), 0.0, power + 1.0);
            }
            else if (DoubleUtils.EqualZero(power))
            {
                zeroBaseIntegral = Create(weight / slope, slope);
                
            }
            else
            {
                var integerPower = power > 0.0 && DoubleUtils.EqualZero(power - Math.Round(power));
                if (!integerPower)
                    throw new Exception("ExpIntegral NotImplementedException yet implemented with non integral power");
                
                zeroBaseIntegral = Create(weight / slope, slope, power);
                var integralPartTerm = Create(-weight / slope * power, slope, power - 1.0).Integral(0.0);
                zeroBaseIntegral = zeroBaseIntegral.Add(integralPartTerm);
            }
            
            return zeroBaseIntegral - zeroBaseIntegral.Eval(basePoint);
        }
        public override RrFunction Derivative()
        {
            var derivative = Create(weight * slope, slope, power);
            if (DoubleUtils.EqualZero(power))
                return derivative;
            return derivative + Create(weight * power, slope, power - 1.0);
        }
        public override RrFunction Inverse()
        {
            return Create(1.0 / weight, -slope, -power);
        }
        
        public bool IsColinearTo(IAlgebraTerm term, out double coeff)
        {
            var expTerm = term as ExpRrFunction;
            if (expTerm != null)
            {
                if (DoubleUtils.MachineEquality(expTerm.slope, slope)
                    && DoubleUtils.MachineEquality(expTerm.power, power))
                {
                    coeff = weight / expTerm.weight;
                    return true;
                }
            }

            var cstTerm = term as ConstantRrFunction;
            if (cstTerm != null && DoubleUtils.EqualZero(slope) 
                                && DoubleUtils.EqualZero(power))
            {
                coeff = Weight / cstTerm.Value;
                return true;
            }

            coeff = double.NaN;
            return false;
        }
        public IAlgebraTerm Mult(IAlgebraTerm term)
        {
            var cst = term as ConstantRrFunction;
            if (cst != null)
                return ProductRrFunctions.Mult(this, cst) as IAlgebraTerm;

            var exp = term as ExpRrFunction;
            if (exp != null)
                return ProductRrFunctions.Mult(this, exp) as IAlgebraTerm;
            
            throw new NotImplementedException();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ExpRrFunction) obj);
        }
        protected bool Equals(ExpRrFunction other)
        {
            return weight.Equals(other.weight) && slope.Equals(other.slope) && power.Equals(other.power);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = weight.GetHashCode();
                hashCode = (hashCode * 397) ^ slope.GetHashCode();
                hashCode = (hashCode * 397) ^ power.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            if (DoubleUtils.EqualZero(power))
            {
                return weight.ToString(CultureInfo.InvariantCulture) + "*exp(" + slope.ToString(CultureInfo.InvariantCulture) + "*X)";
            }

            return weight.ToString(CultureInfo.InvariantCulture)
                   + "*X^(" + power.ToString(CultureInfo.InvariantCulture) + ")*exp("
                   + slope.ToString(CultureInfo.InvariantCulture) + "*X)";
        }
    }

    public class FuncRrFunction : RrFunction
    {
        #region private fields
        private readonly Func<double, double> f;
        #endregion
        public FuncRrFunction(Func<double, double> f)
        {
            this.f = f;
        }
        public override double Eval(double x)
        {
            return f(x);
        }
        public override RrFunction Derivative()
        {
            throw new Exception("FuncRrFunction : not derivable function");
        }
        public override RrFunction Integral(double basePoint)
        {
            throw new Exception("FuncRrFunction : not integrable function");
        }
    }
    
    public class LinearCombinationRrFunction : RrFunction
    {
        #region private fields
        private readonly double[] weights;
        private readonly RrFunction[] functions;
        #endregion
        #region private methods
        private static void AddTerm(ref IList<double> weights, ref IList<RrFunction> functions,
            double weight, RrFunction term)
        {
            var termIndex = functions.IndexOf(term);
            if (termIndex > 0)
            {
                weights[termIndex] += weight;
                return;
            }
            
            var lc = term as LinearCombinationRrFunction;
            if (lc != null)
            {
                for (int j = 0; j < lc.Weights.Length; j++)
                    AddTerm(ref weights, ref functions, weight * lc.Weights[j], lc.Functions[j]);
                return;
            }
            
            var algebraTerm = term as IAlgebraTerm;
            if (algebraTerm != null)
            {
                for (int i = 0; i < functions.Count; i++)
                {
                    var fiAlgebra = functions[i] as IAlgebraTerm;
                    if (fiAlgebra != null)
                    {
                        double coeff;
                        if (algebraTerm.IsColinearTo(fiAlgebra, out coeff))
                        {
                            weights[i] += weight * coeff;
                            return;
                        }
                    }
                }
            }

            weights.Add(weight);
            functions.Add(term);
        }
        private static void AddTerms(double[] weightTerms, RrFunction[] terms,
            ref IList<double> weights, ref IList<RrFunction> functions)
        {
            for (int i = 0; i < weightTerms.Length; i++)
                AddTerm(ref weights, ref functions, weightTerms[i], terms[i]);
        }
        #endregion

        private LinearCombinationRrFunction(double[] weights, RrFunction[] functions)
        {
            this.weights = weights;
            this.functions = functions;
        }
        public static RrFunction Create(double[] weights, RrFunction[] functions)
        {
            IList<double> ws = new List<double>();
            IList<RrFunction> fs = new List<RrFunction>();
            AddTerms(weights, functions, ref ws, ref fs);
            return new LinearCombinationRrFunction(ws.ToArray(), fs.ToArray());
        }

        public double[] Weights
        {
            get { return weights; }
        }
        public RrFunction[] Functions
        {
            get { return functions; }
        }

        public override double Eval(double x)
        {
            return weights.Select((w, i) => w * functions[i].Eval(x)).Sum();
        }

        public override RrFunction Mult(RrFunction other)
        {
            var cst = other as ConstantRrFunction;
            if (cst != null)
                return ProductRrFunctions.Mult(this, cst);

            return Create(weights, functions.Map(f => f.Mult(other)));
        }
        
        public override RrFunction Add(RrFunction other)
        {
            var lc = other as LinearCombinationRrFunction;
            if (lc != null)
                return Add(this, lc);

            var cst = other as ConstantRrFunction;
            if (cst != null && DoubleUtils.EqualZero(cst.Value))
                return this;

            IList<double> ws = new List<double>(weights);
            IList<RrFunction> fs = new List<RrFunction>(functions);
            AddTerm(ref ws, ref fs, 1.0, other);
            return new LinearCombinationRrFunction(ws.ToArray(), fs.ToArray());
        }
        public static RrFunction Add(LinearCombinationRrFunction leftLc, LinearCombinationRrFunction rightLc)
        {
            IList<double> ws = new List<double>();
            IList<RrFunction> fs = new List<RrFunction>();
            AddTerms(leftLc.weights, leftLc.functions, ref ws, ref fs);
            AddTerms(rightLc.weights, rightLc.functions, ref ws, ref fs);
            return new LinearCombinationRrFunction(ws.ToArray(), fs.ToArray());
        }

        public override RrFunction Integral(double basePoint)
        {
            return Create(weights, functions.Map(f => f.Integral(basePoint)));
        }
        public override RrFunction Derivative()
        {
            return Create(weights, functions.Map(f => f.Derivative()));
        }
    }

    public class FractionRrFunction : RrFunction
    {
        #region private fields
        private readonly double weight;
        private readonly RrFunction[] nums;
        private readonly RrFunction[] denoms;
        #endregion
        public FractionRrFunction(double weight, RrFunction[] nums, RrFunction[] denoms)
        {
            this.weight = weight;
            this.nums = nums;
            this.denoms = denoms;
        }
        public FractionRrFunction(double weight, params RrFunction[] prods)
        {
            this.weight = weight;
            nums = prods;
            denoms = new[] {RrFunctions.Constant(1.0)};
        }
        public override double Eval(double x)
        {
            return nums.Aggregate(weight, (current, t) => current * t.Eval(x))
                   / denoms.Aggregate(1.0, (current, t) => current * t.Eval(x));
        }
        public override RrFunction Derivative()
        {
            throw new NotImplementedException("FractionRrFunction.Derivative");
        }
        public override RrFunction Integral(double basePoint)
        {
            throw new NotImplementedException("FractionRrFunction.Integral");
        }
        public override RrFunction Inverse()
        {
            return new FractionRrFunction(1.0 / weight, denoms, nums);
        }
        public override RrFunction Mult(RrFunction other)
        {
            var frac = other as FractionRrFunction;
            if (frac != null)
            {
                var prodWeight = weight * frac.weight;
                if (DoubleUtils.EqualZero(prodWeight))
                    return RrFunctions.Zero;
                return new FractionRrFunction(prodWeight, nums.Union(frac.nums).ToArray(), denoms.Union(frac.denoms).ToArray());
            }

            return new FractionRrFunction(weight, nums.Union(new[] {other}).ToArray(), denoms);
        }
    }

    public static class ProductRrFunctions
    {
        public static RrFunction Mult(ConstantRrFunction left, ConstantRrFunction right)
        {
            return RrFunctions.Constant(left.Value * right.Value);
        }
        public static RrFunction Mult(ExpRrFunction exp, ConstantRrFunction cst)
        {
            return ExpRrFunction.Create(exp.Weight * cst.Value, exp.Slope, exp.Power);
        }
        public static RrFunction Mult(ExpRrFunction letfExp, ExpRrFunction rightExp)
        {
            return ExpRrFunction.Create(letfExp.Weight * rightExp.Weight, letfExp.Slope + rightExp.Slope, letfExp.Power + rightExp.Power);
        }
        public static RrFunction Mult(LinearCombinationRrFunction lc, ConstantRrFunction cst)
        {
            if (DoubleUtils.MachineEquality(1.0, cst.Value))
                return lc;

            if (DoubleUtils.EqualZero(cst.Value))
                return RrFunctions.Zero;

            return LinearCombinationRrFunction.Create(lc.Weights.Map(w => w * cst.Value), lc.Functions);
        }
    }

}
