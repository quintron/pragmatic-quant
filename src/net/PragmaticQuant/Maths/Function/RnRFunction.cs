﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PragmaticQuant.Core;
using PragmaticQuant.Basic;

namespace PragmaticQuant.Maths.Function
{
    public abstract class RnRFunction 
    {
        protected RnRFunction(int dim)
        {
            Dim = dim;
        }

        public abstract double Eval(double[] x);
        public int Dim { get; private set; }

        public virtual RnRFunction Add(RnRFunction right)
        {
            var thisAsAlgebraTerm = this as IAlgebraTerm;
            if (thisAsAlgebraTerm != null)
            {
                return new LinearCombination(new[] {1.0}, new[] {this}).Add(right);
            }
            
            throw new NotImplementedException();
        }
        public virtual RnRFunction Mult(RnRFunction right)
        {
            return new FractionRnRFunction(this, right);
        }
        public virtual RnRFunction Inverse()
        {
            return new FractionRnRFunction(new [] {RnRFunctions.Constant(1.0, Dim)}, new[] {this});
        }

        public static RnRFunction operator +(RnRFunction left, RnRFunction right)
        {
            return left.Add(right);
        }
        public static RnRFunction operator +(RnRFunction left, double right)
        {
            return left.Add(RnRFunctions.Constant(right, left.Dim));
        }
        public static RnRFunction operator +(double left, RnRFunction right)
        {
            return RnRFunctions.Constant(left, right.Dim).Add(right);
        }

        public static RnRFunction operator -(RnRFunction left, RnRFunction right)
        {
            return left.Add((-1.0) * right);
        }
        public static RnRFunction operator -(RnRFunction left, double right)
        {
            return left.Add(RnRFunctions.Constant(-right, left.Dim));
        }
        public static RnRFunction operator -(double left, RnRFunction right)
        {
            return RnRFunctions.Constant(left, right.Dim).Add((-1.0) * right);
        }

        public static RnRFunction operator *(RnRFunction left, RnRFunction right)
        {
            return left.Mult(right);
        }
        public static RnRFunction operator *(RnRFunction left, double right)
        {
            return left.Mult(RnRFunctions.Constant(right, left.Dim));
        }
        public static RnRFunction operator *(double left, RnRFunction right)
        {
            return RnRFunctions.Constant(left, right.Dim).Mult(right);
        }
        
        public static RnRFunction operator /(RnRFunction left, RnRFunction right)
        {
            return left.Mult(right.Inverse());
        }
        public static RnRFunction operator /(RnRFunction left, double right)
        {
            return left * (1.0 / right);
        }
        public static RnRFunction operator /(double left, RnRFunction right)
        {
            return left * right.Inverse();
        }
    }
    
    public static class RnRFunctions
    {
        public static RnRFunction Constant(double value, int dim)
        {
            return new ConstantRnRFunction(dim, value);
        }
        public static RnRFunction Affine(double add, double[] mults)
        {
            return new AffineRnRFunction(add, mults);
        }
        public static RnRFunction ExpAffine(double weight, double[] mults)
        {
            return new ExpAffineRnRFunction(weight, mults);
        }
        public static RnRFunction Sum(params RnRFunction[] functions)
        {
            return new LinearCombination(ArrayUtils.Constant(functions.Length, 1.0), functions);
        }
    }

    [DebuggerDisplay("{Value}")]
    public class ConstantRnRFunction : RnRFunction, IAlgebraTerm
    {
        public ConstantRnRFunction(int dim, double value) : base(dim)
        {
            Value = value;
        }
        public override double Eval(double[] x)
        {
            return Value;
        }
        public double Value { get; private set; }

        public override RnRFunction Add(RnRFunction right)
        {
            var cst = right as ConstantRnRFunction;
            if (cst != null && cst.Dim== Dim)
            {
                return new ConstantRnRFunction(Dim, Value + cst.Value);
            }

            return base.Add(right);
        }
        public override RnRFunction Mult(RnRFunction right)
        {
            var algebraResult = RnRFunctionAlgebraTerm.Mult(this, right);
            if (algebraResult != null) return algebraResult;
            return base.Mult(right);
        }
        
        public bool IsColinearTo(IAlgebraTerm term, out double coeff)
        {
            var cstTerm = term as ConstantRnRFunction;
            if (cstTerm != null)
            {
                if (!DoubleUtils.EqualZero(cstTerm.Value))
                {
                    coeff = Value / cstTerm.Value;
                    return true;
                }
                if (DoubleUtils.EqualZero(Value))
                {
                    coeff = 0.0;
                    return true;
                }
            }

            var expTerm = term as ExpAffineRnRFunction;
            if (expTerm != null && DoubleUtils.EqualZero(expTerm.Weight))
            {
                coeff = Value / expTerm.Weight;
                return true;
            }

            coeff = double.NaN;
            return false;
        }
        public IAlgebraTerm Mult(IAlgebraTerm term)
        {
            var cstTerm = term as ConstantRnRFunction;
            if (cstTerm != null)
            {
                return new ConstantRnRFunction(Dim, cstTerm.Value * Value);
            }

            var expTerm = term as ExpAffineRnRFunction;
            if (expTerm != null)
            {
                return RnRFunctionAlgebraTerm.Mult(this, expTerm);
            }

            throw new NotImplementedException();
        }
        public override RnRFunction Inverse()
        {
            return new ConstantRnRFunction(Dim, 1.0 / Value);
        }
    }

    public sealed class AffineRnRFunction : RnRFunction
    {
        #region private fields
        private readonly double add;
        private readonly double[] mults;
        #endregion
        public AffineRnRFunction(double add, double[] mults)
            : base(mults.Length)
        {
            this.add = add;
            this.mults = mults;
        }
        public override double Eval(double[] x)
        {
            return add + mults.DotProduct(x);
        }
        public override RnRFunction Mult(RnRFunction right)
        {
            var cst = right as ConstantRnRFunction;
            if (cst != null)
            {
                if (DoubleUtils.EqualZero(cst.Value))
                    return RnRFunctions.Constant(0.0, cst.Dim);

                return new ExpAffineRnRFunction(cst.Value * add, mults.Map(m => m * cst.Value));
            }

            return base.Mult(right);
        }
    }

    public sealed class ExpAffineRnRFunction : RnRFunction, IAlgebraTerm
    {
        #region private fields
        public readonly double[] mults;
        #endregion
        public ExpAffineRnRFunction(double weight, double[] mults)
            : base(mults.Length)
        {
            Weight = weight;
            this.mults = mults;
        }
        public readonly double Weight;

        public override double Eval(double[] x)
        {
            double log = 0.0;
            for (int i = 0; i < mults.Length; ++i)
                log += mults[i] * x[i];
            return Weight * Math.Exp(log);
        }
        public override RnRFunction Mult(RnRFunction right)
        {
            var algebraResult = RnRFunctionAlgebraTerm.Mult(this, right);
            if (algebraResult != null) return algebraResult;
            return base.Mult(right);
        }
        public override RnRFunction Inverse()
        {
            if (DoubleUtils.EqualZero(Weight))
                return new ConstantRnRFunction(Dim, double.NaN);

            var invMults = mults.Copy();
            invMults.Mult(-1.0);
            return new ExpAffineRnRFunction(1.0 / Weight, invMults);
        }

        public bool IsConstant()
        {
            return DoubleUtils.EqualZero(mults);
        }
        public bool IsColinearTo(IAlgebraTerm term, out double coeff)
        {
            var expTerm = term as ExpAffineRnRFunction;
            if (expTerm != null)
            {
                if (DoubleUtils.MachineEquality(expTerm.mults, mults))
                {
                    coeff = Weight / expTerm.Weight;
                    return true;
                }
            }

            var cstTerm = term as ConstantRnRFunction;
            if (cstTerm != null && DoubleUtils.EqualZero(mults))
            {
                coeff = Weight / cstTerm.Value;
                return true;
            }

            coeff = double.NaN;
            return false;
        }
        public IAlgebraTerm Mult(IAlgebraTerm term)
        {
            var cstTerm = term as ConstantRnRFunction;
            if (cstTerm != null)
            {
                return RnRFunctionAlgebraTerm.Mult(cstTerm, this);
            }

            var expAffineTerm = term as ExpAffineRnRFunction;
            if (expAffineTerm != null)
            {
                return RnRFunctionAlgebraTerm.Mult(this, expAffineTerm);
            }

            throw new NotImplementedException();
        }
    }

    public class FractionRnRFunction : RnRFunction
    {
        #region private fields
        private readonly RnRFunction[] nums;
        private readonly RnRFunction[] denoms;
        #endregion
        public FractionRnRFunction(RnRFunction[] nums, RnRFunction[] denoms)
            : base(nums.First().Dim)
        {
            Debug.Assert(nums.Select(f => f.Dim).Distinct().Count() == 1);
            Debug.Assert(denoms.Select(f => f.Dim).Distinct().Count() == 1);
            this.nums = nums;
            this.denoms = denoms;
        }
        public FractionRnRFunction(params RnRFunction[] nums)
            : base(nums.First().Dim)
        {
            Debug.Assert(nums.Select(f => f.Dim).Distinct().Count() == 1);
            this.nums = nums;
            denoms = new[] {RnRFunctions.Constant(1.0, nums.First().Dim)};
        }
        public override double Eval(double[] x)
        {
            return nums.Aggregate(1.0, (prev, f) => prev * f.Eval(x))
                   / denoms.Aggregate(1.0, (prev, f) => prev * f.Eval(x));
        }
        public override RnRFunction Inverse()
        {
            return new FractionRnRFunction(denoms, nums);
        }
        public override RnRFunction Mult(RnRFunction right)
        {
            var frac = right as FractionRnRFunction;
            if (frac != null)
            {
                return new FractionRnRFunction(nums.Union(frac.nums).ToArray(), denoms.Union(frac.denoms).ToArray());
            }
            return base.Mult(right);
        }
    }

    public class LinearCombination : RnRFunction
    {
        #region private fields
        private readonly Algebra inner;
        private readonly RnRFunction[] elems;
        #endregion
        private LinearCombination(Algebra inner)
            : base(inner.CastTerms<RnRFunction>().First().Dim)
        {
            if (inner.CastTerms<RnRFunction>().Select(f=>f.Dim).Any(d=> d!=Dim))
                throw new Exception("LinearCombination : incompatible dimension !");

            this.inner = inner;
            elems = inner.CastTerms<RnRFunction>();
        }

        public LinearCombination(double[] weights, RnRFunction[] elems)
            : this(new Algebra(weights, elems.Cast<IAlgebraTerm>().ToArray()))
        {
        }
        
        public override double Eval(double[] x)
        {
            return elems.ZipWith(inner.Weights, (f, w) => f.Eval(x) * w).Sum();
        }
        public override RnRFunction Add(RnRFunction right)
        {
            var term = right as IAlgebraTerm;
            if (term != null)
            {
                return new LinearCombination(inner.Add(term));
            }

            var lc = right as LinearCombination;
            if (lc != null)
            {
                return new LinearCombination(Algebra.Add(inner, lc.inner));
            }

            throw new NotImplementedException();
        }
        public override RnRFunction Mult(RnRFunction right)
        {
            var algebraTerm = right as IAlgebraTerm;
            if (algebraTerm != null)
            {
                return new LinearCombination(inner.Mult(algebraTerm));
            }
            return base.Mult(right);
        }
    }

    public static class RnRFunctionAlgebraTerm
    {
        public static IAlgebraTerm Mult(ConstantRnRFunction cst, ExpAffineRnRFunction exp)
        {
            var prodWeight = cst.Value * exp.Weight;
                if (DoubleUtils.EqualZero(prodWeight))
                    return new ConstantRnRFunction(exp.Dim, 0.0);

            return new ExpAffineRnRFunction(prodWeight, exp.mults);
        }
        public static IAlgebraTerm Mult(ExpAffineRnRFunction leftExp, ExpAffineRnRFunction rightExp)
        {
            var prodWeight = rightExp.Weight * leftExp.Weight;
            if (DoubleUtils.EqualZero(prodWeight))
                return new ConstantRnRFunction(leftExp.Dim, 0.0);

            var prodMults = leftExp.mults.Copy();
            prodMults.Add(rightExp.mults);

            if (DoubleUtils.EqualZero(prodMults))
                return new ConstantRnRFunction(leftExp.Dim, prodWeight);

            return new ExpAffineRnRFunction(prodWeight, prodMults);
        }
        public static RnRFunction Mult(IAlgebraTerm term, RnRFunction right)
        {
            var algebraTerm = right as IAlgebraTerm;
            if (algebraTerm != null)
            {
                return term.Mult(algebraTerm) as RnRFunction;
            }

            var lc = right as LinearCombination;
            if (lc != null)
            {
                return lc.Mult(term as RnRFunction);
            }

            return null;
        }
    }



    public interface IAlgebraTerm
    {
        bool IsColinearTo(IAlgebraTerm term, out double coeff);
        IAlgebraTerm Mult(IAlgebraTerm term);
    }

    public static class AlgebraExtensions
    {
        public static bool IsColinearTo(this IAlgebraTerm thisTerm, IAlgebraTerm term)
        {
            double coeff;
            return thisTerm.IsColinearTo(term, out coeff);
        }
        public static double ColinearityCoeff(this IAlgebraTerm thisTerm, IAlgebraTerm term)
        {
            double coeff;
            if (!thisTerm.IsColinearTo(term, out coeff))
                throw new Exception("IAlgebraTerm : terms are not colinear !");
            return coeff;
        }
    }

    public class Algebra
    {
        #region private fields
        private readonly double[] weights;
        private readonly IAlgebraTerm[] terms;
        #endregion
        #region private methods
        private static void AddTerm(ref IList<double> weights, ref List<IAlgebraTerm> terms,
            double weight, IAlgebraTerm term)
        {
            var termIndex = terms.FindIndex(term.IsColinearTo);
            if (termIndex >= 0)
            {
                double coeff = term.ColinearityCoeff(terms[termIndex]);
                weights[termIndex] += weight * coeff;
                return;
            }
            weights.Add(weight);
            terms.Add(term);
        }
        private static void AddTerms(double[] addWeights, IAlgebraTerm[] addTerms,
            ref IList<double> weights, ref List<IAlgebraTerm> terms)
        {
            for (int i = 0; i < addWeights.Length; i++)
                AddTerm(ref weights, ref terms, addWeights[i], addTerms[i]);
        }
        #endregion

        public Algebra(double[] weights, IAlgebraTerm[] terms, bool simplify = true)
        {
            if (weights.Length != terms.Length)
                throw new Exception("LinearCombinationRRFunction : incompatible size input");

            if (simplify)
            {
                IList<double> ws = new List<double>();
                List<IAlgebraTerm> ts = new List<IAlgebraTerm>();
                AddTerms(weights, terms, ref ws, ref ts);
                this.weights = ws.ToArray();
                this.terms = ts.ToArray();
            }
            else
            {
                this.weights = weights;
                this.terms = terms;
            }

        }

        public double[] Weights
        {
            get { return weights; }
        }
        public IAlgebraTerm[] Terms
        {
            get { return terms; }
        }
        public T[] CastTerms<T>()
        {
            if (!Terms.All(t=> t is T))
                throw new Exception("Algebra : unable to cast terms");

            return Terms.Cast<T>().ToArray();
        }

        public Algebra Add(IAlgebraTerm term)
        {
            return Add(this, new Algebra(new[] { 1.0 }, new[] { term }, false));
        }
        public static Algebra Add(Algebra leftLc, Algebra rightLc)
        {
            IList<double> ws = new List<double>();
            List<IAlgebraTerm> ts = new List<IAlgebraTerm>();
            AddTerms(leftLc.weights, leftLc.terms, ref ws, ref ts);
            AddTerms(rightLc.weights, rightLc.terms, ref ws, ref ts);
            return new Algebra(ws.ToArray(), ts.ToArray(), false);
        }
        public Algebra Mult(IAlgebraTerm term)
        {
            return new Algebra(weights, terms.Map(t => t.Mult(term)), true);
        }
    }

}