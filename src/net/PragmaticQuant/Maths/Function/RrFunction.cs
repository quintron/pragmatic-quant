﻿using System;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Maths.Interpolation;

namespace PragmaticQuant.Maths.Function
{
    public abstract class RrFunction
    {
        public abstract double Eval(double x);

        public abstract RrFunction Derivative();
        public abstract RrFunction Integral(double basePoint);
        
        public virtual RrFunction Add(RrFunction other)
        {
            return RrFunctions.Sum(this, other);
        }
        public virtual RrFunction Mult(RrFunction other)
        {
            var step = other as StepFunction;
            if (step != null)
                return step.Mult(this);

            return RrFunctions.Product(this, other);
        }
        public virtual RrFunction Inverse()
        {
            return new FractionRrFunction(1.0, new[] {RrFunctions.Constant(1.0)}, new[] {this});
        }
        
        public static implicit operator RrFunction(double cst)
        {
            return RrFunctions.Constant(cst);
        }
        public static RrFunction operator +(RrFunction f, RrFunction g)
        {
            return f.Add(g);
        }
        public static RrFunction operator -(RrFunction f, RrFunction g)
        {
            return f.Add(g.Mult(-1.0));
        }
        public static RrFunction operator *(RrFunction f, RrFunction g)
        {
            return f.Mult(g);
        }
        public static RrFunction operator /(RrFunction f, RrFunction g)
        {
            return f * g.Inverse();
        }

    }

    public static class RrFunctions
    {
        public static readonly RrFunction Zero = new ConstantRrFunction(0.0);
        public static RrFunction Constant(double value)
        {
            if (DoubleUtils.EqualZero(value))
                return Zero;
            return new ConstantRrFunction(value);
        }
        public static RrFunction Exp(double slope)
        {
            return ExpRrFunction.Create(1.0, slope);
        }
        public static RrFunction Affine(double slope, double origin)
        {
            if (DoubleUtils.EqualZero(slope))
                return Constant(origin);

            var linearTerm = ExpRrFunction.Create(slope, 0.0, 1.0);
            
            if (DoubleUtils.EqualZero(origin))
                return linearTerm;

            return linearTerm + origin;
        }
        public static RrFunction Func(Func<double, double> f)
        {
            return new FuncRrFunction(f);
        }
        public static RrFunction Sum(params RrFunction[] functions)
        {
            return LinearCombinationRrFunction.Create(functions.Select(f => 1.0).ToArray(), functions);
        }
        public static RrFunction LinearCombination(double[] weights, RrFunction[] funcs)
        {
            return LinearCombinationRrFunction.Create(weights, funcs);
        }
        public static RrFunction Product(RrFunction f, RrFunction g)
        {
            return new FractionRrFunction(1.0, f, g);
        }

        public static RrFunction LinearInterpolation(double[] abscissae, double[] values,
                                                     double leftExtrapolationSlope = 0.0, double rightExtrapolationSlope = 0.0)
        {
            return SplineInterpoler.BuildLinearSpline(abscissae, values, leftExtrapolationSlope, rightExtrapolationSlope);
        }

        public static RrFunction[,] MatrixProd(this RrFunction[,] a, RrFunction[,] b)
        {
            var result = new RrFunction[a.GetLength(0), b.GetLength(1)];
            if (a.GetLength(1) != b.GetLength(0))
                throw new Exception("MatrixProd : a.GetLength(1) should be equal to b.GetLength(0)");

            for (int i = 0; i < result.GetLength(0); i++)
            {
                for (int j = 0; j < result.GetLength(1); j++)
                {
                    result[i, j] = 0.0;
                    for (int k = 0; k < a.GetLength(1); k++)
                    {
                        result[i, j] += a[i, k] * b[k, j];
                    }
                }
            }
            return result;
        }
        public static RrFunction[,] Tranpose(this RrFunction[,] a)
        {
            var result = new RrFunction[a.GetLength(0), a.GetLength(1)];
            for (int i = 0; i < a.GetLength(0); i++)
                for (int j = 0; j < a.GetLength(1); j++)
                    result[j, i] = a[i, j];
            return result;
        } 
    }
}
