﻿using PragmaticQuant.Core.Sobol;
using PragmaticQuant.Basic;

namespace PragmaticQuant.Maths
{
    public interface IRandomGenerator : ICloneable<IRandomGenerator>
    {
        void SkipTo(int skip);
        double[] Next();
        int Dim { get; }
    }

    public interface IRandomGeneratorFactory
    {
        IRandomGenerator Build(int dimension);
    }

    public static class RandomGenerators
    {
        public static IRandomGeneratorFactory GaussianSobol(SobolDirection sobolDirection = SobolDirection.Kuo3)
        {
            return new GaussSobolGeneratorFactory(sobolDirection);
        }

        #region private class
        private class GaussSobolGeneratorFactory : IRandomGeneratorFactory
        {
            private readonly SobolDirection sobolDirection;
            public GaussSobolGeneratorFactory(SobolDirection sobolDirection)
            {
                this.sobolDirection = sobolDirection;
            }
            public IRandomGenerator Build(int dimension)
            {
                return new GaussianSobolGenerator(new SobolRsg(dimension, sobolDirection));
            }
        }
        #endregion
    }

    internal sealed class GaussianSobolGenerator : IRandomGenerator
    {
        #region private fields
        private readonly SobolRsg sobol;
        private readonly int dim;
        #endregion
        public GaussianSobolGenerator(SobolRsg sobol)
        {
            this.sobol = sobol;
            dim = sobol.Dimension;
        }

        public double[] Next()
        {
            double[] uniforms = sobol.NextSequence();
            var gaussians = new double[dim];
            for (int i = 0; i < gaussians.Length; i++)
                gaussians[i] = NormalDistribution.FastCumulativeInverse(uniforms[i]);
            return gaussians;
        }
        public int Dim
        {
            get { return dim; }
        }
        public void SkipTo(int skip)
        {
            sobol.SkipTo(skip);
        }
        public IRandomGenerator Clone()
        {
            return new GaussianSobolGenerator(sobol.Clone());
        }
    }
    
}