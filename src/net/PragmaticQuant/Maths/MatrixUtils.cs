﻿using System;
using System.Collections.Generic;
using System.Linq;
using PragmaticQuant.Core;
using PragmaticQuant.Basic;

namespace PragmaticQuant.Maths
{
    public static class MatrixUtils
    {
        /// <summary>
        /// Compute the matrix product : a * b, stored in result
        /// </summary>
        /// <param name="result"></param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        public static void Prod(ref double[,] result, double[,] a, double[,] b)
        {
            if (result.GetLength(0) != a.GetLength(0))
                throw new Exception("MatrixUtils : result.GetLength(0) should be equal to a.GetLength(0)");
            if (result.GetLength(1) != b.GetLength(1))
                throw new Exception("MatrixUtils : result.GetLength(1) should be equal to b.GetLength(1)");
            if (a.GetLength(1) != b.GetLength(0))
                throw new Exception("MatrixUtils : a.GetLength(1) should be equal to b.GetLength(0)");

            for (int i = 0; i < result.GetLength(0); i++)
            {
                for (int j = 0; j < result.GetLength(1); j++)
                {
                    result[i, j] = 0.0;
                    for (int k = 0; k < a.GetLength(1); k++)
                    {
                        result[i, j] += a[i, k] * b[k, j];
                    }
                }
            }
        }
        
        /// <summary>
        /// Multiply matrix row with index rowIndex by  w
        /// </summary>
        public static void MultRow(ref double[,] result, int rowIndex, double w)
        {
            if (rowIndex < result.GetLowerBound(0) || rowIndex > result.GetUpperBound(0))
                throw new IndexOutOfRangeException();
            for (int j = 0; j < result.GetLength(1); j++)
            {
                result[rowIndex, j] *= w;
            }
        }

        /// <summary>
        /// Compute the matrix transposition of a, stored in result
        /// </summary>
        /// <param name="result"></param>
        /// <param name="a"></param>
        public static void Tranpose(ref double[,] result, double[,] a)
        {
            if (result.GetLength(0) != a.GetLength(1))
                throw new Exception("MatrixUtils : result.GetLength(0) should be equal to a.GetLength(1)");
            if (result.GetLength(1) != a.GetLength(0))
                throw new Exception("MatrixUtils : result.GetLength(1) should be equal to a.GetLength(0)");

            for (int i = 0; i < a.GetLength(0); i++)
                for (int j = 0; j < a.GetLength(1); j++)
                    result[j, i] = a[i, j];
        }

        public static void CorrelFromCovariance(ref double[,] covariance)
        {
            if (covariance.GetLength(0)!=covariance.GetLength(1))
                throw new Exception("Square matrix expected");
            int size = covariance.GetLength(0);
            for (int i = 0; i < size; i++)
            {
                var stdDev = Math.Sqrt(covariance[i, i]);
                for (int j = 0; j < size; j++)
                {
                    covariance[i, j] /= stdDev;
                    covariance[j, i] /= stdDev;
                }
            }
        }

        public static double[,] Prod(this double[,] m, double[,] a)
        {
            var result = new double[m.GetLength(0), a.GetLength(1)];
            Prod(ref result, m, a);
            return result;
        }
        public static double[,] Tranpose(this double[,] m)
        {
            var result = new double[m.GetLength(1), m.GetLength(0)];
            Tranpose(ref result, m);
            return result;
        }
        public static double[,] Correlation(this double[,] covariance)
        {
            var correl = covariance.Copy();
            CorrelFromCovariance(ref correl);
            return correl;
        }

        public static double[,] Id(int size)
        {
            return Diagonal(size, 1.0);
        }
        public static double[,] Diagonal(int size, double a)
        {
            var result = new double[size, size];
            for (int i = 0; i < size; i++)
                result[i, i] = a;
            return result;
        }
        public static double[,] Diagonal(double[] d)
        {
            var result = new double[d.Length, d.Length];
            for (int i = 0; i < d.Length; i++)
                result[i, i] = d[i];
            return result;
        }

        /// <summary>
        /// Compute transpose(left) * m * right
        /// </summary>
        /// <param name="m"></param>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static double BilinearProd(this double[,] m, double[] left, double[] right)
        {
            double prod = 0.0;
            for (int i = 0; i < m.GetLength(0); i++)
            {
                double partial = 0.0;
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    partial += m[i, j] * right[j];
                }
                prod += left[i] * partial;
            }
            return prod;
        }

        public static bool IsSymmetric(this double[,] m)
        {
            if (m.GetLength(0) != m.GetLength(1))
                return false;
            for (int i = 0; i < m.GetLength(0); i++)
            {
                for (int j = 0; j < i; j++)
                {
                    if (!DoubleUtils.MachineEquality(m[i, j], m[j, i]))
                        return false;
                }
            }
            return true;
        }

        public static double[,] EigenValueSquareRoot(this double[,] m, bool truncateZeroEigenValues = false)
        {
            var svd = new Svd(m);

            double[] eigenSqrt;

            if (truncateZeroEigenValues)
            {
                var eigenSqrtList = new List<double>(svd.W.Length);
                for (int i = 0; i < svd.W.Length; i++)
                {
                    if (svd.W[i] <= 0.0)
                        break;
                    eigenSqrtList.Add(Math.Sqrt(svd.W[i]));
                }
                eigenSqrt = eigenSqrtList.ToArray();
            }
            else
            {
                eigenSqrt = svd.W.Map(Math.Sqrt);
            }
            
            var truncU = svd.U.SubArray(0, svd.U.GetLength(0), 0, eigenSqrt.Length);
            var sqrt = truncU.Prod(Diagonal(eigenSqrt));
            
            return sqrt;
        }
    }

    public static class RegressionUtils
    {
        #region private methods
        private static double[] BasisCov(double[][] basis, double[] target)
        {
            var dim = basis.First().Length;
            var result = new double[dim];
            for (int sample = 0; sample < basis.Length; sample++)
            {
                var x = basis[sample];
                var t = target[sample] / target.Length;
                for (int j = 0; j < result.Length; j++)
                    result[j] += x[j] * t;
            }
            return result;
        }
        private static double[,] CovarianceMatrix(double[][] samples)
        {
            var dim = samples.First().Length;
            var cov = new double[dim, dim];
            for (int sample = 0; sample < samples.Length; sample++)
            {
                var x = samples[sample];
                for (int i = 0; i < dim; i++)
                {
                    for (int j = 0; j < i; j++)
                    {
                        var covij = x[i] * x[j] / samples.Length;
                        cov[i, j] += covij;
                        cov[j, i] += covij;
                    }
                    cov[i, i] += x[i] * x[i] / samples.Length;
                }
            }

            return cov;
        }
        #endregion
        public static double[] Regression(double[][] sample, double[] y, double eigenValEps = 1.0e-12)
        {
            var svdCov = new Svd(CovarianceMatrix(sample));
            return svdCov.Solve(BasisCov(sample, y), svdCov.W[0] * eigenValEps);
        }

        public static Polynomial[] PseudoOrthogonalPolynomials(double[] sample, int degree)
        {
            if (degree>sample.Length)
                throw new Exception("RegressionUtils.PseudoOrthogonalPolynomials : too few sample for this degree");

            var sortedSample = sample.OrderBy(s => s).ToArray();
            int n = sortedSample.Length;

            var polynoms = new Polynomial[degree + 1];

            polynoms[0] = Polynomial.One;
            var rootIndexes = new List<int> {n / 2};
            for (int d = 1; d <= degree; d++)
            {
                polynoms[d] = rootIndexes.Aggregate(Polynomial.One,
                    (p, ri) => p * (Polynomial.X - sortedSample[ri]));

                var interlacedRootIndexes = new List<int>
                {
                    rootIndexes.First() / 2,
                    (n + rootIndexes.Last()) / 2
                };
                for (int i = 1; i < rootIndexes.Count; i++) 
                    interlacedRootIndexes.Add((rootIndexes[i] + rootIndexes[i - 1]) / 2);

                rootIndexes = interlacedRootIndexes;
            }
          
            return polynoms;
        }
    }

}