﻿namespace PragmaticQuant.Maths.Stochastic
{
    public interface IProcessPathGenerator
    {
        IProcessPath Path(double[][] dWs);
        double[] Dates { get; }
        int ProcessDim { get; }
        int RandomDim { get; }

        double[] AllSimulatedDates { get; }
    }
}