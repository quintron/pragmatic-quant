using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Maths.Function;

namespace PragmaticQuant.Maths.Stochastic
{
    public class OrnsteinUhlenbeck
    {
        public OrnsteinUhlenbeck(double meanReversion, RrFunction drift, RrFunction volatility, double value0)
        {
            Value0 = value0;
            Volatility = volatility;
            Drift = drift;
            MeanReversion = meanReversion;
        }
        public double MeanReversion { get; private set; }
        public RrFunction Drift { get; private set; }
        public RrFunction Volatility { get; private set; }
        public double Value0 { get; private set; }
        public int Dimension { get { return 1; } }
    }

    public static class OrnsteinUhlenbeckUtils
    {
        public static RrFunction IntegratedDrift(RrFunction instantDrift, double meanReversion, double startDate = 0.0)
        {
            var integratedExpDrift = (instantDrift * RrFunctions.Exp(meanReversion)).Integral(startDate);
            return integratedExpDrift * RrFunctions.Exp(-meanReversion);

        }
        public static RrFunction IntegratedVariance(RrFunction instantVolatility, double meanReversion, double startDate = 0.0)
        {
            return IntegratedCovariance(instantVolatility * instantVolatility, meanReversion, meanReversion, startDate);
        }
        public static RrFunction IntegratedCovariance(RrFunction instantCovariance, double meanReversion1, double meanReversion2, double startDate = 0.0)
        {
            var integratedExpCov = (instantCovariance * RrFunctions.Exp(meanReversion1 + meanReversion2)).Integral(startDate);
            return integratedExpCov * RrFunctions.Exp(-(meanReversion1 + meanReversion2));
        }
        public static RrFunction[,] IntegratedCovariance(RrFunction[,] instantCovariance, double[] meanReversions, double startDate = 0.0)
        {
            if (instantCovariance.GetLength(0) != meanReversions.Length
                || instantCovariance.GetLength(1) != meanReversions.Length)
                throw new Exception("OrnsteinUhlenbeckUtils : incompatible format !");
            var cov = new RrFunction[meanReversions.Length, meanReversions.Length];
            for (int i = 0; i < meanReversions.Length; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    var cov_ij = IntegratedCovariance(instantCovariance[i, j], meanReversions[i], meanReversions[j], startDate);
                    cov[i, j] = cov_ij;
                    if (j < i) cov[j, i] = cov_ij;
                }
            }
            return cov;
        }
        public static RrFunction[,] IntegratedCovariance(this OrnsteinUhlenbeckNd ou, double start = 0.0)
        {
            return IntegratedCovariance(ou.Covariances, ou.MeanReversions, start);
        }

        public static OrnsteinUhlenbeckNd AddDrift(this OrnsteinUhlenbeckNd ou, RrFunction[] drifts)
        {
            var newDrifts = ou.Drifts.ZipWith(drifts, (d1, d2) => d1 + d2);
            return new OrnsteinUhlenbeckNd(ou.MeanReversions, newDrifts, ou.Covariances, ou.Value0);
        }
        public static OrnsteinUhlenbeckNd Correlate(OrnsteinUhlenbeckNd ou1, OrnsteinUhlenbeckNd ou2, RrFunction[,] crossCorrelation)
        {
            var value0 = EnumerableUtils.Append(ou1.Value0, ou2.Value0);
            var meanRevs = EnumerableUtils.Append(ou1.MeanReversions, ou2.MeanReversions);
            var drifts = EnumerableUtils.Append(ou1.Drifts, ou2.Drifts);

            var dim1 = ou1.Dimension;
            var dim2 = ou2.Dimension;
            var covariances = new RrFunction[dim1 + dim2, dim1 + dim2];
            ArrayUtils.SetSubArray(ref covariances, 0, 0, ou1.Covariances);
            ArrayUtils.SetSubArray(ref covariances, dim1, dim1, ou2.Covariances);
            for (int i = 0; i < dim1; i++)
            {
                for (int j = 0; j < dim2; j++)
                {
                    covariances[i, dim1 + j] = crossCorrelation[i, j];
                    covariances[dim1 + j, i] = crossCorrelation[i, j];
                }
            }
            return new OrnsteinUhlenbeckNd(meanRevs, drifts, covariances, value0);
        }
    }

    public class OrnsteinUhlenbeckNd
    {
        public OrnsteinUhlenbeckNd(double[] meanReversions, RrFunction[,] covariances, double[] value0)
            : this(meanReversions, ArrayUtils.Constant(meanReversions.Length, RrFunctions.Zero), covariances, value0)
        {}
        public OrnsteinUhlenbeckNd(double[] meanReversions, RrFunction[] drifts, RrFunction[,] covariances, double[] value0)
        {
            if (meanReversions.Length != drifts.Length
                || meanReversions.Length != value0.Length
                || meanReversions.Length != covariances.GetLength(0)
                || covariances.GetLength(1) != covariances.GetLength(0))
                throw new Exception("OrnsteinUhlenbeckNd : invalid size !");

            MeanReversions = meanReversions;
            Drifts = drifts;
            Covariances = covariances;
            Value0 = value0;
        }
        public static OrnsteinUhlenbeckNd Brownian1D(RrFunction vol, double value0 = 0.0)
        {
            return new OrnsteinUhlenbeckNd(new[] {0.0}, new[,] {{vol * vol}}, new[] {value0});
        }
        
        public double[] MeanReversions { get; private set; }
        public RrFunction[] Drifts { get; private set; }
        public RrFunction[,] Covariances { get; private set; }
        public double[] Value0 { get; private set; }
        public int Dimension
        {
            get { return MeanReversions.Length; }
        }
    }

}