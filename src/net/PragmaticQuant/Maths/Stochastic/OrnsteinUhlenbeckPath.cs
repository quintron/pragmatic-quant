using System;
using PragmaticQuant.Core;
using PragmaticQuant.Basic;

namespace PragmaticQuant.Maths.Stochastic
{
    public sealed class OrnsteinUhlenbeckPath 
    {
        #region private fields
        private readonly double[][] stepSlopes;
        private readonly double[][] stepDrifts;
        private readonly double[][,] stepVolDecomposition;
        private readonly double[] initialValue;
        #endregion
        public OrnsteinUhlenbeckPath(double[] initialValue, double[][] stepSlopes, double[][] stepDrifts, double[][,] stepVolDecomposition)
        {
            this.initialValue = initialValue;
            this.stepSlopes = stepSlopes;
            this.stepDrifts = stepDrifts;
            this.stepVolDecomposition = stepVolDecomposition;
            ProcessDim = initialValue.Length;
        }
        
        public void ComputePathIncrements(ref double[][] dWs)
        {
            var currentProcess = initialValue.Copy();
            var noises = new double[currentProcess.Length];
            for (int i = 0; i < dWs.Length; i++)
            {
                var slopes = stepSlopes[i];
                var drifts = stepDrifts[i];
                
                var stepIncrement = dWs[i];
                Matrix.FillProd(ref noises, stepVolDecomposition[i], stepIncrement);
                for (int j = 0; j < currentProcess.Length; j++)
                {
                    var dx = currentProcess[j] * (slopes[j] - 1.0) + drifts[j] + noises[j];
                    currentProcess[j] += dx;
                    stepIncrement[j] = dx;
                }
            }
        }
        public int ProcessDim { get; private set; }
        
    }

    public static class OrnsteinUhlenbeckPathFactory
    {
        #region private methods
        ///<summary>
        /// exp(-(end-start) * meanrev)
        ///</summary>
        private static double[] StepSlopes(double start, double end, OrnsteinUhlenbeckNd ouProcess)
        {
            var step = end - start;
            var slopes = ouProcess.MeanReversions.Copy();
            slopes.Mult(-step);
            slopes.ApplyExp();
            return slopes;
        }
        private static double[] StepDrift(double start, double end, OrnsteinUhlenbeckNd ouProcess)
        {
            return ouProcess.Drifts.ZipWith(ouProcess.MeanReversions,
                (drift, meanReversion) => OrnsteinUhlenbeckUtils.IntegratedDrift(drift, meanReversion, start).Eval(end));
        }
        private static double[,] StepVol(double start, double end, OrnsteinUhlenbeckNd ouProcess)
        {
            var step = end - start;
            if (step < 0.0)
                throw new Exception("OrnsteinUhlenbeckPathFactory: start must be lower than end");
            
            if (DoubleUtils.EqualZero(step))
                return new double[ouProcess.Covariances.GetLength(0), ouProcess.Covariances.GetLength(1)];
            
            var stepIntegralCov = ouProcess.IntegratedCovariance(start)
                                           .Map(cov => cov.Eval(end) / step);

            return stepIntegralCov.EigenValueSquareRoot();
        }
        #endregion
        public static OrnsteinUhlenbeckPath Build(double[] dates, OrnsteinUhlenbeckNd ouProcess)
        {
            var stepSlopes = new double[dates.Length][];
            var stepDrifts = new double[dates.Length][];
            var stepDecomp = new double[dates.Length][,];
            for (int i = 0; i < dates.Length; i++)
            {
                var previous = (i == 0) ? 0.0 : dates[i - 1];

                stepSlopes[i] = StepSlopes(previous, dates[i], ouProcess);
                stepDrifts[i] = StepDrift(previous, dates[i], ouProcess);
                stepDecomp[i] = StepVol(previous, dates[i], ouProcess);
            }

            return new OrnsteinUhlenbeckPath(ouProcess.Value0, stepSlopes, stepDrifts, stepDecomp);
        }
        public static OrnsteinUhlenbeckPath_Fast Build_Fast(double[] dates, OrnsteinUhlenbeckNd ouProcess)
        {
            var stepSlopes = new double[dates.Length][];
            var stepDrifts = new double[dates.Length][];
            var stepDecomp = new double[dates.Length][,];
            for (int i = 0; i < dates.Length; i++)
            {
                var previous = (i == 0) ? 0.0 : dates[i - 1];

                stepSlopes[i] = StepSlopes(previous, dates[i], ouProcess);
                stepDrifts[i] = StepDrift(previous, dates[i], ouProcess);
                stepDecomp[i] = StepVol(previous, dates[i], ouProcess);
            }

            return new OrnsteinUhlenbeckPath_Fast(ouProcess.Value0, stepSlopes, stepDrifts, stepDecomp);
        }
    }

}