using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Markets.Equity;

namespace PragmaticQuant.Model
{
    public class BlackLawQuantileProvider
    {
        #region private fields
        private readonly VolatilitySurface volSurface;
        private readonly Func<DateTime, double> forwardCurve;
        #endregion
        public BlackLawQuantileProvider(VolatilitySurface volSurface, Func<DateTime, double> forwardCurve)
        {
            this.volSurface = volSurface;
            this.forwardCurve = forwardCurve;
        }
        public RealInterval LogSpotSupport(DateTime maturity, double quantileInf, double quantileSup)
        {
            var quantileInfSup = LogSpotQuantiles(maturity, quantileInf, quantileSup);
            return RealInterval.Compact(quantileInfSup[0], quantileInfSup[1]);
        }
        public double[] LogSpotQuantiles(DateTime maturity, params double[] quantiles)
        {
            double fwd = forwardCurve(maturity);
            double t = volSurface.Time[maturity];
            var impliedVar = volSurface.VarianceFromMoneyness(t) / t;
            Func<double, double> vol = k => Math.Sqrt(impliedVar.Eval(Math.Log(k)));
            return quantiles.Map(q =>
            {
                var moneyness = Math.Max(DoubleUtils.MachineEpsilon, SmileLawUtils.ComputeQuantile(1.0, t, vol, q));
                return Math.Log(moneyness * fwd);

            });
        }
    }
}