using System;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Maths.Stochastic;

namespace PragmaticQuant.Model
{
    public sealed class CompositePathGenerator : IProcessPathGenerator
    {
        #region private fields
        private readonly ISingleRiskPathGenerator[] pathGens;
        private readonly ProcessRange[] processRanges;
        private readonly ProcessRange[] brownianRanges;
        #endregion
        internal CompositePathGenerator(ISingleRiskPathGenerator[] pathGens)
        {
            this.pathGens = pathGens;
            ProcessDim = pathGens.Select(p=>p.ProcessDim).Sum();
            RandomDim = pathGens.Select(p => p.RandomDim).Sum();
            Dates = pathGens.First().Dates;
            AllSimulatedDates = pathGens.First().AllSimulatedDates;

            if (pathGens.Any(pg => pg.AllSimulatedDates.Length != AllSimulatedDates.Length)
                && pathGens.Any(pg => pg.Dates.Length != Dates.Length))
                throw new Exception("CompositePathGenerator : incompatible path generators");
            //TODO more check on dates values
            
            processRanges = pathGens.Select((p, i) => new ProcessRange(i, p.ProcessDim)).ToArray();
            brownianRanges = pathGens.Select((p, i) => new ProcessRange(i, p.RandomDim)).ToArray();
        }

        public IProcessPath Path(double[][] dWs)
        {
            var processPath = ArrayUtils.CreateJaggedArray<double>(Dates.Length, ProcessDim);
            for (int i = 0; i < pathGens.Length; i++)
            {
                pathGens[i].FillProcessPath(ref processPath, dWs, processRanges[i], brownianRanges[i]);
            }
            return new ProcessPath(Dates, ProcessDim, processPath);
        }

        public int ProcessDim { get; private set; }
        public int RandomDim { get; private set; }
        public double[] Dates { get; private set; }
        public double[] AllSimulatedDates { get; private set; }
    }

    public interface ISingleRiskPathGenerator
    {
        void FillProcessPath(ref double[][] processPath, double[][] dWs,
                             ProcessRange processRange, ProcessRange brownianRange);
        int ProcessDim { get; }
        int RandomDim { get; }
        double[] Dates { get; }
        double[] AllSimulatedDates { get; }
    }

    public class ProcessRange
    {
        public readonly int StartIndex;
        public readonly int Size;
        public ProcessRange(int startIndex, int size)
        {
            StartIndex = startIndex;
            Size = size;
        }
    }

}