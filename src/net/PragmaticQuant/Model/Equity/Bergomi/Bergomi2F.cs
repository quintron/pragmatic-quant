﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using PragmaticQuant.Core;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets;
using PragmaticQuant.Maths;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Maths.Stochastic;
using PragmaticQuant.Model.Equity.Dividends;

namespace PragmaticQuant.Model.Equity.Bergomi
{
    public class Bergomi2FModel : Bergomi2FModelBase
    {
        internal Bergomi2FModel(AssetId asset, ITimeMeasure time, double spot, DiscountCurve assetDiscount, DiscreteLocalDividend[] dividends,
            ModelFactorDescription modelFactor,
            StochasticVolParams volParams,
            RrFunction xi)
            : base(asset, time, spot, assetDiscount, dividends, modelFactor,
                volParams, new BergomiStepVolProvider(xi, volParams, time))
        {
            LocalInstantVariance = xi;
        }

        public RrFunction LocalInstantVariance { get; private set; }
    }

    public class Bergomi2FModelBase : EquityModel<AbstractBergomiInstantVol>
    {
        #region private fields
        private readonly ModelFactor process;        
        #endregion
        protected Bergomi2FModelBase(AssetId asset, ITimeMeasure time, double spot, DiscountCurve assetDiscount, DiscreteLocalDividend[] dividends,
                                     ModelFactorDescription modelFactor,
                                     StochasticVolParams volParams,
                                     IStepVolProvider<AbstractBergomiInstantVol> stepVol)
            : base(asset, time, spot, assetDiscount, dividends, stepVol)
        {
            process = modelFactor.ToModelFactor(new[] { 0.0, volParams.X0, volParams.Y0 }, time);
            VolParams = volParams;          
        }
        
        public StochasticVolParams VolParams { get; private set; }
        
        public override RiskFactor[] RiskFactors
        {
            get
            {
                return new[]
                {
                    SpotFactor,                    
                    new RiskFactor(RiskId, "Vol.1"),
                    new RiskFactor(RiskId, "Vol.2")
                };
            }
        }
        public override ModelFactor FactorProcess { get { return process; } }
    }

    public class BergomiStepVolProvider : IStepVolProvider<AbstractBergomiInstantVol>
    {
        #region private fields
        private readonly RrFunction xi;
        private readonly StochasticVolParams volParams;
        private readonly RrFunction[,] ornsteinVolIntegralCovs;
        private readonly ITimeMeasure time;
        #endregion
        internal BergomiStepVolProvider(RrFunction xi,
                                        StochasticVolParams volParams,
                                        ITimeMeasure time)
        {
            this.xi = xi;
            this.volParams = volParams;
            this.time = time;
            ornsteinVolIntegralCovs = volParams.OrnsteinVolDriver()
                                               .IntegratedCovariance();
        }
        public AbstractBergomiInstantVol StepVol(DateTime startDate, DateTime endDate)
        {
            return new BergomiInstantVol(xi, volParams, ornsteinVolIntegralCovs, time[startDate], time[endDate]);
        }
    }

    public abstract class AbstractBergomiInstantVol
    {
        #region protected fields        
        protected readonly double w1;
        protected readonly double w2;
        protected readonly double cvx;
        #endregion
        internal AbstractBergomiInstantVol(StochasticVolParams volParams,
                                           RrFunction[,] ornsteinVolIntegralCovs,
                                           double start)
        {            
            var alpha = volParams.Alpha();
            w1 = 2.0 * (1.0 - volParams.Theta) * volParams.Nu * alpha;
            w2 = 2.0 * volParams.Theta * volParams.Nu * alpha;

            var covar = ornsteinVolIntegralCovs.Map(c => c.Eval(start));
            cvx = -0.5 * covar.BilinearProd(new[] { w1, w2 }, new[] { w1, w2 });
        }       
        public abstract double Vol(double logSpot, double x, double y);
    }

    public sealed class BergomiInstantVol : AbstractBergomiInstantVol
    {
        #region private fields
        private readonly double stepVol;
        #endregion
        internal BergomiInstantVol(RrFunction xi, 
                                   StochasticVolParams volParams,
                                   RrFunction[,] ornsteinVolIntegralCovs,
                                   double start, double end)
            :base(volParams, ornsteinVolIntegralCovs, start)
        {
            stepVol = Math.Sqrt(xi.Integral(start).Eval(end) / (end - start));            
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override double Vol(double logSpot, double x, double y)
        {
            return stepVol * Math.Exp(0.5 * (w1 * x + w2 * y + cvx));
        }
    }
    
    public static class Bergomi2FUtils
    {
        #region private methods
        private static double DoubleExpInt(double k, double t)
        {
            if (Math.Abs(k * k * t * t) > DoubleUtils.MachineEpsilon)
                return (k * t - (1.0 - Math.Exp(-k * t))) / (k * k * t * t);
            return 1.0;
        }
        private static double[,] Correl(StochasticVolParams volParams)
        {
            return new[,] { { 1.0, volParams.RhoXY }, { volParams.RhoXY, 1.0 } };
        }
        private static double[][] VarSwapDeformation(Bergomi2FModel b2F, double[] fwdVolStart, double[] fwdVolEnd)
        {
            Debug.Assert(fwdVolStart.Length == fwdVolEnd.Length);

            var initCurve = b2F.LocalInstantVariance.Integral(0.0);
            var factor1 = (b2F.LocalInstantVariance * RrFunctions.Exp(-b2F.VolParams.K1)).Integral(0.0);
            var factor2 = (b2F.LocalInstantVariance * RrFunctions.Exp(-b2F.VolParams.K2)).Integral(0.0);

            var alpha = Alpha(b2F.VolParams);
            return EnumerableUtils.For(0, fwdVolStart.Length, i =>
            {
                double volMatStart = fwdVolStart[i];
                double volMatEnd = fwdVolEnd[i];

                double initFwdVariance = initCurve.Eval(volMatEnd) - initCurve.Eval(volMatStart);
                double def1 = factor1.Eval(volMatEnd) - factor1.Eval(volMatStart);
                double def2 = factor2.Eval(volMatEnd) - factor2.Eval(volMatStart);
                var def = new[] { (1.0 - b2F.VolParams.Theta) * def1, b2F.VolParams.Theta * def2 };
                def.Mult(b2F.VolParams.Nu * alpha / initFwdVariance);  
                return def;
            });
        }
        #endregion
        public static double Alpha(this StochasticVolParams volParams)
        {
            var correl = Correl(volParams);
            var thetaVect = new[] { 1.0 - volParams.Theta, volParams.Theta };
            return 1.0 / Math.Sqrt(correl.BilinearProd(thetaVect, thetaVect));
        }
        public static double[] FwdVolInstantVol(Bergomi2FModel b2F, double[] fwdVolStart, double[] fwdVolEnd)
        {
            Debug.Assert(fwdVolStart.Length == fwdVolEnd.Length);

            var correl = Correl(b2F.VolParams);
            var varSwapDeformations = VarSwapDeformation(b2F, fwdVolStart, fwdVolEnd);
            return EnumerableUtils.For(0, fwdVolStart.Length, i =>
            {
                var varSwapDef = varSwapDeformations[i];
                return Math.Sqrt(correl.BilinearProd(varSwapDef, varSwapDef));
            });
        }
        public static double[,] FwdVolInstantCovariance(Bergomi2FModel b2F, double[] fwdVolStart, double[] fwdVolEnd)
        {
            Debug.Assert(fwdVolStart.Length == fwdVolEnd.Length);

            var correl = Correl(b2F.VolParams);
            var varSwapDeformations = VarSwapDeformation(b2F, fwdVolStart, fwdVolEnd);
            return ArrayUtils.CartesianProd(varSwapDeformations, varSwapDeformations,
                (varSwapDef1, varSwapDef2) => correl.BilinearProd(varSwapDef1, varSwapDef2));
        }
        public static double[] AtmfSkewApprox(Bergomi2FModel b2F, double[] maturities)
        {
            return AtmfSkewApprox(b2F.VolParams, maturities);
        }
        public static double[] AtmfSkewApprox(StochasticVolParams volParams, double[] maturities)
        {
            var alpha = Alpha(volParams);
            double coeffX = volParams.Nu * alpha * (1.0 - volParams.Theta) * volParams.RhoSX;
            double coeffY = volParams.Nu * alpha * volParams.Theta * volParams.RhoSY;

            return maturities.Map(t => coeffX * DoubleExpInt(volParams.K1, t) + coeffY * DoubleExpInt(volParams.K2, t));
        }
    }

}
