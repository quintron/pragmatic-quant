using System;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Maths.Stochastic;
using PragmaticQuant.Product;

namespace PragmaticQuant.Model.Equity.Bergomi
{
    public class Bergomi2FModelCalibDesc : EquityModelCalibDesc
    {
        public Bergomi2FModelCalibDesc(string asset, RawMapDatas<DateOrDuration, double> dividendYieldProportion, 
                                       StochasticVolParams volParams)
            : this(asset, dividendYieldProportion, volParams, null, null) { }
        public Bergomi2FModelCalibDesc(string asset, RawMapDatas<DateOrDuration, double> dividendYieldProportion, 
                                       StochasticVolParams volParams, 
                                       DateOrDuration[] calibrationMaturities, double[] calibrationVols) 
            : base(asset, dividendYieldProportion)
        {
            VolParams = volParams;
            CalibrationMaturities = calibrationMaturities;
            CalibrationVols = calibrationVols;

            if (calibrationVols != null && calibrationVols.Any())
            {
                if (calibrationMaturities == null)
                    throw new Exception("Bergomi2FModelCalibDesc : missing calibrationMaturities");

                if ( calibrationMaturities.Length != calibrationVols.Length)
                    throw new Exception("Bergomi2FModelCalibDesc : calibrationVols is incompatible with calibrationMaturities !");
            }
        }

        public DateOrDuration[] CalibrationMaturities { get; private set; }
        public double[] CalibrationVols { get; private set; }

        public StochasticVolParams VolParams { get; private set; }
    }

    public class StochasticVolParams
    {
        public StochasticVolParams(double k1, double k2, double theta, double nu, 
                                   double rhoXy, double rhoSx, double rhoSy, 
                                   double x0 = 0.0, double y0 = 0.0)
        {
            K1 = k1;
            K2 = k2;
            Theta = theta;
            Nu = nu;
            RhoXY = rhoXy;
            RhoSX = rhoSx;
            RhoSY = rhoSy;
            X0 = x0;
            Y0 = y0;
        }
        public double K1 { get; private set; }
        public double K2 { get; private set; }
        public double Theta { get; private set; }
        public double Nu { get; private set; }
        public double RhoXY { get; private set; }
        public double RhoSX { get; private set; }
        public double RhoSY { get; private set; }

        public double X0 { get; private set; }
        public double Y0 { get; private set; }
    }

    public static class StochasticVolParamsUtils
    {
        public static double[,] Correlation(this StochasticVolParams volParams)
        {
            return new[,]
            {
                {1.0, volParams.RhoSX, volParams.RhoSY},
                {volParams.RhoSX, 1.0, volParams.RhoXY},
                {volParams.RhoSY, volParams.RhoXY, 1.0}
            };
        }
        public static OrnsteinUhlenbeckNd OrnsteinDriver(this StochasticVolParams volParams)
        {
            return new OrnsteinUhlenbeckNd(new[] {0.0, volParams.K1, volParams.K2},
                                            volParams.Correlation().Map(RrFunctions.Constant),
                                            new[] {0.0, volParams.X0, volParams.Y0});
        }
        public static OrnsteinUhlenbeckNd OrnsteinVolDriver(this StochasticVolParams volParams)
        {
            var volsDriverCovs = new [,]
            {
                {1.0, volParams.RhoXY},
                {volParams.RhoXY, 1.0}
            };
            return new OrnsteinUhlenbeckNd(new[] {volParams.K1, volParams.K2},
                                           volsDriverCovs.Map(RrFunctions.Constant), 
                                           new[] {volParams.X0, volParams.Y0});
        }
    }

    public class Bergomi2FModelCalibration : EquityVolModelCalibration<Bergomi2FModelCalibDesc>
    {
        #region private methods
        private DateOrDuration[] VolTermPillars(AssetMarket assetMkt, Bergomi2FModelCalibDesc model)
        {
            bool pillarsFromMarket = model.CalibrationMaturities == null || !model.CalibrationMaturities.Any();
            if (pillarsFromMarket)
                return assetMkt.VolMatrix.Maturities.Map(m => (DateOrDuration) m);

            return model.CalibrationMaturities; 
        }
        private double[] VolTerm(AssetMarket assetMkt, Bergomi2FModelCalibDesc model, DateOrDuration[] volTermPillars)
        {
            bool logSwapVolFromMarket = model.CalibrationVols == null || !model.CalibrationVols.Any();
            if (logSwapVolFromMarket)
            {
                var refDate = assetMkt.RefDate;
                var logSwapCurve = assetMkt.LogSwapVolCurve();
                return volTermPillars.Map(p => logSwapCurve.Volatility(p.ToDate(refDate)));
            }
            return model.CalibrationVols;
        }
        #endregion
        public static readonly Bergomi2FModelCalibration Instance = new Bergomi2FModelCalibration();

        protected override EquityModelDescription CalibrateSingleAsset(AssetMarket assetMkt, Bergomi2FModelCalibDesc model, IProduct product)
        {
            var divModel = model.DividendYieldProportion[assetMkt.Asset.Name];
            var volTermPillars = VolTermPillars(assetMkt, model);
            var volTerm = new RawMapDatas<DateOrDuration, double>(volTermPillars, VolTerm(assetMkt, model, volTermPillars));
            return new Bergomi2FModelDescription(assetMkt.Asset, assetMkt.AssetCurrency, divModel, model.VolParams, volTerm);
        }
    }

}