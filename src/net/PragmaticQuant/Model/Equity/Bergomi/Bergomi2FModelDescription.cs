using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets;
using PragmaticQuant.Markets.Equity;
using PragmaticQuant.Maths;

namespace PragmaticQuant.Model.Equity.Bergomi
{
    public class Bergomi2FModelDescription : EquityModelDescription
    {
        #region private fields
        private RawMapDatas<DateOrDuration, double>[] Vols()
        {
            return new[] { ConstantRawFunc(1.0), ConstantRawFunc(1.0), ConstantRawFunc(1.0) };
        }
        #endregion

        public Bergomi2FModelDescription(AssetId asset, Currency currency, 
                                         RawMapDatas<DateOrDuration, double> dividendYieldProportion, 
                                         StochasticVolParams volParams,                             
                                         RawMapDatas<DateOrDuration, double> volTerm)
            : base(asset, currency, dividendYieldProportion)
        {
            VolTerm = volTerm;
            VolParams = volParams;
        }
        
        public RawMapDatas<DateOrDuration, double> VolTerm { get; private set; }

        public StochasticVolParams VolParams { get; private set; }
        
        public override RiskFactor[] RiskFactors
        {
            get
            {
                return new[]
                {
                    SpotFactor,
                    new RiskFactor(Risk, "Vol.1"),
                    new RiskFactor(Risk, "Vol.2")
                };
            }
        }
        
        public override ModelFactorDescription ModelFactorDescription
        {
            get
            {
                return new ModelFactorDescription(RiskFactors.Map(f=>f.Name),
                    new[] { 0.0, VolParams.K1, VolParams.K2 }, 
                    MatrixUtils.Id(3), VolParams.Correlation(), Vols());
            }
        }
    }

    internal class Bergomi2FModelFactory : EquityModelFactory<Bergomi2FModelDescription, AbstractBergomiInstantVol>
    {
        public static readonly Bergomi2FModelFactory Instance = new Bergomi2FModelFactory();
        protected override EquityModel<AbstractBergomiInstantVol> BuildEquityModel(Bergomi2FModelDescription b2F, AffineDividend[] affineDivModel, AssetMarket assetMkt)
        {
            ITimeMeasure time = assetMkt.Time;
            var xi = ModelFactoryUtils.InstantVarianceFunction(b2F.VolTerm, time);
            var model = new Bergomi2FModel(assetMkt.Asset,
                                           time,
                                           assetMkt.Spot,
                                           assetMkt.AssetDiscountCurve,
                                           affineDivModel.DivModel(),
                                           b2F.ModelFactorDescription,
                                           b2F.VolParams, 
                                           xi);
            return model;
        }
    }

}