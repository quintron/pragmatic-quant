using System;
using System.Diagnostics;
using PragmaticQuant.Basic;
using PragmaticQuant.Model.Equity.Dividends;

namespace PragmaticQuant.Model.Equity.Bergomi
{
    public sealed class BergomiPathGenerator : ISingleRiskPathGenerator
    {
        #region private fields
        private readonly AbstractBergomiInstantVol[] vols;
        private readonly double[] dts;
        private readonly bool[] isDivDates;
        private readonly double[] logDiscounts;
        private readonly DiscreteLocalDividend[] divs;
        private readonly int[] subStepSizes;
        private readonly double horizonInitialLogForward;
        private readonly double x0;
        private readonly double y0;
        #endregion
        public BergomiPathGenerator(EquitySimulatorStepDatas<AbstractBergomiInstantVol>[] stepDatas, 
                                    double horizonInitialForward, double x0, double y0)
        {
            this.x0 = x0;
            this.y0 = y0;
            
            horizonInitialLogForward = Math.Log(horizonInitialForward);
            vols = EnumerableUtils.Append(stepDatas.Map(step => step.VolDatas));
            logDiscounts = EnumerableUtils.Append(stepDatas.Map(step => step.HorizonDiscounts.Map(Math.Log)));
            divs = EnumerableUtils.Append(stepDatas.Map(step => step.Dividends));
            isDivDates = EnumerableUtils.Append(stepDatas.Map(step => step.IsDivDates));

            dts = EnumerableUtils.Append(stepDatas.Map(step =>
            {
                var stepDates = step.Dates;
                var stepDts = new double[stepDates.Length];
                double prev = step.Step.Inf;
                for (int i = 0; i < stepDts.Length; i++)
                {
                    stepDts[i] = stepDates[i] - prev;
                    prev = stepDates[i];
                }
                return stepDts;
            }));

            subStepSizes = stepDatas.Map(step => step.Dates.Length);
            Dates = stepDatas.Map(step => step.Step.Sup);
            AllSimulatedDates = EnumerableUtils.Append(stepDatas.Map(data => data.Dates));
        }

        public void FillProcessPath(ref double[][] processPath, double[][] dWs, 
                                    ProcessRange processRange, ProcessRange brownianRange)
        {
            Debug.Assert(processRange.Size == ProcessDim && brownianRange.Size == RandomDim);
            int processIndex = processRange.StartIndex;
            int brownianIndex = brownianRange.StartIndex;

            double currentLogFwd = horizonInitialLogForward; 
            double x = x0, y = y0;
            for (int step = 0, index = 0; step < Dates.Length; step++)
            {
                for (int subStep = 0; subStep < subStepSizes[step]; ++subStep, ++index)
                {
                    double[] dW = dWs[index];
                    double logDiscount = logDiscounts[index];
                    double vol = vols[index].Vol(currentLogFwd + logDiscount, x, y);

                    currentLogFwd += vol * dW[brownianIndex] - 0.5 * vol * vol * dts[index];
                    if (isDivDates[index])
                        currentLogFwd = divs[index].ApplyLogSpotDiv(currentLogFwd + logDiscount) - logDiscount;

                    x += dW[brownianIndex + 1];
                    y += dW[brownianIndex + 2];
                }
                var stepProcess = processPath[step];
                stepProcess[processIndex] = currentLogFwd;
                stepProcess[processIndex + 1] = x;
                stepProcess[processIndex + 2] = y;
            }
        }
        public double[] Dates { get; private set; }
        public int ProcessDim { get { return 3; } }
        public int RandomDim
        {
            get { return 3; }
        }
        public double[] AllSimulatedDates { get; private set; }
    }

    public class BergomiPathGenFactory : EquityPathGenFactory<AbstractBergomiInstantVol>
    {
        #region private methods
        protected override ISingleRiskPathGenerator EquityPathGenerator(EquitySimulatorStepDatas<AbstractBergomiInstantVol>[] stepSimulDatas, 
                                                                        EquityModel<AbstractBergomiInstantVol> model, 
                                                                        DateTime probMeasureHorizon)
        {
            var initialHorizonForward = model.Spot / model.AssetDiscount.Zc(probMeasureHorizon);
            var initVal = model.FactorProcess.InitialValue;
            var x0 = initVal[0];
            var y0 = initVal[1];
            return new BergomiPathGenerator(stepSimulDatas, initialHorizonForward, x0, y0);
        }
        #endregion
        public BergomiPathGenFactory(MonteCarloConfig mcConfig)
            : base(mcConfig)
        {
        }
    }
    
}