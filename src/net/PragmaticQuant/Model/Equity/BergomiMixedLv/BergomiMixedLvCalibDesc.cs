using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Model.Equity.Bergomi;
using PragmaticQuant.Model.Equity.LocalVolatility;

namespace PragmaticQuant.Model.Equity.BergomiMixedLv
{
    public class BergomiMixedLvCalibDesc : EquityModelCalibDesc
    {
        public BergomiMixedLvCalibDesc(string asset, RawMapDatas<DateOrDuration, double> dividendYieldProportion,
                                       StochasticVolParams volParams,
                                       LocalVolSampleConfig localVolSampleConfig,
                                       int nbParticle,
                                       ISimulationSchedule calibDiscretization)
            : base(asset, dividendYieldProportion)
        {
            LocalVolSampleConfig = localVolSampleConfig;
            VolParams = volParams;
            NbParticle = nbParticle;
            CalibDiscretization = calibDiscretization;
        }

        public static BergomiMixedLvCalibDesc Create(string asset,
                                                     RawMapDatas<DateOrDuration, double> dividendYieldProportion,
                                                     StochasticVolParams volParams,
                                                     int nbParticle)
        {
            return new BergomiMixedLvCalibDesc(asset, dividendYieldProportion, volParams,
                new LocalVolSampleConfig(1e-03),
                nbParticle,
                SimulationSchedules.ScheduleBuilder(10 * Duration.Day, 100, Duration.Day));
        }

        public StochasticVolParams VolParams { get; private set; }
        public LocalVolSampleConfig LocalVolSampleConfig { get; private set; }

        public int NbParticle { get; private set; }
        public ISimulationSchedule CalibDiscretization { get; private set; }
    }

}