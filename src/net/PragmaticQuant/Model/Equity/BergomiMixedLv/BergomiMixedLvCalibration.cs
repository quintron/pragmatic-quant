using System;
using System.Linq;
using PragmaticQuant.Core;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets;
using PragmaticQuant.Markets.Equity;
using PragmaticQuant.Maths;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Model.Equity.Bergomi;
using PragmaticQuant.Model.Equity.LocalVolatility;
using PragmaticQuant.MonteCarlo;
using PragmaticQuant.Product;
using System.Collections.Generic;

namespace PragmaticQuant.Model.Equity.BergomiMixedLv
{
    public class BergomiMixedLvCalibration : EquityVolModelCalibration<BergomiMixedLvCalibDesc>
    {
        #region private methods
        private DateTime[] CalibrationDates(BergomiMixedLvCalibDesc model, IProduct product, DateTime refDate)
        {
            var constats = product.RetrieveCouponObservations()
                                  .Select(obs => obs.Date)
                                  .Where(d => d > refDate)
                                  .OrderBy(d => d)
                                  .ToArray();
            var calibPillars = new List<DateTime>() { refDate };
            var current = refDate;
            foreach (var date in constats)
            {
                if (date >= current + Duration.Month)
                {
                    calibPillars.Add(date);
                    current = date;
                }
            }
            var lastConstat = constats.Max();
            if (!calibPillars.Contains(lastConstat))
                calibPillars.Add(lastConstat);

            var scheduleBuilder = model.CalibDiscretization;
            return ArrayUtils.Append(EnumerableUtils.For(1, calibPillars.Count - 1,
                i => scheduleBuilder.Schedule(calibPillars[i - 1], calibPillars[i], refDate, new DateTime[0])));
        }
        #endregion
        public static readonly BergomiMixedLvCalibration Instance = new BergomiMixedLvCalibration();
        protected override EquityModelDescription CalibrateSingleAsset(AssetMarket assetMkt, BergomiMixedLvCalibDesc model, IProduct product)
        {
            DateTime[] calibDates = CalibrationDates(model, product, assetMkt.RefDate);

            RawMapDatas<DateOrDuration, double> divModel = model.DividendYieldProportion[assetMkt.Asset.Name];
            ParticleCalibrator particleCalibrator = ParticleCalibrator.Build(assetMkt, divModel);
            LocalVarianceInterpoler conditionalStochVariance = particleCalibrator.Calibrate(calibDates, model); 
            return new BergomiMixedLvModelDescription(assetMkt.Asset, assetMkt.AssetCurrency, divModel,
                                                      model.VolParams, 
                                                      model.LocalVolSampleConfig,
                                                      conditionalStochVariance);
        }
    }

    public class ParticleCalibrator
    {
        #region private fields
        private static readonly IRandomGeneratorFactory randomFactory = RandomGenerators.GaussianSobol();
        private readonly AssetMarket driftFreeMkt;
        private readonly BlackLawQuantileProvider quantileProvider;
        private readonly LogSwapVolCurve logSwapCurve;
        private readonly ITimeMeasure time;
        #endregion
        #region private methods
        private ParticleSystem ParticleInit(DateTime[] calibDates, BergomiMixedLvCalibDesc model)
        {
            var stepStochVolProvider = new BergomiStepVolProvider(RrFunctions.Constant(1.0), model.VolParams, time);
            var stochVolStepDatas = EnumerableUtils.For(0, calibDates.Length,
                t => stepStochVolProvider.StepVol(t > 0 ? calibDates[t - 1] : time.RefDate, calibDates[t]));
            var lastStepVolDatas = stepStochVolProvider.StepVol(calibDates.Last(), calibDates.Last() + Duration.Day);

            double[] particleDates = time[calibDates];
            var dts = EnumerableUtils.For(0, particleDates.Length, t => particleDates[t] - (t > 0 ? particleDates[t - 1] : 0.0));

            var particleSystem = new ParticleSystem(calibDates, model.NbParticle);
            double[][] spotIncrements = particleSystem.SpotStochVolIncrements;
            double[][] spotDrifts = particleSystem.SpotStochVolDrifts;
            double[][] stochVars = particleSystem.StochasticVars;

            var ornsteinPathGen = GaussianFactorPath.Build(model.VolParams.OrnsteinDriver(), particleDates, new double[0]);
            IRandomGenerator randomGenerator = randomFactory.Build(ornsteinPathGen.RandomSize);

            int nbDates = particleSystem.Dates.Length;
            for (int p = 0; p < particleSystem.NbParticle; p++) //TODO to be multithreaded
            {
                var gaussians = randomGenerator.Next();
                var ornsteinPath = ArrayUtils.CreateJaggedArray<double>(particleDates.Length, 3);
                ornsteinPathGen.GaussianFactorIncrements(ref ornsteinPath, gaussians);

                double x = model.VolParams.X0;
                double y = model.VolParams.Y0;
                for (int t = 0; t < nbDates; t++)
                {
                    double stochVol = stochVolStepDatas[t].Vol(double.NaN, x, y);
                    double var = stochVol * stochVol;
                    if (t > 0) stochVars[t - 1][p] = var;
                    spotDrifts[t][p] = -0.5 * dts[t] * var;
                    var dOrnstein = ornsteinPath[t];
                    spotIncrements[t][p] = dOrnstein[0] * stochVol;
                    x += dOrnstein[1];
                    y += dOrnstein[2];
                }
                var lastStepVol = lastStepVolDatas.Vol(double.NaN, x, y);
                stochVars[nbDates - 1][p] = lastStepVol * lastStepVol;
            }
            return particleSystem;
        }
        private LocalVarianceInterpoler ParticlePropagation(ParticleSystem particleSystem, RegularGridAffineInterpol[] stepLocalVols)
        {
            var conditionalStochVars = new RegularGridAffineInterpol[particleSystem.Dates.Length];
            var stepMixedLv = stepLocalVols[0];

            double[] logSpots = new double[particleSystem.NbParticle];
            for (int t = 0; t < particleSystem.Dates.Length; t++)
            {
                double[] spotIncrements = particleSystem.SpotStochVolIncrements[t];
                double[] spotDrifts = particleSystem.SpotStochVolDrifts[t];
                for (int p = 0; p < particleSystem.NbParticle; p++)
                {
                    var logSpot = logSpots[p];
                    var mixedLv = stepMixedLv.Eval(logSpot);
                    logSpot += mixedLv * (spotIncrements[p] + mixedLv * spotDrifts[p]);
                    logSpots[p] = logSpot;
                }

                RegularGridAffineInterpol condStochVar;
                RegularGrid1D grid;
                ComputeConditionalStochVar(particleSystem.Dates[t], logSpots, particleSystem.StochasticVars[t],
                                           out condStochVar, out grid);
                conditionalStochVars[t] = condStochVar;
                if (t < particleSystem.Dates.Length - 1)
                    stepMixedLv = ComputeMixedLocalVol(grid, condStochVar, stepLocalVols[t + 1]);
            }

            var interpolDates = ArrayUtils.Append(new[] { 0.0 }, time[particleSystem.Dates]);
            var interpolCondVars = ArrayUtils.Append(new Func<double, double>[] { s => 1.0 },
                                                     conditionalStochVars.Map(c => (Func<double, double>)c.Eval));
            return new AffineVarianceInterpoler(interpolDates, interpolCondVars);
        }
        private void ComputeConditionalStochVar(DateTime maturity, double[] logSpots, double[] stochVars,
                                                out RegularGridAffineInterpol condVar, out RegularGrid1D grid)
        {
            double t = time[maturity];
            var varSwapVol = logSwapCurve.Volatility(maturity);
            var h = 1.6 * varSwapVol * Math.Sqrt(Math.Max(t, 0.25)) / Math.Pow(logSpots.Length, 1.0 / 5.0);
            var kernel = new KernelRegression(stochVars, logSpots, h);

            double probaGrid = 100.0 / logSpots.Length;
            var gridQuantiles = quantileProvider.LogSpotQuantiles(maturity, probaGrid, 1.0 - probaGrid);
            grid = new RegularGrid1D(251, gridQuantiles[0], gridQuantiles[1]);

            var condVars = grid.ToArray().Map(kernel.Eval);
            condVar = new RegularGridAffineInterpol(grid, condVars);
        }
        private RegularGridAffineInterpol ComputeMixedLocalVol(RegularGrid1D grid,
                                                               RegularGridAffineInterpol condStochVar,
                                                               RegularGridAffineInterpol stepLocalVol)
        {
            var mixedLocalVols = grid.ToArray().Map(logSpot => stepLocalVol.Eval(logSpot) / Math.Sqrt(condStochVar.Eval(logSpot)));
            return new RegularGridAffineInterpol(grid, mixedLocalVols);
        }
        #endregion
        private ParticleCalibrator(AssetMarket driftFreeMkt)
        {
            this.driftFreeMkt = driftFreeMkt;
            time = driftFreeMkt.Time;
            quantileProvider = driftFreeMkt.LawQuantile();
            logSwapCurve = driftFreeMkt.LogSwapVolCurve();
        }
        public static ParticleCalibrator Build(AssetMarket mkt, RawMapDatas<DateOrDuration, double> divModel)
        {
            return new ParticleCalibrator(EquityModelUtils.DriftFreeAssetMarket(mkt, divModel));
        }

        public LocalVarianceInterpoler Calibrate(DateTime[] timeGrid, BergomiMixedLvCalibDesc model)
        {
            var stepLocalVolProvider = StepLocalVolProvider.Build(model.LocalVolSampleConfig, new AffineDividend[0], driftFreeMkt);
            RegularGridAffineInterpol[] stepLocalVols = EnumerableUtils.For(0, timeGrid.Length,
                t => stepLocalVolProvider.StepVol(t > 0 ? timeGrid[t - 1] : driftFreeMkt.RefDate, timeGrid[t]));

            ParticleSystem particleSystem = ParticleInit(timeGrid, model);
            return ParticlePropagation(particleSystem, stepLocalVols);
        }

        #region private class
        private sealed class ParticleSystem
        {
            public readonly int NbParticle;
            public readonly DateTime[] Dates;
            public readonly double[][] SpotStochVolIncrements;
            public readonly double[][] SpotStochVolDrifts;
            public readonly double[][] StochasticVars;
            public ParticleSystem(DateTime[] dates, int nbParticle)
            {
                Dates = dates;
                NbParticle = nbParticle;
                SpotStochVolIncrements = ArrayUtils.CreateJaggedArray<double>(dates.Length, nbParticle);
                SpotStochVolDrifts = ArrayUtils.CreateJaggedArray<double>(dates.Length, nbParticle);
                StochasticVars = ArrayUtils.CreateJaggedArray<double>(dates.Length, nbParticle);
            }
        }

        private sealed class KernelRegression
        {
            #region private fields
            private readonly double[] values;
            private readonly double[] inputs;
            private readonly StepSearcher inputSearch;
            private readonly double h;
            #endregion
            public KernelRegression(double[] values, double[] inputs, double h)
            {
                var sortedInputValues = inputs.Zip(values, Tuple.Create)
                                              .OrderBy(t => t.Item1);
                this.values = sortedInputValues.Map(t=>t.Item2);
                this.inputs = sortedInputValues.Map(t => t.Item1);
                this.h = h;
                inputSearch = new StepSearcher(this.inputs, false);
            }

            public double Eval(double input)
            {
                double width = h * Math.Exp(-input);

                double expectedVals = 0.0;
                double expectedInputs = 0.0;
                int i = Math.Max(0, inputSearch.LocateLeftIndex(input - width));
                for (; i < values.Length; i++)
                {
                    double k = (inputs[i] - input) / width;
                    if (k > 1.0)
                        break;
                    k = 1.0 - k * k;
                    k = k * k;
                    expectedVals += k * values[i];
                    expectedInputs += k;
                }
                return expectedVals / expectedInputs;
            }
        }
        #endregion
    }
    
    public class AffineVarianceInterpoler  : LocalVarianceInterpoler
    {
        private readonly Func<double, double>[] variances;

        public AffineVarianceInterpoler(double[] maturities, Func<double, double>[] variances)
            : base(maturities, false)
        {
            this.variances = variances;
        }
        public override double[] VarianceSample(double t, double[] logSpots)
        {
            var leftIndex = maturityStepSearcher.LocateLeftIndex(t);

            if (leftIndex < 0)
                return logSpots.Map(variances[0]);

            if (leftIndex == maturities.Length-1)
                return logSpots.Map(variances[maturities.Length - 1]);

            double leftMaturity = maturities[leftIndex];
            double rightMaturity = maturities[leftIndex + 1];
            double w = (t - leftMaturity) / (rightMaturity - leftMaturity);

            var variance = logSpots.Map(variances[leftIndex]);
            variance.Mult(1.0 - w);
            variance.Add(logSpots.Map(variances[leftIndex + 1]), w);
            return variance;
        }
    }

}