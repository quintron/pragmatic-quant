using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets;
using PragmaticQuant.Markets.Equity;
using PragmaticQuant.Model.Equity.Bergomi;
using PragmaticQuant.Model.Equity.Dividends;
using PragmaticQuant.Model.Equity.LocalVolatility;

namespace PragmaticQuant.Model.Equity.BergomiMixedLv
{
    public class BergomiMixedLvModelDescription : Bergomi2FModelDescription
    {
        public BergomiMixedLvModelDescription(AssetId asset, Currency currency,
                                              RawMapDatas<DateOrDuration, double> dividendYieldProportion,
                                              StochasticVolParams volParams,
                                              LocalVolSampleConfig localVolSampleConfig,
                                              LocalVarianceInterpoler conditionalStochVariance) 
            : base(asset, currency, dividendYieldProportion, volParams, null)
        {
            LocalVolSampleConfig = localVolSampleConfig;
            ConditionalStochVariance = conditionalStochVariance;
        }
        public LocalVolSampleConfig LocalVolSampleConfig { get; private set; }
        public LocalVarianceInterpoler ConditionalStochVariance { get; private set; }
    }

    internal class BergomiMixedModelFactory : EquityModelFactory<BergomiMixedLvModelDescription, AbstractBergomiInstantVol>
    {
        public static readonly BergomiMixedModelFactory Instance = new BergomiMixedModelFactory();
        protected override EquityModel<AbstractBergomiInstantVol> BuildEquityModel(BergomiMixedLvModelDescription mixedB2F, AffineDividend[] affineDivModel, AssetMarket assetMkt)
        {
            ITimeMeasure time = assetMkt.Time;
            var stepLocalVolProvider = StepLocalVolProvider.Build(mixedB2F.LocalVolSampleConfig, affineDivModel, assetMkt);
            var affineDivUtils = new AffineDivCurveUtils(affineDivModel, assetMkt.AssetDiscountCurve, time);
            var mixedLocalVolProvider = new MixedBergomiStepVolProvider(stepLocalVolProvider, 
                                                                        mixedB2F.ConditionalStochVariance, 
                                                                        affineDivUtils,
                                                                        assetMkt.Spot,
                                                                        mixedB2F.VolParams, time);
            return new MixedBergomi2F(assetMkt.Asset,
                                      time,
                                      assetMkt.Spot,
                                      assetMkt.AssetDiscountCurve,
                                      affineDivModel.DivModel(),
                                      mixedB2F.ModelFactorDescription,
                                      mixedB2F.VolParams,
                                      mixedLocalVolProvider);
        }
    }


}