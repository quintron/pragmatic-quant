﻿using System;
using System.Runtime.CompilerServices;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Core;
using PragmaticQuant.Markets;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Maths.Stochastic;
using PragmaticQuant.Model.Equity.Bergomi;
using PragmaticQuant.Model.Equity.Dividends;
using PragmaticQuant.Model.Equity.LocalVolatility;

namespace PragmaticQuant.Model.Equity.BergomiMixedLv
{
    public class MixedBergomi2F : Bergomi2FModelBase
    {
        internal MixedBergomi2F(AssetId asset, ITimeMeasure time, double spot, DiscountCurve assetDiscount, DiscreteLocalDividend[] dividends,
                                 ModelFactorDescription modelFactor,
                                 StochasticVolParams volParams,
                                 IStepVolProvider<AbstractBergomiInstantVol> mixedLocalVolProvider) 
            : base(asset, time, spot, assetDiscount, dividends, modelFactor,
                   volParams, mixedLocalVolProvider)
        {
        }
    }
    
    public class MixedBergomiStepVolProvider : IStepVolProvider<AbstractBergomiInstantVol>
    {
        #region private fields
        private readonly IStepVolProvider<RegularGridAffineInterpol> localVolProvider;
        private readonly LocalVarianceInterpoler conditionalVariance;
        private readonly StochasticVolParams volParams;
        private readonly RrFunction[,] ornsteinVolIntegralCovs;
        private readonly ITimeMeasure time;
        private readonly AffineDivCurveUtils divUtils;
        private readonly double spot;
        
        #endregion
        private double[] ConditionalVarianceSample(double start, RegularGrid1D logSpotGrid)
        {
            double cashBpv = divUtils.CashDivBpv(start);
            double assetGrowth = divUtils.AssetGrowth(start);
            double[] spotGrid = logSpotGrid.Exp(1.0).Value;
            double[] driftFreeGrid = spotGrid.Map(s => Math.Log((s / assetGrowth + cashBpv) / spot));
            return conditionalVariance.VarianceSample(start, driftFreeGrid);
        }

        internal MixedBergomiStepVolProvider(IStepVolProvider<RegularGridAffineInterpol> localVolProvider,
                                             LocalVarianceInterpoler conditionalVariance,
                                             AffineDivCurveUtils divUtils,                                 
                                             double spot,
                                             StochasticVolParams volParams,
                                             ITimeMeasure time)
        {
            this.localVolProvider = localVolProvider;
            this.volParams = volParams;
            this.time = time;
            this.conditionalVariance = conditionalVariance;
            this.divUtils = divUtils;
            this.spot = spot;
            ornsteinVolIntegralCovs = volParams.OrnsteinVolDriver()
                                               .IntegratedCovariance();
        }
        public AbstractBergomiInstantVol StepVol(DateTime startDate, DateTime endDate)
        {
            double start = time[startDate];
            RegularGridAffineInterpol stepLocalVol = localVolProvider.StepVol(startDate, endDate);
            RegularGrid1D logSpotGrid = new RegularGrid1D(stepLocalVol.Size, stepLocalVol.Support.Inf, stepLocalVol.Support.Sup);
            var condVariances = ConditionalVarianceSample(start, logSpotGrid);
            var inverseCondVar = condVariances.Map(v => 1.0 / Math.Sqrt(v));
            RegularGridAffineInterpol stepMixedLocalVol = stepLocalVol.Mult(inverseCondVar);
            return new MixedBergomiInstantVol(stepMixedLocalVol, volParams, ornsteinVolIntegralCovs, start);
        }
    }

    public sealed class MixedBergomiInstantVol : AbstractBergomiInstantVol
    {
        #region private fields
        private readonly RegularGridAffineInterpol stepLocalVol;
        #endregion
        internal MixedBergomiInstantVol(RegularGridAffineInterpol stepLocalVol,
                                        StochasticVolParams volParams,
                                        RrFunction[,] ornsteinVolIntegralCovs,
                                        double start)
            : base(volParams, ornsteinVolIntegralCovs, start)
        {
            this.stepLocalVol = stepLocalVol;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override double Vol(double logSpot, double x, double y)
        {
            return stepLocalVol.Eval(logSpot) * Math.Exp(0.5 * (w1 * x + w2 * y + cvx));
        }
    }

}
