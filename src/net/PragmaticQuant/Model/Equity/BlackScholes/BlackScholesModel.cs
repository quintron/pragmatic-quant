﻿using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Model.Equity.Dividends;
using PragmaticQuant.Model.Equity.LocalVolatility;

namespace PragmaticQuant.Model.Equity.BlackScholes
{
    public class BlackScholesModel : LocalVolatilityModel
    {
        public BlackScholesModel(ITimeMeasure time, AssetId asset, RrFunction sigma, 
                                 double spot,
                                 DiscountCurve assetDiscount,
                                 DiscreteLocalDividend[] dividends)
            : base(asset, time, new BsStepVolProvider(time, sigma), spot, assetDiscount, dividends)
        {
           Sigma = sigma;
        }
        public RrFunction Sigma { get; private set; }
    }

    public class BsStepVolProvider : IStepVolProvider<RegularGridAffineInterpol>
    {
        private readonly RrFunction variance;
        private readonly ITimeMeasure time;
        public BsStepVolProvider(ITimeMeasure time, RrFunction sigma)
        {
            this.time = time;
            variance = (sigma * sigma).Integral(0.0);
        }
        public RegularGridAffineInterpol StepVol(DateTime startDate, DateTime endDate)
        {
            var start = time[startDate];
            var end = time[endDate];
            var stepVariance = Math.Max(0.0, variance.Eval(end) - variance.Eval(start));
            var stepVol = Math.Sqrt(stepVariance / (end - start));

            return new RegularGridAffineInterpol(RealInterval.RightOpen(double.PositiveInfinity, double.PositiveInfinity), new[] {stepVol});
        }
    }
    

}
