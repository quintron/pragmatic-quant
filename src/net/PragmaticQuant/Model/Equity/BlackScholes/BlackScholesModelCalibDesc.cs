using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets.Equity;
using PragmaticQuant.Markets;
using PragmaticQuant.Model.Equity.Dividends;
using PragmaticQuant.Product;

namespace PragmaticQuant.Model.Equity.BlackScholes
{
    public class BlackScholesModelCalibDesc : EquityModelCalibDesc
    {
        public BlackScholesModelCalibDesc(string asset,
                                          RawMapDatas<DateOrDuration, double> dividendYieldProportion,
                                          DateOrDuration[] calibrationMaturities,
                                          double[] calibrationStrikes = null,
                                          double[] calibrationVols = null)
            : base(asset, dividendYieldProportion)
        {
            if (calibrationStrikes != null && calibrationMaturities.Length != calibrationStrikes.Length)
                throw new Exception("BlackScholesModelCalibDesc : incompatible calibration maturities and strikes ");

            if (calibrationVols != null && calibrationVols.Length != calibrationMaturities.Length)
                throw new Exception("BlackScholesModelCalibDesc : incompatible calibration maturities and vols ");

            CalibrationMaturities = calibrationMaturities;
            CalibrationStrikes = calibrationStrikes;
            CalibrationVols = calibrationVols;
        }
        public DateOrDuration[] CalibrationMaturities { get; private set; }
        public double[] CalibrationStrikes { get; private set; }
        public double[] CalibrationVols { get; private set; }
    }

    public class BlackScholesModelCalibration : EquityVolModelCalibration<BlackScholesModelCalibDesc> 
    {
        public static readonly BlackScholesModelCalibration Instance = new BlackScholesModelCalibration();
        protected override EquityModelDescription CalibrateSingleAsset(AssetMarket assetMkt, BlackScholesModelCalibDesc model, IProduct product)
        {
            var discountCurve = DiscountCurve.Flat(FinancingId.Ois(assetMkt.AssetCurrency), assetMkt.RefDate, 0.0);
            var optionPricer = EquityVanillaPricer.Build(assetMkt, discountCurve);
            var forwardCurve = assetMkt.ForwardCurve();

            var calibMaturities = model.CalibrationMaturities.Map(d => d.ToDate(assetMkt.RefDate));
            var calibDates = assetMkt.Time[calibMaturities];
            
            var calibStrikes = model.CalibrationStrikes;
            if (calibStrikes == null)
            {
                calibStrikes = calibMaturities.Map(m => forwardCurve.Fwd(m));
            }

            double[] targetVols = model.CalibrationVols;
            if (model.CalibrationVols == null)
            {
                var volSurface = assetMkt.VolSurface();
                targetVols = EnumerableUtils.For(0, calibMaturities.Length,
                    i => volSurface.Volatility(calibMaturities[i], calibStrikes[i]));
            }
            
            double[] targetPrices = new double[calibDates.Length];
            double[] optionTypes = new double[calibDates.Length];
            for (int i = 0; i < calibMaturities.Length; i++)
            {
                var optionType = (calibStrikes[i] > forwardCurve.Fwd(calibMaturities[i])) ? OptionType.Call : OptionType.Put;
                targetPrices[i] = optionPricer.Price(calibMaturities[i], calibStrikes[i], optionType, targetVols[i]) / discountCurve.Zc(calibMaturities[i]);
                optionTypes[i] = optionType.Leverage();
            }

            var divModel = model.DividendYieldProportion[assetMkt.Asset.Name];

            AffineDividend[] affineDivModel = EquityModelUtils.AffineDivModel(divModel, assetMkt);

            var bsCalibrator = BlackScholesWithDividendOption.Build(assetMkt.Spot, affineDivModel, assetMkt.AssetDiscountCurve, assetMkt.Time);
            double[] calibVols = bsCalibrator.CalibrateVol(calibDates, targetPrices, calibStrikes, optionTypes);
            var volTerm = new RawMapDatas<DateOrDuration, double>(model.CalibrationMaturities, calibVols);
            return new BlackScholesModelDescription(assetMkt.Asset, assetMkt.AssetCurrency, divModel, volTerm);
        }
    } 

}