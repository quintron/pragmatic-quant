using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets;
using PragmaticQuant.Markets.Equity;
using PragmaticQuant.Model.Equity.LocalVolatility;

namespace PragmaticQuant.Model.Equity.BlackScholes
{
    public class BlackScholesModelDescription : EquityModelDescription
    {
        public BlackScholesModelDescription(AssetId asset,
                                            Currency currency,
                                            RawMapDatas<DateOrDuration, double> dividendYieldProportion,
                                            RawMapDatas<DateOrDuration, double> volTerm)
            : base(asset, currency, dividendYieldProportion)
        {
            VolTermStructure = volTerm;
        }
        public RawMapDatas<DateOrDuration, double> VolTermStructure { get; private set; }
        public override RiskFactor[] RiskFactors
        {
            get { return new[] {SpotFactor}; }
        }
        public override ModelFactorDescription ModelFactorDescription
        {
            get { return ModelFactorDescription.Ornstein1D(SpotFactor.Name, 0.0, ConstantRawFunc(1.0)); }
        }
    }

    internal class BlackScholesModelFactory : EquityModelFactory<BlackScholesModelDescription, RegularGridAffineInterpol>
    {
        public static readonly BlackScholesModelFactory Instance = new BlackScholesModelFactory();
        protected override EquityModel<RegularGridAffineInterpol> BuildEquityModel(BlackScholesModelDescription bs, AffineDividend[] affineDivModel, AssetMarket assetMkt)
        {
            var sigma = ModelFactoryUtils.InstantVolFunction(bs.VolTermStructure, assetMkt.Time);
            return new BlackScholesModel(assetMkt.Time, assetMkt.Asset, sigma, 
                                         assetMkt.Spot, 
                                         assetMkt.AssetDiscountCurve,
                                         affineDivModel.DivModel());
        }
    }
    
}