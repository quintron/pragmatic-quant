using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets;
using PragmaticQuant.Markets.Equity;
using PragmaticQuant.Maths;
using PragmaticQuant.Maths.Function;

namespace PragmaticQuant.Model.Equity.Dividends
{
    public class AffineDivCurveUtils
    {
        #region private fields
        private readonly Func<double, double> assetGrowth;
        private readonly StepFunction cashDivBpv;
        private readonly RrFunction cashBpvIntegral;
        
        private readonly StepFunction squareTimeWeightedCash;
        #endregion
        public AffineDivCurveUtils(AffineDividend[] affineDividends,
                                   DiscountCurve assetDiscountCurve,
                                   ITimeMeasure time)
        {
            if (affineDividends.Length > 0)
            {
                Debug.Assert(EnumerableUtils.IsSorted(affineDividends.Select(div => div.Date)));

                double[] divDates = affineDividends.Map(div => time[div.Date]);
                double[] spotYieldGrowths = affineDividends.Scan(1.0, (prev, div) => prev * (1.0 - div.Yield));
                var spotYieldGrowth = new StepFunction(divDates, spotYieldGrowths, 1.0);
                assetGrowth = t => spotYieldGrowth.Eval(t) / assetDiscountCurve.Zc(t);

                double[] discountedCashs = affineDividends.Map(div => div.Cash / assetGrowth(time[div.Date]));
                double[] cashBpvs = discountedCashs.Scan(0.0, (prev, c) => prev + c);
                cashDivBpv = new StepFunction(divDates, cashBpvs, 0.0);
                cashBpvIntegral = cashDivBpv.Integral(0.0);

                double[] squareTimeWeightedCashs = discountedCashs.ZipWith(divDates, (c, t) => c * t * t);
                squareTimeWeightedCash = new StepFunction(divDates, squareTimeWeightedCashs, 0.0);
            }
            else
            {
                assetGrowth = t => 1.0 / assetDiscountCurve.Zc(t);
                cashDivBpv = new StepFunction(new[] { 0.0 }, new[] { 0.0 }, double.NaN);
                cashBpvIntegral = RrFunctions.Zero;
                
                squareTimeWeightedCash = new StepFunction(new[] { 0.0 }, new[] { 0.0 }, double.NaN);
            }
        }

        public double AssetGrowth(double t)
        {
            return assetGrowth(t);
        }
        public double CashDivBpv(double t)
        {
            return cashDivBpv.Eval(t);
        }
        public double CashBpvAverage(double start, double end)
        {
            if (end > start)
                return (cashBpvIntegral.Eval(end) - cashBpvIntegral.Eval(start)) / (end - start);
            return cashBpvIntegral.Eval(start);
        }
        public double CashBpvTimeWeightedAverage(double t)
        {
            return cashDivBpv.Eval(t) - squareTimeWeightedCash.Eval(t) / (t * t );
        }
        public double Forward(double t, double spot)
        {
            return (spot - CashDivBpv(t)) * AssetGrowth(t);
        }

        public void LehmanProxy(double maturity, double spot,
                                out double effectiveForward, out double strikeShift)
        {
            var growth = AssetGrowth(maturity);
            var cashBpvAvg = CashBpvAverage(0.0, maturity);

            effectiveForward = (spot - cashBpvAvg) * growth;
            strikeShift = growth * (CashDivBpv(maturity) - cashBpvAvg);
        }

        private RawMapDatas<double, double> DriftFreeSmile(double t, RawMapDatas<double, double> blackSmile, double spot)
        {
            var assetGrowthAtT = AssetGrowth(t);
            var cashBpvAtT = CashDivBpv(t);
            var forward = (spot - cashBpvAtT) * assetGrowthAtT;

            var driftFreeStrikes = new List<double>();
            var driftFreeVols = new List<double>();
            var strikes = blackSmile.Pillars;
            var vols = blackSmile.Values;
            for (int k = 0; k < strikes.Length; k++)
            {
                var strike = strikes[k];
                var optionType = strike > spot ? 1.0 : -1.0;
                var optionPrice = BlackScholesOption.Price(forward, strike, vols[k], t, optionType);

                var driftFreeOptionPrice = optionPrice / assetGrowthAtT;
                var driftFreeStrike = (strike / assetGrowthAtT + cashBpvAtT);
                var driftFreeVol = BlackScholesOption.ImpliedVol(driftFreeOptionPrice, spot, driftFreeStrike, t, optionType);

                driftFreeStrikes.Add(driftFreeStrike);
                driftFreeVols.Add(driftFreeVol);
            }
            return new RawMapDatas<double, double>(driftFreeStrikes.ToArray(), driftFreeVols.ToArray());
        }
        public VolatilityMatrix DriftFreeVolMatrix(VolatilityMatrix volMatrix, double spot)
        {
            var driftFreeSmiles = EnumerableUtils.For(0, volMatrix.Maturities.Length,
                p =>
                {
                    var t = volMatrix.Time[volMatrix.Maturities[p]];
                    return DriftFreeSmile(t, volMatrix.Smiles[p], spot);
                });
            return new VolatilityMatrix(volMatrix.Time, volMatrix.Maturities, driftFreeSmiles);
        }
    }
}