﻿using System;
using System.Collections.Generic;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets;
using PragmaticQuant.Markets.Equity;

namespace PragmaticQuant.Model.Equity.Dividends
{
    public class AffineDivModelStripping
    {
        #region private fields
        private readonly double spot;
        private readonly DiscountCurve assetDiscountCurve;
        private readonly ITimeMeasure time;
        #endregion
        public AffineDivModelStripping(double spot, DiscountCurve assetDiscountCurve, ITimeMeasure time)
        {
            this.spot = spot;
            this.assetDiscountCurve = assetDiscountCurve;
            this.time = time;
        }

        public AffineDividend[] StripAffineDiv(RawMapDatas<DateTime, double> forwardDivs, Func<double, double> yieldProportion)
        {
            if (!forwardDivs.Pillars.Any())
                return new AffineDividend[0];

            if (!EnumerableUtils.IsSorted(forwardDivs.Pillars))
                throw new Exception("AffineDivModelStripping : forward div curve must be sorted !");

            List<AffineDividend> divQuotes = new List<AffineDividend>();

            double previousAssetFwd = spot;
            double previousDiscount = 1.0;
            for (int d = 0; d < forwardDivs.Pillars.Length; d++)
            {
                var date = forwardDivs.Pillars[d];
                var discount = assetDiscountCurve.Zc(date);
                double assetFwdBeforeDiv = previousAssetFwd * previousDiscount / discount;

                var div = forwardDivs.Values[d];
                var yieldPart = yieldProportion(time[date]) * div / assetFwdBeforeDiv;
                var cashPart = div - yieldPart * assetFwdBeforeDiv;

                divQuotes.Add(new AffineDividend(date, cashPart, yieldPart));

                previousAssetFwd = assetFwdBeforeDiv - div;
                previousDiscount = discount;
            }

            return divQuotes.ToArray();
        }
        public RawMapDatas<DateTime, double> StripForwardDivs(AffineDividend[] affineDivs)
        {
            affineDivs = affineDivs.OrderBy(d => d.Date).ToArray();

            var forwardDivs = new double[affineDivs.Length];

            double previousAssetFwd = spot;
            double previousDiscount = 1.0;
            for (int d = 0; d < affineDivs.Length; d++)
            {
                var affineDiv = affineDivs[d];

                var date = affineDiv.Date;
                var discount = assetDiscountCurve.Zc(date);
                double assetFwdBeforeDiv = previousAssetFwd * previousDiscount / discount;

                var div = affineDiv.Cash + affineDiv.Yield * assetFwdBeforeDiv;
                forwardDivs[d] = div;
                
                previousAssetFwd = assetFwdBeforeDiv - div;
                previousDiscount = discount;
            }

            return new RawMapDatas<DateTime, double>(affineDivs.Map(div => div.Date), forwardDivs);
        }
    }
    
}
