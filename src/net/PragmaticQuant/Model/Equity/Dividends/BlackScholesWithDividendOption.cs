using System;
using System.Diagnostics;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets;
using PragmaticQuant.Markets.Equity;
using PragmaticQuant.Maths;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Maths.Integration;

namespace PragmaticQuant.Model.Equity.Dividends
{
    /// <summary>
    /// Vanilla option pricer for Black-Scholes model with dividends.
    /// </summary>
    public class BlackScholesWithDividendOption
    {
        #region private fields
        private readonly ITimeMeasure time;
        private readonly double spot;
        private readonly AffineDivCurveUtils affineDivUtils;
        private readonly double[] quadPoints;
        private readonly double[] quadWeights;
        #endregion
        
        public BlackScholesWithDividendOption(double spot, ITimeMeasure time, AffineDivCurveUtils affineDivUtils, int quadratureNbPoints = 10)
        {
            this.time = time;
            this.affineDivUtils = affineDivUtils;
            this.spot = spot;
            GaussHermite.GetQuadrature(quadratureNbPoints, out quadPoints, out quadWeights);
        }

        public static BlackScholesWithDividendOption Build(double spot,
                                                           AffineDividend[] affineDividends,
                                                           DiscountCurve assetDiscountCurve,
                                                           ITimeMeasure time)
        {
            var divUtils = new AffineDivCurveUtils(affineDividends, assetDiscountCurve, time);
            return new BlackScholesWithDividendOption(spot, time, divUtils);
        }
        
        /// <summary>
        /// Price option using a two step quadrature.
        /// </summary>
        /// <param name="maturity">option maturity</param>
        /// <param name="strike">option strike</param>
        /// <param name="vol">volatility</param>
        /// <param name="q">option type : 1 for call, -1 for put</param>
        /// <returns></returns>
        public double Price(double maturity, double strike, double vol, double q)
        {
            return new BsDivPrice(maturity, strike, spot, affineDivUtils, quadPoints, quadWeights).Price(vol, q);
        }

        /// <summary>
        /// Price option using a two step quadrature.
        /// </summary>
        /// <param name="maturity">option maturity</param>
        /// <param name="strike">option strike</param>
        /// <param name="terminalVol"> terminal volatility : quadratic average of BS instant volatility </param>
        /// <param name="q">option type : 1 for call, -1 for put</param>
        /// <returns></returns>
        public double Price(double maturity, double strike, Func<double, double> terminalVol, double q)
        {
            return new BsDivPrice(maturity, strike, spot, affineDivUtils, quadPoints, quadWeights).Price(terminalVol, q);
        }

        /// <summary>
        /// Implied volatility from option price. (Inversion of the two step quadrature formula.)
        /// </summary>
        /// <param name="maturity">option maturity</param>
        /// <param name="strike">option strike</param>
        /// <param name="price">option price </param>
        /// <param name="q">option type : 1 for call, -1 for put</param>
        /// <returns></returns>
        public double ImpliedVol(double maturity, double strike, double price, double q)
        {
            //Proxy using Lehman Formula
            double proxyFwd, proxyDk;
            affineDivUtils.LehmanProxy(maturity, spot, out proxyFwd, out proxyDk);
            double lehmanTargetVol = BlackScholesOption.ImpliedVol(price, proxyFwd, strike + proxyDk, maturity, q);
            
            var pricer = new BsDivPrice(maturity, strike, spot, affineDivUtils, quadPoints, quadWeights);
            Func<double, double> volToLehmanVolErr =
                v => BlackScholesOption.ImpliedVol(pricer.Price(v, q), proxyFwd, strike + proxyDk, maturity, q) - lehmanTargetVol;
            
            //Solve
            double v1 = lehmanTargetVol;
            double err1 = volToLehmanVolErr(v1);

            double v2 = lehmanTargetVol - err1;
            double err2 = volToLehmanVolErr(v2);
            double v3 = v1 - err1 * (v2 - v1) / (err2 - err1);
            
            if (Math.Abs(v3 - v2) < v2 * 1.0e-13)
                return v3;
            double err3 = volToLehmanVolErr(v3);
            double v4 = RootUtils.TrinomRoot(v1, v2, v3, err1, err2, err3);
            
            if (Math.Abs(v4 - v3) < v3 * 1.0e-13)
                return v4;
            double err4 = volToLehmanVolErr(v4);
            double v5 = RootUtils.TrinomRoot(v2, v3, v4, err2, err3, err4);
            
            return v5;
        }

        public double ImpliedVol(DateTime maturity, OptionType optionType, double strike, double price)
        {
            double q;
            switch (optionType)
            {
                case OptionType.Call:
                    q = 1.0;
                    break;
                case OptionType.Put:
                    q = -1.0;
                    break;
                default :
                    throw new Exception(string.Format("Unhandled option type : {0}", optionType));
            }
            return ImpliedVol(time[maturity], strike, price, q);
        }

        public double[] CalibrateVol(double[] maturities, double[] targetPrices, double[] strikes, double[] optionTypes)
        {
            Debug.Assert(EnumerableUtils.IsSorted(maturities));
            Debug.Assert(maturities.Length == strikes.Length 
                         && strikes.Length == targetPrices.Length
                         && targetPrices.Length== optionTypes.Length);
            
            double[] variances = new double[maturities.Length + 1];
            double[] varPillars = new[] {0.0}.Concat(maturities).ToArray();

            double[] calibVols = new double[maturities.Length];
            for (int step = 0; step < maturities.Length; step++)
            {
                var maturity = maturities[step];
                var strike = strikes[step];
                var targetPrice = targetPrices[step];
                var q = optionTypes[step];
                
                //Proxy using Lehman Formula
                double proxyFwd, proxyDk;
                affineDivUtils.LehmanProxy(maturity, spot, out proxyFwd, out proxyDk);
                double lehmanTargetVol = BlackScholesOption.ImpliedVol(targetPrice, proxyFwd, strike + proxyDk, maturity, q);

                var pricer = new BsDivPrice(maturities[step], strikes[step], spot, affineDivUtils, quadPoints, quadWeights);
                Func<double, double> volToLehmanVolErr = v =>
                {
                    variances[1 + step] = v * v * maturity;
                    var varFunc = RrFunctions.LinearInterpolation(varPillars, variances);
                    
                    Func<double, double> volFunc = t => Math.Sqrt(varFunc.Eval(t) / t);
                    var price = pricer.Price(volFunc, q);
                    return BlackScholesOption.ImpliedVol(price, proxyFwd, strike + proxyDk, maturity, q) - lehmanTargetVol;
                };//TODO use a cache
                
                //Bracket & Solve
                double v1 = lehmanTargetVol;
                double v2;
                if (step == 0)
                {
                    v2 = lehmanTargetVol - volToLehmanVolErr(lehmanTargetVol);
                }
                else
                {
                    var volIfZeroVolOnStep = Math.Sqrt(calibVols[step - 1] * calibVols[step - 1] * maturities[step - 1] / maturities[step]);
                    var minError = volToLehmanVolErr(volIfZeroVolOnStep);
                    if (minError > 0.0) //saturation case
                    {
                        calibVols[step] = volIfZeroVolOnStep;
                        variances[1 + step] = volIfZeroVolOnStep * volIfZeroVolOnStep * maturity;
                        continue;
                    }
                    v2 = volIfZeroVolOnStep;
                }
                
                if (!RootUtils.Bracket(volToLehmanVolErr, v1, v2, out v1, out v2))
                    throw new Exception("Failed to inverse vol");
                var impliedVol = RootUtils.Brenth(volToLehmanVolErr, v1, v2, 1.0e-10, 2.0 * DoubleUtils.MachineEpsilon, 10);
                
                calibVols[step] = impliedVol;
                variances[1 + step] = impliedVol * impliedVol * maturity;
            }

            return calibVols;
        }
        public double[] CalibrateVol(double[] maturities, double[] targetPrices, double strike, double optionType)
        {
            return CalibrateVol(maturities, targetPrices,
                ArrayUtils.Constant(maturities.Length, strike),
                ArrayUtils.Constant(maturities.Length, optionType));
        }
        
        /// <summary>
        /// Price option using Lehman Brother proxy.
        /// </summary>
        /// <param name="maturity">option maturity</param>
        /// <param name="strike">option strike</param>
        /// <param name="vol">volatility</param>
        /// <param name="q">option type : 1 for call, -1 for put</param>
        /// <returns></returns>
        public double PriceLehman(double maturity, double strike, double vol, double q)
        {
            double effectiveForward, strikeShift;
            affineDivUtils.LehmanProxy(maturity, spot, out effectiveForward, out strikeShift);
            return BlackScholesOption.Price(effectiveForward, strike + strikeShift, vol, maturity, q);
        }
        
        public double PriceProxy(double maturity, double strike, double vol, double q)
        {
            var growth = affineDivUtils.AssetGrowth(maturity);
            double cashBpv = affineDivUtils.CashDivBpv(maturity);
            double cashBpvTimeWAvg = affineDivUtils.CashBpvTimeWeightedAverage(maturity);
            double cashBpbAvg = affineDivUtils.CashBpvAverage(0.0, maturity);
            double volAdj = 1.0 + (cashBpvTimeWAvg - cashBpbAvg) / spot;

            double displacement = cashBpvTimeWAvg / volAdj;
            double formulaForward = growth * (spot - displacement);
            double formulaStrike = strike + growth * (cashBpv - displacement);
            double formulaVol = vol * volAdj;
            return BlackScholesOption.Price(formulaForward, formulaStrike, formulaVol, maturity, q);
        }

        #region private class
        private sealed class BsDivPrice
        {
            #region private fields
            private readonly double k;
            private readonly double a;
            private readonly double b;
            private readonly double midT;
            private readonly double dT;
            private readonly double maturity;
            private readonly double[] z;
            private readonly double[] quadWeights;
            #endregion

            public BsDivPrice(double maturity, double strike, double spot,
                AffineDivCurveUtils affineDivUtils, double[] quadPoints, double[] quadWeights)
            {
                this.quadWeights = quadWeights;
                this.maturity = maturity;

                midT = 0.5 * maturity; //TODO find a best heuristic !
                dT = maturity - midT;

                double sqrtMidT = Math.Sqrt(midT);
                z = quadPoints.Map(p => p * sqrtMidT);

                var displacement1 = affineDivUtils.CashBpvAverage(0.0, midT);
                var displacement2 = affineDivUtils.CashBpvAverage(midT, maturity);
                var maturityGrowth = affineDivUtils.AssetGrowth(maturity);

                k = strike + maturityGrowth * (affineDivUtils.CashDivBpv(maturity) - displacement2);
                a = maturityGrowth * (spot - displacement1);
                b = maturityGrowth * (displacement1 - displacement2);
            }
            private double Price(double volBeforeMidT, double volAfterMidT, double q)
            {
                double aCvx = a * Math.Exp(-0.5 * volBeforeMidT * volBeforeMidT * midT);

                var price = 0.0;
                for (int i = 0; i < z.Length; i++)
                {
                    double x = aCvx * Math.Exp(volBeforeMidT * z[i]) + b;
                    double midTprice = (x > 0.0) ? BlackScholesOption.Price(x, k, volAfterMidT, dT, q)
                        : (q > 0.0) ? 0.0 : k - x;
                    price += quadWeights[i] * midTprice;
                }
                return price;
            }
            public double Price(double vol, double q)
            {
                return Price(vol, vol, q);
            }
            public double Price(Func<double, double> terminalVol, double q)
            {
                var volBeforeMidT = terminalVol(midT);
                var varBeforeMidT = Math.Pow(volBeforeMidT, 2.0) * midT;
                var varAtMaturity = Math.Pow(terminalVol(maturity), 2.0) * maturity;

                if (varAtMaturity < varBeforeMidT)
                    Trace.TraceWarning("BS with div pricer : Calendar spread arbitrage in vol term structure !");

                var volAfterMidT = Math.Max(0.0, (varAtMaturity - varBeforeMidT) / dT);
                volAfterMidT = Math.Sqrt(volAfterMidT);

                return Price(volBeforeMidT, volAfterMidT, q);
            }
        }
        #endregion
    }
}