﻿using System;
using System.Runtime.CompilerServices;

namespace PragmaticQuant.Model.Equity.Dividends
{
    public sealed class DiscreteLocalDividend
    {
        private readonly double cash;
        private readonly double yield;
        #region protected method
        private DiscreteLocalDividend(DateTime date, double cash, double yield)
        {
            Date = date;
            this.cash = cash;
            this.yield = yield;
        }
        #endregion

        public static DiscreteLocalDividend AffineDividend(DateTime date, double cash, double yield)
        {
            return new DiscreteLocalDividend(date, cash, yield);
        }
        public static DiscreteLocalDividend ZeroDiv(DateTime date)
        {
            return AffineDividend(date, 0.0, 0.0);
        }
        
        public DateTime Date { get; private set; }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public double ApplyLogSpotDiv(double logSpot)
        {
            double spotBeforeDiv = Math.Exp(logSpot);
            double div = Math.Min(spotBeforeDiv * 0.90, yield * spotBeforeDiv + cash);
            return Math.Log(spotBeforeDiv - div);
        }
    }
    
}