using System;
using PragmaticQuant.Basic;

namespace PragmaticQuant.Model.Equity
{
    public static class EquityFactorRepresentationFactory
    {
        public static EquitySpotRepresentation Build(EquityModel model, RiskFactor[] modelFactors, PaymentInfo probaMeasure)
        {            
            var asset = model.Asset;
            var spotFactorIndex = modelFactors.FindIndex(rf => rf.Equals(RiskFactor.EquitySpot(asset)));
            var assetDiscountCurve = model.AssetDiscount;

            if (!probaMeasure.Financing.Currency.Equals(probaMeasure.Currency)
                || !probaMeasure.Currency.Equals(assetDiscountCurve.Financing.Currency))
                throw new NotImplementedException("TODO !");
            
            return new EquitySpotRepresentation(asset, assetDiscountCurve, probaMeasure, spotFactorIndex, modelFactors.Length);
        }
    }
}