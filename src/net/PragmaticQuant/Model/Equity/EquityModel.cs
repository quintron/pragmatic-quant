using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets;
using PragmaticQuant.Model.Equity.Dividends;

namespace PragmaticQuant.Model.Equity
{
    public abstract class EquityModel : ISingleRiskModel
    {
        protected EquityModel(AssetId asset,
                              ITimeMeasure time, 
                              double spot, 
                              DiscountCurve assetDiscount,  
                              DiscreteLocalDividend[] dividends)
        {
            if (!asset.Name.Equals(assetDiscount.Financing.Id, StringComparison.InvariantCultureIgnoreCase ))
                throw new Exception("EquityModel : Invalid asset discount curve !");

            Asset = asset;
            Time = time;
            Spot = spot;
            AssetDiscount = assetDiscount;
            Dividends = dividends;            

            RiskId = RiskId.Equity(asset);
            SpotFactor = RiskFactor.EquitySpot(Asset);
        }
        
        public AssetId Asset { get; private set; }
        
        public double Spot { get; private set; }
        public DiscountCurve AssetDiscount { get; private set; }
        public DiscreteLocalDividend[] Dividends { get; private set; }        

        public abstract RiskFactor[] RiskFactors { get; }
        public abstract ModelFactor FactorProcess { get; }
        public ITimeMeasure Time { get; private set; }
        public RiskId RiskId { get; private set; }
        public RiskFactor SpotFactor { get; private set; }
    }

    public abstract class EquityModel<TVol> : EquityModel
    {
        protected EquityModel(AssetId asset,
                              ITimeMeasure time,
                              double spot,
                              DiscountCurve assetDiscount,
                              DiscreteLocalDividend[] dividends,
                              IStepVolProvider<TVol> volProvider)
            : base(asset, time, spot, assetDiscount, dividends)
        {            
            VolProvider = volProvider;
        }
                
        public IStepVolProvider<TVol> VolProvider { get; private set; }        
    }
}