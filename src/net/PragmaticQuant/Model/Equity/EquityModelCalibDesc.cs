using System.Collections.Generic;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;

namespace PragmaticQuant.Model.Equity
{
    public class EquityModelCalibDesc : ICalibrationDescription
    {
        public EquityModelCalibDesc(string asset, RawMapDatas<DateOrDuration, double> dividendYieldProportion)
        {
            Assets = new[] {asset};
            DividendYieldProportion = new Dictionary<string, RawMapDatas<DateOrDuration, double>> {{asset, dividendYieldProportion}};
            EquityCorrelations = new LabelledMatrix<string, string, double>(Assets, Assets, new[,] {{1.0}});
        }
        public EquityModelCalibDesc(string[] assets,
                                    RawMapDatas<DateOrDuration, double>[] dividendYieldProportion, 
                                    LabelledMatrix<string, string, double> equityCorrelations) 
        {
            Assets = assets;
            EquityCorrelations = equityCorrelations;
            DividendYieldProportion = assets.ZipToDictionary(dividendYieldProportion);
        }
        
        public string[] Assets { get; private set; }
        public IDictionary<string, RawMapDatas<DateOrDuration, double>> DividendYieldProportion { get; private set; }
        public LabelledMatrix<string, string, double> EquityCorrelations { get; private set; }
    }
}