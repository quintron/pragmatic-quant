using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets.Equity;
using PragmaticQuant.Markets;

namespace PragmaticQuant.Model.Equity
{
    public abstract class EquityModelDescription : ISingleRiskModelDescription
    {
        #region protected methods
        protected static RawMapDatas<DateOrDuration, double> ConstantRawFunc(double value)
        {
            return new RawMapDatas<DateOrDuration, double>(new[] {new DateOrDuration(0 * Duration.Day)}, new[] {value});
        }
        protected EquityModelDescription(AssetId asset, Currency assetCurrency, RawMapDatas<DateOrDuration, double> dividendYieldProportion)
        {
            Asset = asset;
            AssetCurrency = assetCurrency;
            DividendYieldProportion = dividendYieldProportion;
            SpotFactor = RiskFactor.EquitySpot(asset);
        }
        #endregion 
        
        public AssetId Asset { get; private set; }
        public Currency AssetCurrency { get; private set; }
        public RawMapDatas<DateOrDuration, double> DividendYieldProportion { get; private set; }
        public RiskId Risk
        {
            get { return RiskId.Equity(Asset); }
        }
        public readonly RiskFactor SpotFactor;
        public abstract RiskFactor[] RiskFactors { get; }
        public abstract ModelFactorDescription ModelFactorDescription { get; }
    }

    public abstract class EquityModelFactory<TModelDesc, TVol> : SingleRiskModelFactory<TModelDesc>
                            where TModelDesc : EquityModelDescription                            
    {
        public override ISingleRiskModel Build(TModelDesc equityModelDesc, Market market)
        {
            AssetMarket assetMkt = market.AssetMarketFromName(equityModelDesc.Asset.Name);
            AffineDividend[] affineDivModel = EquityModelUtils.AffineDivModel(equityModelDesc.DividendYieldProportion, assetMkt);
            var equityModel = BuildEquityModel(equityModelDesc, affineDivModel, assetMkt);
            return equityModel;
        }
        protected abstract EquityModel<TVol> BuildEquityModel(TModelDesc equityModel, AffineDividend[] affineDivModel, AssetMarket assetMkt);
    }
}