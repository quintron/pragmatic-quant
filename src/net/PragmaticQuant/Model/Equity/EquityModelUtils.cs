using System;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Core;
using PragmaticQuant.Markets;
using PragmaticQuant.Markets.Equity;
using PragmaticQuant.Model.Equity.Dividends;

namespace PragmaticQuant.Model.Equity
{
    public static class EquityModelUtils
    {
        public static DiscreteLocalDividend[] DivModel(this AffineDividend[] affineDivs)
        {
            return affineDivs.Map(div=>  DiscreteLocalDividend.AffineDividend(div.Date, div.Cash, div.Yield));
        }
        public static AffineDividend[] AffineDivModel(RawMapDatas<DateOrDuration, double> dividendYieldProportion, AssetMarket assetMkt)
        {
            if (!dividendYieldProportion.Pillars.Any())
                throw new Exception("EquityModelUtils.AffineDivModel : dividend yield proportion need at least one pillar !");

            var divYieldProportion = dividendYieldProportion.ToAffineStepFunction(assetMkt.Time);
            return AffineDivModel(divYieldProportion.Eval, assetMkt);
        }
        public static AffineDividend[] AffineDivModel(Func<double, double> dividendYieldProportion, AssetMarket assetMkt)
        {
            var divModelStripping = new AffineDivModelStripping(assetMkt.Spot, assetMkt.AssetDiscountCurve, assetMkt.Time);
            return divModelStripping.StripAffineDiv(assetMkt.ForwardDivs, dividendYieldProportion);
        }

        public static AssetMarket DriftFreeAssetMarket(AssetMarket assetMkt, RawMapDatas<DateOrDuration, double> divModel)
        {
            AffineDividend[] affineDivModel = AffineDivModel(divModel, assetMkt);
            VolatilityMatrix driftFreeVolMatrix = DriftFreeVolMatrix(assetMkt.Spot, assetMkt.AssetDiscountCurve, affineDivModel, assetMkt.VolMatrix);
            foreach (var smile in driftFreeVolMatrix.Smiles)
                smile.Pillars.Mult(1.0 / assetMkt.Spot);
            var flatCurve = DiscountCurve.Flat(assetMkt.AssetDiscountCurve.Financing, assetMkt.AssetDiscountCurve.RefDate, 0.0);
            return new AssetMarket(assetMkt.Asset, assetMkt.Time,
                                   1.0, flatCurve, RawMapDatasUtils.Constant<DateTime, double>(0.0),
                                   driftFreeVolMatrix);
        }
        public static VolatilityMatrix DriftFreeVolMatrix(double spot, 
                                                          DiscountCurve assetDiscountCurve, 
                                                          AffineDividend[] affineDivModel, 
                                                          VolatilityMatrix volMatrix)
        {
            var affineDivUtils = new AffineDivCurveUtils(affineDivModel, assetDiscountCurve, volMatrix.Time);
            return affineDivUtils.DriftFreeVolMatrix(volMatrix, spot);
        }
    }
}