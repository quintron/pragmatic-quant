using System;
using System.Collections.Generic;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Maths.Stochastic;
using PragmaticQuant.Model.Equity.BlackScholes;

namespace PragmaticQuant.Model.Equity
{
    public abstract class EquityPathGenFactory<TVol> : IModelPathGeneratorFactory       
    {
        #region private fields
        private readonly ISimulationSchedule simulSchedule;
        #endregion
        #region private methods
        private ISimulationSchedule ScheduleBuilder(EquityModel<TVol>[] models)
        {
            //Specific schedule builder for Black-Scholes model
            if (models.All(m => m is BlackScholesModel))
                return new RegularSimulationSchedule(1000 * Duration.Year);

            return simulSchedule;
        }
        private static DateTime[] StepSimulatedDates(EquityModel<TVol>[] eqtyModels, ISimulationSchedule scheduleBuilder, DateTime refDate, DateTime stepStart, DateTime stepEnd)
        {
            if (refDate > stepStart || stepStart >= stepEnd)
                throw new Exception("invalid dates");

            var allTechDates = new List<DateTime>();
            foreach (var model in eqtyModels)
            {
                var divDates = model.Dividends.Where(div => stepStart < div.Date && div.Date <= stepEnd)
                                    .Select(div => div.Date)
                                    .OrderBy(d => d)
                                    .ToArray();
                allTechDates.AddRange(divDates);
            }

            var techDates = allTechDates.Distinct()
                                        .OrderBy(d => d)
                                        .ToArray();

            return scheduleBuilder.Schedule(stepStart, stepEnd, refDate, techDates);
        }
        protected abstract ISingleRiskPathGenerator EquityPathGenerator(EquitySimulatorStepDatas<TVol>[] stepSimulDatas, EquityModel<TVol> eqtyModel, DateTime probMeasureHorizon);
        #endregion
        protected EquityPathGenFactory(MonteCarloConfig mcConfig)
        {
            simulSchedule = mcConfig.ScheduleBuilder;
        }
        public IProcessPathGenerator Build(IModel model, PaymentInfo probaMeasure, DateTime[] pathDates)
        {
            EquityModel<TVol>[] eqtyModels = model.GetEquityParts<EquityModel<TVol>>();

            var refDate = eqtyModels.Select(m => m.Time.RefDate).Distinct().Single();
            ISimulationSchedule scheduleBuilder = ScheduleBuilder(eqtyModels);
            var steps = EnumerableUtils.For(0, pathDates.Length, i =>
                            new { Start = i > 0 ? pathDates[i - 1] : refDate, End = pathDates[i] });
            var simulatedDates = steps.Map(step => StepSimulatedDates(eqtyModels, scheduleBuilder, refDate, step.Start, step.End));

            var eqtyPathGens = eqtyModels.Map(eqtyModel =>
            {
                if (!probaMeasure.Financing.Currency.Equals(probaMeasure.Currency)
                    || !probaMeasure.Currency.Equals(eqtyModel.AssetDiscount.Financing.Currency))
                    throw new NotImplementedException("TODO !");
                
                EquityStepSimulBuilder<TVol> stepVolBuilder = new EquityStepSimulBuilder<TVol>(eqtyModel, probaMeasure.Date, eqtyModel.VolProvider);
                EquitySimulatorStepDatas<TVol>[] stepSimulDatas = EnumerableUtils.For(0, pathDates.Length, i =>
                {
                    var step = steps[i];
                    return stepVolBuilder.StepDatas(step.Start, step.End, simulatedDates[i]);
                });
                return EquityPathGenerator(stepSimulDatas, eqtyModel, probaMeasure.Date);
            });
            
            return new CompositePathGenerator(eqtyPathGens);
        }
        
        public OrnsteinUhlenbeckNd GaussianFactors(IModel model, PaymentInfo probaMeasure)
        {
            var factorProcess = model.ModelFactor();
            if (factorProcess.Item1.Any(f => f.Id.Type != RiskType.Eqy))
                throw new Exception("LocalPathGenFactory : equity model expected !");
            return factorProcess.Item2.OrnsteinProcess();
        }
        public bool AcceptModel(IModel model)
        {
            return model.IsDetRateEquityModel<TVol>();
        }
    }
}