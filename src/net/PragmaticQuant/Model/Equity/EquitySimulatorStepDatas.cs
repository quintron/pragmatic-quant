using System.Diagnostics;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Model.Equity.Dividends;

namespace PragmaticQuant.Model.Equity
{
    public class EquitySimulatorStepDatas
    {
        public EquitySimulatorStepDatas(RealInterval step, double[] dates, double[] horizonDiscounts, DiscreteLocalDividend[] dividends, bool[] isDivDates)
        {
            Debug.Assert(dates.Length == isDivDates.Length);
            Debug.Assert(dividends.Length == horizonDiscounts.Length);
            Debug.Assert(dates.All(step.Contain));
            Debug.Assert(EnumerableUtils.IsSorted(dividends.Where(d=> d!=null)
                                                           .Map(div => div.Date)));
            Debug.Assert(isDivDates.Length == dividends.Length);

            Step = step;
            Dates = dates;
            HorizonDiscounts = horizonDiscounts;
            Dividends = dividends;
            IsDivDates = isDivDates;
        }
        public EquitySimulatorStepDatas(EquitySimulatorStepDatas datas)
            : this(datas.Step, datas.Dates, datas.HorizonDiscounts, datas.Dividends, datas.IsDivDates) { }

        public RealInterval Step { get; private set; }
        public double[] Dates { get; private set; }
        public double[] HorizonDiscounts { get; private set; }
        public DiscreteLocalDividend[] Dividends { get; private set; }
        public bool[] IsDivDates { get; private set; }
    }

    public class EquitySimulatorStepDatas<TVol> : EquitySimulatorStepDatas
    {
        public EquitySimulatorStepDatas(EquitySimulatorStepDatas eqyStepDatas, TVol[] volDatas)
            : base(eqyStepDatas)
        {
            Debug.Assert(eqyStepDatas.HorizonDiscounts.Length == volDatas.Length);
            VolDatas = volDatas;
        }
        public TVol[] VolDatas { get; private set; }
    }
}