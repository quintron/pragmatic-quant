using System;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Model.Equity.Dividends;

namespace PragmaticQuant.Model.Equity
{
    public class EquityStepSimulBuilder<TVol>
    {
        #region private fields
        private readonly EquityModel<TVol> model;
        private readonly DateTime probaMeasureHorizon;
        private readonly IStepVolProvider<TVol> stepVolProvider;
        #endregion
        #region private methods
        private DiscreteLocalDividend[] StepDividends(DateTime start, DateTime end)
        {
            return model.Dividends.Where(div => start < div.Date && div.Date <= end)
                        .OrderBy(div => div.Date)
                        .ToArray();
        }
        private int[] FindSubDatesIndexes(DateTime[] dates, DateTime[] subDates)
        {
            return subDates.Map(dates.FindIndex);
        }
        private EquitySimulatorStepDatas InnerStepDatas(DateTime start, DateTime end, DateTime[] simulatedDates)
        {
            if (simulatedDates.Any(d => (d <= start && d > end)))
                throw new Exception("EquityStepSimulBuilder : simulatedDates must be contains in step !");

            var stepDivs = StepDividends(start, end);
            DiscreteLocalDividend[] dividends = new DiscreteLocalDividend[simulatedDates.Length];
            var divIndexes = FindSubDatesIndexes(simulatedDates, stepDivs.Map(div => div.Date));
            for (int i = 0; i < divIndexes.Length; i++)
            {
                var divIndex = divIndexes[i];
                if (divIndex < 0)
                    throw new Exception("EquityStepSimulBuilder : dividend date is not simulated");
                dividends[divIndex] = stepDivs[i];
            }

            bool[] isDivDate = dividends.Map(d => d != null);

            var horizonZc = model.AssetDiscount.Zc(probaMeasureHorizon);
            var horizonDiscounts = simulatedDates.Map(d => horizonZc / model.AssetDiscount.Zc(d));
            var step = RealInterval.Compact(model.Time[start], model.Time[end]);
            return new EquitySimulatorStepDatas(step, simulatedDates.Map(d => model.Time[d]), horizonDiscounts, dividends, isDivDate);
        }
        private TVol[] VolDatas(DateTime start, DateTime[] dates)
        {
            var vols = EnumerableUtils.For(0, dates.Length,
                i => stepVolProvider.StepVol(i > 0 ? dates[i - 1] : start, dates[i]));
            return vols;
        }
        #endregion
        internal EquityStepSimulBuilder(EquityModel<TVol> model, DateTime probaMeasureHorizon, IStepVolProvider<TVol> stepVolProvider)
        {
            this.model = model;
            this.probaMeasureHorizon = probaMeasureHorizon;
            this.stepVolProvider = stepVolProvider;
        }
        
        public EquitySimulatorStepDatas<TVol> StepDatas(DateTime start, DateTime end, DateTime[] simulatedDates)
        {
            var eqtyStepDatas = InnerStepDatas(start, end, simulatedDates);
            return new EquitySimulatorStepDatas<TVol>(eqtyStepDatas, VolDatas(start, simulatedDates));
        }
        
    }

    public interface IStepVolProvider<out TVol>
    {
        TVol StepVol(DateTime startDate, DateTime endDate);
    }

}