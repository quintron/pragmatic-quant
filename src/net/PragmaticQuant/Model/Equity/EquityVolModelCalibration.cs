using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets;
using PragmaticQuant.Product;
using PragmaticQuant.Model.Rate;

namespace PragmaticQuant.Model.Equity
{
    public abstract class EquityVolModelCalibration<TCalib> : ModelCalibration<TCalib>
        where TCalib : EquityModelCalibDesc
    {
        protected abstract EquityModelDescription CalibrateSingleAsset(AssetMarket assetMkt, TCalib model, IProduct product);
        
        public override IModelDescription Calibrate(TCalib model, Market market, IProduct product)
        {
            var assetModels = model.Assets.Map(asset =>
            {
                AssetMarket assetMkt = market.AssetMarketFromName(asset);
                return CalibrateSingleAsset(assetMkt, model, product);
            });

            var interStockCorrels = new RiskFactorCorrelation();
            for (int i = 0; i < assetModels.Length; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    double correl = model.EquityCorrelations.GetVal(model.Assets[i], model.Assets[j]);
                    interStockCorrels.AddCorrelation(assetModels[i].SpotFactor, assetModels[j].SpotFactor, correl);
                }
            }

            var assetCurrencies = assetModels.Select(eqtyModel => eqtyModel.AssetCurrency).Distinct();
            var detRateModels = assetCurrencies.Map(c => new DeterministicRateDescription(c));

            var riskParts = ArrayUtils.Append<ISingleRiskModelDescription>(assetModels, detRateModels);
            return new ModelDescription(interStockCorrels, riskParts);
        }
    }
}