using System;
using System.Linq;
using PragmaticQuant.Core;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Maths;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Maths.Interpolation;

namespace PragmaticQuant.Model.Equity.LocalVolatility
{
    public abstract class LocalVarianceInterpoler
    {
        #region private fields
        private readonly bool useTwoPointQuadrature;
        protected readonly StepSearcher maturityStepSearcher;
        protected readonly double[] maturities;
        #endregion
        #region private methods
        private double[] QuadratureSample(double start, double end, double[] logSpots)
        {
            var dt = (end - start);
            if (useTwoPointQuadrature)
            {
                //2 points Gauss-Legendre quadrature
                var average = VarianceSample(start + dt * 0.211324865405187, logSpots);
                var sample2 = VarianceSample(start + dt * 0.788675134594813, logSpots);
                average.Add(sample2);
                average.Mult(0.5 * dt);
                return average;
            }
            else
            {
                var average = VarianceSample(start + dt * 0.5, logSpots);
                average.Mult(dt);
                return average;
            }
        }
        #endregion
        protected LocalVarianceInterpoler(double[] maturities, bool useTwoPointQuadrature)
        {
            this.maturities = maturities;
            this.useTwoPointQuadrature = useTwoPointQuadrature;
            maturityStepSearcher = new StepSearcher(maturities);
        }
        
        public abstract double[] VarianceSample(double t, double[] logSpots);
        public double[] AveragedVarianceSample(double start, double end, double[] logSpots)
        {
            if (start > end)
                throw new Exception("LocalVariance.TimeAverage : start must be lower than end");

            var dt = (end - start);

            if (DoubleUtils.EqualZero(dt))
            {
                return VarianceSample(0.5 * (start + end), logSpots);
            }

            int startIndex = maturityStepSearcher.LocateLeftIndex(start);
            int endIndex = maturityStepSearcher.LocateLeftIndex(end);

            double[] averageSample;
            if (startIndex == endIndex)
            {
                averageSample = QuadratureSample(start, end, logSpots);
            }
            else
            {
                averageSample = QuadratureSample(start, maturities[startIndex + 1], logSpots);
                for (int i = startIndex + 1; i < endIndex; i++)
                {
                    averageSample.Add(QuadratureSample(maturities[i], maturities[i + 1], logSpots));
                }
                averageSample.Add(QuadratureSample(maturities[endIndex], end, logSpots));
            }

            averageSample.Mult(1.0 / (end - start));
            return averageSample;
        }
    }

    public sealed class LocalVariance : LocalVarianceInterpoler
    {
        #region private fields
        private readonly bool fixArbitrage;
        private readonly double densityRatioFloor;
        private readonly double arbitrageCvxFloor;
        private readonly StepFunction<Polynomial>[] pillarVariances; 
        #endregion
        #region private methods
        private void SplineData(StepFunction<Polynomial> spline, int index, 
                                out Polynomial poly, out double pillar, out double nextPillar)
        {
            poly = spline.Values[index];
            pillar = spline.Pillars[Math.Max(0, index)];
            nextPillar = (index + 1 < spline.Pillars.Length) ? spline.Pillars[index + 1] : double.PositiveInfinity;
        }
        private void SampleVarianceAndDerivatives(StepFunction<Polynomial> spline, double[] y, 
                                                  ref double[] v, ref double[] dvdy, ref double[] d2vd2y)
        {
            int currentLeftIndex;
            Polynomial currentPoly = spline.Eval(y[0], out currentLeftIndex);
            double currentPillar = spline.Pillars[Math.Max(0, currentLeftIndex)];
            double nextPillar = (currentLeftIndex + 1 < spline.Pillars.Length) ? spline.Pillars[currentLeftIndex + 1] : double.PositiveInfinity;
            
            for (int i = 0; i < y.Length; i++)
            {
                double yi = y[i];
                while(yi > nextPillar)
                {
                    currentLeftIndex++;
                    SplineData(spline, currentLeftIndex, out currentPoly, out currentPillar, out nextPillar);
                }
                
                var derivatives = currentPoly.TaylorDev(yi - currentPillar).Coeffs;
                v[i] = derivatives[0];
                dvdy[i] = derivatives[1];
                d2vd2y[i] = (derivatives.Length > 2) ? 2.0 * derivatives[2] : 0.0;
            }

        }
        private void SampleVarianceAndDerivatives(double t, int stepIndex, double[] y, 
                                                  ref double[] v, ref double[] dvdy, ref double[] d2vd2y, ref double[] dvdt)
        {
            if (stepIndex < 0) //LeftExtrapolation
            {
                SampleVarianceAndDerivatives(pillarVariances[0], y, ref v, ref dvdy, ref d2vd2y);

                dvdt.Add(v, 1.0 / maturities[0]);
                double w = t / maturities[0];
                v.Mult(w);
                dvdy.Mult(w);
                d2vd2y.Mult(w);
                return;
            }

            if (stepIndex == maturities.Length - 1) //Right Extrapolation
            {
                SampleVarianceAndDerivatives(pillarVariances[stepIndex], y, ref v, ref dvdy, ref d2vd2y);

                dvdt.Add(v, 1.0 / maturities[stepIndex]);
                double w = t / maturities[stepIndex];
                v.Mult(w);
                dvdy.Mult(w);
                d2vd2y.Mult(w);
                
            }
            else
            {
                SampleVarianceAndDerivatives(pillarVariances[stepIndex], y, ref v, ref dvdy, ref d2vd2y);

                var step = maturities[stepIndex + 1] - maturities[stepIndex];
                double w = (t - maturities[stepIndex]) / step;
                
                //TODO handle zero w
                //if (DoubleUtils.EqualZero(w))
                //    return;
                
                double[] nextv = new double[v.Length];
                double[] nextdvdy = new double[dvdy.Length];
                double[] nextd2vd2y = new double[d2vd2y.Length];
                SampleVarianceAndDerivatives(pillarVariances[stepIndex + 1], y, ref nextv, ref nextdvdy, ref nextd2vd2y);
                
                for (int i = 0; i < v.Length; i++)
                {
                    dvdt[i] = (nextv[i] - v[i]) / step;
                    v[i] = (1.0 - w) * v[i] + w * nextv[i];
                    dvdy[i] = (1.0 - w) * dvdy[i] + w * nextdvdy[i];
                    d2vd2y[i] = (1.0 - w) * d2vd2y[i] + w * nextd2vd2y[i];
                }
            }

        }
        private void SampleInstantVarAndDensityRatio(double t, int stepIndex, double[] logSpots, 
                                                     ref double[] densityRatioSample, ref double[] dVdTSample)
        {
            double[] v = new double[logSpots.Length];
            double[] dvdy = new double[logSpots.Length];
            double[] d2vd2y = new double[logSpots.Length];
            SampleVarianceAndDerivatives(t, stepIndex, logSpots, ref v, ref dvdy, ref d2vd2y, ref dVdTSample);
            if (fixArbitrage)
            {
                LocalVarianceUtils.FillDensityRatio(ref densityRatioSample, logSpots, v, dvdy, d2vd2y, arbitrageCvxFloor);
                densityRatioSample.MaxWith(densityRatioFloor);
                dVdTSample.MaxWith(0.0);
            }
            else
            {
                LocalVarianceUtils.FillDensityRatio(ref densityRatioSample, logSpots, v, dvdy, d2vd2y);
            }

        }
        /// <summary>
        ///                                  sigma^2
        /// short local variance = -------------------------------------------
        ///                         (y * (dsigma^2/dy) / (2 * sigma^2) - 1)^2 
        /// </summary>
        private void SampleShortTimeLocalVariance(double[] logSpots, ref double[] localVar)
        {
            double[] v = new double[logSpots.Length];
            double[] dvdy = new double[logSpots.Length];
            double[] d2vd2y = new double[logSpots.Length];
            SampleVarianceAndDerivatives(pillarVariances[0], logSpots, ref v, ref dvdy, ref d2vd2y);

            double oneOverDt = 1.0 / maturities[0];
            for (int i = 0; i < localVar.Length; i++)
            {
                var sigma2 = v[i] * oneOverDt;
                localVar[i] = sigma2 / Math.Pow(0.5 * logSpots[i] * dvdy[i] * oneOverDt / sigma2 - 1.0, 2.0);
            }
        }
        private LocalVariance(double[] maturities, StepFunction<Polynomial>[] pillarVariances, 
                              bool useTwoPointQuadrature,
                              bool fixArbitrage,
                              double densityRatioFloor, 
                              double arbitrageCvxFloor)
            :base(maturities, useTwoPointQuadrature)
        {
            if (maturities.Length != pillarVariances.Length)
                throw new Exception("LocalVariance : incompatible size !");

            if (!maturities.All(t => t > 0.0))
                throw new Exception("LocalVariance : all maturities should be positives !");

            this.pillarVariances = pillarVariances;
            this.fixArbitrage = fixArbitrage;
            this.densityRatioFloor = densityRatioFloor;
            this.arbitrageCvxFloor = arbitrageCvxFloor;
        }
        #endregion

        private const double DefaultDensityRatioFloor = 0.01;
        private const double DefaultArbitrageCvxFloor = -0.40;
        public static LocalVariance Build(double[] maturities, RawMapDatas<double, double>[] variances,
                                          bool fixArbitrage = false,
                                          double densityRatioFloor = DefaultDensityRatioFloor, 
                                          double arbitrageCvxFloor = DefaultArbitrageCvxFloor,
                                          bool useTwoPointQuadrature = false)
        {
            var pillarVariances = variances.Map(v => SplineUtils.BuildCubicSpline(v.Pillars, v.Values));
            return new LocalVariance(maturities, pillarVariances, useTwoPointQuadrature, fixArbitrage, densityRatioFloor, arbitrageCvxFloor);
        }
        
        /// <summary>
        /// Return instant variance at date t : dv/dt .
        /// </summary>
        public double[] InstantVarianceSample(double t, double[] logSpots)
        {
            if (t < 0.0)
                throw new Exception("LocalVariance : maturity should be positive");

            int stepIndex = maturityStepSearcher.LocateLeftIndex(t);
            double[] densityRatioSample = new double[logSpots.Length];
            double[] dVdTSample = new double[logSpots.Length];
            SampleInstantVarAndDensityRatio(t, stepIndex, logSpots, ref densityRatioSample, ref dVdTSample);
            return dVdTSample;
        }

        /// <summary>
        /// Return density ratio at date t : (y * (dv/dy) / (2 * v) - 1)^2 + 1/2 * (d^2 v/dy^2) - 1/4 * (1/4 + 1/v) * (dv/dy)^2 .
        /// </summary>
        public double[] DensityRatioSample(double t, double[] logSpots)
        {
            if (t < 0.0)
                throw new Exception("LocalVariance : maturity should be positive");

            int stepIndex = maturityStepSearcher.LocateLeftIndex(t);
            double[] densityRatioSample = new double[logSpots.Length];
            double[] dVdTSample = new double[logSpots.Length];
            SampleInstantVarAndDensityRatio(t, stepIndex, logSpots, ref densityRatioSample, ref dVdTSample);
            return densityRatioSample;
        }

        /// <summary>
        /// Return local variance at date t given by formula :
        ///                                                   dv/dt 
        /// local variance = ------------------------------------------------------------------------------------
        ///                   (y * (dv/dy) / (2 * v) - 1)^2 + 1/2 * (d^2 v/dy^2) - 1/4 * (1/4 + 1/v) * (dv/dy)^2
        /// </summary>
        public override double[] VarianceSample(double t, double[] logSpots)
        {
            if (t < 0.0)
                throw new Exception("LocalVariance : maturity should be positive");

            if (t < 1.0e-12)
            {
                var shortTimeLocalVol = new double[logSpots.Length];
                SampleShortTimeLocalVariance(logSpots, ref shortTimeLocalVol);
                return shortTimeLocalVol;
            }

            int stepIndex = maturityStepSearcher.LocateLeftIndex(t);
            double[] densityRatioSample = new double[logSpots.Length];
            var dVdTSample = new double[logSpots.Length];
            SampleInstantVarAndDensityRatio(t, stepIndex, logSpots, ref densityRatioSample, ref dVdTSample);
            return dVdTSample.ZipWith(densityRatioSample, (dvdt, dr) => dvdt / dr);
        }
    }

    public static class LocalVarianceUtils
    {
        #region private fields
        private static readonly RrFunction Y = RrFunctions.Affine(1.0, 0.0);
        #endregion

        /// <summary>
        /// return denominator of local variance : (y * (dv/dy) / (2 * v) - 1)^2 + 1/2 * (d^2 v/dy^2) - 1/4 * (1/4 + 1/v) * (dv/dy)^2
        /// </summary>
        public static RrFunction DensityRatio(RrFunction variance, RrFunction dVdY, RrFunction d2Vd2Y)
        {
            var densityRatio = (0.5 * Y) * dVdY / variance - 1.0;
            densityRatio = densityRatio * densityRatio + 0.5 * d2Vd2Y - (0.25 * 0.25 + 0.25 / variance) * dVdY * dVdY;
            return densityRatio;
        }

        /// <summary>
        /// return denominator of local variance : (y * (dv/dy) / (2 * v) - 1)^2 + 1/2 * (d^2 v/dy^2) - 1/4 * (1/4 + 1/v) * (dv/dy)^2
        /// </summary>
        public static RrFunction DensityRatio(RrFunction variance)
        {
            RrFunction dVdY = variance.Derivative();
            RrFunction d2Vd2Y = dVdY.Derivative();
            return DensityRatio(variance, dVdY, d2Vd2Y);
        }

        /// <summary>
        /// return denominator of local variance : (y * (dv/dy) / (2 * v) - 1)^2 + 1/2 * (d^2 v/dy^2) - 1/4 * (1/4 + 1/v) * (dv/dy)^2
        /// </summary>
        public static void FillDensityRatio(ref double[] densityRatios, double[] y, double[] variance, double[] dVdY, double[] d2Vd2Y, double arbitrageCvxFloor = double.NaN)
        {
            if (!double.IsNaN(arbitrageCvxFloor))
            {
                for (int i = 0; i < y.Length; i++)
                {
                    var dvdy = dVdY[i];
                    var densityRatio = 0.5 * y[i] * dvdy / variance[i] - 1.0;
                    densityRatio = densityRatio * densityRatio - 0.25 * (0.25 + 1.0 / variance[i]) * dvdy * dvdy;
                    densityRatio *= 1.0 + Math.Max(arbitrageCvxFloor, 0.5 * d2Vd2Y[i] / densityRatio);
                    densityRatios[i] = densityRatio;
                }
            }
            else
            {
                for (int i = 0; i < y.Length; i++)
                {
                    var dvdy = dVdY[i];
                    var densityRatio = 0.5 * y[i] * dvdy / variance[i] - 1.0;
                    densityRatio = densityRatio * densityRatio + 0.5 * d2Vd2Y[i] - 0.25 * (0.25 + 1.0 / variance[i]) * dvdy * dvdy;
                    densityRatios[i] = densityRatio;
                }
            }
        }

        /// <summary>
        ///                                  sigma^2
        /// short local variance = -------------------------------------------
        ///                         (y * (dsigma^2/dy) / (2 * sigma^2) - 1)^2 
        /// </summary>
        public static RrFunction ShortMaturityLocalVol(RrFunction shortInstantVariance)
        {
            var shortLocalVar = 0.5 * Y * shortInstantVariance.Derivative() / shortInstantVariance - 1.0;
            shortLocalVar = shortInstantVariance / (shortLocalVar * shortLocalVar);
            return shortLocalVar;
        }
    }
    
}