using System;
using System.Diagnostics;
using PragmaticQuant.Basic;
using PragmaticQuant.Model.Equity.Dividends;

namespace PragmaticQuant.Model.Equity.LocalVolatility
{
    using LocalVolSimulatorStepDatas = EquitySimulatorStepDatas<RegularGridAffineInterpol>;

    public sealed class LocalVolEquityPathGenerator : ISingleRiskPathGenerator
    {
        #region private fields
        private readonly RegularGridAffineInterpol[] localVols;
        private readonly double[] dts;
        private readonly bool[] isDivDates;
        private readonly double[] logDiscounts;
        private readonly DiscreteLocalDividend[] divs;
        private readonly int[] subStepSizes;
        private readonly double horizonInitialLogForward;
        #endregion
        public LocalVolEquityPathGenerator(EquitySimulatorStepDatas<RegularGridAffineInterpol>[] stepDatas, double horizonInitialForward)
        {
            horizonInitialLogForward = Math.Log(horizonInitialForward);
            localVols = EnumerableUtils.Append(stepDatas.Map(step => step.VolDatas));
            logDiscounts = EnumerableUtils.Append(stepDatas.Map(step => step.HorizonDiscounts.Map(Math.Log)));
            divs = EnumerableUtils.Append(stepDatas.Map(step => step.Dividends));
            isDivDates = EnumerableUtils.Append(stepDatas.Map(step => step.IsDivDates));
            
            dts = EnumerableUtils.Append(stepDatas.Map(step =>
            {
                var stepDates = step.Dates;
                var stepDts = new double[stepDates.Length];
                double prev = step.Step.Inf;
                for (int i = 0; i < stepDts.Length; i++)
                {
                    stepDts[i] = stepDates[i] - prev;
                    prev = stepDates[i];
                }
                return stepDts;
            }));

            subStepSizes = stepDatas.Map(step => step.Dates.Length);
            Dates = stepDatas.Map(step => step.Step.Sup);
            AllSimulatedDates = EnumerableUtils.Append(stepDatas.Map(data => data.Dates));
        }

        public void FillProcessPath(ref double[][] processPath, double[][] dWs, 
                                    ProcessRange processRange, ProcessRange brownianRange)
        {
            Debug.Assert(processRange.Size == ProcessDim && brownianRange.Size == RandomDim);
            int processIndex = processRange.StartIndex;
            int brownianIndex = brownianRange.StartIndex;

            double currentLogFwd = horizonInitialLogForward;
            for (int step = 0, index = 0; step < Dates.Length; step++)
            {
                for (int subStep = 0; subStep < subStepSizes[step]; ++subStep, ++index)
                {
                    double[] dW = dWs[index];
                    double logDiscount = logDiscounts[index];
                    double localVol = localVols[index].Eval(currentLogFwd + logDiscount);
                    currentLogFwd += localVol * dW[brownianIndex] - 0.5 * localVol * localVol * dts[index];
                    if (isDivDates[index])
                        currentLogFwd = divs[index].ApplyLogSpotDiv(currentLogFwd + logDiscount) - logDiscount;
                }
                processPath[step][processIndex] = currentLogFwd;
            }
        }
        
        public double[] Dates { get; private set; }
        public int ProcessDim { get { return 1; } }
        public int RandomDim { get { return 1; } }
        public double[] AllSimulatedDates { get; private set; }
    }
    
}