using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;

namespace PragmaticQuant.Model.Equity.LocalVolatility
{
    public class LocalVolModelCalibDesc : EquityModelCalibDesc
    {
        public LocalVolModelCalibDesc(string asset,
                                      RawMapDatas<DateOrDuration, double> dividendYieldProportion, 
                                      LocalVolSampleConfig localVolSampleConfig)
            : base(asset, dividendYieldProportion)
        {
            LocalVolSampleConfig = localVolSampleConfig;
        }
       
        public LocalVolModelCalibDesc(string[] assets,
                                      RawMapDatas<DateOrDuration, double>[] dividendYieldProportion, 
                                      LabelledMatrix<string, string, double> equityCorrelations,
                                      LocalVolSampleConfig localVolSampleConfig) 
            :base(assets, dividendYieldProportion, equityCorrelations)
        {
            LocalVolSampleConfig = localVolSampleConfig;
        }
        
        public LocalVolSampleConfig LocalVolSampleConfig { get; private set; }
    }

    public class LocalVolSampleConfig
    {
        public LocalVolSampleConfig(double gridQuantile = DefaultGridQuantile,
                                    int gridSize = DefaultGridSize,
                                    bool tryFixArbitrage = DefaultTryFixArbitrage)
        {
            GridQuantile = gridQuantile;
            GridSize = gridSize;
            TryFixArbitrage = tryFixArbitrage;
        }

        public const double DefaultGridQuantile = 1.0e-6;
        public const int DefaultGridSize = 251;
        public const bool DefaultTryFixArbitrage = true;

        public double GridQuantile { get; private set; }
        public int GridSize { get; private set; }
        public bool TryFixArbitrage { get; private set; }
    }
}