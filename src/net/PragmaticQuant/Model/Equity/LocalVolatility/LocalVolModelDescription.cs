using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets;
using PragmaticQuant.Product;

namespace PragmaticQuant.Model.Equity.LocalVolatility
{
    public class LocalVolModelDescription : EquityModelDescription
    {
        public LocalVolModelDescription(AssetId asset,
                                        Currency assetCurrency,
                                        RawMapDatas<DateOrDuration, double> dividendYieldProportion,
                                        LocalVolSampleConfig localVolSampleConfig)
            : base(asset, assetCurrency, dividendYieldProportion)
        {
            LocalVolSampleConfig = localVolSampleConfig;
        }
        
        public LocalVolSampleConfig LocalVolSampleConfig { get; private set; }
        public override RiskFactor[] RiskFactors
        {
            get { return new[] { SpotFactor }; }
        }
        public override ModelFactorDescription ModelFactorDescription
        {
            get { return ModelFactorDescription.Ornstein1D(SpotFactor.Name, 0.0, ConstantRawFunc(1.0)); }
        }
    }

    public class LocalVolModelCalibration : EquityVolModelCalibration<LocalVolModelCalibDesc>
    {
        public static readonly LocalVolModelCalibration Instance = new LocalVolModelCalibration();
        protected override EquityModelDescription CalibrateSingleAsset(AssetMarket assetMkt, LocalVolModelCalibDesc model, IProduct product)
        {
            var divModel = model.DividendYieldProportion[assetMkt.Asset.Name];
            return new LocalVolModelDescription(assetMkt.Asset, assetMkt.AssetCurrency, divModel, model.LocalVolSampleConfig);
        }
    }
}