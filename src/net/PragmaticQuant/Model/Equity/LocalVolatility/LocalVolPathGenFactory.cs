﻿using System;

namespace PragmaticQuant.Model.Equity.LocalVolatility
{
    public class LocalVolPathGenFactory : EquityPathGenFactory<RegularGridAffineInterpol>
    {
        #region private methods
        protected override ISingleRiskPathGenerator EquityPathGenerator(EquitySimulatorStepDatas<RegularGridAffineInterpol>[] stepSimulDatas, 
                                                                        EquityModel<RegularGridAffineInterpol> eqtyModel, 
                                                                        DateTime probMeasureHorizon)
        {
            var initialHorizonForward = eqtyModel.Spot / eqtyModel.AssetDiscount.Zc(probMeasureHorizon);
            return new LocalVolEquityPathGenerator(stepSimulDatas, initialHorizonForward); 
        }
        #endregion
        public LocalVolPathGenFactory(MonteCarloConfig mcConfig)
            :base(mcConfig)
        {
        }
    }
    
}