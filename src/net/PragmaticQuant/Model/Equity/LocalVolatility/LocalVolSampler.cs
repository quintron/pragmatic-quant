using System;
using PragmaticQuant.Core;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets;
using PragmaticQuant.Markets.Equity;
using PragmaticQuant.Model.Equity.Dividends;

namespace PragmaticQuant.Model.Equity.LocalVolatility
{
    public class LocalVolSampler
    {
        #region private fields
        private const double LocalVolCap = 6.0;
        private readonly ITimeMeasure time;
        private readonly LocalVarianceInterpoler driftFreeLocalVariance;
        private readonly AffineDivCurveUtils divCurveUtils;
        private readonly double spot;
        #endregion
        private LocalVolSampler(LocalVarianceInterpoler driftFreeLocalVariance, 
                                 AffineDivCurveUtils divCurveUtils, 
                                 double spot,
                                 ITimeMeasure time)
        {
            this.driftFreeLocalVariance = driftFreeLocalVariance;
            this.divCurveUtils = divCurveUtils;
            this.spot = spot;
            this.time = time;
        }
        
        public static LocalVolSampler Build(double spot, 
                                            DiscountCurve assetDiscountCurve, 
                                            AffineDividend[] affineDivModel, 
                                            VolatilityMatrix volMatrix, 
                                            bool tryFixArbitrage = true)
        {
            var time = volMatrix.Time;
            var affineDivUtils = new AffineDivCurveUtils(affineDivModel, assetDiscountCurve, time);
            VolatilityMatrix driftFreeVolMatrix = affineDivUtils.DriftFreeVolMatrix(volMatrix, spot);
            LocalVariance driftFreeLocalVariance = LocalVolUtils.LocalVariance(driftFreeVolMatrix, spot, tryFixArbitrage);
            return new LocalVolSampler(driftFreeLocalVariance, affineDivUtils, spot, time);
        }

        public double[] SampleLogSpotLocalVol(DateTime maturity, RegularGrid1D logSpotGrid)
        {
            return SampleLogSpotLocalVol(maturity, maturity, logSpotGrid);
        }
        public double[] SampleLogSpotLocalVol(DateTime startDate, DateTime endDate, RegularGrid1D logSpotGrid)
        {
            double start = time[startDate];
            double end = time[endDate];
            return SampleLogSpotLocalVol(start, end, logSpotGrid);
        }
        public double[] SampleLogSpotLocalVol(double start, double end, RegularGrid1D logSpotGrid)
        {
            double[] spotGrid = logSpotGrid.Exp(1.0).Value;
            return SampleLocalVol(start, end, spotGrid);
        }

        public double[] SampleLocalVol(DateTime maturity, double[] spotGrid)
        {
            return SampleLocalVol(maturity, maturity, spotGrid);
        }
        public double[] SampleLocalVol(DateTime startDate, DateTime endDate, double[] spotGrid)
        {
            double start = time[startDate];
            double end = time[endDate];
            return SampleLocalVol(start, end, spotGrid);
        }
        public double[] SampleLocalVol(double start, double end, double[] spotGrid)
        {
            double cashBpv = divCurveUtils.CashBpvAverage(start, end);
            double assetGrowth = Math.Sqrt((divCurveUtils.AssetGrowth(start) * divCurveUtils.AssetGrowth(end)));

            double[] driftFreeGrid = spotGrid.Map(s => Math.Log((s / assetGrowth + cashBpv) / spot));
            double[] driftFreeLocalVarianceSamples = driftFreeLocalVariance.AveragedVarianceSample(start, end, driftFreeGrid);
            
            return spotGrid.ZipWith(driftFreeLocalVarianceSamples,
                (s, v) => Math.Min(LocalVolCap, Math.Sqrt(v) * (1.0 + assetGrowth * cashBpv / s)));
        }
    }
    
}