using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets.Equity;

namespace PragmaticQuant.Model.Equity.LocalVolatility
{
    public static class LocalVolUtils
    {
        /// <summary>
        /// Local variance as function of logmoneyness of drift free underlying without div, rate, repo 
        /// </summary>
        public static LocalVariance LocalVariance(VolatilityMatrix driftFreeVolMatrix, double spot, bool tryFixArbitrage = true)
        {
            double[] timePillars = driftFreeVolMatrix.Time[driftFreeVolMatrix.Maturities];
            var pillarsLogVariances = driftFreeVolMatrix.LogMoneynessVariances(MoneynessProvider.LogSpot(spot));
            return LocalVolatility.LocalVariance.Build(timePillars, pillarsLogVariances, tryFixArbitrage);
        }
        
        public static RawMapDatas<double, double>[] LogMoneynessVariances(this VolatilityMatrix volMatrix, MoneynessProvider moneynessInterpolation)
        {
            double[] timePillars = volMatrix.Time[volMatrix.Maturities];
            return EnumerableUtils.For(0, timePillars.Length, i =>
            {
                double t = timePillars[i];
                var moneyness = moneynessInterpolation.Moneyness(t);
                double[] moneynessPillars = volMatrix.Smiles[i].Pillars.Map(moneyness);
                double[] varianceSlice = volMatrix.Smiles[i].Values.Map(v => t * v * v);
                return new RawMapDatas<double, double>(moneynessPillars, varianceSlice);
            });
        } 

    }

}