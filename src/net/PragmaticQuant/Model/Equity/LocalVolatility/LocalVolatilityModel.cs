﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using PragmaticQuant.Core;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets.Equity;
using PragmaticQuant.Markets;
using PragmaticQuant.Model.Equity.Dividends;

namespace PragmaticQuant.Model.Equity.LocalVolatility
{
    public class LocalVolatilityModel : EquityModel<RegularGridAffineInterpol>
    {
        public LocalVolatilityModel(AssetId asset, ITimeMeasure time,
                                    IStepVolProvider<RegularGridAffineInterpol> localVol,
                                    double spot,
                                    DiscountCurve assetDiscount,
                                    DiscreteLocalDividend[] dividends)
            : base(asset, time, spot, assetDiscount, dividends, localVol)
        {
        }
        
        public override RiskFactor[] RiskFactors
        {
            get { return new[] {SpotFactor}; }
        }
        public override ModelFactor FactorProcess
        {
            get
            {
                return ModelFactor.Brownian1D(RiskFactors.First().Name, 0.0);
            }
        }
    }
    
    public sealed class RegularGridAffineInterpol
    {
        #region private fields
        private readonly double inf;
        private readonly double step;

        private readonly double[] f;
        private readonly double[] df;

        private readonly int lastIndex;
        private readonly double rightVal;
        private readonly double leftVal;
        #endregion
        public RegularGridAffineInterpol(RegularGrid1D grid, double[] f)
                : this(RealInterval.Compact(grid.BoundaryInf, grid.BoundarySup), f)
        {
            if (grid.Size!=f.Length)
                throw new Exception("RegularGridAffineInterpol : f incompatible with grid size");
        }
        public RegularGridAffineInterpol(RealInterval support, double[] f)
        {
            this.f = f;
            Support = support;
            Size = f.Length;
            
            lastIndex = f.Length - 1;
            rightVal = f[lastIndex];
            leftVal = f[0];

            inf = support.Inf;
            step = support.Length / (f.Length - 1);
            df = EnumerableUtils.For(0, f.Length - 1, i => f[i + 1] - f[i]);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public double Eval(double x)
        {
            double ratio = (x - inf) / step;
            int index = (int) ratio;

            if (index < 0)
                return leftVal;

            if (index >= lastIndex)
                return rightVal;
            
            return f[index] + (ratio - index) * df[index];
        }

        public RealInterval Support { get; private set; }
        public int Size { get; private set; }

        public RegularGridAffineInterpol Mult(double[] mults)
        {
            if (mults.Length != Size)
                throw new Exception("RegularGridAffineInterpol.Mult : incompatible mults size !");
            var newVals = f.Copy();
            newVals.Mult(mults);
            return new RegularGridAffineInterpol(Support, newVals);
        }
    }
    
    public class LocalVolModelFactory : EquityModelFactory<LocalVolModelDescription, RegularGridAffineInterpol>
    {
        public static readonly LocalVolModelFactory Instance = new LocalVolModelFactory();
        protected override EquityModel<RegularGridAffineInterpol> BuildEquityModel(LocalVolModelDescription lv, AffineDividend[] affineDivModel, AssetMarket assetMkt)
        {
            var stepLocalVolProvider = StepLocalVolProvider.Build(lv.LocalVolSampleConfig, affineDivModel, assetMkt);
            return new LocalVolatilityModel(assetMkt.Asset, assetMkt.Time, stepLocalVolProvider, 
                                            assetMkt.Spot, assetMkt.AssetDiscountCurve, affineDivModel.DivModel());
        }
    }
    
}