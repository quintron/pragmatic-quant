using System;
using PragmaticQuant.Core;
using PragmaticQuant.Basic;
using PragmaticQuant.Markets;
using PragmaticQuant.Markets.Equity;

namespace PragmaticQuant.Model.Equity.LocalVolatility
{
    public class StepLocalVolProvider : IStepVolProvider<RegularGridAffineInterpol>
    {
        #region private fields
        private readonly LocalVolSampleConfig config;
        private readonly BlackLawQuantileProvider lawQuantileProvider;
        private readonly LocalVolSampler localVolSampler;
        #endregion
        public StepLocalVolProvider(LocalVolSampleConfig config, BlackLawQuantileProvider lawQuantileProvider, LocalVolSampler localVolSampler)
        {
            this.config = config;
            this.lawQuantileProvider = lawQuantileProvider;
            this.localVolSampler = localVolSampler;
        }

        public static IStepVolProvider<RegularGridAffineInterpol> Build(LocalVolSampleConfig lvConfig, AffineDividend[] affineDivModel, AssetMarket assetMkt)
        {
            var localVolSampler = LocalVolSampler.Build(assetMkt.Spot, assetMkt.AssetDiscountCurve, affineDivModel,
                                                        assetMkt.VolMatrix, lvConfig.TryFixArbitrage);
            return new StepLocalVolProvider(lvConfig, assetMkt.LawQuantile(), localVolSampler);
        }

        public RegularGridAffineInterpol StepVol(DateTime startDate, DateTime endDate)
        {
            RealInterval logSpotSupport = lawQuantileProvider.LogSpotSupport(endDate, config.GridQuantile, 1.0 - config.GridQuantile);
            var logSpotGrid  = new RegularGrid1D(config.GridSize, logSpotSupport.Inf, logSpotSupport.Sup);
            double[] localVolSample = localVolSampler.SampleLogSpotLocalVol(startDate, endDate, logSpotGrid);
            return new RegularGridAffineInterpol(logSpotGrid, localVolSample);
        }
    }
    
}