using System.Diagnostics;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Markets.Equity;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Maths.Interpolation;

namespace PragmaticQuant.Model.Equity.LocalVolatility
{
    public class VarianceInterpoler
    {
        #region private fields
        private readonly StepSearcher maturityStepSearcher;
        private readonly double[] maturityPillars;
        private readonly RrFunction[] pillarVariances;
        #endregion
        private VarianceInterpoler(double[] maturityPillars, RrFunction[] pillarVariances)
        {
            Debug.Assert(maturityPillars.All(t => t > 0.0));

            this.maturityPillars = maturityPillars;
            this.pillarVariances = pillarVariances;
            maturityStepSearcher = new StepSearcher(maturityPillars);
        }
        
        public static VarianceInterpoler BuildInterpol(VolatilityMatrix volMatrix, MoneynessProvider moneynessInterpolation)
        {
            double[] timePillars = volMatrix.Time[volMatrix.Maturities];
            var rawLogVariances = volMatrix.LogMoneynessVariances(moneynessInterpolation);
            var interpolatedLogVariances = rawLogVariances.Map(v =>
            {
                if (v.Pillars.Length == 1)
                    return SplineInterpoler.BuildLinearSpline(v.Pillars, v.Values, 0.0, 0.0);

                if (v.Pillars.Length == 2)
                    return SplineInterpoler.BuildLinearSpline(v.Pillars, v.Values, double.NaN, double.NaN);

                return (RrFunction) SplineInterpoler.BuildCubicSpline(v.Pillars, v.Values);
            });
            return new VarianceInterpoler(timePillars, interpolatedLogVariances);
        } 

        public double Eval(double t, double y)
        {
            var linearIndex = maturityStepSearcher.LocateLeftIndex(t);

            if (linearIndex < 0)
            {
                return t * pillarVariances[0].Eval(y) / maturityPillars[0];
            }
            if (linearIndex == pillarVariances.Length - 1)
            {
                return t * pillarVariances[pillarVariances.Length - 1].Eval(y) / maturityPillars[pillarVariances.Length - 1];
            }

            double w = (t - maturityPillars[linearIndex]) / (maturityPillars[linearIndex + 1] - maturityPillars[linearIndex]);
            return (1.0 - w) * pillarVariances[linearIndex].Eval(y) + w * pillarVariances[linearIndex + 1].Eval(y);
        }
        public RrFunction TimeSlice(double t)
        {
            var linearIndex = maturityStepSearcher.LocateLeftIndex(t);

            if (linearIndex < 0)
            {
                return t * pillarVariances[0] / maturityPillars[0];
            }
            if (linearIndex == pillarVariances.Length - 1)
            {
                return t * pillarVariances[pillarVariances.Length - 1] / maturityPillars[pillarVariances.Length - 1];
            }

            double w = (t - maturityPillars[linearIndex]) / (maturityPillars[linearIndex + 1] - maturityPillars[linearIndex]);
            return (1.0 - w) * pillarVariances[linearIndex] + w * pillarVariances[linearIndex + 1];
        }
    }
}