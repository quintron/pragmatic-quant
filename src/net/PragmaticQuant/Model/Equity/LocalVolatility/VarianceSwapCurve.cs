using System;
using System.Linq;
using PragmaticQuant.Core;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets;
using PragmaticQuant.Markets.Equity;

namespace PragmaticQuant.Model.Equity.LocalVolatility
{

    //TODO WIP Divs not yet included
    public class VarianceSwapCurve
    {
        #region private fields
        private const double SpaceGridQuantile = 1.0e-6;
        private const int SpaceGridSize = 801;
        private readonly ITimeMeasure time;
        private readonly BlackLawQuantileProvider lawSupport;
        private readonly LocalVolSampler volsampler;
        private readonly DiscountCurve assetDiscountCurve; 
        private readonly AffineDividend[] affineDivModel;
        #endregion
        #region private methods
        private IPdeStepSolver<RegularGrid1D, double[]> BuildScheme(double varianceStart, double varianceEnd)
        {
            var variancePde = new LocalVolVariancePde(volsampler, assetDiscountCurve, varianceStart, varianceEnd);
            var stepOperator = new FiniteDiff1D(variancePde, BoundaryCondition1D.NoConvexity());
            return PdeStepSolver.ThetaScheme(stepOperator, 0.5);
        }
        private ISlice<RegularGrid1D, double[]> TerminalVariance(DateTime end)
        {
            var logSpotGrid = Grid(end);
            ISlice<RegularGrid1D, double[]> variance = logSpotGrid.Exp(0.0);
            variance.Value.FillValue(0.0);
            return variance;
        }
        private RegularGrid1D Grid(DateTime d)
        {
            var support = lawSupport.LogSpotSupport(d, SpaceGridQuantile, 1.0 - SpaceGridQuantile);
            return new RegularGrid1D(SpaceGridSize, support.Inf, support.Sup);
        }
        private void RollBackSlice(ref ISlice<RegularGrid1D, double[]> slice, double start, double end, double dt, IPdeStepSolver<RegularGrid1D, double[]> scheme)
        {
            double currentDate = end - dt;
            while (currentDate > start)
            {
                scheme.Backward(ref slice, currentDate, currentDate + dt);
                currentDate -= dt;
            }
            scheme.Backward(ref slice, start, Math.Max(start, currentDate + dt));
        }
        private void AddDividendVarPayoff(ref ISlice<RegularGrid1D, double[]> slice, AffineDividend div)
        {
            var divVariance = slice.State.ToArray().Map(x =>
            {
                var divReturn = 1.0 - div.Yield - div.Cash * Math.Exp(-x);
                var divLogReturn = Math.Log(divReturn);
                return divLogReturn * divLogReturn;
            });
            slice.Add(divVariance);
        }
        private void RollBackSliceThrowDividend(ref ISlice<RegularGrid1D, double[]> slice, AffineDividend div)
        {
            var backVals = slice.State.ToArray();
            for (int i = 0; i < backVals.Length; i++)
            {
                double x = backVals[i]; 
                var xMinus = x + Math.Log(1.0 - div.Yield - div.Cash * Math.Exp(-x));
                backVals[i] = slice.LinearInterpolation(xMinus);
            }
            Vector.SetValue(slice.Value, backVals);
        }
        private void RollBackSlice(ISlice<RegularGrid1D, double[]> slice, DateTime start, DateTime end, double dt, IPdeStepSolver<RegularGrid1D, double[]> scheme)
        {
            var stepDivs = affineDivModel.Where(div => div.Date > start && div.Date < end).ToArray();
            var divDates = time[stepDivs.Map(div => div.Date)];

            var next = time[end];
            for (int i = stepDivs.Length - 1; i >= 0; i--)
            {
                RollBackSlice(ref slice, divDates[i], next, dt, scheme);
                AddDividendVarPayoff(ref slice, stepDivs[i]);
                RollBackSliceThrowDividend(ref slice, stepDivs[i]);
                next = divDates[i];
            }
            RollBackSlice(ref slice, time[start], next, dt, scheme);
        }
        #endregion

        public VarianceSwapCurve(ITimeMeasure time, LocalVolSampler volsampler, BlackLawQuantileProvider lawSupport, DiscountCurve assetDiscountCurve, AffineDividend[] affineDivModel)
        {
            this.time = time;
            this.volsampler = volsampler;
            this.lawSupport = lawSupport;
            this.assetDiscountCurve = assetDiscountCurve;
            this.affineDivModel = affineDivModel;
        }
        public static VarianceSwapCurve Build(AssetMarket assetMkt, AffineDividend[] affineDivModel)
        {
            var localVolSampler = LocalVolSampler.Build(assetMkt.Spot, assetMkt.AssetDiscountCurve, affineDivModel, assetMkt.VolMatrix, true);
            var lawSupport = new BlackLawQuantileProvider(assetMkt.VolSurface(), assetMkt.ForwardCurve().Fwd);
            return new VarianceSwapCurve(assetMkt.VolMatrix.Time, localVolSampler, lawSupport, assetMkt.AssetDiscountCurve, affineDivModel);
        }

        public double Vol(DateTime startDate, DateTime endDate)
        {
            var discreteSchedule = ScheduleUtils.RawSchedule(startDate, endDate, Duration.Day, true, false);
            discreteSchedule = BusinessConventions.ModifiedFollowing.Adjust(discreteSchedule, new SimpleCalendar());
            var dayAdj = 252.0 / discreteSchedule.Length;
            var start = discreteSchedule.First();
            var end = discreteSchedule.Last();
            
            var dt = time[end] / 251;
            IPdeStepSolver<RegularGrid1D, double[]> scheme = BuildScheme(time[start], time[end]);

            ISlice<RegularGrid1D, double[]> variance = TerminalVariance(end);
            RollBackSlice(variance, start, end, dt, scheme);
            if (start > time.RefDate)
                RollBackSlice(variance, time.RefDate, start, dt, scheme);
            
            var varSwapPrice = variance.LinearInterpolation(0.0);
            var volVarSwap = Math.Sqrt(varSwapPrice * dayAdj);
            return volVarSwap;
        }

        #region private class
        private class LocalVolVariancePde : PdeCoeffSampler1D<RegularGrid1D>
        {
            #region private fields
            private readonly double varianceStart;
            private readonly double varianceEnd;
            private readonly LocalVolSampler volsampler;
            private readonly DiscountCurve assetDiscountCurve;
            #endregion
            public LocalVolVariancePde(LocalVolSampler volsampler, 
                                       DiscountCurve assetDiscountCurve, 
                                       double varianceStart, 
                                       double varianceEnd)
            {
                this.volsampler = volsampler;
                this.assetDiscountCurve = assetDiscountCurve;
                this.varianceStart = varianceStart;
                this.varianceEnd = varianceEnd;
            }
            public override void FillCoeff(double start, double end, RegularGrid1D logSpotGrid, ref double[] d2X, ref double[] dX, ref double[] I)
            {
                var stepVariance = volsampler.SampleLogSpotLocalVol(start, end, logSpotGrid);
                stepVariance.ApplyPow(2.0); 
                d2X.Add(stepVariance, 0.5);
                dX.Add(stepVariance, -0.5);

                var discountDrift = Math.Log(assetDiscountCurve.Zc(start) / assetDiscountCurve.Zc(end)) / (end - start);
                dX.Add(discountDrift);

                I.FillValue(0.0);
            }
            public override void FillSourceTerm(double start, double end, RegularGrid1D logSpotGrid, ref double[] sourceTerm)
            {
                if (end <= start || start > varianceEnd || end < varianceStart)
                    return;

                var adjStart = Math.Max(varianceStart, start);
                var adjEnd = Math.Min(varianceEnd, end);

                var stepVol= volsampler.SampleLogSpotLocalVol(adjStart, adjEnd, logSpotGrid);
                sourceTerm.Add(stepVol);
                sourceTerm.ApplyPow(2.0);
                sourceTerm.Mult((adjEnd - adjStart) / (end - start));
            }
        }
        #endregion
    }

}