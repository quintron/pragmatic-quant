using System;
using PragmaticQuant.Basic;

namespace PragmaticQuant.Model.Equity
{
    public static class SabrFormulas
    {
        public static Func<double, double> Smile(double atmVol, double skew, double cvx)
        {
            var nu = Math.Sqrt(4.0 * skew * skew + cvx * cvx);
            var rho = - 2.0 * skew / nu;

            return strike =>
            {
                if (DoubleUtils.MachineEquality(1.0, strike))
                {
                    return atmVol;
                }

                var z = nu * Math.Log(strike) / atmVol;
                return atmVol * (-z) / Math.Log((Math.Sqrt(1.0 + 2.0 * rho * z + z * z) - z - rho) / (1.0 - rho));
            };
        }
        public static double[] ComputeSmile(double[] strikes, double atmVol, double skew, double cvx)
        {
            return strikes.Map(Smile(atmVol, skew, cvx));
        }
    }
}