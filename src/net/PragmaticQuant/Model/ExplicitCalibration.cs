using PragmaticQuant.Markets;
using PragmaticQuant.Product;

namespace PragmaticQuant.Model
{
    public class ExplicitCalibration : ICalibrationDescription
    {
        public ExplicitCalibration(IModelDescription model)
        {
            Model = model;
        }
        public ExplicitCalibration(params ISingleRiskModelDescription[] riskModels)
                : this(new ModelDescription(new RiskFactorCorrelation(), riskModels))
        {
        }
        public IModelDescription Model { get; private set; }
    }

    public class ExplicitModelCalibration : ModelCalibration<ExplicitCalibration>
    {
        public override IModelDescription Calibrate(ExplicitCalibration model, Market market, IProduct product)
        {
            return model.Model;
        }
    }

}