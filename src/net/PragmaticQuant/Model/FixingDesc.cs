using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PragmaticQuant.Basic.Dates;

namespace PragmaticQuant.Model
{
    public abstract class FixingDesc
    {
    }

    public abstract class ForwardDesc : FixingDesc
    {
        protected ForwardDesc(Duration maturity)
        {
            Maturity = maturity;
        }
        public Duration Maturity { get; private set; }
        public abstract FixingDesc NewMaturity(Duration newMaturity);
    }

    [DebuggerDisplay("{Maturity}{Underlying}")]
    public class ForwardDesc<T> : ForwardDesc
    {
        public ForwardDesc(Duration maturity, T underlying)
            : base(maturity)
        {
            Underlying = underlying;
        }
        public T Underlying { get; private set; }
        public override FixingDesc NewMaturity(Duration newMaturity)
        {
            return new ForwardDesc<T>(newMaturity, Underlying);
        }
    }

    public class LinearCombinationFixingDesc : FixingDesc
    {
        #region private methods
        private LinearCombinationFixingDesc(ForwardDesc[] fixings, double[] weights)
        {
            if (fixings.Length != weights.Length)
                throw new Exception("LinearCombinationFixingDesc : incompatible size !");
            Fixings = fixings;
            Weights = weights;
        }
        #endregion
        public static LinearCombinationFixingDesc Build(FixingDesc[] fixings, double[] weights)
        {
            var fs = new List<ForwardDesc>();
            var ws = new List<double>();
            for (int i = 0; i < fixings.Length; i++)
            {
                var lc = fixings[i] as LinearCombinationFixingDesc;
                if (lc != null)
                {
                    fs.AddRange(lc.Fixings);
                    ws.AddRange(lc.Weights.Select(w => w * weights[i]));
                }
                else
                {
                    fs.Add((ForwardDesc)fixings[i]);
                    ws.Add(weights[i]);
                }
            }

            return new LinearCombinationFixingDesc(fs.ToArray(), ws.ToArray());
        }
        public ForwardDesc[] Fixings { get; private set; }
        public double[] Weights { get; private set; }
    }
}