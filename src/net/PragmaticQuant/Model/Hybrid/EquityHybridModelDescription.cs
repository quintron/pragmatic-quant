using System.Linq;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets;
using PragmaticQuant.Model.Equity;
using PragmaticQuant.Model.Rate.HullWhite;
using PragmaticQuant.Product;

namespace PragmaticQuant.Model.Hybrid
{
    public class EquityHybridCalib : ICalibrationDescription
    {
        public EquityHybridCalib(ICalibrationDescription rateModel, ICalibrationDescription equityModel, 
                                 LabelledMatrix<string, string, double> equityRateCorrelation)
        {
            EquityRateCorrelation = equityRateCorrelation;
            RateModel = rateModel;
            EquityModel = equityModel;
        }
        public LabelledMatrix<string, string, double> EquityRateCorrelation { get; private set; }
        public ICalibrationDescription RateModel { get; private set; }
        public ICalibrationDescription EquityModel { get; private set; }
    }

    public class EquityHybridModelCalibration : ModelCalibration<EquityHybridCalib>
    {
        public override IModelDescription Calibrate(EquityHybridCalib model, Market market, IProduct product)
        {
            HwnModelDescription rate = (HwnModelDescription)ModelCalibration.Instance.Calibrate(model.RateModel, market, product).GetRateParts().Single();
            EquityModelDescription equity = (EquityModelDescription)ModelCalibration.Instance.Calibrate(model.EquityModel, market, product).GetEquityParts().Single();
            
            var equityRateCorrels = new RiskFactorCorrelation();
            for (int i = 0; i < rate.RiskFactors.Length; i++)
            {
                double eqtyRateCorrel = model.EquityRateCorrelation.GetVal(rate.RiskFactors[i].Name, equity.Asset.Name);
                equityRateCorrels.AddCorrelation(equity.SpotFactor, rate.RiskFactors[i], eqtyRateCorrel);
            }

            return new ModelDescription(equityRateCorrels, rate, equity);
        }
    }

}