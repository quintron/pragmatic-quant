using System;
using System.Collections.Generic;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets;
using PragmaticQuant.Model.Equity.Bergomi;
using PragmaticQuant.Model.Equity.BergomiMixedLv;
using PragmaticQuant.Model.Equity.BlackScholes;
using PragmaticQuant.Model.Equity.LocalVolatility;
using PragmaticQuant.Model.Hybrid;
using PragmaticQuant.Model.Rate.HullWhite;
using PragmaticQuant.Product;

namespace PragmaticQuant.Model
{
    public interface ICalibrationDescription
    {}

    public interface IModelCalibration
    {
        IModelDescription Calibrate(ICalibrationDescription calibDescription, Market market, IProduct product);
    }

    public class ModelCalibration : Singleton<ModelCalibration>, IModelCalibration
    {
        #region private fields
        private static readonly IDictionary<Type, IModelCalibration> factories = GetFactories();
        #endregion
        #region private methods
        private static IDictionary<Type, IModelCalibration> GetFactories()
        {
            var result = new Dictionary<Type, IModelCalibration>
            {
                {typeof (ExplicitCalibration), new ExplicitModelCalibration()},
                {typeof (HwnModelCalibDesc), HwnModelCalibration.Instance},
                {typeof (LocalVolModelCalibDesc), LocalVolModelCalibration.Instance},
                {typeof (BlackScholesModelCalibDesc), BlackScholesModelCalibration.Instance},
                {typeof (Bergomi2FModelCalibDesc), Bergomi2FModelCalibration.Instance},
                {typeof (BergomiMixedLvCalibDesc), BergomiMixedLvCalibration.Instance},
                {typeof (EquityHybridCalib), new EquityHybridModelCalibration()}
            };

            return result;
        }
        #endregion

        public IModelDescription Calibrate(ICalibrationDescription calibDescription, Market market, IProduct product)
        {
            IModelCalibration modelfactory;
            if (factories.TryGetValue(calibDescription.GetType(), out modelfactory))
                return modelfactory.Calibrate(calibDescription, market, product);
            throw new ArgumentException(string.Format("Missing Model Calibration for {0}", calibDescription));
        }
    }
    
    public abstract class ModelCalibration<TCalib> : IModelCalibration
        where TCalib : class, ICalibrationDescription
    {
        public abstract IModelDescription Calibrate(TCalib model, Market market, IProduct product);
        public IModelDescription Calibrate(ICalibrationDescription calibDescription, Market market, IProduct product)
        {
            var calibDescImplem = calibDescription as TCalib;
            if (calibDescImplem == null)
                throw new Exception(string.Format("ModelCalibration : {0} expected but was {1}",
                                                  typeof (TCalib), calibDescription.GetType()));
            return Calibrate(calibDescImplem, market, product);
        }
    }

}