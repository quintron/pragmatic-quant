﻿using System;
using System.Collections.Generic;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets;
using PragmaticQuant.Markets.Rate;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Model.Rate;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.Model
{
    public interface IFactorModelRepresentation
    {
        RnRFunction this[IObservation observation] { get; }
    }

    public class FactorRepresentation : IFactorModelRepresentation
    {
        #region private fields
        private readonly Market market;
        private readonly IDictionary<Currency, RateZcRepresentation> zcRepresentations;
        private readonly IDictionary<AssetId, EquitySpotRepresentation> equityRepresentations;
        #endregion
        #region Rate methods
        private RnRFunction Zc(Zc zc)
        {
            RateZcRepresentation zcRepresentation;
            if (zcRepresentations.TryGetValue(zc.Currency, out zcRepresentation))
            {
                var discountCurve = market.DiscountCurve(zc.FinancingId);
                var fwdZc = discountCurve.Zc(zc.PayDate) / discountCurve.Zc(zc.Date);
                return zcRepresentation.Zc(zc.Date, zc.PayDate, fwdZc);
            }
            throw new Exception(string.Format("Model do not handle this currency : {0}", zc.Currency));
        }
        private RnRFunction ZcRate(ZcRate zcRate)
        {
            RateZcRepresentation zcRepresentation;
            if (zcRepresentations.TryGetValue(zcRate.Currency, out zcRepresentation))
            {
                var discountCurve = market.DiscountCurve(FinancingId.Ois(zcRate.Currency));
                var fwdZc = discountCurve.Zc(zcRate.EndDate) / discountCurve.Zc(zcRate.Date);
                var fwdZcRate = -Math.Log(fwdZc) / DayCountFrac.Act365Fixed.Count(zcRate.Date, zcRate.EndDate);
                return zcRepresentation.ZcRate(zcRate.Date, zcRate.EndDate, fwdZcRate);
            }
            throw new Exception(string.Format("Model don't handle currency : {0}", zcRate.Currency));
        }
        private RnRFunction ForwardLibor(DateTime observationDate, Libor libor)
        {
            if(libor.Date < observationDate)
                throw new Exception("FactorRepresentation.ForwardLibor: past fixing representation not allowed!");

            var convention = SwapConventions.For(libor.Currency);
            var floatLegConvention = convention.FloatLegConvention;
            var schedule = libor.Schedule(floatLegConvention.ScheduleBuilder);
            var theta = schedule.Theta(floatLegConvention.Dcf);

            var borFinancing = FinancingId.Bor(libor.Currency, libor.Tenor);
            var zcStart = Zc(new Zc(observationDate, schedule.Start, libor.Currency, borFinancing));
            var zcEnd = Zc(new Zc(observationDate, schedule.End, libor.Currency, borFinancing));
            
            return (zcStart / zcEnd - 1.0) / theta;
        }
        private RnRFunction FloatCouponMtm(DateTime observationDate, CouponSchedule cs, double theta, Duration perio, Currency currency)
        {
            if (cs.Fixing < observationDate)
                throw new Exception("FactorRepresentation.FloatCouponMtm: past fixing representation not allowed!");
            
            var fwdLibor = ForwardLibor(observationDate, new Libor(currency, cs.Fixing, perio));
            var discount = Zc(new Zc(observationDate, cs.Pay, currency, FinancingId.Ois(currency)));
            return theta * fwdLibor * discount;
        }
        private RnRFunction FloatLeg(DateTime observationDate, DateTime startDate, DateTime endDate, Currency currency, SwapLegConvention convention)
        {
            LegDatas floatLeg = convention.BuildLeg(startDate, endDate);
            RnRFunction[] floatCoupons = floatLeg.Schedule.ZipWith(floatLeg.DayFracs,
                (cs, theta) => FloatCouponMtm(observationDate, cs, theta, convention.Period, currency));
            return floatCoupons.Skip(1).Aggregate(floatCoupons.First(), (prev, cur) => prev + cur);
        }
        
        private RnRFunction FixedCouponMtm(DateTime observationDate, DateTime payDate, double theta, double rate, Currency currency)
        {
            var discount = Zc(new Zc(observationDate, payDate, currency, FinancingId.Ois(currency)));
            return theta * rate * discount;
        }
        private RnRFunction FixedLeg(DateTime observationDate, LegDatas leg, Currency currency, double strike)
        {
            RnRFunction[] fixedCoupons =  leg.Schedule.ZipWith(leg.DayFracs, (cs, theta) => FixedCouponMtm(observationDate, cs.Pay, theta, strike, currency));
            return fixedCoupons.Skip(1).Aggregate(fixedCoupons.First(), (prev, cur) => prev + cur);
        }
        private RnRFunction FixedLeg(DateTime observationDate, DateTime startDate, DateTime endDate, Currency currency, double fixedRate, SwapLegConvention convention)
        {
            var leg = convention.BuildLeg(startDate, endDate);
            return FixedLeg(observationDate, leg, currency, fixedRate);
        }

        private RnRFunction SwapMtm(SwapMtm swap)
        {
            var floatLeg = FloatLeg(swap.Date, swap.StartDate, swap.EndDate, swap.Currency, swap.Convention.FloatLegConvention);
            var fixedLeg = FixedLeg(swap.Date, swap.StartDate, swap.EndDate, swap.Currency, swap.Strike, swap.Convention.FixedLegConvention);

            switch (swap.Side)
            {
                case SwapSide.Payer:
                    return floatLeg - fixedLeg;

                case SwapSide.Receiver:
                    return fixedLeg - floatLeg;
            }
            throw new Exception("Unknown swap side !");
        }
        private RnRFunction FixedLegMtm(FixedLegMtm fixedLeg)
        {
            List<RnRFunction> fixedCoupons =new List<RnRFunction>();
            for (int i = 0; i < fixedLeg.PayDates.Length; i++)
            {
                fixedCoupons.Add(FixedCouponMtm(fixedLeg.Date, fixedLeg.PayDates[i], 1.0, fixedLeg.Amounts[i], fixedLeg.Currency));
            }
            return fixedCoupons.Skip(1).Aggregate(fixedCoupons.First(), (prev, cur) => prev + cur);
        }
        private RnRFunction FwdCmsRate(DateTime date, Cms cms)
        {
            var conventions  = SwapConventions.For(cms.Currency);
            
            var floatStart = conventions.FloatLegConvention.ScheduleBuilder.StartDate(cms.Date);
            var floatLeg  = FloatLeg(date, floatStart, floatStart + cms.Tenor, cms.Currency, conventions.FloatLegConvention);

            var fixedStart = conventions.FixedLegConvention.ScheduleBuilder.StartDate(cms.Date);
            var fixedLeg = FixedLeg(date, fixedStart, fixedStart + cms.Tenor, cms.Currency, 1.0, conventions.FixedLegConvention);
            
            return floatLeg / fixedLeg;
        }
        private RnRFunction SwapRate(SwapRate rate)
        {
            var floatLeg = FloatLeg(rate.Date, rate.StartDate, rate.EndDate, rate.Currency, rate.Convention.FloatLegConvention);
            var fixedLeg = FixedLeg(rate.Date, rate.StartDate, rate.EndDate, rate.Currency, 1.0, rate.Convention.FixedLegConvention);
            return floatLeg / fixedLeg;
        }
        private RnRFunction RateRepresentation(IObservation observation)
        {
            var libor = observation as Libor;
            if (libor != null) return ForwardLibor(libor.Date, libor);
            
            var cms = observation as Cms;
            if (cms != null) return FwdCmsRate(cms.Date, cms);

            var zc = observation as Zc;
            if (zc != null) return Zc(zc);

            var zcRate = observation as ZcRate;
            if (zcRate != null) return ZcRate(zcRate);

            var rate = observation as SwapRate;
            if (rate != null) return SwapRate(rate);

            var swapMtm = observation as SwapMtm;
            if (swapMtm != null) return SwapMtm(swapMtm);

            var fixedLegMtm = observation as FixedLegMtm;
            if (fixedLegMtm != null) return FixedLegMtm(fixedLegMtm);

            var fwdFixing = observation as ForwardFixing;
            if (fwdFixing != null)
            {
                var underlying = fwdFixing.Underlying;
                
                var liborUnderlying = underlying as Libor;
                if (liborUnderlying != null)
                    return ForwardLibor(fwdFixing.Date, liborUnderlying);

                var cmsUnderlying = underlying as Cms;
                if (cmsUnderlying != null)
                    return FwdCmsRate(fwdFixing.Date, cmsUnderlying);
            }

            return null;
        }
        #endregion
        #region Equity methods
        private RnRFunction EquitySpot(EquitySpot eqtySpot)
        {
            EquitySpotRepresentation equityRepresentation;
            if (equityRepresentations.TryGetValue(eqtySpot.AssetId, out equityRepresentation))
            {
                return eqtySpot.LogSpot
                    ? equityRepresentation.LogSpot(eqtySpot.Date)
                    : equityRepresentation.Spot(eqtySpot.Date);
            }
            throw new Exception(string.Format("Not handled equity {0}", eqtySpot.AssetId));
        }
        private RnRFunction EquityRepresentation(IObservation observation)
        {
            var eqtySpot = observation as EquitySpot;
            if (eqtySpot != null) return EquitySpot(eqtySpot);

            return null;
        }
        #endregion
        public FactorRepresentation(Market market,
                                    IEnumerable<RateZcRepresentation> zcs,
                                    IEnumerable<EquitySpotRepresentation> equities)
        {
            this.market = market;
            zcRepresentations = zcs.ToDictionary(zcRep => zcRep.Currency, zcRep => zcRep);
            equityRepresentations = equities.ToDictionary(eqty => eqty.Asset, eqty => eqty);
        }
        public FactorRepresentation(Market market, params RateZcRepresentation[] zcs)
            : this(market, zcs, new EquitySpotRepresentation[0]) { }

        public RnRFunction this[IObservation observation]
        {
            get
            {
                var rateRepresentation = RateRepresentation(observation);
                if (rateRepresentation != null) return rateRepresentation;

                var equityRepresentation = EquityRepresentation(observation);
                if (equityRepresentation != null) return equityRepresentation;

                throw new NotImplementedException("FactorRepresentation, fixing not handled : " + observation);
            }
        }
    }

    public abstract class RateZcRepresentation
    {
        protected RateZcRepresentation(Currency currency)
        {
            Currency = currency;
        }
        public abstract RnRFunction Zc(DateTime date, DateTime maturity, double fwdZc);
        public abstract RnRFunction ZcRate(DateTime date, DateTime maturity, double zcRate);
        public Currency Currency { get; private set; }
    }

    public class DeterministicZcRepresentation : RateZcRepresentation
    {
        #region private fields
        private readonly int dimension;
        #endregion
        public DeterministicZcRepresentation(DeterministicRateModel deterministicRate)
            : base(deterministicRate.Currency)
        {
            dimension = deterministicRate.Dim;
        }
        public override RnRFunction Zc(DateTime date, DateTime maturity, double fwdZc)
        {
            return RnRFunctions.Constant(fwdZc, dimension);
        }
        public override RnRFunction ZcRate(DateTime date, DateTime maturity, double zcRate)
        {
            return RnRFunctions.Constant(zcRate, dimension);
        }
    }
    
    public class EquitySpotRepresentation
    {
        #region private fields
        private readonly DiscountCurve assetDiscountCurve;
        private readonly PaymentInfo probaMeasure;
        private readonly int spotFactorIndex;
        private readonly int modelDim;
        #endregion
        public EquitySpotRepresentation(AssetId asset, DiscountCurve assetDiscountCurve, PaymentInfo probaMeasure, 
                                        int spotFactorIndex, int modelDim)
        {
            this.assetDiscountCurve = assetDiscountCurve;
            this.probaMeasure = probaMeasure;
            this.spotFactorIndex = spotFactorIndex;
            this.modelDim = modelDim;
            Asset = asset;
        }
        public RnRFunction Spot(DateTime date)
        {
            var forwardWithoutDivs = assetDiscountCurve.Zc(probaMeasure.Date) / assetDiscountCurve.Zc(date);
            var mults = new double[modelDim];
            mults[spotFactorIndex] = 1.0;
            return RnRFunctions.ExpAffine(forwardWithoutDivs, mults);
        }
        public RnRFunction LogSpot(DateTime date)
        {
            var forwardWithoutDivs = assetDiscountCurve.Zc(probaMeasure.Date) / assetDiscountCurve.Zc(date);
            var mults = new double[modelDim];
            mults[spotFactorIndex] = 1.0;
            return RnRFunctions.Affine(Math.Log(forwardWithoutDivs), mults);
        }
        public AssetId Asset { get; private set; }
    }

}