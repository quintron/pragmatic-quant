using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Markets;
using PragmaticQuant.Model.Equity;
using PragmaticQuant.Model.Rate;
using PragmaticQuant.Model.Rate.HullWhite;

namespace PragmaticQuant.Model
{
    public interface IFactorRepresentationFactory
    {
        IFactorModelRepresentation Build(IModel model, Market market, PaymentInfo probaMeasure);
    }

    public class FactorRepresentationFactory : IFactorRepresentationFactory
    {
        #region private methods
        private RateZcRepresentation GetZcRepresentation(ISingleRiskModel singleRiskModel)
        {
            if (singleRiskModel.RiskId.Type != RiskType.Rate)
                throw new Exception("FactorRepresentationFactory : rate risk model expected !");

            var deterministicRate = singleRiskModel as DeterministicRateModel;
            if (deterministicRate != null)
                return new DeterministicZcRepresentation(deterministicRate);
            
            var hwn = singleRiskModel as HwnModel;
            if (hwn != null)
                return new HwnModelZcRepresentation(hwn);
            
            throw new Exception("FactorRepresentationFactory, rate model is not handled !");
        }
        private EquitySpotRepresentation GetEquitySpotRepresentation(ISingleRiskModel singleRiskModel, RiskFactor[] modelFactors, PaymentInfo probaMeasure)
        {
            if (singleRiskModel.RiskId.Type != RiskType.Eqy
                || !(singleRiskModel is EquityModel))
                throw new Exception("FactorRepresentationFactory, equity model expected !");

            return EquityFactorRepresentationFactory.Build((EquityModel)singleRiskModel, modelFactors, probaMeasure);
        }
        #endregion
        public static readonly IFactorRepresentationFactory Instance = new FactorRepresentationFactory();
        public IFactorModelRepresentation Build(IModel model, Market market, PaymentInfo probaMeasure)
        {
            var modelFactors = model.AllRiskFactors;
            var zcs = model.GetRateParts()
                           .Map(GetZcRepresentation);
            var equitySpots = model.GetEquityParts()
                                   .Map(eqyModel => GetEquitySpotRepresentation(eqyModel, modelFactors, probaMeasure));
            return new FactorRepresentation(market, zcs, equitySpots);
        }
    }

}