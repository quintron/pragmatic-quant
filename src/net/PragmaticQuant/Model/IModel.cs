﻿using System;
using System.Collections.Generic;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Model.Equity;
using PragmaticQuant.Model.Rate;

namespace PragmaticQuant.Model
{
    public interface IModel
    {
        ITimeMeasure Time { get; }
        Currency PivotCurrency { get; }

        RiskId[] RiskIds { get; }
        ISingleRiskModel GetModelPart(RiskId riskId);

        RiskFactorCorrelation RiskFactorCorrelations { get; }
        RiskFactor[] AllRiskFactors { get; }
    }

    public interface ISingleRiskModel
    {
        ITimeMeasure Time { get; }
        RiskId RiskId { get; }
        RiskFactor[] RiskFactors { get; }
        ModelFactor FactorProcess { get; }
    }

    public class RiskFactorCorrelation
    {
        #region private fields
        private readonly IDictionary<Tuple<RiskFactor, RiskFactor>, double> correls =
                new Dictionary<Tuple<RiskFactor, RiskFactor>, double>();
        #endregion
        public double GetCorrelation(RiskFactor firstFactor, RiskFactor secondFactor)
        {
            double correl;
            if (correls.TryGetValue(Tuple.Create(firstFactor, secondFactor), out correl)
                || correls.TryGetValue(Tuple.Create(secondFactor, firstFactor), out correl))
                return correl;
            throw new Exception(string.Format("RiskFactorCorrelation : missing correl for <{0} ; {1}>", firstFactor, secondFactor));
        }
        public void AddCorrelation(RiskFactor firstFactor, RiskFactor secondFactor, double correl)
        {
            if (correls.ContainsKey(Tuple.Create(firstFactor, secondFactor))
                || correls.ContainsKey(Tuple.Create(secondFactor, firstFactor)))
                throw new Exception("RiskFactorCorrelation : risk factor pair already present !");
            correls.Add(Tuple.Create(firstFactor, secondFactor), correl);
        }
    }

    public static class ModelUtils
    {
        public static ISingleRiskModel[] GetRiskTypeParts(this IModel model, RiskType riskType)
        {
            return model.RiskIds.Where(risk => risk.Type == riskType)
                .Map(model.GetModelPart);
        }
        public static ISingleRiskModelDescription[] GetRiskTypeParts(this IModelDescription model, RiskType riskType)
        {
            return model.Risks.Where(risk => risk.Type == riskType)
                .Map(model.GetModelPart);
        }
        public static RateModel[] GetRateParts(this IModel model)
        {
            return GetRiskTypeParts(model, RiskType.Rate).Cast<RateModel>().ToArray();
        }
        public static ISingleRiskModelDescription[] GetRateParts(this IModelDescription model)
        {
            return GetRiskTypeParts(model, RiskType.Rate);
        }

        public static EquityModel[] GetEquityParts(this IModel model)
        {
            return GetRiskTypeParts(model, RiskType.Eqy).Cast<EquityModel>().ToArray();
        }
        public static T[] GetEquityParts<T>(this IModel model)
        {
            return model.GetEquityParts()
                        .Cast<T>()
                        .ToArray();
        }
        public static ISingleRiskModelDescription[] GetEquityParts(this IModelDescription model)
        {
            return GetRiskTypeParts(model, RiskType.Eqy);
        }
        public static bool IsDetRateEquityModel<TVol>(this IModel model)
        {
            var eqyModels = model.GetEquityParts();
            var rateModels = model.GetRateParts();

            foreach (var eqyModel in eqyModels)
            {
                if (!(eqyModel is EquityModel<TVol>))
                    return false;

                var assetCurrency = eqyModel.AssetDiscount.Financing.Currency;
                var assetCurrencyModels = rateModels.Where(rateModel => rateModel.Currency.Equals(assetCurrency)).ToArray();

                if (!assetCurrencyModels.Any())
                    return false;

                if (assetCurrencyModels.Count() > 1)
                    throw new Exception("Too many rate model for currency : " + assetCurrency);

                var assetCurrencyModel = assetCurrencyModels.Single();
                if (!(assetCurrencyModel is DeterministicRateModel))
                    return false;
            }

            return true;
        }

        public static Tuple<RiskFactor[], ModelFactor> ModelFactor(this IModel model)
        {
            var factorCorrelation = model.RiskFactorCorrelations;
            RiskFactor[] currentFactors = new RiskFactor[0];
            ModelFactor currentProcess = new ModelFactor(new string[0], new double[0], new double[0], 
                                                         new double[0, 0], new double[0, 0], new RrFunction[0]);
            foreach (var riskId in model.RiskIds)
            {
                var riskModelPart = model.GetModelPart(riskId);
                var riskFactorsPart = riskModelPart.RiskFactors;
                if (riskFactorsPart.Length > 0)
                {
                    var crossCorrelation = ArrayUtils.CartesianProd(currentFactors, riskFactorsPart,
                        (f1, f2) => factorCorrelation.GetCorrelation(f1, f2));
                    currentFactors = EnumerableUtils.Append(currentFactors, riskFactorsPart);
                    currentProcess = ModelFactorDescriptionUtils.Correlate(currentProcess, riskModelPart.FactorProcess, crossCorrelation);
                }
            }
            return Tuple.Create(currentFactors, currentProcess);
        }
    }

    public class Model : IModel
    {
        #region private fields
        private readonly IDictionary<RiskId, ISingleRiskModel> singleRiskParts;
        #endregion
        public Model(RiskFactorCorrelation riskFactorCorrelations, params ISingleRiskModel[] riskParts)
        {
            RiskFactorCorrelations = riskFactorCorrelations;
            var rateModels = riskParts.Where(m => m.RiskId.Type.Equals(RiskType.Rate)).ToArray();
            
            if (rateModels.Length > 1)
                throw new Exception("Model : only single currency model yet implemented !");
            if (rateModels.Length == 0)
                throw new Exception("Model : missing rate model !");

            RateModel pivotRateModel = (RateModel) rateModels.Single();
            Time = pivotRateModel.Time;
            PivotCurrency = pivotRateModel.Currency;

            singleRiskParts = riskParts.ToDictionary(m => m.RiskId, m => m);
            AllRiskFactors = ArrayUtils.Append(singleRiskParts.Map(srp => srp.Value.RiskFactors));
        }

        public ITimeMeasure Time { get; private set; }
        public Currency PivotCurrency { get; private set; }
        public RiskId[] RiskIds
        {
            get { return singleRiskParts.Keys.ToArray(); }
        }
        public ISingleRiskModel GetModelPart(RiskId riskId)
        {
            ISingleRiskModel riskPart;
            if (!singleRiskParts.TryGetValue(riskId, out riskPart))
                throw new Exception("Missing model for risk : " + riskId);
            return riskPart;
        }
        public RiskFactorCorrelation RiskFactorCorrelations { get; private set; }
        public RiskFactor[] AllRiskFactors { get; private set; }
    }

}
