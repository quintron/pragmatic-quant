using System;
using System.Linq;
using System.Collections.Generic;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Model.Equity.Bergomi;
using PragmaticQuant.Model.Equity.BergomiMixedLv;
using PragmaticQuant.Model.Equity.BlackScholes;
using PragmaticQuant.Model.Equity.LocalVolatility;
using PragmaticQuant.Model.Rate;
using PragmaticQuant.Model.Rate.HullWhite;

namespace PragmaticQuant.Model
{
    public interface IModelDescription
    {
        RiskId[] Risks { get; }
        ISingleRiskModelDescription GetModelPart(RiskId risk);
        RiskFactorCorrelation RiskFactorCorrelations { get; }
    }

    public interface ISingleRiskModelDescription
    {
        RiskId Risk { get; }
        RiskFactor[] RiskFactors { get; }
        ModelFactorDescription ModelFactorDescription { get; }
    }

    public class ModelDescription : IModelDescription
    {
        private readonly IDictionary<RiskId, ISingleRiskModelDescription> singleRiskParts;
        public ModelDescription(RiskFactorCorrelation riskFactorCorrelations, params ISingleRiskModelDescription[] riskParts)
        {
            RiskFactorCorrelations = riskFactorCorrelations;
            singleRiskParts = riskParts.ToDictionary(m => m.Risk, m => m);
        }

        public RiskId[] Risks
        {
            get { return singleRiskParts.Keys.ToArray(); }
        }
        public ISingleRiskModelDescription GetModelPart(RiskId risk)
        {
            ISingleRiskModelDescription riskPart;
            if (!singleRiskParts.TryGetValue(risk, out riskPart))
                throw new Exception("Missing model for risk : " + risk);
            return riskPart;
        }
        public RiskFactorCorrelation RiskFactorCorrelations { get; private set; }
    }
    
    public interface IModelFactory
    {
        IModel Build(IModelDescription modelDescription, Market market);
    }

    public class ModelFactory : Singleton<ModelFactory>, IModelFactory
    {
        #region private fields
        private static readonly IDictionary<Type, ISingleRiskModelFactory> factories = GetFactories();
        #endregion
        #region private methods
        private static IDictionary<Type, ISingleRiskModelFactory> GetFactories()
        {
            var result = new Dictionary<Type, ISingleRiskModelFactory>
            {
                //Rate
                {typeof (DeterministicRateDescription), DeterministicRateDescriptionFactory.Instance},
                {typeof (HwnModelDescription), HwnModelFactory.Instance},

                //Equity
                {typeof (BlackScholesModelDescription), BlackScholesModelFactory.Instance},
                {typeof (LocalVolModelDescription), LocalVolModelFactory.Instance},
                {typeof (Bergomi2FModelDescription), Bergomi2FModelFactory.Instance},
                {typeof(BergomiMixedLvModelDescription), BergomiMixedModelFactory.Instance}
            };
            return result;
        }
        private static ISingleRiskModelFactory GetFactoryFor(ISingleRiskModelDescription singleRiskModel)
        {
            ISingleRiskModelFactory riskModelFactory;
            if (!factories.TryGetValue(singleRiskModel.GetType(), out riskModelFactory))
                throw new Exception("Missing factory for " + singleRiskModel.GetType());
            return riskModelFactory;
        }
        private static void AddCrossRiskCorrelation(ref RiskFactorCorrelation riskFactorCorrelations,
                                                    ISingleRiskModel[] singleRiskModels, RiskFactorCorrelation riskCorrelationDescs)
        {
            for (int i = 0; i < singleRiskModels.Length; i++)
            {
                var model_i = singleRiskModels[i];
                for (int j = 0; j < i; j++)
                {
                    var model_j = singleRiskModels[j];
                    
                    var riskPairs = from r1 in model_i.RiskFactors
                                    from r2 in model_j.RiskFactors
                                    select new {R1 = r1, R2 = r2, Correl = riskCorrelationDescs.GetCorrelation(r1, r2)};
                    foreach(var riskPair in riskPairs)
                        riskFactorCorrelations.AddCorrelation(riskPair.R1, riskPair.R2, riskPair.Correl);
                }
            }
        }
        #endregion

        public IModel Build(IModelDescription modelDescription, Market market)
        {
            var riskModelParts = new List<ISingleRiskModel>();
            foreach (var risk in modelDescription.Risks)
            {
                ISingleRiskModelDescription singleRiskModel = modelDescription.GetModelPart(risk);
                var factory = GetFactoryFor(singleRiskModel);
                riskModelParts.Add(factory.Build(singleRiskModel, market));
            }

            var riskFactorCorrelations = new RiskFactorCorrelation();
            AddCrossRiskCorrelation(ref riskFactorCorrelations,
                                   riskModelParts.ToArray(), 
                                   modelDescription.RiskFactorCorrelations);

            return new Model(riskFactorCorrelations, riskModelParts.ToArray());
        }
    }

    public interface ISingleRiskModelFactory
    {
        ISingleRiskModel Build(ISingleRiskModelDescription model, Market market);
    }
    
    public abstract class SingleRiskModelFactory<TModelDesc> : ISingleRiskModelFactory
                            where TModelDesc : class, ISingleRiskModelDescription
    {
        public abstract ISingleRiskModel Build(TModelDesc model, Market market);
        public ISingleRiskModel Build(ISingleRiskModelDescription riskModel, Market market)
        {
            var modelDescImplem = riskModel as TModelDesc;
            if (modelDescImplem == null)
                throw new Exception(string.Format("ModelFactory : {0} expected but was {1}",
                                                  typeof(TModelDesc), riskModel.GetType()));
            return Build(modelDescImplem, market);
        }
    }

    public static class ModelFactoryUtils
    {
        public static ITimeMeasure DefaultTime(DateTime refDate)
        {
            return TimeMeasure.Act365(refDate);
        }
        public static RrFunction ToStepFunction(this RawMapDatas<DateOrDuration, double> datasFunc, ITimeMeasure time)
        {
            var abscissae = datasFunc.Pillars.Map(d => time[d.ToDate(time.RefDate)]);
            return new StepFunction(abscissae, datasFunc.Values, datasFunc.Values.First());
        }
        public static RrFunction ToAffineStepFunction(this RawMapDatas<DateOrDuration, double> datasFunc, ITimeMeasure time)
        {
            return RrFunctions.LinearInterpolation(time[datasFunc.Pillars], datasFunc.Values);
        }

        public static RrFunction InstantVarianceFunction(RawMapDatas<DateOrDuration, double> volTermStructure, ITimeMeasure time)
        {
            var matVars = EnumerableUtils.For(0, volTermStructure.Pillars.Length, i =>
            {
                var mat = time[volTermStructure.Pillars[i].ToDate(time.RefDate)];
                var variance = volTermStructure.Values[i] * volTermStructure.Values[i] * mat;
                return new {Mat = mat, Variance = variance};
            }).OrderBy(t => t.Mat).ToArray();

            if (!DoubleUtils.EqualZero(matVars.First().Mat))
            {
                matVars = matVars.Concat(new[] {new {Mat = 0.0, Variance = 0.0}})
                    .OrderBy(t => t.Mat).ToArray();
            }

            var varianceFunc = RrFunctions.LinearInterpolation(matVars.Map(t => t.Mat),
                matVars.Map(t => t.Variance),
                0.0, double.NaN);
            return varianceFunc.Derivative();
        }
        public static RrFunction InstantVolFunction(RawMapDatas<DateOrDuration, double> volTermStructure, ITimeMeasure time)
        {
            var instantVariance = InstantVarianceFunction(volTermStructure, time);

            var stepFunction = instantVariance as StepFunction;
            if (stepFunction != null)
                return stepFunction.Map(Math.Sqrt);

            var cst = instantVariance as ConstantRrFunction;
            if (cst != null)
                return RrFunctions.Constant(Math.Sqrt(cst.Value));

            throw new Exception("should never get there");
        }
    }

}