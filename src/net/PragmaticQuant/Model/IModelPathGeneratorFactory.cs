﻿using System;
using System.Collections.Generic;
using PragmaticQuant.Basic;
using PragmaticQuant.Maths.Stochastic;
using PragmaticQuant.Model.Equity.Bergomi;
using PragmaticQuant.Model.Equity.LocalVolatility;
using PragmaticQuant.Model.Rate.HullWhite;

namespace PragmaticQuant.Model
{
    public interface IModelPathGeneratorFactory
    {
        IProcessPathGenerator Build(IModel model, PaymentInfo probaMeasure, DateTime[] pathDates);

        OrnsteinUhlenbeckNd GaussianFactors(IModel model, PaymentInfo probaMeasure);

        bool AcceptModel(IModel model);
    }

    public static class ModelPathGeneratorFactories
    {
        #region private methods
        private static IEnumerable<IModelPathGeneratorFactory> AllFactories(MonteCarloConfig mcConfig)
        {
            yield return new HwnModelPathGeneratorFactory();
            yield return new LocalVolPathGenFactory(mcConfig);
            yield return new BergomiPathGenFactory(mcConfig);
        }
        #endregion

        public static IModelPathGeneratorFactory For(IModel model, MonteCarloConfig mcConfig)
        {
            foreach (var factory in AllFactories(mcConfig))
            {
                if (factory.AcceptModel(model))
                    return factory;
            }
            throw new ArgumentException("No Path Generator Factory found for model !");
        }
    }
}