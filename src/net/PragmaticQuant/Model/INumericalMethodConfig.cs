﻿using System;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Maths;
using PragmaticQuant.MonteCarlo.Bmc;

namespace PragmaticQuant.Model
{
    public interface INumericalMethodConfig { }

    public class MonteCarloConfig : INumericalMethodConfig
    {
        public MonteCarloConfig(int nbPaths, bool multithread, BmcStrategyConfig bmcStrategy = null)
            : this(nbPaths, multithread, SimulationSchedules.DefaultScheduleBuilder, bmcStrategy) { }

        public MonteCarloConfig(int nbPaths, bool multithread, ISimulationSchedule scheduleBuilder, BmcStrategyConfig bmcStrategy = null)
            : this(nbPaths, multithread, scheduleBuilder, RandomGenerators.GaussianSobol(), bmcStrategy) { }

        public MonteCarloConfig(int nbPaths, bool multithread, 
                                ISimulationSchedule scheduleBuilder, IRandomGeneratorFactory randomGenerator, 
                                BmcStrategyConfig bmcStrategy = null)
        {
            if (nbPaths < 1)
                throw new Exception("MonteCarloConfig : nbPaths should be positive !");

            RandomGenerator = randomGenerator;
            NbPaths = nbPaths;
            ScheduleBuilder = scheduleBuilder;
            Multithread = multithread;
            BmcStrategy = bmcStrategy;
        }
        
        public bool Multithread { get; private set; }
        public int NbPaths { get; private set; }
        public IRandomGeneratorFactory RandomGenerator { get; private set; }
        public ISimulationSchedule ScheduleBuilder { get; private set; }
        public BmcStrategyConfig BmcStrategy { get; private set; }

        public MonteCarloConfig NewNbPaths(int newNbPaths)
        {
            return new MonteCarloConfig(newNbPaths, Multithread, ScheduleBuilder, RandomGenerator, BmcStrategy);
        }
        public MonteCarloConfig NewScheduleBuilder(ISimulationSchedule newScheduleBuilder)
        {
            return new MonteCarloConfig(NbPaths, Multithread, newScheduleBuilder, RandomGenerator, BmcStrategy);
        }
        public MonteCarloConfig ForceSingleThread()
        {
            return new MonteCarloConfig(NbPaths, false, ScheduleBuilder, RandomGenerator, BmcStrategy);
        }
    }

    public class BmcStrategyConfig 
    {
        public BmcStrategyConfig(int strategyNbPaths)
            : this(strategyNbPaths, new RegressionVariablePolicy(), new SimplePolynomialStrategy()) { }
        public BmcStrategyConfig(int strategyNbPaths,
                                 IRegressionVariablePolicy regressionVariablePolicy,
                                 IBmcStrategyAlgorithm strategyAlgorithm)
        {
            LsStrategyNbPaths = strategyNbPaths;
            RegressionVariablePolicy = regressionVariablePolicy;
            StrategyAlgorithm = strategyAlgorithm;
        }

        public int LsStrategyNbPaths { get; private set; }
        public IRegressionVariablePolicy RegressionVariablePolicy { get; private set; }
        public IBmcStrategyAlgorithm StrategyAlgorithm { get; private set; }
    }

    public class PdeConfig : INumericalMethodConfig
    {
        public PdeConfig(ISimulationSchedule scheduleBuilder, double? theta)
        {
            ScheduleBuilder = scheduleBuilder;
            Theta = theta;
        }
        public ISimulationSchedule ScheduleBuilder { get; private set; }
        public double? Theta { get; private set; }
    }
}