using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Maths.Stochastic;

namespace PragmaticQuant.Model
{
    public class ModelFactorDescription : ModelFactorDescription<RawMapDatas<DateOrDuration, double>>
    {
        public ModelFactorDescription(string[] factorNames,
                                      double[] meanReversions,
                                      double[,] factors,
                                      double[,] correlation,
                                      RawMapDatas<DateOrDuration, double>[] factorVols)
            :base(factorNames, meanReversions, factors, correlation, factorVols)
        {
        }
        public static ModelFactorDescription Ornstein1D(string factorName, double meanReversion, RawMapDatas<DateOrDuration, double> vol)
        {
            return new ModelFactorDescription(new[] { factorName }, new[] { meanReversion }, new[,] { { 1.0 } }, new[,] { { 1.0 } }, new[] { vol });
        }
    }

    public class ModelFactor : ModelFactorDescription<RrFunction>
    {
        public ModelFactor(string[] factorNames, 
                           double[] initialValue,
                           double[] meanReversions, 
                           double[,] factors, 
                           double[,] correlation, 
                           RrFunction[] factorVols) 
            : base(factorNames, meanReversions, factors, correlation, factorVols)
        {
            if (initialValue.Length != meanReversions.Length)
                throw new Exception("ModelFactor : incompatible initialValue size !");

            InitialValue = initialValue;
        }

        public static ModelFactor Brownian1D(string factorName, double initialValue)
        {
            return new ModelFactor(new[] { factorName }, new []{initialValue}, new[] { 0.0 }, new[,] { { 1.0 } }, new[,] { { 1.0 } }, new [] { RrFunctions.Constant(1.0) });
        }
        public double[] InitialValue { get; private set; }
    }

    public class ModelFactorDescription<TVol>
    {
        public ModelFactorDescription(string[] factorNames, double[] meanReversions, double[,] factors, double[,] correlation, TVol[] factorVols)
        {
            if (meanReversions.Length != factors.GetLength(0))
                throw new Exception("OrnsteinDescription : incompatible size beetween meanReversions and factors !");

            if (correlation.GetLength(1) != correlation.GetLength(0))
                throw new Exception("OrnsteinDescription : correlation must be a square matrix !");

            if (factors.GetLength(1) != correlation.GetLength(0))
                throw new Exception("OrnsteinDescription : incompatible size beetween  factors and correlation !");

            if (factors.GetLength(1) != factorVols.Length)
                throw new Exception("OrnsteinDescription : incompatible size beetween  factors and factorVols !");

            MeanReversions = meanReversions;
            Factors = factors;
            Correlation = correlation;
            FactorNames = factorNames;
            FactorVols = factorVols;
        }
        public string[] FactorNames { get; private set; }
        public double[] MeanReversions { get; private set; }
        public double[,] Factors { get; private set; }
        public double[,] Correlation { get; private set; }
        public TVol[] FactorVols { get; private set; }
    }

    public static class ModelFactorDescriptionUtils
    {
        #region private methods
        private static RrFunction[,] FactorCovariance(ModelFactor modelFactor)
        {
            RrFunction[] factorVols = modelFactor.FactorVols;
            double[,] factorCorrelation = modelFactor.Correlation;

            var factorCovariance = new RrFunction[factorCorrelation.GetLength(0), factorCorrelation.GetLength(1)];
            for (int i = 0; i < factorCovariance.GetLength(0); i++)
            {
                for (int j = 0; j < i; j++)
                {
                    var cov = factorVols[i] * factorVols[j] * factorCorrelation[i, j];
                    factorCovariance[i, j] = cov;
                    factorCovariance[j, i] = cov;
                }
                factorCovariance[i, i] = factorVols[i] * factorVols[i] * factorCorrelation[i, i];
            }
            return factorCovariance;
        }
        private static RrFunction[,] OrnsteinCovariance(RrFunction[,] factorCovariances, double[,] factors)
        {
            //factors * factorCovariances *transpose(factors)
            var factorsAsRr = factors.Map(f => (RrFunction)f);
            return factorsAsRr.MatrixProd(factorCovariances).MatrixProd(factorsAsRr.Tranpose());
        }
        #endregion
        public static RrFunction[,] OrnsteinCovariance(this ModelFactorDescription modelFactorDescription, double[] initialValue, ITimeMeasure time)
        {
            return OrnsteinCovariance(modelFactorDescription.ToModelFactor(initialValue, time));
        }
        public static RrFunction[,] OrnsteinCovariance(this ModelFactor modelFactor)
        {
            RrFunction[,] factorCovariances = FactorCovariance(modelFactor);
            return OrnsteinCovariance(factorCovariances, modelFactor.Factors);
        }
        public static OrnsteinUhlenbeckNd OrnsteinProcess(this ModelFactorDescription modelFactorDescription, double[] value0, ITimeMeasure time)
        {
            var modelFactor = modelFactorDescription.ToModelFactor(value0, time);
            return OrnsteinProcess(modelFactor);
        }
        public static OrnsteinUhlenbeckNd OrnsteinProcess(this ModelFactor modelFactor)
        {
            return new OrnsteinUhlenbeckNd(modelFactor.MeanReversions, modelFactor.OrnsteinCovariance(), modelFactor.InitialValue);
        }
        public static ModelFactor ToModelFactor(this ModelFactorDescription modelFactorDescription, double[] initialValue, ITimeMeasure time)
        {
            StepFunction[] factorVols = modelFactorDescription.FactorVols.Map(vol => (StepFunction)vol.ToStepFunction(time));
            return new ModelFactor(modelFactorDescription.FactorNames, initialValue, modelFactorDescription.MeanReversions,
                                   modelFactorDescription.Factors, modelFactorDescription.Correlation, factorVols);
        }
        public static ModelFactor Correlate(ModelFactor factor1, ModelFactor factor2, double[,] crossCorrelation)
        {
            var factorNames = EnumerableUtils.Append(factor1.FactorNames, factor2.FactorNames);
            var value0 = EnumerableUtils.Append(factor1.InitialValue, factor2.InitialValue);
            var meanRevs = EnumerableUtils.Append(factor1.MeanReversions, factor2.MeanReversions);
            var factorVols = EnumerableUtils.Append(factor1.FactorVols, factor2.FactorVols);
            
            var factors = new double[factor1.Factors.GetLength(0) + factor2.Factors.GetLength(0), factor1.Factors.GetLength(1) + factor2.Factors.GetLength(1)]; 
            ArrayUtils.SetSubArray(ref factors, 0, 0, factor1.Factors);
            ArrayUtils.SetSubArray(ref factors, factor1.Factors.GetLength(0), factor1.Factors.GetLength(1), factor2.Factors);
            
            var dim1 = factor1.FactorNames.Length;
            var dim2 = factor2.FactorNames.Length;
            if (crossCorrelation.GetLength(0) != dim1 || crossCorrelation.GetLength(1) != dim2)
                throw new Exception("FactorialOrnsteinDescriptionUtils : invalid cross correlation format");

            var correl = new double[dim1 + dim2, dim1 + dim2];
            ArrayUtils.SetSubArray(ref correl, 0, 0, factor1.Correlation);
            ArrayUtils.SetSubArray(ref correl, dim1, dim1, factor2.Correlation);
            for (int i = 0; i < dim1; i++)
            {
                for (int j = 0; j < dim2; j++)
                {
                    correl[i, dim1 + j] = crossCorrelation[i, j];
                    correl[dim1 + j, i] = crossCorrelation[i, j];
                }
            }

            return new ModelFactor(factorNames, value0, meanRevs, factors, correl, factorVols);
        }    
    }

}