using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets;

namespace PragmaticQuant.Model.Rate
{
    public class DeterministicRateDescription : ISingleRiskModelDescription
    {
        public DeterministicRateDescription(Currency currency)
        {
            Currency = currency;
        }
        public Currency Currency { get; private set; }
        public RiskId Risk
        {
            get { return RiskId.Rate(Currency); }
        }
        public RiskFactor[] RiskFactors
        {
            get { return new RiskFactor[0]; }
        }
        public ModelFactorDescription ModelFactorDescription
        {
            get
            {
                return new ModelFactorDescription(new string[0], new double[0], 
                                                        new double[0, 0], new double[0, 0],
                                                        new RawMapDatas<DateOrDuration, double>[0]);
            }
        }
    }

    internal class DeterministicRateDescriptionFactory : SingleRiskModelFactory<DeterministicRateDescription>
    {
        public static readonly DeterministicRateDescriptionFactory Instance = new DeterministicRateDescriptionFactory();
        public override ISingleRiskModel Build(DeterministicRateDescription detRate, Market market)
        {
            var time = ModelFactoryUtils.DefaultTime(market.RefDate);
            var detRateModel = new DeterministicRateModel(detRate.Currency, time);
            return detRateModel;
        }
    }

}