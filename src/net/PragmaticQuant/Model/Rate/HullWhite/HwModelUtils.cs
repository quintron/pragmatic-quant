using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Maths;
using PragmaticQuant.Maths.Function;

namespace PragmaticQuant.Model.Rate.HullWhite
{
    public static class HwModelUtils
    {
        /// <summary>
        /// t -> (exp(- meanReversion * (T-t)) - 1) / meanReversion 
        /// </summary>
        /// <param name="maturity"></param>
        /// <param name="meanReversion"></param>
        /// <returns></returns>
        public static RrFunction ZcCoeffFunction(double maturity, double meanReversion)
        {
            if (Math.Pow(meanReversion * maturity, 2.0) < 16.0 * DoubleUtils.MachineEpsilon)
            {
                return RrFunctions.Affine(1.0, -maturity);
            }

            return (Math.Exp(-meanReversion * maturity) / meanReversion) * RrFunctions.Exp(meanReversion) - (1.0 / meanReversion);
        }
        
        public static RrFunction ZcRateCoeffFunction(double maturity, double tenor, double meanReversion)
        {
            double tenorCoeff;
            if (Math.Pow(tenor * meanReversion, 2.0) < 16.0 * DoubleUtils.MachineEpsilon)
            {
                tenorCoeff = 1.0 - 0.5 * tenor * meanReversion;
            }
            else
                tenorCoeff = (1.0 - Math.Exp(-meanReversion * tenor)) / (meanReversion * tenor);


            if (Math.Pow(maturity * meanReversion, 2.0) < 16.0 * DoubleUtils.MachineEpsilon)
            {
                return tenorCoeff * RrFunctions.Affine(meanReversion, 1.0 - meanReversion * maturity);
            }

            return (tenorCoeff * Math.Exp(-maturity * meanReversion)) * RrFunctions.Exp(meanReversion);
        }
        
        public static double ZcCoeff(double duration, double meanReversion)
        {
            return Math.Pow(duration * meanReversion, 2.0) < 6.0 * DoubleUtils.MachineEpsilon
                ? -duration * (1.0 - 0.5 * meanReversion * duration)
                : -(1.0 - Math.Exp(-duration * meanReversion)) / meanReversion;
        }
        
        public static RnRFunction ZcFunction(double duration, double fwdZc, double[] meanReversions, double[,] covariances)
        {
            var zcCoeffs = meanReversions.Map(l => ZcCoeff(duration, l));
            double cvx = covariances.BilinearProd(zcCoeffs, zcCoeffs);
            return RnRFunctions.ExpAffine(fwdZc * Math.Exp(-0.5 * cvx), zcCoeffs);
        }
        public static AffineRnRFunction ZcRateFunction(double duration, double fwdZcRate, double[] meanReversions, double[,] covariances)
        {
            var zcCoeffs = meanReversions.Map(l => ZcCoeff(duration, l));
            var zcRateCoeffs = zcCoeffs.Map(c => -c / duration);
            double cvx = covariances.BilinearProd(zcCoeffs, zcCoeffs);
            return new AffineRnRFunction(fwdZcRate + 0.5 * cvx / duration, zcRateCoeffs);
        }
        
    }

}