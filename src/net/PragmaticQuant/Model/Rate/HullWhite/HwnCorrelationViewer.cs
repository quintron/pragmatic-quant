using System;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Maths.Stochastic;

namespace PragmaticQuant.Model.Rate.HullWhite
{
    public class HwnCorrelationViewer
    {
        #region private fields
        private readonly ITimeMeasure time;
        private readonly double[] meanReversions;
        private readonly RrFunction[,] covariance;
        #endregion
        #region private methods
        private RrFunction[] FwdZcRateWeight(DateTime zcStart, DateTime zcEnd)
        {
            var maturity = time[zcStart];
            var tenor = time[zcEnd] - maturity;

            var weights = new RrFunction[meanReversions.Length];
            for (int i = 0; i < meanReversions.Length-1; i++)
                weights[i] = HwModelUtils.ZcRateCoeffFunction(maturity, tenor, meanReversions[i]);

            weights[meanReversions.Length - 1] = RrFunctions.Zero; //Term correspondant � l'asset
            return weights;
        }
        private RrFunction[] ZcWeight(DateTime zcMaturity)
        {
            var maturity = time[zcMaturity];
            var weights = new RrFunction[meanReversions.Length];
            for (int i = 0; i < meanReversions.Length-1; i++)
                weights[i] = HwModelUtils.ZcCoeffFunction(maturity, meanReversions[i]);

            weights[meanReversions.Length-1] = RrFunctions.Zero; //Term correspondant � l'asset
            return weights;
        }
        private RrFunction[] AssetFwdWeight(DateTime fwdMaturity)
        {
            var zcWeights = ZcWeight(fwdMaturity);

            var weights = ArrayUtils.Constant(meanReversions.Length, RrFunctions.Zero);
            weights[meanReversions.Length-1] = 1.0;
            
            for (int i = 0; i < weights.Length; i++)
            {
                weights[i] -= zcWeights[i];
            }
            return weights;
        }
        private RrFunction[] Weights(FixingDesc fixing)
        {
            var rate = fixing as ForwardDesc<Duration>;
            if (rate != null)
            {
                var fwdStart = time.RefDate + rate.Maturity;
                var fwdEnd = fwdStart + rate.Underlying;
                return FwdZcRateWeight(fwdStart, fwdEnd);
            }
            
            var asset = fixing as ForwardDesc<string>;
            if(asset!=null)
                return AssetFwdWeight(time.RefDate + asset.Maturity);

            var lc = fixing as LinearCombinationFixingDesc;
            if (lc==null) throw new Exception("Not handled fixing desc");
            var fixingsWeights = lc.Fixings.Map(Weights).ZipWith(lc.Weights, (ws, a) => ws.Map(f => f * a));
            var lcWeights = fixingsWeights.Aggregate(
                ArrayUtils.Constant(meanReversions.Length, RrFunctions.Zero),
                (current, w) => current.ZipWith(w, (a, b) => a + b));
            return lcWeights;
        }
        private RrFunction Covariance(RrFunction[] weights1, RrFunction[] weights2)
        {
            var cov = RrFunctions.Zero;
            for (int i = 0; i < weights1.Length; i++)
                for (int j = 0; j < weights2.Length; j++)
                    cov += weights1[i] * weights2[j] * covariance[i, j];
            return cov;
        }
        #endregion

        //TODO,FIX clarify asset position
        private HwnCorrelationViewer(ITimeMeasure time, OrnsteinUhlenbeckNd hybridHwn)
        {
            this.time = time;
            meanReversions = hybridHwn.MeanReversions;
            covariance = hybridHwn.Covariances;
        }

        public static HwnCorrelationViewer Build(IModel model)
        {
            var factor = model.ModelFactor();
            var modelFactor = factor.Item2;
            var ornstein = modelFactor.OrnsteinProcess();
            if (factor.Item1.All(rf => rf.Id.Type != RiskType.Eqy))
                ornstein = OrnsteinUhlenbeckUtils.Correlate(ornstein,
                                                            OrnsteinUhlenbeckNd.Brownian1D(RrFunctions.Zero),
                                                            ArrayUtils.Constant(ornstein.Dimension, RrFunctions.Zero).AsColumn());
            return new HwnCorrelationViewer(model.Time, ornstein);
        }

        public RrFunction Covariance(FixingDesc fixing1, FixingDesc fixing2)
        {
            RrFunction[] weights1 = Weights(fixing1);
            RrFunction[] weights2 = Weights(fixing2);
            return Covariance(weights1, weights2);
        }

        public RrFunction Variance(FixingDesc fixing)
        {
            RrFunction[] weights = Weights(fixing);
            return Covariance(weights, weights);
        }

    }

}