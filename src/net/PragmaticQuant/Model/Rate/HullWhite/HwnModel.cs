using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Maths.Stochastic;

namespace PragmaticQuant.Model.Rate.HullWhite
{
    public class HwnModel : RateModel
    {
        #region private fields
        private readonly RiskFactor[] riskFactors;
        private readonly ModelFactor factorProcess;
        #endregion

        public HwnModel(Currency currency, ITimeMeasure time, ModelFactor modelFactor)
            : base(currency, time)
        {
            factorProcess = modelFactor;
            
            var rateRisk = RiskId.Rate(currency);
            riskFactors = EnumerableUtils.For(0, factorProcess.MeanReversions.Length,
                i => new RiskFactor(rateRisk, modelFactor.FactorNames[i]));
        }
        public static HwnModel Hw1(ITimeMeasure time, Currency currency, double meanReversion, RrFunction sigma)
        {
            var factor = new ModelFactor(new []{RiskId.Rate(currency).ToString()}, new[] {0.0}, new[] {meanReversion},
                                         new [,] {{1.0}}, new [,] {{1.0}}, new[] {sigma});
            return new HwnModel(currency, time, factor);
        }

        public override RiskFactor[] RiskFactors
        {
            get { return riskFactors; }
        }
        public override ModelFactor FactorProcess
        {
            get { return factorProcess; }
        }
    }

    public static class HwnModelUtils
    {
        public static RrFunction[] Drifts(OrnsteinUhlenbeckNd ou, ITimeMeasure time, PaymentInfo probaMeasure)
        {
            var integratedCovs = OrnsteinUhlenbeckUtils.IntegratedCovariance(ou.Covariances, ou.MeanReversions);
            RrFunction[] drifts = EnumerableUtils.For(0, integratedCovs.GetLength(1), i => RrFunctions.Sum(integratedCovs.Row(i)));

            double probaMat = time[probaMeasure.Date];
            if (probaMat > 0.0)
            {
                var probaDrifts = EnumerableUtils.For(0, drifts.Length, i =>
                {
                    var instantCov = ou.Covariances.Row(i);
                    var probaDrift = RrFunctions.Zero;
                    for (int j = 0; j < instantCov.Length; j++)
                        probaDrift += instantCov[j] * HwModelUtils.ZcCoeffFunction(probaMat, ou.MeanReversions[j]);
                    return probaDrift;
                });

                drifts = drifts.ZipWith(probaDrifts, (drift, proba) => drift + proba);
            }

            return drifts;
        }
        public static OrnsteinUhlenbeckNd DrivingOrnsteinProcess(this HwnModel hwn, PaymentInfo probaMeasure)
        {
            if (!hwn.Currency.Equals(probaMeasure.Currency))
                throw new Exception("HwnModel only domestic currency numeraire allowed !");
            OrnsteinUhlenbeckNd ou = hwn.FactorProcess.OrnsteinProcess();
            RrFunction[] drift = Drifts(ou, hwn.Time, probaMeasure);
            return ou.AddDrift(drift);
        }
    }
    
}