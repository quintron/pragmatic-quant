using System;
using System.Collections.Generic;
using System.Linq;
using PragmaticQuant.Core;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets;
using PragmaticQuant.Maths;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Maths.Stochastic;
using PragmaticQuant.Product;

namespace PragmaticQuant.Model.Rate.HullWhite
{
    public class HwnModelCalibDesc : ICalibrationDescription
    {
        public HwnModelCalibDesc(Currency currency,
                                 double[] meanReversions,
                                 IDictionary<string, double[]> factors,
                                 double[] factorVolRatios,
                                 double[,] factorCorrelations,
                                 SwaptionSmilePoint[] calibrationPattern,
                                 double[] calibrationVols = null)
            : this(currency,
                   meanReversions,
                   factors.Keys.ToArray(),
                   ArrayUtils.AppendRows(factors.Values.ToArray()).Tranpose(),
                   factorVolRatios,
                   factorCorrelations,
                   calibrationPattern, calibrationVols)
        {
        }

        public HwnModelCalibDesc(Currency currency,
                                 double[] meanReversions,
                                 string[] factorNames,
                                 double[,] factors,
                                 double[] factorVolRatios,
                                 double[,] correlation,
                                 SwaptionSmilePoint[] calibrationPattern,
                                 double[] calibrationVols = null)

        {
            if (meanReversions.Length != factors.GetLength(0))
                throw new Exception("HwnModelCalibDesc : incompatible size beetween meanReversions and factors !");

            if (factorNames.Length != factorVolRatios.Length)
                throw new Exception("HwnModelCalibDesc : incompatible size beetween factorNames and factorVolRatios !");

            if (correlation.GetLength(1) != correlation.GetLength(0))
                throw new Exception("HwnModelCalibDesc : correlation must be a square matrix !");

            if (factors.GetLength(1) != correlation.GetLength(0))
                throw new Exception("HwnModelCalibDesc : incompatible size beetween factors and correlation !");

            if ((calibrationVols != null) && (calibrationPattern.Length != calibrationVols.Length))
                throw new Exception("HwnModelCalibDesc : incompatible size beetween calibrationPattern and calibrationVols !");
            
            Currency = currency;
            MeanReversions = meanReversions;
            Factors = factors;
            Correlation = correlation;
            FactorNames = factorNames;
            CalibrationPattern = calibrationPattern;
            CalibrationVols = calibrationVols;
            FactorVolRatios = factorVolRatios;
        }

        public Currency Currency { get; private set; }
        public double[] MeanReversions { get; private set; }
        public string[] FactorNames { get; private set; }
        public double[,] Factors { get; private set; }
        public double[,] Correlation { get; private set; }
        public double[] FactorVolRatios { get; private set; }

        public SwaptionSmilePoint[] CalibrationPattern { get; private set; }
        public double[] CalibrationVols { get; private set; }
    }

    public class HwnModelCalibration : ModelCalibration<HwnModelCalibDesc>
    {
        #region private methods
        private static IModel FlatVolModel(HwnModelCalibDesc hwnDesc, Market market, double vol)
        {
            var unscaledFactorsVols = hwnDesc.FactorVolRatios.Map(ratio => new RawMapDatas<DateOrDuration, double>(new DateOrDuration[] { 0 * Duration.Year }, new[] { ratio * vol }));

            var factorDescription = new ModelFactorDescription(hwnDesc.FactorNames, hwnDesc.MeanReversions, hwnDesc.Factors, hwnDesc.Correlation, unscaledFactorsVols);
            var uncalibratedHwnDesc = new HwnModelDescription(hwnDesc.Currency, factorDescription);
            var hwnModel = HwnModelFactory.Instance.Build(uncalibratedHwnDesc, market);
            return new Model(new RiskFactorCorrelation(), hwnModel);
        }
        private static Tuple<Swaption, double>[] CalibTarget(HwnModelCalibDesc hwnDesc, Market market)
        {
            var calibOptions = hwnDesc.CalibrationPattern.Map(p => p.ToSwaption(market));

            double[] targetVols = hwnDesc.CalibrationVols;
            if (targetVols == null)
            {
                var rateVol = market.RateMarket(hwnDesc.Currency).VolCube();
                if (rateVol == null)
                    throw new Exception(string.Format("Missing {0} Swaption Market vol !", hwnDesc.Currency));

                targetVols = calibOptions.Map(swaption =>
                {
                    if (!swaption.Currency.Equals(rateVol.Currency))
                        throw new Exception("Invalid swaption currency for calibration");
                    return rateVol.Volatility(swaption.Date, swaption.Underlying, swaption.Strike);
                });
            }

            return calibOptions.ZipWith(targetVols, Tuple.Create)
                               .Where(t => t.Item1.Date > market.RefDate)
                               .OrderBy(t => t.Item1.Date)
                               .ToArray();
        }
        private static void ComputeVariances(double prevMaturity, double maturity, OrnsteinUhlenbeckNd ornstein, double[] rateWeights,
                                            out double prevMatVariance, out double stepVariance)
        {
            var unscaledStepCovs = OrnsteinUhlenbeckUtils.IntegratedCovariance(ornstein.Covariances, ornstein.MeanReversions, prevMaturity)
                                                         .Map(c => c.Eval(maturity));
            stepVariance = unscaledStepCovs.BilinearProd(rateWeights, rateWeights);

            var previousCovs = OrnsteinUhlenbeckUtils.IntegratedCovariance(ornstein.Covariances, ornstein.MeanReversions)
                                                     .Map(c => c.Eval(maturity));
            previousCovs.Sub(unscaledStepCovs);
            prevMatVariance = previousCovs.BilinearProd(rateWeights, rateWeights);
        }
        private static void UpdateCovariance(OrnsteinUhlenbeckNd ornstein, double previousMaturity, double maturity, double stepVol)
        {
            var stepVaScaling = new StepFunction(new[] { previousMaturity, maturity }, new[] { stepVol * stepVol, 1.0 }, 1.0);
            for (int i = 0; i < ornstein.Covariances.GetLength(0); i++)
                for (int j = 0; j < ornstein.Covariances.GetLength(1); j++)
                    ornstein.Covariances[i, j] *= stepVaScaling;
        }
        #endregion
        public static readonly HwnModelCalibration Instance = new HwnModelCalibration();
        public override IModelDescription Calibrate(HwnModelCalibDesc model, Market market, IProduct product)
        {
            var zeroVolHwn = FlatVolModel(model, market, 0.0);
            var spotProba = new PaymentInfo(model.Currency, market.RefDate);

            var factorRep = FactorRepresentationFactory.Instance.Build(zeroVolHwn, market, spotProba);
            var unitVolHwn = (HwnModel) FlatVolModel(model, market, 1.0).GetModelPart(RiskId.Rate(model.Currency));
            OrnsteinUhlenbeckNd ornstein = unitVolHwn.DrivingOrnsteinProcess(spotProba);
            var time = unitVolHwn.Time;

            var calibratedVols = new List<double>(model.CalibrationPattern.Length);
            double previous = time[market.RefDate];

            foreach (var target in CalibTarget(model, market))
            {
                Swaption option = target.Item1;
                double vol = target.Item2;

                var maturity = time[option.Date];
                var rateFunction = factorRep[option.UnderlyingRate()];
                var rateWeights = FiniteDifferenceUtils.CenteredGradient(rateFunction.Eval, ornstein.Value0, 1.0e-5);

                double previousMaturityVariance, unscaledStepVariance;
                ComputeVariances(previous, maturity, ornstein, rateWeights, out previousMaturityVariance, out unscaledStepVariance);

                var targetStepVariance = vol * vol * maturity - previousMaturityVariance;
                if (targetStepVariance < 0.0)
                    throw new Exception("TODO : Handle saturation ");
                var stepVol = Math.Sqrt(targetStepVariance / unscaledStepVariance);

                UpdateCovariance(ornstein, previous, maturity, stepVol);
                calibratedVols.Add(stepVol);
                previous = maturity;
            }

            var calibVol = new RawMapDatas<DateOrDuration, double>(model.CalibrationPattern.Map(p => p.Maturity), calibratedVols.ToArray());
            var calibFactorVols = model.FactorVolRatios.Map(ratio => calibVol.MapValues(v => v * ratio));
            var calibFactorDesc = new ModelFactorDescription(model.FactorNames, model.MeanReversions, model.Factors, model.Correlation, calibFactorVols);
            var hwn = new HwnModelDescription(model.Currency, calibFactorDesc);
            return new ModelDescription(new RiskFactorCorrelation(), hwn);
        }
    }
}