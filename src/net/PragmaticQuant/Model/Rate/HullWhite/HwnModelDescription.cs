using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets;

namespace PragmaticQuant.Model.Rate.HullWhite
{
    public class HwnModelDescription : ISingleRiskModelDescription
    {
        public HwnModelDescription(Currency currency, ModelFactorDescription modelFactorDescription)
        {
            Currency = currency;
            ModelFactorDescription = modelFactorDescription;
            Risk = RiskId.Rate(Currency);
            RiskFactors = EnumerableUtils.For(0, modelFactorDescription.FactorNames.Length,
                                i => new RiskFactor(Risk, modelFactorDescription.FactorNames[i]));
        }

        public Currency Currency { get; private set; }
        public RiskId Risk { get; private set; }
        public RiskFactor[] RiskFactors { get; private set; }

        public ModelFactorDescription ModelFactorDescription { get; private set; }
    }
    
    internal class HwnModelFactory : SingleRiskModelFactory<HwnModelDescription>
    {
        public static readonly HwnModelFactory Instance = new HwnModelFactory();
        public override ISingleRiskModel Build(HwnModelDescription hwn, Market market)
        {
            ITimeMeasure time = ModelFactoryUtils.DefaultTime(market.RefDate);
            ModelFactorDescription modelFactorDescription = hwn.ModelFactorDescription;
            var initialValue = new double[modelFactorDescription.MeanReversions.Length];
            var modelFactor = modelFactorDescription.ToModelFactor(initialValue, time);
            return new HwnModel(hwn.Currency, time, modelFactor);
        }
    }

}