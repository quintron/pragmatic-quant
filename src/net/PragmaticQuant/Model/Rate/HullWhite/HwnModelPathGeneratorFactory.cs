using System;
using System.Linq;
using PragmaticQuant.Core;
using PragmaticQuant.Basic;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Maths.Stochastic;

namespace PragmaticQuant.Model.Rate.HullWhite
{
    public class HwnModelPathGeneratorFactory : IModelPathGeneratorFactory
    {
        #region private methods
        private HwnModel GetHwn(IModel model)
        {
            return model.GetRateParts().Single() as HwnModel;
        }
        #endregion
        
        public IProcessPathGenerator Build(IModel model, PaymentInfo probaMeasure, DateTime[] pathDates)
        {
            HwnModel hwn = GetHwn(model);
            return new HwnPathGenerator(hwn.Time[pathDates], new double[hwn.RiskFactors.Length]);
        }
        public OrnsteinUhlenbeckNd GaussianFactors(IModel model, PaymentInfo probaMeasure)
        {
            var factorProcess = model.ModelFactor();
            
            if (factorProcess.Item1.Any(f => f.Id.Type != RiskType.Rate))
                throw new Exception("HwnModelPathGeneratorFactory : rate model expected !");

            var rateIds = factorProcess.Item1
                                        .Select(f => f.Id)
                                        .Distinct()
                                        .Where(id => id.Type == RiskType.Rate)
                                        .ToArray();
            if (rateIds.Length > 1)
                throw new Exception("HwnModelPathGeneratorFactory : single currency rate model expected !");

            if (!rateIds.Single().Equals(RiskId.Rate(probaMeasure.Currency)))
                throw new Exception("HwnModel : only domestic currency numeraire allowed !");

            var process = factorProcess.Item2.OrnsteinProcess();
            RrFunction[] drift = HwnModelUtils.Drifts(process, model.Time, probaMeasure);
            return process.AddDrift(drift);
        }
        public bool AcceptModel(IModel model)
        {
            if (model.RiskIds.Length != 1 
                || model.RiskIds.Single().Type != RiskType.Rate )
                return false;
            
            var rateModel = model.GetRateParts().Single();
            return rateModel is HwnModel;
        }

        #region private class
        private class HwnPathGenerator : IProcessPathGenerator
        {
            #region private fields
            private readonly double[] dates;
            private readonly double[] initialValue;
            public HwnPathGenerator(double[] dates, double[] initialValue)
            {
                this.dates = dates;
                this.initialValue = initialValue;
            }
            #endregion
            public IProcessPath Path(double[][] gaussianIncrements)
            {
                double[][] processPath = new double[gaussianIncrements.Length][];
                var currentProcess = initialValue.Copy();
                for (int i = 0; i < gaussianIncrements.Length; i++)
                {
                    currentProcess.Add(gaussianIncrements[i]);
                    processPath[i] = currentProcess.Copy();
                }
                return new ProcessPath(dates, initialValue.Length, processPath);
            }
            public double[] Dates
            {
                get { return dates; }
            }
            public int ProcessDim
            {
                get { return initialValue.Length; }
            }
            public int RandomDim
            {
                get { return initialValue.Length; }
            }
            public double[] AllSimulatedDates
            {
                get { return dates; }
            }
        }
        #endregion
    }

}