using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Maths.Stochastic;

namespace PragmaticQuant.Model.Rate.HullWhite
{
    public class HwnModelZcRepresentation : RateZcRepresentation
    {
        #region private fields
        private readonly ITimeMeasure time;
        private readonly double[] meanReversions;
        private readonly RrFunction[,] ouCovariances;
        #endregion
        public HwnModelZcRepresentation(HwnModel hwnModel)
            : base(hwnModel.Currency)
        {
            time = hwnModel.Time;
            meanReversions = hwnModel.FactorProcess.MeanReversions;
            var covariances = hwnModel.FactorProcess.OrnsteinCovariance();
            ouCovariances = OrnsteinUhlenbeckUtils.IntegratedCovariance(covariances, hwnModel.FactorProcess.MeanReversions);
        }
        public override RnRFunction Zc(DateTime date, DateTime maturity, double fwdZc)
        {
            if (maturity < date)
                throw new Exception("HwnModelZcRepresentation.Zc: past zero-coupon not allowed !");

            double d = time[date];
            var covariances = ouCovariances.Map(cov => cov.Eval(d));
            return HwModelUtils.ZcFunction(time[maturity] - d, fwdZc, meanReversions, covariances);
        }
        public override RnRFunction ZcRate(DateTime date, DateTime maturity, double zcRate)
        {
            if (maturity < date)
                throw new Exception("HwnModelZcRepresentation.Zc: past zero-coupon not allowed !");

            double d = time[date];
            var covariances = ouCovariances.Map(cov => cov.Eval(d));
            return HwModelUtils.ZcRateFunction(time[maturity] - d, zcRate, meanReversions, covariances);
        }
    }
}