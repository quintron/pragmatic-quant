using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Maths.Function;

namespace PragmaticQuant.Model.Rate
{
    public abstract class RateModel : ISingleRiskModel
    {
        #region private fields
        private readonly RiskId rateIdRiskId;
        #endregion
        protected RateModel(Currency currency, ITimeMeasure time)
        {
            rateIdRiskId = RiskId.Rate(currency);
            Time = time;
            Currency = currency;
        }

        public Currency Currency { get; private set; }
        
        public ITimeMeasure Time { get; private set; }
        public RiskId RiskId
        {
            get { return rateIdRiskId; }
        }
        public abstract RiskFactor[] RiskFactors { get; }
        public abstract ModelFactor FactorProcess { get; }
    }

    public class DeterministicRateModel : RateModel
    {
        public DeterministicRateModel(Currency currency, ITimeMeasure time)
            : base(currency, time)
        {
        }

        public override RiskFactor[] RiskFactors
        {
            get { return new RiskFactor[0]; }
        }
        public override ModelFactor FactorProcess
        {
            get { return new ModelFactor(new string[0], new double[0], new double[0], new double[0, 0], new double[0, 0], new StepFunction[0]); }
        }
        public int Dim { get { return 0; } }
    }
    
}