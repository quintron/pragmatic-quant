﻿using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets;
using PragmaticQuant.Product;

namespace PragmaticQuant.Model.Rate
{
    public class SwaptionSmilePoint
    {
        public SwaptionSmilePoint(Currency currency,
                                  DateOrDuration maturity,
                                  DateOrDuration underlying,
                                  double strike,
                                  bool isStrikeDeviation)
        {
            Currency = currency;
            Maturity = maturity;
            Underlying = underlying;
            Strike = strike;
            IsStrikeDeviation = isStrikeDeviation;
        }
        public Currency Currency { get; private set; }
        public DateOrDuration Maturity { get; private set; }
        public DateOrDuration Underlying { get; private set; }
        public double Strike { get; private set; }

        /// <summary>
        /// Mean that the strike is given in absolute deviation from forward
        /// </summary>
        public bool IsStrikeDeviation { get; private set; }

        public Swaption ToSwaption(Market mkt)
        {
            var rateMarket = mkt.RateMarket(Currency);
            
            var optionDate = Maturity.ToDate(rateMarket.RefDate);
            var fwd = rateMarket.SwapRateCurve().Fwd(optionDate, Underlying);
            var strike = IsStrikeDeviation ? fwd + Strike : Strike;
            var optionType = (strike < fwd) ? OptionType.Put : OptionType.Call;
            return new Swaption(Currency, Underlying, optionDate, optionType, strike);
        }
    }

}
