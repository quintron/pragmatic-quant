using PragmaticQuant.Basic;

namespace PragmaticQuant.Model
{

    public enum RiskType
    {
        Rate,
        Eqy
    }

    public abstract class RiskId
    {
        private RiskId(RiskType type)
        {
            Type = type;
        }

        public static RiskId Rate(Currency cur)
        {
            return new RateId(cur);
        }
        public static RiskId Equity(AssetId asset)
        {
            return new EquityId(asset);
        }
        
        public RiskType Type { get; private set; }

        #region public class
        public class RateId : RiskId
        {
            public RateId(Currency cur) : base(RiskType.Rate)
            {
                Currency = cur;
            }
            public Currency Currency { get; private set; }

            public override bool Equals(object obj)
            {
                var rateId = obj as RateId;
                return rateId != null && Equals(rateId);
            }
            protected bool Equals(RateId other)
            {
                return Equals(Type, other.Type)
                       && Equals(Currency, other.Currency);
            }
            public override int GetHashCode()
            {
                return (Currency != null ? Currency.GetHashCode() : 0);
            }
            public override string ToString()
            {
                return Type + "." + Currency;
            }
        }

        public class EquityId : RiskId
        {
            public EquityId(AssetId asset) : base(RiskType.Eqy)
            {
                Asset = asset;
            }
            public AssetId Asset { get; private set; }
            public override bool Equals(object obj)
            {
                var eqyId = obj as EquityId;
                return (eqyId != null) && Equals(eqyId);
            }
            protected bool Equals(EquityId other)
            {
                return Equals(Type, other.Type)
                       && Equals(Asset, other.Asset);
            }
            public override int GetHashCode()
            {
                return (Asset != null ? Asset.GetHashCode() : 0);
            }
            public override string ToString()
            {
                return Type + "." + Asset.Name;
            }
        }
        #endregion
    }

    public class RiskFactor
    {
        public RiskFactor(RiskId id, string name)
        {
            Id = id;
            Name = name;
        }
        public static RiskFactor EquitySpot(AssetId asset)
        {
            return new RiskFactor(RiskId.Equity(asset), "Spot");
        }

        public RiskId Id { get; private set; }
        public string Name { get; private set; }

        public override bool Equals(object obj)
        {
            var rf = obj as RiskFactor;
            return (rf != null) && Equals(rf);
        }
        protected bool Equals(RiskFactor other)
        {
            return Equals(Id, other.Id)
                && string.Equals(Name, other.Name);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                return ((Id != null ? Id.GetHashCode() : 0) * 397) ^ (Name != null ? Name.GetHashCode() : 0);
            }
        }
        
        public override string ToString()
        {
            return Id + "." + Name;
        }
    }

}