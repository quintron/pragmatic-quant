using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Maths;

namespace PragmaticQuant.Model
{
    public static class SmileLawUtils
    {
        public static double Cumulative(double forward, double maturity, Func<double, double>  vol, double strike)
        {
            var v = vol(strike);
            var dk = strike * 1e-5;
            var skew = (vol(strike + dk) - vol(strike - dk)) / (2.0 * dk);
            return BlackScholesOption.PriceDigit(forward, strike, v, maturity, -1.0, skew);
        }
        public static double ComputeQuantile(double forward, double maturity, Func<double, double> vol, double proba)
        {
            double s = vol(forward) * Math.Sqrt(maturity);
            double kGuess = forward * Math.Exp(s * (NormalDistribution.FastCumulativeInverse(proba) + 0.5 * s));
            
            Func<double, double> cumErr = m =>
                (Cumulative(forward, maturity, vol, forward * Math.Exp(m))) - proba;
            
            double m1, m2;
            RootUtils.Bracket(cumErr, Math.Log(kGuess / forward), 0.0, out m1, out m2);
            var mQuantile = RootUtils.Brenth(cumErr, m1, m2, forward * 1.0e-5, DoubleUtils.MachineEpsilon);
            return forward * Math.Exp(mQuantile);
        }
    }
}