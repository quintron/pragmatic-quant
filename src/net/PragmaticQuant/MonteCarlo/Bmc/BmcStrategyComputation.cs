using System;
using System.Linq;
using PragmaticQuant.Core;
using PragmaticQuant.Basic;
using PragmaticQuant.MonteCarlo.Product;
using PragmaticQuant.Product;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.MonteCarlo.Bmc
{
    public class BmcStrategyComputation : IPathResultAgregator<PathFlows<double, CouponFlowLabel>, BmcExerciseStrategy>
    {
        #region private methods
        private readonly IBmcStrategyAlgorithm strategyAlgorithm;
        private readonly ExerciseItem[] exerciseItems;
        #endregion
        #region private methods
        private static int FindIndex<T>(T[] list, T elem)
        {
            var matchElems = list.Select((t, i) => Tuple.Create(t, i))
                                 .Where(t=> t.Item1.Equals(elem))
                                 .ToArray();
            return matchElems.Single().Item2;
        }
        private static double[] NextCancelledFlows(ExerciseItem item, PathFlows<double, CouponFlowLabel>[] paths)
        {
            var cancelledFlowsIndexes = item.NextCancelledCpns.Map(
                cpn => FindIndex(paths[0].Labels, new CouponFlowLabel(cpn.PaymentInfo, cpn.CouponId)));

            var cancelledFlows = new double[paths.Length];

            if (cancelledFlowsIndexes.Length == 0)
                return cancelledFlows;

            for (int p = 0; p < paths.Length; p++)
            {
                var pathFlows = paths[p].Flows;

                var flow = 0.0;
                for (int j = 0; j < cancelledFlowsIndexes.Length; j++)
                    flow += pathFlows[cancelledFlowsIndexes[j]]; //TODO optimize for contiguous indexes

                cancelledFlows[p] = flow;
            }
            return cancelledFlows;
        }
        private static double[] RedemptionFlows(ExerciseItem item, PathFlows<double, CouponFlowLabel>[] paths)
        {
            var redemptionIndex = FindIndex(paths[0].Labels,
                                            new CouponFlowLabel(item.RedemptionCpn.PaymentInfo,
                                                                item.RedemptionCpn.CouponId));
                
            var redemptionFlows = new double[paths.Length];
            for (int p = 0; p < paths.Length; p++)
            {
                redemptionFlows[p] = paths[p].Flows[redemptionIndex];
            }
            return redemptionFlows;
        }
        private static double[][] RegressionVariables(ExerciseItem item, PathFlows<double, CouponFlowLabel>[] paths)
        {
            var regressionFlowsIndexes = item.RegressionVariableCpns.Map(
                cpn => FindIndex(paths[0].Labels, new CouponFlowLabel(cpn.PaymentInfo, cpn.CouponId)));

            var regressionFlows = new double[paths.Length][];
            for (int p = 0; p < paths.Length; p++)
            {
                var pathFlows = paths[p].Flows;

                var flows = new double[regressionFlowsIndexes.Length];
                for (int j = 0; j < flows.Length; j++)
                    flows[j] = pathFlows[regressionFlowsIndexes[j]];

                regressionFlows[p] = flows;
            }
            return regressionFlows;
        }
        #endregion

        public BmcStrategyComputation(ExerciseItem[] exerciseItems, IBmcStrategyAlgorithm strategyAlgorithm)
        {
            this.strategyAlgorithm = strategyAlgorithm;
            this.exerciseItems = exerciseItems.OrderBy(it => it.ExerciseDate).ToArray();
        }
        public BmcExerciseStrategy Aggregate(PathFlows<double, CouponFlowLabel>[] paths)
        {
            var holdFlows = new double[paths.Length];
            IFixingFunction[] triggers = new IFixingFunction[exerciseItems.Length];
            for (int exercise = exerciseItems.Length - 1; exercise >= 0; --exercise)
            {
                ExerciseItem exerciseItem = exerciseItems[exercise];
                double[] redemptionFlows = RedemptionFlows(exerciseItem, paths);
                double[] nextCancelledFlows = NextCancelledFlows(exerciseItem, paths);
                double[][] regressionVariables = RegressionVariables(exerciseItem, paths);

                double[] currentHoldFlows = holdFlows.Copy();
                currentHoldFlows.Add(nextCancelledFlows);

                //Compute exercise strategy
                bool[] exerciseOption;
                IFixingFunction triggerStrategy = strategyAlgorithm.ComputeStrategy(currentHoldFlows, redemptionFlows, 
                                                                                    regressionVariables, exerciseItem.RegressionVariables, 
                                                                                    out exerciseOption);
                triggers[exercise] = triggerStrategy;
                
                //Update hold flows
                for (int p = 0; p < paths.Length; p++)
                {
                    if (exerciseOption[p])
                    {
                        holdFlows[p] = redemptionFlows[p];
                    }
                    else
                    {
                        holdFlows[p] = currentHoldFlows[p];
                    }
                }

            }

            return new BmcExerciseStrategy(exerciseItems.Map(item => item.ExerciseDate), triggers);
        }
    }
    
    public class BmcExerciseStrategy
    {
        public BmcExerciseStrategy(DateTime[] callDates, IFixingFunction[] exerciseTriggers)
        {
            CallDates = callDates;
            ExerciseTriggers = exerciseTriggers;
        }
        public DateTime[] CallDates { get; private set; }
        public IFixingFunction[] ExerciseTriggers { get; private set; }
    }
    
    public class ExerciseItem
    {
        public ExerciseItem(DateTime exerciseDate,
                            Coupon[] nextCancelledCpns, Coupon redemptionCpn,
                            IObservation[] regressionVariables,
                            Coupon[] regressionVariableCpns)
        {
            ExerciseDate = exerciseDate;
            NextCancelledCpns = nextCancelledCpns;
            RedemptionCpn = redemptionCpn;

            RegressionVariables = regressionVariables;
            RegressionVariableCpns = regressionVariableCpns;
        }

        public DateTime ExerciseDate { get; private set; }

        public Coupon[] NextCancelledCpns { get; private set; }
        public Coupon RedemptionCpn { get; private set; }

        public IObservation[] RegressionVariables { get; private set; }
        public Coupon[] RegressionVariableCpns { get; private set; }
    }
    
}