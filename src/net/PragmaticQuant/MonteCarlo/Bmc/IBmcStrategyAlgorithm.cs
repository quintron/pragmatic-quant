using System;
using System.Linq;
using PragmaticQuant.Core;
using PragmaticQuant.Basic;
using PragmaticQuant.Maths;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.MonteCarlo.Bmc
{
    public interface IBmcStrategyAlgorithm
    {
        IFixingFunction ComputeStrategy(double[] holdValues, double[] redemptionValues, double[][] regressionVariableValues, IObservation[] regressionVariables, 
                                        out bool[] exerciseOption);
    }

    public class SimplePolynomialStrategy : IBmcStrategyAlgorithm
    {
        #region private methods
        public void ComputeMeanAndVariance(double[][] samples, out double[] mean, out double[] variance)
        {
            var dim = samples.First().Length;
            mean = new double[dim];
            variance = new double[dim];
            for (int sample = 0; sample < samples.Length; sample++)
            {
                mean.Add(samples[sample]);

                var square = samples[sample].Copy();
                square.Mult(samples[sample]);
                variance.Add(square);
            }
            
            mean.Mult(1.0 / samples.Length);
            variance.Mult(1.0 / samples.Length);
            var meanSquare = mean.Copy();
            meanSquare.Mult(mean);
            variance.Sub(meanSquare);
        }
        public double[,] CovarianceMatrix(double[][] samples)
        {
            var dim = samples.First().Length;
            var cov = new double[dim, dim];
            for (int sample = 0; sample < samples.Length; sample++)
            {
                var x = samples[sample];
                for (int i = 0; i < dim; i++)
                {
                    for (int j = 0; j < i; j++)
                    {
                        var covij = x[i] * x[j] / samples.Length;
                        cov[i, j] += covij;
                        cov[j, i] += covij;
                    }
                    cov[i, i] += x[i] * x[i] / samples.Length;
                }
            }

            return cov;
        }
        public double[] BasisCov(double[][] basis, double[] target)
        {
            var dim = basis.First().Length;
            var result = new double[dim];
            for (int sample = 0; sample < basis.Length; sample++)
            {
                var x = basis[sample];
                var t = target[sample] / target.Length;
                for (int j = 0; j < result.Length; j++)
                    result[j] += x[j] * t ;
            }
            return result;
        }

        public void SelectNonConstantVariables(double[][] regressionVariableValues, IObservation[] regressionVariables,
                                               out double[][] selectedVariableValues, out IObservation[] selectedVariables)
        {
            double[] variableMeans, variableVariances;
            ComputeMeanAndVariance(regressionVariableValues, out variableMeans, out variableVariances);

            var maxVariance = variableVariances.Max();
            var nonCstIndexes = variableVariances.Select((v, i) => new {Variance = v, Index = i})
                                                 .Where(t => t.Variance > maxVariance * 16.0 * DoubleUtils.MachineEpsilon)
                                                 .Map(t => t.Index);

            selectedVariables = nonCstIndexes.Map(i => regressionVariables[i]);
            selectedVariableValues = regressionVariableValues.Map(vals => nonCstIndexes.Map(i => vals[i]));
        }
        #endregion

        public IFixingFunction ComputeStrategy(double[] holdValues, double[] redemptionValues, double[][] regressionVariableValues, IObservation[] regressionVariables, 
                                               out bool[] exerciseOption)
        {
            double[][] selectedVariableValues;
            IObservation[] selectedVariables;
            SelectNonConstantVariables(regressionVariableValues, regressionVariables,
                                       out selectedVariableValues, out selectedVariables);
            
            var basisEval = new PolynomialEval(3);
            
            var basisSamples = selectedVariableValues.Map(v => basisEval.Eval(v));
            var exerciceValues = redemptionValues.Copy();
            exerciceValues.Sub(holdValues);
            var basisCoeffs = RegressionUtils.Regression(basisSamples, exerciceValues, 1.0e-8);

            Func<double[], double> strategy = regVars =>
            {
                var basis = basisEval.Eval(regVars);
                var exercice = basisCoeffs.DotProduct(basis);
                return (exercice > 0.0) ? 1.0 : 0.0;
            };
            
            exerciseOption = selectedVariableValues.Map(regVars => strategy(regVars) > 0.5);
            return new GenericFixingFunction(strategy, selectedVariables);
        }
        #region private class
        private class PolynomialEval
        {
            #region private fields
            private readonly int degree;
            #endregion
            public PolynomialEval(int degree)
            {
                this.degree = degree;
            }
            public double[] Eval(double[] values)
            {
                var result = new double[1 + degree * values.Length + values.Length * (values.Length - 1) / 2];
                var index = 0;

                //Constant Term
                result[index++] = 1.0;

                //Polynomial Terms
                for (int i = 0; i < values.Length; i++)
                {
                    var value = values[i];
                    var term = value;
                    for (int d = 1; d <= degree; d++)
                    {
                        result[index++] = term;
                        term *= value;
                    }
                }

                //Cross-terms
                for (int i = 0; i < values.Length; i++)
                {
                    for (int j = 0; j < i; j++)
                        result[index++] = values[i] * values[j];
                }

                return result;
            }
        }
        #endregion
    }
}