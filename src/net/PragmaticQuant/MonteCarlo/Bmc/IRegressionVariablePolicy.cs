using System;
using System.Collections.Generic;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets;
using PragmaticQuant.Product;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.MonteCarlo.Bmc
{
    public interface IRegressionVariablePolicy
    {
        IRegressionVariableBuilder GetBuilder(Market market);
    }

    public interface IRegressionVariableBuilder
    {
        IObservation[] GetRegressionVariables(DateTime callDate, Coupon[] couponToRegress);   
    }

    public class RegressionVariablePolicy : IRegressionVariablePolicy
    {
        public IRegressionVariableBuilder GetBuilder(Market market)
        {
            return new RegressionVariableBuilder(market);
        }
    }

    public class RegressionVariableBuilder : IRegressionVariableBuilder
    {
        #region private fields
        private readonly Market market;
        #endregion
        #region private methods
        private IObservation GetFwd(DateTime callDate, IFixing fixing)
        {
            if (fixing.Date <= callDate)
                return fixing;

            //TODO avoid equity forward whose model representation is not implemented
            var eqySpot = fixing as EquitySpot;
            if (eqySpot != null)
            {
                var spot0 = market.AssetMarket(eqySpot.AssetId).Spot;
                var spot = new EquitySpot(callDate, eqySpot.AssetId);
                return new GenericFixingFunction(s => s[0] / spot0, spot);
            }

            return new ForwardFixing(callDate, fixing);
        }
        private IObservation[] SelectFixings(DateTime callDate, IFixing[] fixings)
        {
            IFixing[] orderedFixings = fixings.OrderBy(f => f.Date).ToArray();
            var selectedFixings = new List<IObservation>();

            var pastFixings = orderedFixings.Where(f => f.Date <= callDate).ToArray();
            if (pastFixings.Any())
            {
                selectedFixings.Add(pastFixings.First());
                if (pastFixings.Length > 1)
                    selectedFixings.Add(pastFixings.Last());
            }

            var futureFixings = orderedFixings.Where(f => f.Date >= callDate).ToArray();
            if (futureFixings.Any())
            {
                selectedFixings.Add(GetFwd(callDate, futureFixings.First()));
                if (futureFixings.Length>1)
                    selectedFixings.Add(GetFwd(callDate, futureFixings.Last()) );
            }
            
            return selectedFixings.Distinct().ToArray();
        }
        private IObservation[] Fixings(DateTime callDate, Coupon[] couponToRegress)
        {
            var allObservations = couponToRegress.Aggregate(new IObservation[0], (obs, cpn) => obs.Union(cpn.Payoff.Observations).ToArray());
            var allFixings = allObservations.Where(obs => obs is IFixing).Cast<IFixing>();
            var selectedFixings = allFixings.GroupBy(fix => fix.GetType())
                                            .Map(fixGroup => SelectFixings(callDate, fixGroup.ToArray()));
            return EnumerableUtils.Merge(selectedFixings);
        }
        private static IObservation[] DiscountRates(DateTime callDate, Coupon[] couponToRegress)
        {
            var allPayments = couponToRegress.Map(cpn => cpn.PaymentInfo);
            return allPayments.GroupBy(payInfo => payInfo.Currency)
                                .Select(payInfos =>
                                {
                                    Currency currency = payInfos.Key;
                                    var maxPayDate = payInfos.Select(pi => pi.Date).Max();
                                    return new ZcRate(callDate, maxPayDate, currency);
                                })
                                .Where(r => r.EndDate > r.Date + Duration.Month)
                                .ToArray();
        }
        #endregion
        public RegressionVariableBuilder(Market market)
        {
            this.market = market;
        }
        public IObservation[] GetRegressionVariables(DateTime callDate, Coupon[] couponToRegress)
        {
            //WIP
            //var fixedCoupons = couponToRegress.Where(cpn => cpn.Observations.Length == 0).ToArray();
            //var fixedCouponMtm = ArrayUtils.Append(fixedCoupons.GroupBy(cpn => cpn.PaymentInfo.Currency)
            //                                                    .Map(gp =>
            //                                                    {
            //                                                        var currency = gp.Key;
            //                                                        var payDates = fixedCoupons.Map(cpn => cpn.PaymentInfo.Date);
            //                                                        var amounts = fixedCoupons.Map(cpn => cpn.Payoff.Value(new double[0]));
            //                                                        return new FixedLegMtm(callDate, currency, payDates, amounts);
            //                                                    }));

            var fixings = Fixings(callDate, couponToRegress);
            var zcRates = DiscountRates(callDate, couponToRegress);
            return EnumerableUtils.Merge(fixings, zcRates);
        }
    }
}