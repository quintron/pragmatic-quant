﻿namespace PragmaticQuant.MonteCarlo
{
    public interface IPathGenerator<TPath>
    {
        void ComputePath(ref TPath path, double[] randoms);

        TPath NewPath();
        int SizeOfPath { get; }
    }
}