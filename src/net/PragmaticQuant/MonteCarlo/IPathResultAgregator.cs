namespace PragmaticQuant.MonteCarlo
{
    public interface IPathResultAgregator<in TPath, out TResult>
    {
        TResult Aggregate(TPath[] paths);
    }
    
}