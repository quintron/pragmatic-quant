﻿using System.Collections.Concurrent;
using System.Threading.Tasks;
using PragmaticQuant.Maths;

namespace PragmaticQuant.MonteCarlo
{
    public sealed class McEngine<TPath, TResult>
    {
        #region private fields
        private readonly bool multithread;
        private readonly IRandomGenerator randomGenerator;
        private readonly IPathGenerator<TPath> pathGenerator;
        private readonly IPathResultAgregator<TPath, TResult> pathResultAggregator;
        #endregion
        #region private methods
        private void Run(ref TPath[] pathResults, int fromInclusive, int toExclusive)
        {
            var gen = randomGenerator.Clone();
            gen.SkipTo(fromInclusive);
            for (int i = fromInclusive; i < toExclusive; i++)
            {
                var randoms = gen.Next();
                pathGenerator.ComputePath(ref pathResults[i], randoms);
            }
        }
        #endregion
        public McEngine(IRandomGenerator randomGenerator,
                        IPathGenerator<TPath> pathGenerator,
                        IPathResultAgregator<TPath, TResult> pathResultAggregator,
                        bool multithread)
        {
            this.randomGenerator = randomGenerator;
            this.pathGenerator = pathGenerator;
            this.pathResultAggregator = pathResultAggregator;
            this.multithread = multithread;
        }
        public TResult Run(int nbPaths)
        {
            TPath[] pathResults = new TPath[nbPaths];
            for (int i = 0; i < nbPaths; i++)
                pathResults[i] = pathGenerator.NewPath();

            if (multithread)
            {
                var partitioner = Partitioner.Create(0, nbPaths);
                Parallel.ForEach(partitioner,
                    range => Run(ref pathResults, range.Item1, range.Item2));
            }
            else
            {
                Run(ref pathResults, 0, nbPaths);
            }

            var result = pathResultAggregator.Aggregate(pathResults);
            return result;
        }
    }
}
