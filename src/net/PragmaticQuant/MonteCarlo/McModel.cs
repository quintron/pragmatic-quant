﻿using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Maths;
using PragmaticQuant.Maths.Stochastic;
using PragmaticQuant.Model;

namespace PragmaticQuant.MonteCarlo
{
    public class McModel
    {
        public McModel(IFactorModelRepresentation factorRepresentation,
                       DateTime[] pathDates,
                       IRandomGenerator randomGenerator,
                       GaussianFactorPath gaussianFactorPath,
                       IProcessPathGenerator processPathGen,
                       PaymentInfo probaMeasure, double numeraire0)
        {
            Numeraire0 = numeraire0;
            ProbaMeasure = probaMeasure;
            ProcessPathGen = processPathGen;
            RandomGenerator = randomGenerator;
            GaussianFactorPath = gaussianFactorPath;
            PathDates = pathDates;
            FactorRepresentation = factorRepresentation;
        }

        public IFactorModelRepresentation FactorRepresentation { get; private set; }
        
        public IRandomGenerator RandomGenerator { get; private set; }
        public GaussianFactorPath GaussianFactorPath { get; private set; }
        public IProcessPathGenerator ProcessPathGen { get; private set; }
        
        public DateTime[] PathDates { get; private set; }
        public PaymentInfo ProbaMeasure { get; private set; }
        public double Numeraire0 { get; private set; }
    }

}