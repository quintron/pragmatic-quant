using System;
using System.Collections.Generic;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Markets;
using PragmaticQuant.Maths;
using PragmaticQuant.Maths.Stochastic;
using PragmaticQuant.Model;

namespace PragmaticQuant.MonteCarlo
{
    public class McModelFactory
    {
        #region private fields
        private readonly MonteCarloConfig mcConfig;
        #endregion
        #region private methods
        private PaymentInfo ProbaMeasure(IEnumerable<DateTime> simulatedDates, IModel model)
        {
            var horizon = simulatedDates.Max();
            return new PaymentInfo(model.PivotCurrency, horizon);
        }
        #endregion
        public McModelFactory(MonteCarloConfig mcConfig)
        {
            this.mcConfig = mcConfig;
        }

        public McModel Build(IModel model, Market market, DateTime[] pathDates)
        {
            PaymentInfo probaMeasure = ProbaMeasure(pathDates, model);
            double numeraire0 = market.DiscountCurve(probaMeasure.Financing).Zc(probaMeasure.Date);

            IFactorModelRepresentation factorRepresentation = FactorRepresentationFactory.Instance.Build(model, market, probaMeasure);

            var modelPathGenFactory = ModelPathGeneratorFactories.For(model, mcConfig);
            IProcessPathGenerator processPathGenerator = modelPathGenFactory.Build(model, probaMeasure, pathDates);

            var gaussianFactor = modelPathGenFactory.GaussianFactors(model, probaMeasure);
            GaussianFactorPath gaussianFactorPath = GaussianFactorPath.Build(gaussianFactor, processPathGenerator.AllSimulatedDates, processPathGenerator.Dates); 
            IRandomGenerator randomGenerator = mcConfig.RandomGenerator.Build(gaussianFactorPath.RandomSize);
            
            return new McModel(factorRepresentation, pathDates,
                               randomGenerator, gaussianFactorPath, processPathGenerator,
                               probaMeasure, numeraire0);
        }
    }

}