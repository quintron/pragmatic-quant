using System.Linq;
using PragmaticQuant.Core;

namespace PragmaticQuant.MonteCarlo
{
    public class PriceFlowsAggregator<TLabel>
        : IPathResultAgregator<PathFlows<double, TLabel>, PathFlows<double, TLabel>>
    {
        public PathFlows<double, TLabel> Aggregate(PathFlows<double, TLabel>[] paths)
        {
            var prices = new double[paths.First().Flows.Length];
            for (int path = 0; path < paths.Length; path++)
            {
                prices.Add(paths[path].Flows);
            }
            prices.Mult(1.0 / paths.Length);

            return new PathFlows<double, TLabel>(prices, paths.First().Labels);
        }
    }
}