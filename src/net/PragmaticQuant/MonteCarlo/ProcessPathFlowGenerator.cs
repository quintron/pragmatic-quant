﻿using PragmaticQuant.Core;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Maths.Stochastic;

namespace PragmaticQuant.MonteCarlo
{

    public class ProcessPathFlowGenerator<TFlow, TLabel> : IPathGenerator<PathFlows<TFlow, TLabel>>
    {
        #region private fields
        private readonly GaussianFactorPath gaussianFactorPath;
        private readonly IProcessPathGenerator processPathGen;
        private readonly IPathFlowCalculator<TFlow, TLabel> flowPathCalculator;
        private readonly ObjectPool<PoolableJaggedArray> pool; 
        #endregion
        public ProcessPathFlowGenerator(GaussianFactorPath gaussianFactorPath,
                                        IProcessPathGenerator processPathGen,
                                        IPathFlowCalculator<TFlow, TLabel> flowPathCalculator)
        {
            this.gaussianFactorPath = gaussianFactorPath;
            this.processPathGen = processPathGen;
            this.flowPathCalculator = flowPathCalculator;
            pool = new ObjectPool<PoolableJaggedArray>(() => PoolableJaggedArray.Create(gaussianFactorPath.nbDates, gaussianFactorPath.dim, false), 10);
        }

        public void ComputePath(ref PathFlows<TFlow, TLabel> path, double[] gaussians)
        {
            var buffer = pool.GetObject();
            var dWs = buffer.Data;
            
            gaussianFactorPath.GaussianFactorIncrements(ref dWs, gaussians);
            IProcessPath processPath = processPathGen.Path(dWs);
            flowPathCalculator.ComputeFlows(ref path, processPath);

            pool.ReturnObject(buffer);
        }
        
        public PathFlows<TFlow, TLabel> NewPath()
        {
            return flowPathCalculator.NewPathFlow();
        }
        public int SizeOfPath
        {
            get { return flowPathCalculator.SizeOfPath; }
        }
    }

    public sealed class GaussianFactorPath
    {
        #region private fields
        private readonly BrownianBridge brownianBridge;
        private readonly OrnsteinUhlenbeckPath_Fast gaussianSimulator;
        public readonly int dim;
        public readonly int nbDates;
        #endregion
        private GaussianFactorPath(BrownianBridge brownianBridge, OrnsteinUhlenbeckPath_Fast gaussianSimulator)
        {
            this.brownianBridge = brownianBridge;
            this.gaussianSimulator = gaussianSimulator;
            dim = gaussianSimulator.ProcessDim;
            nbDates = brownianBridge.Dates.Length;
        }
        public static GaussianFactorPath Build(OrnsteinUhlenbeckNd gaussianFactor, double[] allSimulatedDates, double[] importantDates)
        {
            StepSearcher allDatesSearcher = new StepSearcher(allSimulatedDates);
            int[] importantIndexes = importantDates.Map(d => allDatesSearcher.FindPillarIndex(d));
            BrownianBridge brownianBridge = BrownianBridge.Create(allSimulatedDates, importantIndexes);
            var gaussianSimulator = OrnsteinUhlenbeckPathFactory.Build_Fast(allSimulatedDates, gaussianFactor);
            return new GaussianFactorPath(brownianBridge, gaussianSimulator);
        }

        public void GaussianFactorIncrements(ref double[][] dWs, double[] gaussians)
        {
            brownianBridge.FillPathIncrements(ref dWs, gaussians);
            gaussianSimulator.ComputePathIncrements(ref dWs);
        }
        public int RandomSize
        {
            get { return brownianBridge.GaussianSize(dim); }
        }
    }

    public interface IPathFlowCalculator<TFlow, TLabel>
    {
        void ComputeFlows(ref PathFlows<TFlow, TLabel> pathFlows, IProcessPath processPath);
        PathFlows<TFlow, TLabel> NewPathFlow();
        int SizeOfPath { get; }
    }

}