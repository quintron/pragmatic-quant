using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.MonteCarlo.Product
{

    internal class AutocallPathFlow : IProductPathFlow
    {
        #region private fields
        private readonly CouponPathFlow[] underlyingPathFlows;
        private readonly CouponPathFlow[] redemptionPathFlows;
        private readonly ObservationPathEvaluator observationsEvaluator;
        private readonly int[] triggersObsIndexes;
        private readonly int[] underlyingCancellationIndexes;
        private readonly CouponFlowLabel[] labels;
        #endregion
        #region private methods
        private int CallIndex(double[] observations)
        {
            int triggerIndex = 0;
            for (; triggerIndex < triggersObsIndexes.Length; triggerIndex++)
            {
                var trigger = observations[triggersObsIndexes[triggerIndex]];

                if (trigger > 0.5)
                    break;
            }
            return triggerIndex;
        }
        #endregion
        internal AutocallPathFlow(CouponPathFlow[] underlyingPathFlows,
                                 CouponPathFlow[] redemptionPathFlows,
                                 ObservationPathEvaluator observationsEvaluator,
                                 int[] triggersObsIndexes,
                                 int[] underlyingCancellationIndexes,
                                 PaymentInfo[][] payments)
        {
            if (redemptionPathFlows.Length != triggersObsIndexes.Length
                || underlyingCancellationIndexes.Length != triggersObsIndexes.Length)
                throw new Exception("AutocallPathFlow : invalid array size");

            this.underlyingPathFlows = underlyingPathFlows;
            this.redemptionPathFlows = redemptionPathFlows;
            this.observationsEvaluator = observationsEvaluator;
            this.triggersObsIndexes = triggersObsIndexes;
            this.underlyingCancellationIndexes = underlyingCancellationIndexes;
            Fixings = observationsEvaluator.Fixings;
            Payments = payments;
            
            var underlyingLabels = underlyingPathFlows.Map(pf => pf.CouponLabel);
            var redemptionLabels = redemptionPathFlows.Map(pf => pf.CouponLabel);
            labels = EnumerableUtils.Append(underlyingLabels, redemptionLabels);
        }

        public void ComputePathFlows(ref PathFlows<double, CouponFlowLabel> pathFlows,
                                     PathFlows<double[], IObservation[]> fixingsPath,
                                     PathFlows<double[], PaymentInfo[]> flowRebasementPath)
        {
            double[][] rebasements = flowRebasementPath.Flows;

            double[] autocallFlows = pathFlows.Flows;
            for (int i = 0; i < autocallFlows.Length; i++)
                autocallFlows[i] = 0.0;

            double[] observations = new double[observationsEvaluator.Observations.Length];
            observationsEvaluator.FillObservations(ref observations, fixingsPath.Flows);

            int callIndex = CallIndex(observations);
            int firstCancelledCouponIndex = (callIndex < underlyingCancellationIndexes.Length)
                                                ? underlyingCancellationIndexes[callIndex]
                                                : underlyingPathFlows.Length;

            for (int i = 0; i < firstCancelledCouponIndex; i++)
                autocallFlows[i] = underlyingPathFlows[i].FlowValue(observations, rebasements);

            if (callIndex < redemptionPathFlows.Length)
            {
                autocallFlows[underlyingPathFlows.Length + callIndex] = redemptionPathFlows[callIndex].FlowValue(observations, rebasements);
            }
        }
        public PathFlows<double, CouponFlowLabel> NewPathFlow()
        {
            return new PathFlows<double, CouponFlowLabel>(new double[labels.Length], labels);
        }
        public int SizeOfPath
        {
            get { return labels.Length * sizeof(double); }
        }
        public IObservation[][] Fixings { get; private set; }
        public PaymentInfo[][] Payments { get; private set; }
    }

}