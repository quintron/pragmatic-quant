using PragmaticQuant.Basic;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.MonteCarlo.Product
{
    internal class CouponArrayPathFlow : IProductPathFlow
    {
        #region private fields
        private readonly CouponPathFlow[] couponPathFlows;
        private readonly ObservationPathEvaluator observationsEvaluator;
        private readonly CouponFlowLabel[] labels;
        #endregion
        public CouponArrayPathFlow(CouponPathFlow[] couponPathFlows,
                                   ObservationPathEvaluator observationsEvaluator, 
                                   PaymentInfo[][] payments)
        {
            this.couponPathFlows = couponPathFlows;
            this.observationsEvaluator = observationsEvaluator;
            Fixings = observationsEvaluator.Fixings;
            Payments = payments;
            labels = couponPathFlows.Map(l => l.CouponLabel);
        }

        public void ComputePathFlows(ref PathFlows<double, CouponFlowLabel> pathFlows,
                                     PathFlows<double[], IObservation[]> fixingsPath, 
                                     PathFlows<double[], PaymentInfo[]> flowRebasementPath)
        {
            var observations = new double[observationsEvaluator.Observations.Length];
            observationsEvaluator.FillObservations(ref observations, fixingsPath.Flows);
            double[][] rebasements = flowRebasementPath.Flows;
            double[] couponsFlows = pathFlows.Flows;
            for (int i = 0; i < couponPathFlows.Length; i++)
            {
                couponsFlows[i] = couponPathFlows[i].FlowValue(observations, rebasements);
            }
        }

        public PathFlows<double, CouponFlowLabel> NewPathFlow()
        {
            return new PathFlows<double, CouponFlowLabel>(new double[couponPathFlows.Length], labels);
        }
        public int SizeOfPath
        {
            get { return couponPathFlows.Length * sizeof(double); }
        }

        public IObservation[][] Fixings { get; private set; }
        public PaymentInfo[][] Payments { get; private set; }
    }
    
}