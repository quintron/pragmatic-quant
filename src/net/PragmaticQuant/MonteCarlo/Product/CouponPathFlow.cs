namespace PragmaticQuant.MonteCarlo.Product
{
    internal sealed class CouponPathFlow
    {
        #region private fields
        private readonly int payoffIndex;
        private readonly FixingCoord paymentCoordinate;
        #endregion
        public CouponPathFlow(int payoffIndex,
                              CouponFlowLabel couponFlowLabel,
                              FixingCoord paymentCoordinate)
        {
            this.payoffIndex = payoffIndex;
            CouponLabel = couponFlowLabel;
            this.paymentCoordinate = paymentCoordinate;
        }

        public CouponFlowLabel CouponLabel { get; private set; }
        public double FlowValue(double[] simulatedObservables, double[][] rebasements)
        {
            double flowRebasement = rebasements[paymentCoordinate.DateIndex][paymentCoordinate.Index];
            double payoff = simulatedObservables[payoffIndex];
            return payoff * flowRebasement;
        }
    }
    
    internal struct FixingCoord
    {
        public readonly int DateIndex;
        public readonly int Index;
        public FixingCoord(int dateIndex, int index)
        {
            DateIndex = dateIndex;
            Index = index;
        }
    }

}