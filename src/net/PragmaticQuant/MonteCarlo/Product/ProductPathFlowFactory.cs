using System;
using System.Collections.Generic;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Product;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.MonteCarlo.Product
{
    public static class ProductPathFlowFactory
    {
        #region private methods
        private static PaymentInfo[][] PaymentsByDate(IProduct product)
        {
            var allPayments = product.RetrievePaymentInfos();
            var paymentsByDate = allPayments.GroupBy(p => p.Date)
                                            .OrderBy(g=>g.Key)
                                            .ToArray();
            return paymentsByDate.Map(g => g.ToArray());
        }        
        #endregion

        public static IProductPathFlow ProductPathFlow(IProduct product)
        {
            IObservation[] productObservations = product.RetrieveCouponObservations();
            ObservationPathEvaluator observationsEvaluator = ObservationPathEvaluator.Build(productObservations);
            PaymentInfo[][] paymentsByDate = PaymentsByDate(product);
            var pathFlowVisitor = new ProductPathFlowVisitor(observationsEvaluator, paymentsByDate);
            return product.Accept(pathFlowVisitor);
        }        
    }
    
    internal class ProductPathFlowVisitor : IProductVisitor<IProductPathFlow>
    {
        #region private fields
        private readonly ObservationPathEvaluator observationsEvaluator;
        private readonly PaymentInfo[][] simulatedPayments;
        #endregion
        #region private methods
        private static FixingCoord FindPaymentIndex(PaymentInfo[][] payments, PaymentInfo searchedPayment)
        {
            var dateIndex = payments.Map(ps => ps.First().Date)
                                    .FindIndex(searchedPayment.Date);
            var paymentIndex = payments[dateIndex].FindIndex(searchedPayment);
            return new FixingCoord(dateIndex, paymentIndex);
        }
        private static CouponPathFlow[] BuildCouponPathFlow(IObservation[] simulatedObservations, 
                                                            PaymentInfo[][] simulatedPayments,
                                                            params Coupon[] coupons)
        {
            CouponPathFlow[] couponFlows = coupons.Map(cpn =>
            {
                var payoffIndex = simulatedObservations.FindIndex(cpn.Payoff);
                if (payoffIndex < 0)
                    throw new Exception("ProductPathFlowVisitor: coupon payoff is not simulated !");

                FixingCoord paymentCoordinate = FindPaymentIndex(simulatedPayments, cpn.PaymentInfo);
                var couponFlowLabel = new CouponFlowLabel(cpn.PaymentInfo, cpn.CouponId);
                return new CouponPathFlow(payoffIndex, couponFlowLabel, paymentCoordinate);
            });
            return couponFlows;
        }
        private IProductPathFlow LegPathFlow(params Coupon[] coupons)
        {
            var couponFlows = BuildCouponPathFlow(observationsEvaluator.Observations, simulatedPayments, coupons);
            return new CouponArrayPathFlow(couponFlows, observationsEvaluator, simulatedPayments);
        }
        #endregion
        public ProductPathFlowVisitor(ObservationPathEvaluator observationsEvaluator, PaymentInfo[][] simulatedPayments)
        {
            this.simulatedPayments = simulatedPayments;
            this.observationsEvaluator = observationsEvaluator;
        }     

        public IProductPathFlow Visit(Coupon coupon)
        {
            return LegPathFlow(coupon);
        }
        public IProductPathFlow Visit(ICouponDecomposable couponDecomposable)
        {
            return LegPathFlow(couponDecomposable.Decomposition());
        }
        public IProductPathFlow Visit(AutoCall autocall)
        {
            List<Coupon> underlyingsCouponsByPayDate = autocall.Underlying.Decomposition()
                                                                .OrderBy(cpn => cpn.PaymentInfo.Date)
                                                                .ToList();
            CouponPathFlow[] underlyingPathFlows = BuildCouponPathFlow(observationsEvaluator.Observations, 
                                                                       simulatedPayments, 
                                                                       underlyingsCouponsByPayDate.ToArray());

            int[] underlyingCancellationIndexes = autocall.CancellationItems.Map(callItem =>
            {
                var callDate = callItem.Date;
                var effectiveCallDate = callDate + callItem.Notice;
                var idx = underlyingsCouponsByPayDate.FindIndex(cpn => cpn.PaymentInfo.Date >= effectiveCallDate);
                if (idx == -1)
                    return underlyingsCouponsByPayDate.Count;
                return idx;
            });
            
            var triggerPathEvals = autocall.CancellationItems.Map(callItem =>
            {
                var trigger = ((TriggerItem)callItem).Trigger;
                return observationsEvaluator.Observations.FindIndex(trigger);
            });

            var callFeePathFlows = autocall.CancellationItems.Map(
                callItem => BuildCouponPathFlow(observationsEvaluator.Observations, simulatedPayments, callItem.CancelFee).Single());

            return new AutocallPathFlow(underlyingPathFlows,
                                        callFeePathFlows,
                                        observationsEvaluator,
                                        triggerPathEvals,
                                        underlyingCancellationIndexes,
                                        simulatedPayments);
        }
        public IProductPathFlow Visit(Callable callable)
        {
            throw new Exception("ProductPathFlowVisitor : Callable should not be evaluated directly");
        }
    }

    public class ObservationPathEvaluator
    {
        #region private fields
        private readonly int nbAllObservables;
        private readonly IFixingFunction[] funcObservables;
        private readonly int[][] funcInputIndexes;
        private readonly int[] outputIndexes;
        #endregion
        #region private methods
        private ObservationPathEvaluator(IObservation[][] fixings,
                                         IFixingFunction[] funcObservables,
                                         int[][] funcInputIndexes,
                                         IObservation[] observations,
                                         int[] outputIndexes)
        {
            Fixings = fixings;
            Observations = observations;
            this.outputIndexes = outputIndexes;
            this.funcObservables = funcObservables;
            this.funcInputIndexes = funcInputIndexes;            
            nbAllObservables = funcObservables.Length + fixings.Select(f => f.Length).Sum();
        }
        #endregion
        #region private static methods
        private static void AddObservation(IObservation obs, Dictionary<DateTime, List<IObservation>> sortedObservationByDates)
        {
            var fixingFunction = obs as IFixingFunction;
            if (fixingFunction != null)
            {
                foreach (var inner in fixingFunction.Observations)
                    AddObservation(inner, sortedObservationByDates);
            }

            List<IObservation> observationsAtDate;
            if (!sortedObservationByDates.TryGetValue(obs.Date, out observationsAtDate))
            {
                observationsAtDate = new List<IObservation>();
                sortedObservationByDates[obs.Date] = observationsAtDate;
            }
            if (!observationsAtDate.Contains(obs))
                observationsAtDate.Add(obs);
        }
        private static IObservation[] SortedObservations(params IObservation[] observables)
        {
            var sortedObservationByDates = new Dictionary<DateTime, List<IObservation>>();
            foreach (var observation in observables)
                AddObservation(observation, sortedObservationByDates);
            return EnumerableUtils.Append(sortedObservationByDates.OrderBy(kv => kv.Key)
                                                                  .Map(kv => kv.Value.ToArray()));
        }
        #endregion
        public static ObservationPathEvaluator Build(params IObservation[] observables)
        {
            var sortedAllObservations = SortedObservations(observables);
            var fixingByDates = sortedAllObservations.Where(obs => (obs as IFixingFunction) == null)
                                                     .GroupBy(obs => obs.Date)
                                                     .Map(g => g.ToArray());
            var funcs = sortedAllObservations.Where(obs => (obs as IFixingFunction) != null)
                                             .Cast<IFixingFunction>().ToList();
            var allObservations = EnumerableUtils.Append(EnumerableUtils.Append(fixingByDates), funcs);
            var funcInputIndexes = funcs.Map(f => f.Observations.Map(o => allObservations.FindIndex(o)));

            var flatFixings = EnumerableUtils.Append(fixingByDates).ToList();
            var outputIndexes = observables.Map(obs =>
            {
                int fixingIndex = flatFixings.IndexOf(obs);
                if (fixingIndex >= 0)
                    return fixingIndex;

                if (!(obs is IFixingFunction))
                    throw new Exception("BUG, should not get there !");
                int funcIndex = funcs.IndexOf((IFixingFunction)obs);
                if (funcIndex < 0)
                    throw new Exception("BUG, should not get there !");

                return flatFixings.Count + funcIndex;
            });

            return new ObservationPathEvaluator(fixingByDates,
                                                funcs.ToArray(),
                                                funcInputIndexes,
                                                observables, 
                                                outputIndexes);
        }

        public void FillObservations(ref double[] observations, double[][] fixings)
        {
            var allObservations = new double[nbAllObservables];
            int allObsIndex = 0;
            for (int t = 0; t < fixings.Length; t++)
            {
                var f = fixings[t];
                for (int i = 0; i < f.Length; i++)
                    allObservations[allObsIndex++] = f[i];
            }
            for (int i = 0; i < funcObservables.Length; i++)
            {
                var inputIndexes = funcInputIndexes[i];
                var inputBuffer = new double[inputIndexes.Length];
                for (int j = 0; j < inputIndexes.Length; j++)
                    inputBuffer[j] = allObservations[inputIndexes[j]];

                allObservations[allObsIndex++] = funcObservables[i].Value(inputBuffer);
            }

            for (int i = 0; i < observations.Length; i++)
                observations[i] = allObservations[outputIndexes[i]];
        }
        public IObservation[] Observations { get; private set; }
        public IObservation[][] Fixings { get; private set; }        
    }
    
}