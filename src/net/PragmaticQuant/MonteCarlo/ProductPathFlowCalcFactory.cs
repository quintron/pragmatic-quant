﻿using System;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Model;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.MonteCarlo
{
    public static class ProductPathFlowCalcFactory
    {
        #region private methods
        private static Func<double[], double> FlowRebasement(PaymentInfo payment, McModel model)
        {
            PaymentInfo modelMeasure = model.ProbaMeasure;

            if (modelMeasure.Date < payment.Date
                || !modelMeasure.Currency.Equals(payment.Currency)
                || !modelMeasure.Financing.Equals(payment.Financing))
                throw new NotImplementedException("Flow Rebasement not yet handled !"); //TODO finish the job !

            var zc = new Zc(payment.Date, modelMeasure.Date, payment.Currency, payment.Financing);
            var zcFunc = model.FactorRepresentation[zc];
            double num0 = model.Numeraire0;
            return f => num0 / zcFunc.Eval(f);
        }
        private static ArrayPathCalculator<PaymentInfo> NumerairePathCalc(PaymentInfo[][] payments, McModel model)
        {
            var payDates = payments.Map(ps => ps.Map(p => p.Date).Single());
            int[] datesIndexes = payDates.Map(model.PathDates.FindIndex);

            Func<double[], double>[][] funcsByDate = payments.Map(ps => ps.Map(p => FlowRebasement(p, model)));
            return new ArrayPathCalculator<PaymentInfo>(datesIndexes, payments, funcsByDate);
        }
        private static ArrayPathCalculator<IObservation> FixingPathCalc(DateTime[] simulatedDates,
                                                                        IObservation[][] fixings,
                                                                        IFactorModelRepresentation factorRepresentation)
        {
            var fixingDates = fixings.Map(fs => fs.Select(f => f.Date).Distinct().Single());
            var dateIndexes = fixingDates.Map(simulatedDates.FindIndex);
            RnRFunction[][] fixingFromFactors = fixings.Map(fs => fs.Map(f => factorRepresentation[f]));
            var fixingFromFactorFuncs = fixingFromFactors.Map(fs => fs.Map<RnRFunction, Func<double[], double>>(f => f.Eval));
            return new ArrayPathCalculator<IObservation>(dateIndexes, fixings, fixingFromFactorFuncs);
        }
        #endregion
        public static ProductPathFlowCalculator BuildCalculator(IProductPathFlow productPathFlow, McModel model)
        {
            ArrayPathCalculator<IObservation> fixingPathCalc = FixingPathCalc(model.PathDates,
                                                                              productPathFlow.Fixings,
                                                                              model.FactorRepresentation);
            ArrayPathCalculator<PaymentInfo> numerairePathCalc = NumerairePathCalc(productPathFlow.Payments, model);
            return new ProductPathFlowCalculator(fixingPathCalc, numerairePathCalc, productPathFlow);
        }
    }

}