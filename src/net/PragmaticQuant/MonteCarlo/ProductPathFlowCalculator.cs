﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Maths.Stochastic;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.MonteCarlo
{
    public class ProductPathFlowCalculator : IPathFlowCalculator<double, CouponFlowLabel>
    {
        #region private fields
        private readonly IProductPathFlow productPathFlow;
        private readonly IPathFlowCalculator<double[], IObservation[]> fixingPathCalculator;
        private readonly IPathFlowCalculator<double[], PaymentInfo[]> numerairePathCalc;
        private readonly ObjectPool<PoolableFixingPath> fixingPathPool; 
        #endregion
        public ProductPathFlowCalculator(IPathFlowCalculator<double[], IObservation[]> fixingPathCalculator,
                                         IPathFlowCalculator<double[], PaymentInfo[]> numerairePathCalc,
                                         IProductPathFlow productPathFlow)
        {
            this.fixingPathCalculator = fixingPathCalculator;
            this.numerairePathCalc = numerairePathCalc;
            this.productPathFlow = productPathFlow;
            fixingPathPool = new ObjectPool<PoolableFixingPath>(() => new PoolableFixingPath(fixingPathCalculator.NewPathFlow()), 10);
        }

        public void ComputeFlows(ref PathFlows<double, CouponFlowLabel> pathFlows, IProcessPath processPath)
        {
            var buffer = fixingPathPool.GetObject();
            PathFlows<double[], IObservation[]> fixingsPath = buffer.Data;
            fixingPathCalculator.ComputeFlows(ref fixingsPath, processPath);
            
            PathFlows<double[], PaymentInfo[]> numerairePath = numerairePathCalc.NewPathFlow();
            numerairePathCalc.ComputeFlows(ref numerairePath, processPath);

            productPathFlow.ComputePathFlows(ref pathFlows, fixingsPath, numerairePath);

            fixingPathPool.ReturnObject(buffer);
        }
        public PathFlows<double, CouponFlowLabel> NewPathFlow()
        {
            return productPathFlow.NewPathFlow();
        }
        public int SizeOfPath
        {
            get { return productPathFlow.SizeOfPath; }
        }

        #region private class
        private class PoolableFixingPath : IPoolableObject
        {
            public PoolableFixingPath(PathFlows<double[], IObservation[]> data)
            {
                Data = data;
                Size = data.Flows.Select(a => a.Length * sizeof (double)).Sum();
            }
            public PathFlows<double[], IObservation[]> Data { get; private set; }

            public int Size { get; private set; }
            public void Reset()
            {
            }
        }
        #endregion
    }
    
    public interface IProductPathFlow
    {
        void ComputePathFlows(ref PathFlows<double, CouponFlowLabel> pathFlows,
                              PathFlows<double[], IObservation[]> fixingsPath,
                              PathFlows<double[], PaymentInfo[]> flowRebasementPath);

        PathFlows<double, CouponFlowLabel> NewPathFlow();
        int SizeOfPath { get; }

        IObservation[][] Fixings { get; }
        PaymentInfo[][] Payments { get; }
    }

    [DebuggerDisplay("{Label}")]
    public class CouponFlowLabel : IEquatable<CouponFlowLabel>
    {
        public CouponFlowLabel(PaymentInfo payment, string label)
        {
            Payment = payment;
            Label = label;
        }
        public PaymentInfo Payment { get; private set; }
        public string Label { get; private set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((CouponFlowLabel)obj);
        }
        public bool Equals(CouponFlowLabel other)
        {
            return other != null &&
                   EqualityComparer<PaymentInfo>.Default.Equals(Payment, other.Payment) &&
                   Label == other.Label;
        }
        public override int GetHashCode()
        {
            var hashCode = 964494166;
            hashCode = hashCode * -1521134295 + EqualityComparer<PaymentInfo>.Default.GetHashCode(Payment);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Label);
            return hashCode;
        }
    }
  
}