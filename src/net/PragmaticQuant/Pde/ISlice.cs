using System;
using PragmaticQuant.Core;
using PragmaticQuant.Basic;

namespace PragmaticQuant.Pde
{
    public interface ISlice
    {
        Currency Currency { get; }
        DateTime Date { get; set; }

        void SetValue(double val);
        void Add(ISlice other);
        void Mult(ISlice other);
        void Apply(Func<double , double> f);

        ISlice Clone();
    }

    public class Slice<TState, TValue> : ISlice
    {
        public Currency Currency
        {
            get { throw new NotImplementedException(); }
        }
        public DateTime Date
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public ISlice<TState, TValue> SliceData { get; private set; }

        public void SetValue(double val)
        {
            throw new NotImplementedException();
        }
        public void Add(ISlice other)
        {
            SliceData.Add(((Slice<TState, TValue>)other).SliceData.Value);
            throw new NotImplementedException();
        }
        public void Mult(ISlice other)
        {
            throw new NotImplementedException();
        }
        public void Apply(Func<double, double> f)
        {
            throw new NotImplementedException();
        }

        public ISlice Clone()
        {
            throw new NotImplementedException();
        }
    }
}