using System;
using System.Collections.Generic;
using PragmaticQuant.Core;
using PragmaticQuant.Basic;
using PragmaticQuant.Markets;
using PragmaticQuant.Model;
using PragmaticQuant.Model.Equity.BlackScholes;

namespace PragmaticQuant.Pde
{
    public class PdeEngineFactory
    {
        #region private fields
        private readonly PdeConfig pdeConfig;
        #endregion
        public PdeEngineFactory(PdeConfig pdeConfig)
        {
            this.pdeConfig = pdeConfig;
        }

        public IPdeEngine Build(IModel model, Market market, DateTime[] simulatedDates)
        {
            PaymentInfo spotNumeraire = new PaymentInfo(model.PivotCurrency, market.RefDate);

            IFactorModelRepresentation factorRepresentation = FactorRepresentationFactory.Instance.Build(model, market, spotNumeraire);

            IPdeModelBuilder pdeModelBuilder = PdeModelBuilders.For(model);
            
            //TODO abstract this code to more general pde
            PdeModel1D pdeModel = pdeModelBuilder.Build(model, market, simulatedDates);
            var stepOperator = new FiniteDiff1D(pdeModel.PdeCoeff, pdeModel.BoundaryCondition);
            var stepSolver = PdeStepSolver.ThetaScheme(stepOperator, pdeConfig.Theta ?? 0.5);
            var engine = new PdeEngine1D(model.Time, pdeConfig.ScheduleBuilder, stepSolver, factorRepresentation, pdeModel.Grid);
            
            return engine;
        }
    }
    

    public interface IPdeModelBuilder
    {
        PdeModel1D Build(IModel model, Market market, DateTime[] simulatedDates);
    }
    
    public class PdeModel1D
    {
        public PdeModel1D(RegularGrid1D grid, BoundaryCondition1D boundaryCondition, PdeCoeffSampler1D<RegularGrid1D> pdeCoeff)
        {
            Grid = grid;
            BoundaryCondition = boundaryCondition;
            PdeCoeff = pdeCoeff;
        }
        public RegularGrid1D Grid { get; private set; }
        public BoundaryCondition1D BoundaryCondition { get; private set; }
        public PdeCoeffSampler1D<RegularGrid1D> PdeCoeff { get; private set; }
    }
    
    public static class PdeModelBuilders
    {
        #region private fields
        private static readonly IDictionary<Type, IPdeModelBuilder> factories = GetFactories();
        #endregion
        #region private methods
        private static IDictionary<Type, IPdeModelBuilder> GetFactories()
        {
            var result = new Dictionary<Type, IPdeModelBuilder>
            {
                //{typeof (Hw1Model), Hw1FactorRepresentationFactory.Instance},
                {typeof (BlackScholesModel), null},
                //{typeof (LocalVolatilityModel), EquityFactorRepresentationFactory.Instance}
            };
            return result;
        }
        #endregion

        public static IPdeModelBuilder For(IModel model)
        {
            IPdeModelBuilder pdeModelBuilder;
            if (factories.TryGetValue(model.GetType(), out pdeModelBuilder))
                return pdeModelBuilder;
            throw new ArgumentException(string.Format("Missing PdeModelBuilder for {0}", model));
        }
    }

    public abstract class PdeModelBuilder<TModel> : IPdeModelBuilder where TModel : IModel
    {
        protected abstract PdeModel1D Build(TModel model, Market market, DateTime[] simulatedDates);
        public PdeModel1D Build(IModel model, Market market, DateTime[] simulatedDates)
        {
            if (!(model is TModel))
                throw new Exception();
            return Build((TModel) model, market, simulatedDates);
        }
    }

}