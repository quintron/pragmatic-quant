﻿using System;
using PragmaticQuant.Core;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Model;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.Pde
{
    public interface IPdeEngine
    {
        ISlice NewSlice(DateTime date);
        void FillObservation(ref ISlice slice, IObservation observation);
        void RollBack(ref ISlice slice, DateTime start, DateTime end);
        
        PaymentInfo SpotNumeraire { get; }
        Price SpotPrice(ISlice slice);
    }
    
    public abstract class PdeEngine<TState, TValue> : IPdeEngine
    {
        #region private fields
        private readonly ISimulationSchedule scheduleBuilder;
        private readonly ITimeMeasure time;
        private readonly IPdeStepSolver<TState, TValue> stepSolver;
        private readonly IFactorModelRepresentation factorRepresentation;
        private readonly TState state;
        #endregion
        #region private & protected methods
        private Slice<TState, TValue> ConvertSlice(ISlice slice)
        {
            return (Slice<TState, TValue>)slice;
        }
        protected PdeEngine(ITimeMeasure time, ISimulationSchedule scheduleBuilder,
                            IPdeStepSolver<TState, TValue> stepSolver,
                            IFactorModelRepresentation factorRepresentation,
                            TState state)
        {
            this.time = time;
            this.stepSolver = stepSolver;
            this.factorRepresentation = factorRepresentation;
            this.state = state;
            this.scheduleBuilder = scheduleBuilder;
        }
        #endregion
        #region abstract methods
        protected abstract TValue Fill(RnRFunction f, TState state);
        protected abstract Slice<TState, TValue> NewSlice(TState state);
        #endregion
        
        public ISlice NewSlice(DateTime date)
        {
            var newSlice = NewSlice(state);
            newSlice.Date = date;
            newSlice.SetValue(0.0);
            return newSlice;
        }
        public void FillObservation(ref ISlice slice, IObservation observation)
        {
            RnRFunction observationFunc = factorRepresentation[observation];
            ISlice<TState, TValue> innerSlice = ConvertSlice(slice).SliceData;
            var observationValue = Fill(observationFunc, innerSlice.State);
            innerSlice.FillValue(observationValue);
        }
        public void RollBack(ref ISlice slice, DateTime start, DateTime end)
        {
            var innerSlice = ConvertSlice(slice).SliceData;
            var steps = scheduleBuilder.Schedule(start, end, time.RefDate, new DateTime[0]);
            for (int i = steps.Length - 1; i > 0; i--)
            {
                stepSolver.Backward(ref innerSlice, time[steps[i - 1]], time[steps[i]]);
            }
            stepSolver.Backward(ref innerSlice, time[start], time[steps[0]]);

            slice.Date = start;
        }
        
        public PaymentInfo SpotNumeraire { get; private set; }
        public Price SpotPrice(ISlice slice)
        {
            if (slice.Date != SpotNumeraire.Date)
                throw new Exception("Slice date should be equal to Spot Numeraire date");
            throw new NotImplementedException();
        }
    }

    public class PdeEngine1D : PdeEngine<RegularGrid1D, double[]>
    {
        protected override double[] Fill(RnRFunction f, RegularGrid1D state)
        {
            if (f.Dim != 1)
                throw new Exception("PdeEngine1D : only Dim = 1 function allowed !");

            return state.ToArray().Map(x => f.Eval(new[] {x}));
        }
        protected override Slice<RegularGrid1D, double[]> NewSlice(RegularGrid1D state)
        {
            return new Slice<RegularGrid1D, double[]>();
        }
        public PdeEngine1D(ITimeMeasure time, ISimulationSchedule scheduleBuilder, 
                           IPdeStepSolver<RegularGrid1D, double[]> stepSolver, 
                           IFactorModelRepresentation factorRepresentation,
                           RegularGrid1D grid)
            : base(time, scheduleBuilder, stepSolver, factorRepresentation, grid)
        {
        }
    }

}
