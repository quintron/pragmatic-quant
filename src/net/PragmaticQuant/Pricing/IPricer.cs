using PragmaticQuant.Markets;
using PragmaticQuant.Model;
using PragmaticQuant.Product;

namespace PragmaticQuant.Pricing
{
    public interface IPricer
    {
        IPricingResult Price(IProduct product, IModel model, Market market);
    }
}