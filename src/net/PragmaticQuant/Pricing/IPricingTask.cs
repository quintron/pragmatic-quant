using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PragmaticQuant.Basic;
using PragmaticQuant.Markets.Scenarios;
using PragmaticQuant.Markets;
using PragmaticQuant.Model;
using PragmaticQuant.Product;

namespace PragmaticQuant.Pricing
{

    public interface IPricingTask<out T>
    {
        T Price(PriceableConfig priceableConfig);
    }

    public class PriceComputationTask : IPricingTask<IPricingResult>
    {
        #region private fields
        private static IPricer BuildPricer(INumericalMethodConfig algorithm)
        {
            var mcConfig = algorithm as MonteCarloConfig;
            if (mcConfig != null)
            {
                return McPricers.McWithDetails(mcConfig);
            }
            
            throw new Exception("Unknown numerical method !");
        }
        private static IModel CalibrateModel(Market market, ICalibrationDescription modelCalibDesc, IProduct product)
        {
            return ComputationRunnerUtils.RunWithLog("Model calibration", () =>
            {
                IModelDescription modelDesc = ModelCalibration.Instance.Calibrate(modelCalibDesc, market, product);
                return ModelFactory.Instance.Build(modelDesc, market);
            });
        }
        #endregion

        public IPricingResult Price(PriceableConfig priceableConfig)
        {
            PricingConfig pricingConfig = priceableConfig.PricingConfig;
            Market market = pricingConfig.Market.BuildMarket();
            IPricer pricer = BuildPricer(pricingConfig.NumericalMethod);
            IProduct product = priceableConfig.Product;
            IModel calibratedModel = CalibrateModel(market, pricingConfig.Model, product);
            return ComputationRunnerUtils.RunWithLog("Pricing",
                () => pricer.Price(product, calibratedModel, market));
        }
    }

    public class RiskAnalysisTask : IPricingTask<IPricingResult>
    {
        #region private fields
        private readonly RiskProfilTask riskProfilTask;
        #endregion
        public RiskAnalysisTask(IRiskAnalysis riskAnalysis) : this(riskAnalysis, true) { }
        public RiskAnalysisTask(IRiskAnalysis riskAnalysis, bool parallelComputation)
        {
            riskProfilTask = new RiskProfilTask(riskAnalysis, new[] {MarketScenario.Identity()}, parallelComputation);
        }
        public RiskAnalysisTask(IRiskAnalysis[] riskAnalysis) : this(riskAnalysis, true) { }
        public RiskAnalysisTask(IRiskAnalysis[] riskAnalysis, bool parallelComputation)
                        : this(new MultipleRisks(riskAnalysis), parallelComputation) { }
        
        public IPricingResult Price(PriceableConfig priceableConfig)
        {
            var riskProfilResult = riskProfilTask.Price(priceableConfig);
            RiskProfilDetails riskProfil = riskProfilResult.GetDetails<RiskProfilDetails>();
            var arPricingResult = new PricingResult(riskProfil.Prices.Single());
            arPricingResult.AddDetail(riskProfil.RiskResults.Single());
            return arPricingResult;
        }
    }

    public class RiskProfilTask : IPricingTask<IPricingResult>
    {
        #region private fields
        private readonly IPricingTask<IPricingResult> priceComputation;
        private readonly IRiskAnalysis riskAnalysis;
        private readonly MarketScenario[] mktScenarios;
        private readonly bool parallelComputation;
        #endregion
        #region private methods
        private IEnumerable<PriceableConfig> ShockedConfigs(PriceableConfig priceableConfig)
        {
            var pricingConfig = priceableConfig.PricingConfig;
            var initialScenario = pricingConfig.Market.MarketScenario ?? MarketScenario.Identity(); 
            foreach (var mktScenario in mktScenarios)
            {
                var baseScenarioConfig = priceableConfig.Clone();
                baseScenarioConfig.PricingConfig.Market.MarketScenario = initialScenario.CompoundLeft(mktScenario);
                yield return baseScenarioConfig;

                foreach (var mktShock in riskAnalysis.MarketShocks)
                {
                    var shockScenarioConfig = priceableConfig.Clone();
                    shockScenarioConfig.PricingConfig.Market.MarketScenario = initialScenario.CompoundLeft(mktScenario).CompoundLeft(mktShock);
                    yield return shockScenarioConfig;
                }
            }
        }
        private Price[] ComputeAllPrices(PriceableConfig[] shockedConfigs)
        {
            if (parallelComputation)
            {
                Price[] allPrices = new Price[shockedConfigs.Length];
                Parallel.For(0, shockedConfigs.Length, i =>
                {
                    var indexClosure = i;
                    var config = shockedConfigs[indexClosure];
                    
                    //Force each monte-carlo pricing on single thread
                    var mcConfig = config.PricingConfig.NumericalMethod as MonteCarloConfig;
                    if (mcConfig != null)
                    {  
                        var singleThreadedMc = mcConfig.ForceSingleThread() ;
                        config.PricingConfig.NumericalMethod = singleThreadedMc;
                    }

                    allPrices[indexClosure] = priceComputation.Price(config).Price;
                });
                return allPrices;
            }
            return EnumerableUtils.For(0, shockedConfigs.Length, i => priceComputation.Price(shockedConfigs[i]).Price);
        }
        #endregion
        public RiskProfilTask(IRiskAnalysis riskAnalysis,
                              MarketScenario[] mktScenarios,
                              bool parallelComputation = true)
        {
            priceComputation = new PriceComputationTask();
            this.riskAnalysis = riskAnalysis;
            this.mktScenarios = mktScenarios;
            this.parallelComputation = parallelComputation;
        }
        public RiskProfilTask(IRiskAnalysis[] riskAnalysis,
                              MarketScenario[] mktScenarios,
                              bool parallelComputation = true)
            : this(new MultipleRisks(riskAnalysis), mktScenarios, parallelComputation) { }

        public IPricingResult Price(PriceableConfig priceableConfig)
        {
            PriceableConfig[] shockedConfigs = ShockedConfigs(priceableConfig).ToArray();
            Price[] allPrices = ComputeAllPrices(shockedConfigs);
            
            var nbPriceByScenario = riskAnalysis.MarketShocks.Length + 1;
            Tuple<Price,RiskAnalysisDetails>[] riskResults = EnumerableUtils.For(0, mktScenarios.Count(), i =>
            {
                Price[] scenarioPrices = allPrices.Skip(i * nbPriceByScenario).Take(nbPriceByScenario).ToArray();
                Price centeredPrice = scenarioPrices.First();
                double[] shockedPrices = scenarioPrices.Skip(1).Map(p => p.Value);
                double[] risks = riskAnalysis.ComputeRisks(centeredPrice.Value, shockedPrices);

                var riskDetails= new RiskAnalysisDetails(riskAnalysis.RiskNames.ZipWith(risks, (name, value) => new Tuple<string, double>(name, value)));
                return Tuple.Create(centeredPrice, riskDetails);
            });

            var riskProfilDetails = new RiskProfilDetails(mktScenarios.Map(sc => sc.Name), 
                                                          riskResults.Map(t => t.Item2),
                                                          riskResults.Map(t => t.Item1));
            var pricingResult = new PricingResult(null);
            pricingResult.AddDetail(riskProfilDetails);
            return pricingResult;
        }
    }

}