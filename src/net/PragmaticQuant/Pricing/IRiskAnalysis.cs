using System;
using System.Linq;
using System.Collections.Generic;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets.Scenarios;

namespace PragmaticQuant.Pricing
{
    public static class RiskAnalysis
    {
        #region private methods
        private static IRiskAnalysis Vega(string[] riskArgs)
        {
            if (riskArgs.Length == 0)
                throw new ArgumentException("Vega : one argument expected !");

            //TODO handle non equity case...
            var asset = riskArgs[0];
            if (riskArgs.Length==1)
                return Equity.Vega(asset);

            var buckets = riskArgs.Skip(1).Map(DateAndDurationConverter.ConvertDateOrDuration);
            return Equity.VegaWave(asset, buckets);
        }
        private static IRiskAnalysis Delta(string[] riskArgs)
        {
            if (riskArgs.Length != 1)
                throw new ArgumentException("Delta : one argument expected !");
            string underlying = riskArgs[0];

            Currency currency;
            bool isRateDelta = Currency.TryParse(underlying, out currency);
            if (isRateDelta)
            {
                return Rate.Delta(currency.ToString());
            }
            return Equity.Delta(underlying);
        }
        private static IRiskAnalysis DeltaRepo(string[] riskArgs)
        {
            if (riskArgs.Length != 1)
                throw new ArgumentException("Repo : one argument expected !");
            
            string underlying = riskArgs[0];
            return Equity.DeltaRepo(underlying);
        }
        private static IRiskAnalysis ForwarRateBucketedDelta(string[] riskArgs)
        {
            if (riskArgs.Length < 2)
                throw new Exception("At least two arguments expected for rate forward delta ");

            var currency = riskArgs[0].Trim();
            var buckets = riskArgs.Skip(1).Map(DateAndDurationConverter.ConvertDateOrDuration);

            return Rate.ForwarRateBucketedDelta(currency, buckets);
        }
        private static IRiskAnalysis ForwarRepoBucketedDelta(string[] riskArgs)
        {
            if (riskArgs.Length < 2)
                throw new Exception("At least two arguments expected for rate forward repo delta ");

            var underlying = riskArgs[0].Trim();
            var buckets = riskArgs.Skip(1).Map(DateAndDurationConverter.ConvertDateOrDuration);
            return Equity.ForwarRepoBucketedDelta(underlying, buckets);
        }
        #endregion
        public static IRiskAnalysis Parse(string riskdesc)
        {
            var bracketSplit = riskdesc.Trim().Split('(', ')');
            if (bracketSplit.Length != 3)
                throw new Exception(string.Format("Invalid risk description {0}", riskdesc));
            var riskType = bracketSplit[0].Trim().ToLowerInvariant();
            var riskArgs = bracketSplit[1].Split(';').Map(a => a.Trim().ToLowerInvariant());

            switch (riskType)
            {
                case "vega":
                    return Vega(riskArgs);

                case "delta":
                    return Delta(riskArgs);

                case "deltarepo":
                    return DeltaRepo(riskArgs);

                case "forwardratedelta":
                    return ForwarRateBucketedDelta(riskArgs);

                case "forwardrepodelta":
                    return ForwarRepoBucketedDelta(riskArgs);

                default:
                    throw new Exception("Unkown risk : " + riskdesc);
            }
        }

        public static class Rate
        {
            private const double RateDeltaShock = 0.005;
            public static IRiskAnalysis Delta(string asset)
            {
                return Delta(asset, RateDeltaShock);
            }
            public static IRiskAnalysis Delta(string currency, double rateShock)
            {
                var spotShift = ParametricScenarios.RateShift(currency);
                var riskName = string.Format("Delta({0})", currency);
                return new SimpleSensi(spotShift, riskName, rateShock);
            }
            public static IRiskAnalysis ForwarRateBucketedDelta(string asset, DateOrDuration[] buckets)
            {
                return ForwarRateBucketedDelta(asset, buckets, RateDeltaShock);
            }
            public static IRiskAnalysis ForwarRateBucketedDelta(string currency, DateOrDuration[] buckets, double rateShock)
            {
                return new MultipleRisks(EnumerableUtils.For(0, buckets.Length - 1, i =>
                {
                    var bucketShift = ParametricScenarios.ForwardRateBucketShift(currency, buckets[i], buckets[i + 1]);
                    var riskName = string.Format("ForwardRateDelta({0}, {1}, {2})", currency, buckets[i], buckets[i + 1]);
                    return new SimpleSensi(bucketShift, riskName, rateShock);
                }));
            }

            private const double RateVegaShock = 0.0004;
            public static IRiskAnalysis Vega(string currency)
            {
                return Vega(currency, RateVegaShock);
            }
            public static IRiskAnalysis Vega(string currency, double volShock)
            {
                var volShift = ParametricScenarios.RateVolShift(currency);
                var riskName = string.Format("Vega({0})", currency);
                return new SimpleSensi(volShift, riskName, volShock, true);
            }
        }

        public static class Equity
        {
            public const double VegaVolShock = 0.03;
            public static IRiskAnalysis Vega(string asset)
            {
                return Vega(asset, VegaVolShock);
            }
            public static IRiskAnalysis Vega(string asset, double volShock)
            {
                var volShift = ParametricScenarios.EquityVolShift(asset);
                var riskName = string.Format("Vega({0})", asset);
                return new SimpleSensi(volShift, riskName, volShock);
            }
            public static IRiskAnalysis VegaWave(string asset, params DateOrDuration[] buckets)
            {
                return VegaWave(asset, VegaVolShock, buckets);
            }
            public static IRiskAnalysis VegaWave(string asset, double volShock, params DateOrDuration[] buckets)
            {
                var waves = buckets.Map(b => ParametricScenarios.EquityVolWaveShift(asset, b));
                var names = new string[buckets.Length];
                for (int i = 0; i < buckets.Length; i++)
                {
                    var bucketName = (i == buckets.Length - 1)
                        ? string.Format("[{0}, Infinity]", buckets[i])
                        : string.Format("[{0}, {1}]", buckets[i], buckets[i + 1]);

                    names[i] = string.Format("Vega({0}, {1})", asset, bucketName);
                }
                return new WaveSensi(waves, names, volShock);
            }

            const double DeltaSpotShock = 0.01;
            public static IRiskAnalysis Delta(string asset)
            {
                return Delta(asset, DeltaSpotShock);
            }
            public static IRiskAnalysis Delta(string asset, double spotShock)
            {
                var spotShift = ParametricScenarios.EquitySpotShift(asset, true);//Sticky strike delta
                var riskName = string.Format("Delta({0})", asset);
                return new SimpleSensi(spotShift, riskName, spotShock, true);
            }

            private const double RepoDeltaShock = 0.005;
            public static IRiskAnalysis DeltaRepo(string asset)
            {
                return DeltaRepo(asset, RepoDeltaShock);
            }
            public static IRiskAnalysis DeltaRepo(string asset, double repoShock)
            {
                IParametricMktScenario repoShift = ParametricScenarios.EquityRepoShift(asset);
                var riskName = string.Format("DeltaRepo({0})", asset);
                var repoDelta = new SimpleSensi(repoShift, riskName, repoShock);
                return repoDelta;
            }
            public static IRiskAnalysis ForwarRepoBucketedDelta(string asset, DateOrDuration[] buckets)
            {
                return ForwarRepoBucketedDelta(asset, buckets, RepoDeltaShock);
            }
            public static IRiskAnalysis ForwarRepoBucketedDelta(string asset, DateOrDuration[] buckets, double repoShock)
            {
                return new MultipleRisks(EnumerableUtils.For(0, buckets.Length - 1, i =>
                {
                    var bucketShift = ParametricScenarios.EquityForwardRepoBucketShift(asset, buckets[i], buckets[i + 1]);
                    var riskName = string.Format("ForwardRepoDelta({0}, {1}, {2})", asset, buckets[i], buckets[i + 1]);
                    return new SimpleSensi(bucketShift, riskName, repoShock);
                }));
            }
        }
    }

    public interface IRiskAnalysis
    {
        MarketScenario[] MarketShocks { get; }
        double[] ComputeRisks(double centeredPrice, double[] shockedPrices);
        string[] RiskNames { get; }
    }
    
    public class SimpleSensi : IRiskAnalysis
    {
        #region private fields
        private readonly string name;
        private readonly double shock;
        private readonly bool centeredSensi;
        private readonly MarketScenario positiveScenario;
        private readonly MarketScenario negativeScenario;
        #endregion
        public SimpleSensi(IParametricMktScenario marketDeformation, string name, double shock, bool centeredSensi=false)
        {
            this.name = name;
            this.shock = shock;
            this.centeredSensi = centeredSensi;
            positiveScenario = marketDeformation.Scenario(shock);
            if (centeredSensi)
                negativeScenario = marketDeformation.Scenario(-shock);
        }

        public MarketScenario[] MarketShocks
        {
            get
            {
                if (centeredSensi)
                    return new[] {negativeScenario, positiveScenario};
                return new[] {positiveScenario};
            }
        }
        public double[] ComputeRisks(double centeredPrice, double[] shockedPrices)
        {
            if (shockedPrices.Length != MarketShocks.Length)
                throw new Exception("");
            if (centeredSensi)
                return new[] {(shockedPrices[1] - shockedPrices[0]) / (2.0 * shock)};
            return new[] { (shockedPrices[0] - centeredPrice) / shock };
        }
        public string[] RiskNames
        {
            get { return new[] { name }; }
        }
        public override string ToString()
        {
            return name;
        }
    }

    public class MultipleRisks : IRiskAnalysis
    {
        #region private fields
        private readonly IRiskAnalysis[] risks;
        #endregion
        public MultipleRisks(IRiskAnalysis[] risks)
        {
            this.risks = risks;
        }

        public MarketScenario[] MarketShocks
        {
            get { return EnumerableUtils.Append(risks.Map(r => r.MarketShocks)); }
        }
        public double[] ComputeRisks(double centeredPrice, double[] shockedPrices)
        {
            var riskResults = new List<double[]>();
            int currentIndex = 0;
            for (int i = 0; i < risks.Length; i++)
            {
                var risk = risks[i];
                double[] riskResult = risk.ComputeRisks(centeredPrice, shockedPrices.SubArray(currentIndex, risk.MarketShocks.Length));
                riskResults.Add(riskResult);
                currentIndex += risk.MarketShocks.Length;
            }
            return EnumerableUtils.Append(riskResults.ToArray());
        }
        public string[] RiskNames
        {
            get { return EnumerableUtils.Append(risks.Map(r => r.RiskNames)); }
        }
        public override string ToString()
        {
            return "(" + string.Join(",", RiskNames) + ")";
        }
    }

    public class WaveSensi : IRiskAnalysis
    {
        #region private fields
        private readonly IParametricMktScenario[] waves;
        private readonly string[] names;
        private readonly double shock;
        #endregion 
        public WaveSensi(IParametricMktScenario[] waves, string[] names, double shock)
        {
            if (waves.Length != names.Length)
                throw new Exception("WaveSensi : incompatible size");

            this.waves = waves;
            this.names = names;
            this.shock = shock;
        }

        public MarketScenario[] MarketShocks
        {
            get { return waves.Map(w => w.Scenario(shock)); }
        }
        public double[] ComputeRisks(double centeredPrice, double[] shockedPrices)
        {
            var result = new double[waves.Length];
            for (int i = 0; i < shockedPrices.Length; i++)
            {
                var diracShock = shockedPrices[i];
                diracShock -= (i < shockedPrices.Length - 1) ? shockedPrices[i + 1] : centeredPrice;
                result[i] = diracShock / shock;
            }
            return result;
        }
        public string[] RiskNames
        {
            get {return names;}
        }
        public override string ToString()
        {
            return "(" + string.Join(",", RiskNames) + ")";
        }
    }

}