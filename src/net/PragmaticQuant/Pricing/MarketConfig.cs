using PragmaticQuant.Basic;
using PragmaticQuant.Markets;
using PragmaticQuant.Markets.RawDatas;
using PragmaticQuant.Markets.Scenarios;

namespace PragmaticQuant.Pricing
{
    public class MarketConfig : ICloneable<MarketConfig>
    {
        public MarketDatas MarketDatas { get; set; }
        public MarketScenario MarketScenario { get; set; }
        public MarketConfig Clone()
        {
            return new MarketConfig
            {
                MarketDatas = MarketDatas.Clone(),
                MarketScenario = MarketScenario
            };
        }
    }

    public static class MarketConfigUtils
    {
        public static Market BuildMarket(this MarketConfig mktConfig)
        {
            var scenario = mktConfig.MarketScenario ?? MarketScenario.Identity();
            return scenario.BuildMarket(mktConfig.MarketDatas, MarketDatasInterpolation.ToMarket);
        }
    }

}