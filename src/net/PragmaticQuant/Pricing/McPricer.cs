using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using PragmaticQuant.Basic;
using PragmaticQuant.Markets;
using PragmaticQuant.Model;
using PragmaticQuant.MonteCarlo;
using PragmaticQuant.MonteCarlo.Bmc;
using PragmaticQuant.MonteCarlo.Product;
using PragmaticQuant.Product;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.Pricing
{
    using CouponPathFlows = PathFlows<double, CouponFlowLabel>;

    public static class McPricers
    {
        public static McPricer McWithDetails(MonteCarloConfig mcConfig)
        {
            return new McPricer(mcConfig,
                                new McPricingOutputWithDetails(),
                                new PriceFlowsAggregator<CouponFlowLabel>());
        }
    }

    public interface IMcPricingOutput<in T>
    {
        IPricingResult PricingResult(T result, IProduct product, Market market);
    }

    internal class McPricingOutputWithDetails : IMcPricingOutput<CouponPathFlows>
    {
        public IPricingResult PricingResult(CouponPathFlows result, IProduct product, Market market)
        {
            var refCurrency = product.Financing.Currency;
            var priceDetails = new List<Tuple<string, PaymentInfo, Price>>();
            var totalPrice = 0.0;
            for (int i = 0; i < result.Flows.Length; i++)
            {
                CouponFlowLabel couponFlow = result.Labels[i];
                var price = new Price(result.Flows[i], couponFlow.Payment.Currency);
                priceDetails.Add(new Tuple<string, PaymentInfo, Price>(couponFlow.Label, couponFlow.Payment, price));
                totalPrice += price.Convert(refCurrency, market).Value;
            }

            var priceResult = new PricingResult(new Price(totalPrice, refCurrency));
            priceResult.AddDetail(new PriceDetails(priceDetails.ToArray()));
            return priceResult;
        }
    }

    public class McPricer : IPricer
    {
        #region private fields
        private readonly McModelFactory mcModelFactory;
        private readonly MonteCarloConfig mcConfig;
        private readonly IPathResultAgregator<CouponPathFlows, CouponPathFlows> pathAgregator;
        private readonly IMcPricingOutput<CouponPathFlows> pricingOutput;
        #endregion
        #region private methods (mc)        
        private static McEngine<CouponPathFlows, T> McEngine<T>(IProduct product, 
                                                                McModel mcModel,
                                                                IPathResultAgregator<CouponPathFlows, T> pathAgregator,
                                                                bool multithread)
        {
            IProductPathFlow productPathFlow = ProductPathFlowFactory.ProductPathFlow(product);
            var productPathFlowCalculator = ProductPathFlowCalcFactory.BuildCalculator(productPathFlow, mcModel);
            var processPathFlowGen = new ProcessPathFlowGenerator<double, CouponFlowLabel>(mcModel.GaussianFactorPath,
                                                                                           mcModel.ProcessPathGen,
                                                                                           productPathFlowCalculator);
            return new McEngine<CouponPathFlows, T>(mcModel.RandomGenerator, 
                                                    processPathFlowGen, 
                                                    pathAgregator, 
                                                    multithread);
        }
        private McModel BuildMcModel(IProduct product, IModel model, Market market)
        {
            var simulatedDates = product.RetrieveEventDates();

            if (simulatedDates.Min() <= market.RefDate)
                throw new Exception("Past fixing not yet handled in Monte Carlo engine");

            return mcModelFactory.Build(model, market, simulatedDates);
        }
        #endregion
        #region private methods (backward mc)
        private static string CallId(DateTime calldate)
        {
            return "Call@" + calldate.ToShortDateString();
        }
        private static Coupon[] NextHoldCoupons(DateTime callDate, ICancellableProduct cancellable)
        {
            Coupon[] nextHoldCoupons = cancellable.NextCancelledCoupons(callDate);
            return nextHoldCoupons.Map(cpn =>
            {
                var id = CallId(callDate) + "[" + cpn.CouponId + "]";
                return cpn.NewCouponId(id);
            });
        }
        private static Coupon RedemptionCoupon(DateTime callDate, ICancellableProduct cancellable)
        {
            Coupon cancelFee = cancellable.CancellationItem(callDate).CancelFee;
            string redemptionCpnId = CallId(callDate) + "Redemption";
            return cancelFee.NewCouponId(redemptionCpnId);
        }
        private Coupon[] RegressionCoupons(DateTime callDate, IObservation[] regressionVariables, FinancingId payoffFinancing)
        {
            return regressionVariables.Select((variableObs, i) =>
            {
                var id = CallId(callDate) + "RegressionVariable_" + i.ToString(CultureInfo.InvariantCulture);
                var payInfo = new PaymentInfo(payoffFinancing.Currency, callDate, payoffFinancing);
                var payoff = (variableObs as IFixingFunction) ?? new GenericFixingFunction(obsValues => obsValues[0], variableObs);
                return new Coupon(id, payInfo, payoff);
            }).ToArray();
        }
        private ExerciseItem[] ExerciseItems(ICancellableProduct cancellable, Market market)
        {
            var regVariableBuilder = mcConfig.BmcStrategy.RegressionVariablePolicy.GetBuilder(market);
            DateTime[] aliveCallDates = cancellable.CancellationItems.Select(it => it.Date)
                .Where(d => d >= market.RefDate)
                .OrderBy(d => d).ToArray();
            var residualCoupons = new List<Coupon>(aliveCallDates.Length);
            var exerciseItems = new List<ExerciseItem>(aliveCallDates.Length);
            for (int exercice = aliveCallDates.Length - 1; exercice >= 0; exercice--)
            {
                var callDate = aliveCallDates[exercice];
                Coupon[] nextHoldCoupons = NextHoldCoupons(callDate, cancellable);
                residualCoupons.AddRange(nextHoldCoupons);
                if (residualCoupons.Any())
                {
                    Coupon redemptionCoupon = RedemptionCoupon(callDate, cancellable);

                    var residualCpnRegVariables = regVariableBuilder.GetRegressionVariables(callDate, residualCoupons.ToArray());
                    var redemptionCpnRegVariables = regVariableBuilder.GetRegressionVariables(callDate, new[] {redemptionCoupon});
                    var regressionVariables = residualCpnRegVariables.Union(redemptionCpnRegVariables).ToArray();
                    Coupon[] regressionCpns = RegressionCoupons(callDate, regressionVariables, cancellable.Financing);

                    var exerciceItem = new ExerciseItem(callDate,
                                                        nextHoldCoupons,
                                                        redemptionCoupon,
                                                        regressionVariables,
                                                        regressionCpns);
                    exerciseItems.Add(exerciceItem);
                }
            }

            return exerciseItems.OrderBy(item => item.ExerciseDate).ToArray();
        }
        private CouponLinearCombination StrategyLeg(ExerciseItem[] exerciseItems)
        {
            var itemCoupons = new List<Coupon>();
            foreach (ExerciseItem exerciseItem in exerciseItems)
            {
                itemCoupons.AddRange(exerciseItem.NextCancelledCpns);
                itemCoupons.Add(exerciseItem.RedemptionCpn);
                itemCoupons.AddRange(exerciseItem.RegressionVariableCpns);
            }
            return CouponLinearCombination.Create(itemCoupons.ToArray());
        }
        private McEngine<CouponPathFlows, BmcExerciseStrategy> StrategyEngine(ICancellableProduct cancellable, McModel mcModel, Market market)
        {
            ExerciseItem[] exerciseItems = ExerciseItems(cancellable, market);
            var strategyAgregator = new BmcStrategyComputation(exerciseItems, mcConfig.BmcStrategy.StrategyAlgorithm);
            CouponLinearCombination strategyComputationLeg = StrategyLeg(exerciseItems);
            return McEngine(strategyComputationLeg, mcModel, strategyAgregator, mcConfig.Multithread);
        }
        private static IProduct AsTriggerProduct(ICancellableProduct cancellable, BmcExerciseStrategy strategy)
        {
            var exerciceTriggersItems = EnumerableUtils.For(0, strategy.CallDates.Length, i =>
            {
                var callItem = cancellable.CancellationItems[i];
                if (callItem.Date != strategy.CallDates[i])
                    throw new Exception("McPricer : incompatible call date");

                var trigger = strategy.ExerciseTriggers[i];
                return new TriggerItem(callItem.Date, callItem.Notice, callItem.CancelFee, trigger);
            });

            return new AutoCall(cancellable.Underlying, exerciceTriggersItems);
        }
        #endregion
        public McPricer(MonteCarloConfig mcConfig,
                        IMcPricingOutput<CouponPathFlows> pricingOutput,
                        IPathResultAgregator<CouponPathFlows, CouponPathFlows> pathAgregator)
        {
            this.mcConfig = mcConfig;
            this.pricingOutput = pricingOutput;
            this.pathAgregator = pathAgregator;
            mcModelFactory = new McModelFactory(mcConfig);
        }

        public IPricingResult Price(IProduct product, IModel model, Market market)
        {
            McModel mcModel = BuildMcModel(product, model, market);

            var callable = product as Callable;
            if (callable != null)
            {
                //Exercice Strategy Run
                if (mcConfig.BmcStrategy == null)
                    throw new Exception("Bmc pricer : missing LsStrategyNbPaths !");

                var strategyEngine = StrategyEngine(callable, mcModel, market);
                BmcExerciseStrategy strategy = strategyEngine.Run(mcConfig.BmcStrategy.LsStrategyNbPaths);
                product = AsTriggerProduct(callable, strategy);
            }

            //Main Run
            var priceEngine = McEngine(product, mcModel, pathAgregator, mcConfig.Multithread);
            var result = priceEngine.Run(mcConfig.NbPaths);
            return pricingOutput.PricingResult(result, product, market);
        }
    }
}