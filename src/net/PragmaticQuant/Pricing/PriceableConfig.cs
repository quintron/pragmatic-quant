using PragmaticQuant.Basic;
using PragmaticQuant.Model;
using PragmaticQuant.Product;

namespace PragmaticQuant.Pricing
{
    public class PriceableConfig : ICloneable<PriceableConfig>
    {
        public PriceableConfig()
        {
            PricingConfig = new PricingConfig();
        }
        public PricingConfig PricingConfig { get; set; }
        public IProduct Product { get; set; }

        public PriceableConfig Clone()
        {
            return new PriceableConfig
            {
                PricingConfig = PricingConfig.Clone(),
                Product = Product
            };
        }
    }

    public class PricingConfig : ICloneable<PricingConfig>
    {
        public MarketConfig Market { get; set; }
        public ICalibrationDescription Model { get; set; }
        public INumericalMethodConfig NumericalMethod { get; set; }

        public PricingConfig Clone()
        {
            return new PricingConfig
            {
                Market = Market.Clone(),
                Model = Model,
                NumericalMethod = NumericalMethod
            };
        }
    }
    
}