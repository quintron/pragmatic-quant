﻿using System;
using System.Collections.Generic;
using PragmaticQuant.Basic;

namespace PragmaticQuant.Pricing
{
    public interface IPricingResult
    {
        Price Price { get;}
        bool TryGetDetail<TDetail>(out TDetail detail) where TDetail : class;
    }

    public static class PricingResultExtensions
    {
        public static TDetail GetDetails<TDetail>(this IPricingResult pricingResult) where TDetail : class
        {
            TDetail detail;
            if (!pricingResult.TryGetDetail(out detail))
                throw new Exception("PricingResult dot not contain detail of type " + typeof (TDetail));
            return detail;
        }
    }
    
    public class PricingResult : IPricingResult
    {
        #region private fields
        private readonly IDictionary<Type, object> details;
        #endregion
        public PricingResult(Price price)
        {
            Price = price;
            details=  new Dictionary<Type, object>();
        }

        public Price Price { get; private set; }
        public bool TryGetDetail<TDetail>(out TDetail detail) where TDetail : class
        {
            object detailObject;
            if (details.TryGetValue(typeof (TDetail), out detailObject))
            {
                detail = detailObject as TDetail;
                return (detail != null);
            }
            detail = null;
            return false;
        }
        public void AddDetail<TDetail>(TDetail detail) where TDetail : class
        {
            if (details.ContainsKey(typeof(TDetail)))
                throw new Exception("PriceResult : already contain this detail");
            details.Add(typeof (TDetail), detail);
        }
    }
    
    public class PriceDetails
    {
        public PriceDetails(Tuple<string, PaymentInfo, Price>[] details)
        {
            Details = details;
        }
        public Tuple<string, PaymentInfo, Price>[] Details { get; private set; }
    }
    
    public class RiskAnalysisDetails
    {
        public RiskAnalysisDetails(Tuple<string, double>[] riskResults)
        {
            RiskResults = riskResults;
        }
        public Tuple<string, double>[] RiskResults { get; private set; }
    }

    public class RiskProfilDetails
    {
        public RiskProfilDetails(string[] scenarios, RiskAnalysisDetails[] riskResults, Price[] prices)
        {
            Scenarios = scenarios;
            RiskResults = riskResults;
            Prices = prices;
        }
        public string[] Scenarios { get; private set; }
        public Price[] Prices { get; private set; }
        public RiskAnalysisDetails[] RiskResults { get; private set; }
    }

}