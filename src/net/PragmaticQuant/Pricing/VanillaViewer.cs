using System;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets;
using PragmaticQuant.Markets.Equity;
using PragmaticQuant.Product;

namespace PragmaticQuant.Pricing
{
    public static class VanillaViewer
    {
        public static double[,] EquitySmile(PricingConfig pricingConfig, string asset, DateOrDuration[] maturities, double[] strikes)
        {
            Market market = pricingConfig.Market.BuildMarket();
            AssetId assetId = market.AssetIdFromName(asset);
            AssetMarket assetMkt = market.AssetMarket(assetId);

            double spot = assetMkt.Spot;
            var maturityDates = maturities.Map(o => o.ToDate(market.RefDate));
            var options = (from date in maturityDates
                            from strike in strikes
                            let optionType = (strike < spot) ? OptionType.Put : OptionType.Call
                            select CouponDescriptions.Equity.VanillaOption(assetId, date, assetMkt.AssetCurrency, optionType, strike)
                            ).ToArray();

            var optionPriceableConfig = new PriceableConfig
            {
                PricingConfig = pricingConfig,
                Product = CouponLinearCombination.Create(options.Map(op => op.GetCoupon))
            };
            IPricingResult priceResult = new PriceComputationTask().Price(optionPriceableConfig);
            PriceDetails priceDetails = priceResult.GetDetails<PriceDetails>();

            var vanillaPricer = EquityVanillaPricer.Build(assetId, market);
            var optionVols = ArrayUtils.CartesianProd(maturityDates, strikes, (mat, k) =>
            {
                EquityOption option = options.Single(op => op.Maturity == mat && DoubleUtils.MachineEquality(k, op.Strike));
                Price price = priceDetails.Details.Single(det => det.Item1.Equals(option.Id, StringComparison.InvariantCultureIgnoreCase)).Item3;
                return vanillaPricer.ImpliedVol(mat, k, option.OptionType, price.Value);
            });
            return optionVols;
        }
    }
}