using System;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.Product
{
    public class Coupon : ICouponDecomposable
    {
        public Coupon(string couponId, PaymentInfo paymentInfo, IFixingFunction payoff)
        {
            CouponId = couponId;
            PaymentInfo = paymentInfo;
            Payoff = payoff;

            if (payoff.Observations.Any(f => f.Date > paymentInfo.Date))
                throw new Exception("Coupon : all fixings must be previous to payment date !");
        }
        public Coupon(string couponId, Currency payCurrency, DateTime payDate, IFixingFunction payoff)
            : this(couponId, new PaymentInfo(payCurrency, payDate), payoff) { }

        public string CouponId { get; private set; }
        public IFixingFunction Payoff { get; private set; }
        public PaymentInfo PaymentInfo { get; private set;}
        public FinancingId Financing
        {
            get { return PaymentInfo.Financing; }
        }

        public TResult Accept<TResult>(IProductVisitor<TResult> visitor)
        {
            return visitor.Visit(this);
        }
        public Coupon[] Decomposition()
        {
            return new[] {this};
        }
        public Coupon NewCouponId(string newId)
        {
            return new Coupon(newId, PaymentInfo, Payoff);
        }

        public override string ToString()
        {
            return string.Format("Coupon : Cur = {0}, Date = {1}, Financing = {2}", PaymentInfo.Currency, PaymentInfo.Date, PaymentInfo.Financing);
        }
    }
    
}