using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.Product
{
    [DebuggerDisplay("{Id}")]
    public class EquityOption : ICouponDescription
    {
        public EquityOption(AssetId asset, DateTime maturity, Currency payCurrency, OptionType optionType, double strike)
        {
            Asset = asset;
            Maturity = maturity;
            PayCurrency = payCurrency;
            Strike = strike;
            OptionType = optionType;
            Id = string.Format("{1} {0}@{2} {3}", Asset.Name, OptionType, Strike.ToString(CultureInfo.InvariantCulture), Maturity.ToShortDateString());
        }
        
        public DateTime Maturity { get; private set; }
        public Currency PayCurrency { get; private set; }
        public double Strike { get; private set; }
        public OptionType OptionType { get; private set; }
        public AssetId Asset { get; private set; }
        public string Id { get; private set; }

        public Coupon GetCoupon
        {
            get
            {
                var payoff = CapFloorPayoff.CreateOption(new EquitySpot(Maturity, Asset), OptionType, Strike);
                return new Coupon(Id, new PaymentInfo(PayCurrency, Maturity), payoff);
            }
        }
    }

    [DebuggerDisplay("{Id}")]
    public class LogSwap : ICouponDescription
    {
        #region private fields
        private readonly double strike;
        #endregion
        public LogSwap(AssetId asset, DateTime maturity, Currency payCurrency, double strike)
        {
            this.strike = strike;
            Asset = asset;
            Maturity = maturity;
            PayCurrency = payCurrency;
            Id = string.Format("LogSwap {0}@{1}", Asset.Name, Maturity.ToShortDateString());
        }
        
        public DateTime Maturity { get; private set; }
        public Currency PayCurrency { get; private set; }
        public AssetId Asset { get; private set; }
        public string Id { get; private set; }

        public Coupon GetCoupon
        {
            get
            {
                var logPayoff = new GenericFixingFunction(spot => -2.0 * Math.Log(spot[0] / strike) + 2.0 * (spot[0] / strike - 1.0), new EquitySpot(Maturity, Asset));
                return new Coupon(Id, new PaymentInfo(PayCurrency, Maturity), logPayoff);
            }
        }
    }

    [DebuggerDisplay("{Id}")]
    public class CliquetAccumulator : ICouponDescription
    {
        public CliquetAccumulator(AssetId asset, DateTime payDate, Currency payCurrency, 
            double globalStrike, double localCap, bool multiplicativeCliquet, params DateTime[] constatDates)
        {
            Asset = asset;
            PayDate = payDate;
            PayCurrency = payCurrency;
            GlobalStrike = globalStrike;
            LocalCap = localCap;
            MultiplicativeCliquet = multiplicativeCliquet;
            ConstatDates = constatDates.OrderBy(d => d).ToArray();
            Id = string.Format("CliquetAccu {0} on {1} [{2} To {3}]",
                 LocalCap.ToString("P"), Asset.Name, ConstatDates.First().ToShortDateString(), ConstatDates.Last().ToShortDateString());
        }
        public DateTime PayDate { get; private set; }
        public Currency PayCurrency { get; private set; }

        public AssetId Asset { get; private set; }
        public DateTime[] ConstatDates { get; private set; }

        public double LocalCap { get; private set; }
        public double GlobalStrike { get; private set; }
        public bool MultiplicativeCliquet { get; private set; }

        public string Id { get; private set; }

        public Coupon GetCoupon
        {
            get
            {
                var spotProcess = new EquitySpotProcess(Asset);
                var fixings = ConstatDates.Select(d => spotProcess.Fixing(d)).ToArray();
                var accuPerf = MultiplicativeCliquet
                    ? (IFixingFunction) new MultiplicativeCliquetPerf(fixings, LocalCap, double.NegativeInfinity)
                    : new AdditiveCliquetPerf(fixings, LocalCap, double.NegativeInfinity, 1.0);

                var strike = MultiplicativeCliquet ? 1.0 + GlobalStrike : GlobalStrike;
                var payoff = new CapFloorPayoff(accuPerf, double.PositiveInfinity, 0.0, -strike, 1.0);

                return new Coupon(Id, new PaymentInfo(PayCurrency, PayDate), payoff);

            }
        }

    }
    
}