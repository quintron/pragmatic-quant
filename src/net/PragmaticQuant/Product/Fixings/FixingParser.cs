using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;

namespace PragmaticQuant.Product.Fixings
{
    public interface IFixingParser
    {
        IObservationProcess Parse(string fixingDesc);
    }

    public class FixingParser : IFixingParser
    {
        #region private fields
        private const string AssetTemplate = "Asset(name)";
        private const string LiborTemplate = "Libor(tenor, currency)";
        private const string CmsTemplate = "Cms(tenor, currency)";
        private const string VarianceTemplate = "Variance(name, start)";
        private const string MaxTemplate = "Max(name, start, frequency)";
        private const string MinTemplate = "Min(name, start, frequency)";
        #endregion
        #region private methods
        private static void ThrowFixingParsingException(string fixingType, string template, string actual)
        {
            throw new Exception(string.Format("Unable to parse {0} fixing type, should be {1} , but was : {2}",
                fixingType, template, actual));
        }
        private bool TryParseAsset(string[] args, bool logSpot, out IObservationProcess asset)
        {
            if (args.Length != 1)
            {
                asset = null;
                return false;
            }

            AssetId assetId = new AssetId(args[0].Trim());
            asset = ObservationProcesses.Equity.Spot(assetId, logSpot);
            return true;
        }
        private static bool TryParseLibor(string[] args, out IObservationProcess libor)
        {
            Currency currency = null;
            bool validLibor = args.Length == 2 && Currency.TryParse(args[1], out currency);
            if (!validLibor)
            {
                libor = null;
                return false;
            }

            Duration tenor;
            Duration.TryParse(args[0], out tenor);

            libor = ObservationProcesses.Rate.Libor(currency, tenor);
            return true;
        }
        private static bool TryParseCms(string[] args, out IObservationProcess cms)
        {
            Currency currency = null;
            bool validLibor = args.Length == 2 && Currency.TryParse(args[1], out currency);
            if (!validLibor)
            {
                cms = null;
                return false;
            }

            Duration tenor;
            Duration.TryParse(args[0], out tenor);

            cms = ObservationProcesses.Rate.Cms(currency, tenor);
            return true;
        }
        #endregion
        #region private methods (fixing functions)
        private bool TryParseVariance(string[] args, out IObservationProcess variance)
        {
            DateTime start = DateTime.MaxValue;
            IObservationProcess process = null;
            bool validVariance = args.Length == 2
                                 && DateAndDurationConverter.TryConvertDate(args[1], out start)
                                 && TryParseAsset(new[] { args[0] }, true, out process);
            if (!validVariance)
            {
                variance = null;
                return false;
            }

            variance = new VarianceProcess(process, start);
            return true;
        }
        private bool TryParseMax(string[] args, out IObservationProcess max)
        {
            IObservationProcess process = null;
            DateTime start = DateTime.MaxValue;
            Duration frequency = null;
            bool valid = args.Length == 3
                         && DateAndDurationConverter.TryConvertDate(args[1], out start)
                         && Duration.TryParse(args[2], out frequency)
                         && TryParseAsset(new[] {args[0]}, false, out process);
            if (!valid)
            {
                max = null;
                return false;
            }

            max = new MaxProcess(process, start, frequency);
            return true;
        }
        private bool TryParseMin(string[] args, out IObservationProcess min)
        {
            IObservationProcess process = null;
            DateTime start = DateTime.MaxValue;
            Duration frequency = null;
            bool valid = args.Length == 3
                         && DateAndDurationConverter.TryConvertDate(args[1], out start)
                         && Duration.TryParse(args[2], out frequency)
                         && TryParseAsset(new[] {args[0]}, false, out process);
            if (!valid)
            {
                min = null;
                return false;
            }
            
            min = new WorstOfProcess(start, frequency, new[] {process}, new[] {1.0});
            return true;
        }
        #endregion
        
        public IObservationProcess Parse(string fixingDesc)
        {
            var elems = fixingDesc.Trim().Split('(', ')');

            if (elems.Length < 2)
                throw new Exception("Unable to find fixing type Asset, Libor or Cms : " + fixingDesc);

            string fixingType = elems[0];
            string[] args = elems[1].Split(',', ';');

            IObservationProcess fixing;
            bool parsingSucces;
            switch (fixingType.ToLowerInvariant())
            {
                case "asset" :
                    parsingSucces = TryParseAsset(args, false, out fixing);
                    if (!parsingSucces)
                        ThrowFixingParsingException(fixingType, AssetTemplate, fixingDesc);
                    break;
                
                case "libor" :
                    parsingSucces = TryParseLibor(args, out fixing);
                    if (!parsingSucces)
                        ThrowFixingParsingException(fixingType, LiborTemplate, fixingDesc);
                    break;

                case "cms":
                    parsingSucces = TryParseCms(args, out fixing);
                    if (!parsingSucces)
                        ThrowFixingParsingException(fixingType, CmsTemplate, fixingDesc);
                    break;

                case "variance":
                    parsingSucces = TryParseVariance(args, out fixing);
                    if (!parsingSucces)
                        ThrowFixingParsingException(fixingType, VarianceTemplate, fixingDesc);
                    break;

                case "max" :
                    parsingSucces = TryParseMax(args, out fixing);
                    if (!parsingSucces)
                        ThrowFixingParsingException(fixingType, MaxTemplate, fixingDesc);
                    break;

                case "min":
                    parsingSucces = TryParseMin(args, out fixing);
                    if (!parsingSucces)
                        ThrowFixingParsingException(fixingType, MinTemplate, fixingDesc);
                    break;

                default :
                    throw new ArgumentException(string.Format("Unknow fixing type : {0}", fixingType));
            }

            return fixing;
        }
    }
    
}