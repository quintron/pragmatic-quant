using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;

namespace PragmaticQuant.Product.Fixings
{
    public interface IFixing : IObservation
    {
    }

    public class EquitySpot : IFixing
    {
        #region private method
        protected bool Equals(EquitySpot other)
        {
            return Equals(AssetId, other.AssetId) && Date.Equals(other.Date) && LogSpot == other.LogSpot;
        }
        #endregion
        public EquitySpot(DateTime date, AssetId assetId, bool logSpot=false)
        {
            AssetId = assetId;
            LogSpot = logSpot;
            Date = date;
        }

        public AssetId AssetId { get; private set; }
        public DateTime Date { get; private set;}
        public bool LogSpot { get; private set; }
        
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((EquitySpot) obj);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Date.GetHashCode();
                hashCode = (hashCode * 397) ^ (AssetId != null ? AssetId.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (LogSpot.GetHashCode());
                return hashCode;
            }
        }

        public override string ToString()
        {
            return LogSpot
                ? "Equity LogSpot " + AssetId + " @ " + Date
                : "Equity Spot " + AssetId + " @ " + Date;
        }
    }

    public class Libor : IFixing
    {
        #region private fields
        protected bool Equals(Libor other)
        {
            return Date.Equals(other.Date)
                   && Equals(Currency, other.Currency)
                   && Equals(Tenor, other.Tenor);
        }
        #endregion
        public Libor(Currency currency, DateTime fixingDate, Duration tenor)
        {
            Currency = currency;
            Tenor = tenor;
            Date = fixingDate;
        }

        public DateTime Date { get; private set; }
        public Currency Currency { get; private set; }
        public Duration Tenor { get; private set; }
        
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Libor) obj);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Date.GetHashCode();
                hashCode = (hashCode * 397) ^ (Currency != null ? Currency.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Tenor != null ? Tenor.GetHashCode() : 0);
                return hashCode;
            }
        }

        public override string ToString()
        {
            return "Bor " + Currency + " " + Tenor + " @ " + Date.ToShortDateString();
        }
    }
    
    public class Cms : IFixing
    {
        #region private methods
        private bool Equals(Cms other)
        {
            return Date.Equals(other.Date) && Equals(Currency, other.Currency) && Equals(Tenor, other.Tenor);
        }
        #endregion 
        public Cms(Currency currency, DateTime fixingDate, Duration tenor)
        {
            Currency = currency;
            Tenor = tenor;
            Date = fixingDate;
        }

        public DateTime Date { get; private set; }
        public Currency Currency { get; private set; }
        public Duration Tenor { get; private set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Cms) obj);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Date.GetHashCode();
                hashCode = (hashCode * 397) ^ (Currency != null ? Currency.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Tenor != null ? Tenor.GetHashCode() : 0);
                return hashCode;
            }
        }

        public override string ToString()
        {
            return "Cms " + Currency + " " + Tenor + " @ " + Date.ToShortDateString();
        }
    }

    public class ForwardFixing : IObservation
    {
        public ForwardFixing(DateTime date, IFixing underlying)
        {
            Date = date;
            Underlying = underlying;
            if (Underlying.Date < date)
                throw new Exception("Forward fixing date must be previous to underlying observation !"); 
        }
        public DateTime Date { get; private set; }
        public IFixing Underlying { get; private set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ForwardFixing) obj);
        }
        protected bool Equals(ForwardFixing other)
        {
            return Date.Equals(other.Date) && Equals(Underlying, other.Underlying);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                return (Date.GetHashCode() * 397) ^ (Underlying != null ? Underlying.GetHashCode() : 0);
            }
        }

        public override string ToString()
        {
            return "Fwd[" + Underlying + "] @ " + Date;
        }
    }
    
}