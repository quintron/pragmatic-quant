﻿using System;
using System.Linq;
using PragmaticQuant.Basic;

namespace PragmaticQuant.Product.Fixings
{
    public interface IFixingFunction : IObservation
    {
        IObservation[] Observations { get; }
        double Value(double[] fixings);
    }

    public class GenericFixingFunction : IFixingFunction
    {
        #region private fields
        private readonly Func<double[], double> value;
        #endregion
        public GenericFixingFunction(Func<double[], double> value, params IObservation[] fixings)
        {
            Observations = fixings;
            this.value = value;
        }
        public DateTime Date
        {
            get
            {
                return Observations.Length > 0
                     ? Observations.Select(f => f.Date).Max()
                     : DateTime.MinValue;
            }
        }
        public IObservation[] Observations { get; private set; }
        public double Value(double[] fixings)
        {
            return value(fixings);
        }
    }

    public class WeightedFixingFunction : IFixingFunction
    {
        #region private fields
        private readonly double weigth;
        private readonly IFixingFunction fixingFunc;
        #endregion
        public WeightedFixingFunction(double weigth, IFixingFunction fixingFunc)
        {
            this.weigth = weigth;
            this.fixingFunc = fixingFunc;
        }
        public DateTime Date
        {
            get { return fixingFunc.Date; }
        }
        public IObservation[] Observations
        {
            get { return fixingFunc.Observations; }
        }
        public double Value(double[] fixings)
        {
            return weigth * fixingFunc.Value(fixings);
        }
    }
    
    public class CapFloorPayoff : IFixingFunction
    {
        #region private fields
        private readonly IObservation underlying;
        private readonly double cap;
        private readonly double floor;
        private readonly double add;
        private readonly double mult;
        #endregion
        public CapFloorPayoff(IObservation underlying, double cap, double floor, double add, double mult)
        {
            this.underlying = underlying;
            this.cap = cap;
            this.floor = floor;
            this.add = add;
            this.mult = mult;
        }
        public static IFixingFunction CreateOption(IObservation underlying, OptionType optionType, double strike, double leverage = 1.0)
        {
            switch (optionType)
            {
                case OptionType.Call:
                    return new CapFloorPayoff(underlying, double.PositiveInfinity, 0.0, -leverage * strike, leverage);
                
                case OptionType.Put:
                    return new CapFloorPayoff(underlying, double.PositiveInfinity, 0.0, leverage * strike, -leverage);

                case OptionType.Fwd:
                    return new CapFloorPayoff(underlying, double.PositiveInfinity, double.NegativeInfinity, -leverage * strike, leverage);
            }
            throw new Exception("Not handled option type : " + optionType);
        }

        public DateTime Date
        {
            get { return underlying.Date; }
        }
        public IObservation[] Observations
        {
            get { return new[] {underlying}; }
        }
        public double Value(double[] fixings)
        {
            return Math.Max(floor, Math.Min(cap, mult * fixings[0] + add));
        }
    }

}