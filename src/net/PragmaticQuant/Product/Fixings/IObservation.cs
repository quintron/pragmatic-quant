﻿using System;
using System.Diagnostics;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Markets.Rate;

namespace PragmaticQuant.Product.Fixings
{
    public interface IObservation
    {
        DateTime Date { get; }
    }
    
    [DebuggerDisplay("Discount {Currency} {PayDate} @ {Date}")]
    public class Zc : IObservation
    {
        #region private fields
        protected bool Equals(Zc other)
        {
            return Date.Equals(other.Date) && PayDate.Equals(other.PayDate) && Equals(Currency, other.Currency) && Equals(FinancingId, other.FinancingId);
        }
        #endregion
        public Zc(DateTime date, DateTime payDate, Currency currency, FinancingId financingId)
        {
            if (payDate < date)
                throw new Exception("Zc : date observation must be previous to payDate !");

            FinancingId = financingId;
            Currency = currency;
            PayDate = payDate;
            Date = date;
        }
        public Zc(DateTime date, PaymentInfo payInfo)
            : this(date, payInfo.Date, payInfo.Currency, payInfo.Financing) { }

        public DateTime Date { get; private set; }
        public DateTime PayDate { get; private set; }
        public Currency Currency { get; private set; }
        public FinancingId FinancingId { get; private set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Zc)obj);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Date.GetHashCode();
                hashCode = (hashCode * 397) ^ PayDate.GetHashCode();
                hashCode = (hashCode * 397) ^ (Currency != null ? Currency.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (FinancingId != null ? FinancingId.GetHashCode() : 0);
                return hashCode;
            }
        }
    }

    [DebuggerDisplay("ZcRate {Currency} {EndDate} @ {Date}")]
    public class ZcRate : IObservation
    {
        public ZcRate(DateTime date, DateTime endDate, Currency currency)
        {
            Date = date;
            EndDate = endDate;
            Currency = currency;
        }
        public DateTime Date { get; private set; }
        public DateTime EndDate { get; private set; }
        public Currency Currency { get; private set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ZcRate) obj);
        }
        protected bool Equals(ZcRate other)
        {
            return Date.Equals(other.Date) 
                && EndDate.Equals(other.EndDate) 
                && Equals(Currency, other.Currency);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Date.GetHashCode();
                hashCode = (hashCode * 397) ^ EndDate.GetHashCode();
                hashCode = (hashCode * 397) ^ (Currency != null ? Currency.GetHashCode() : 0);
                return hashCode;
            }
        }
    }

    [DebuggerDisplay("SwapRate {Currency} {StartDate}->{EndDate} @ {Date}")]
    public class SwapRate : IObservation
    {
        public SwapRate(DateTime date, DateTime startDate, DateTime endDate, Currency currency, SwapConvention convention)
        {
            if (startDate < date)
                throw new Exception("SwapRate : date should be previous to startDate !");
            if (endDate < startDate)
                throw new Exception("SwapRate : startDate should be previous to endDate !");

            Date = date;
            StartDate = startDate;
            EndDate = endDate;
            Currency = currency;
            Convention = convention;
        }

        public DateTime Date { get; private set; }
        public DateTime StartDate { get; private set; }
        public DateTime EndDate { get; private set; }
        public Currency Currency { get; private set; }
        public SwapConvention Convention { get; private set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((SwapRate)obj);
        }
        protected bool Equals(SwapRate other)
        {
            return Date.Equals(other.Date)
                && StartDate.Equals(other.StartDate)
                && EndDate.Equals(other.EndDate)
                && Equals(Currency, other.Currency)
                && Equals(Convention, other.Convention);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Date.GetHashCode();
                hashCode = (hashCode * 397) ^ StartDate.GetHashCode();
                hashCode = (hashCode * 397) ^ EndDate.GetHashCode();
                hashCode = (hashCode * 397) ^ (Currency != null ? Currency.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Convention != null ? Convention.GetHashCode() : 0);
                return hashCode;
            }
        }
    }

    [DebuggerDisplay("SwapMtm {Strike} {Currency} {StartDate}->{EndDate} @ {Date}")]
    public class SwapMtm : IObservation
    {
        #region private methods
        protected bool Equals(SwapMtm other)
        {
            return Date.Equals(other.Date)
                && Side == other.Side
                && Strike.Equals(other.Strike)
                && StartDate.Equals(other.StartDate)
                && EndDate.Equals(other.EndDate)
                && Equals(Currency, other.Currency)
                && Equals(Convention, other.Convention);
        }
        #endregion
        public SwapMtm(DateTime date, SwapSide side, double strike, DateTime startDate, DateTime endDate, Currency currency, SwapConvention convention)
        {
            Date = date;
            StartDate = startDate;
            EndDate = endDate;
            Currency = currency;
            Convention = convention;
            Side = side;
            Strike = strike;
        }

        public DateTime Date { get; private set; }

        public SwapSide Side { get; private set; }
        public double Strike { get; private set; }

        public DateTime StartDate { get; private set; }
        public DateTime EndDate { get; private set; }
        public Currency Currency { get; private set; }
        public SwapConvention Convention { get; private set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((SwapMtm)obj);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Date.GetHashCode();
                hashCode = (hashCode * 397) ^ (int)Side;
                hashCode = (hashCode * 397) ^ Strike.GetHashCode();
                hashCode = (hashCode * 397) ^ StartDate.GetHashCode();
                hashCode = (hashCode * 397) ^ EndDate.GetHashCode();
                hashCode = (hashCode * 397) ^ (Currency != null ? Currency.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Convention != null ? Convention.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
    
    public class FixedLegMtm : IObservation
    {
        public FixedLegMtm(DateTime date, Currency currency, DateTime[] payDates, double[] amounts)
        {
            if (payDates.Any(p=> p<date))
                throw new Exception("FixedLegMtm : date must be previous to payDates");

            if (payDates.Length!=amounts.Length)
                throw new Exception("FixedLegMtm : incompatible size payDates vs amounts");

            Date = date;
            Currency = currency;
            PayDates = payDates;
            Amounts = amounts;
        }
        public DateTime Date { get; private set; }
        
        public Currency Currency { get; private set; }
        public DateTime[] PayDates { get; private set; }
        public double[] Amounts { get; private set; }
         
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((FixedLegMtm)obj);
        }
        protected bool Equals(FixedLegMtm other)
        {
            return Date.Equals(other.Date) 
                && Equals(Currency, other.Currency) 
                && Equals(PayDates, other.PayDates) 
                && Equals(Amounts, other.Amounts);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Date.GetHashCode();
                hashCode = (hashCode * 397) ^ (Currency != null ? Currency.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (PayDates != null ? PayDates.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Amounts != null ? Amounts.GetHashCode() : 0);
                return hashCode;
            }
        }
    }

    public enum SwapSide
    {
        Payer, Receiver
    }
}