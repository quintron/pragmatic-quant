using System;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;

namespace PragmaticQuant.Product.Fixings
{
    public interface IObservationProcess
    {
        IObservation Observation(DateTime date);
    }

    public static class ObservationProcesses
    {
        public static class Equity
        {
            public static IObservationProcess Spot(AssetId asset, bool logSpot = false)
            {
                return new EquitySpotProcess(asset, logSpot);
            }
            public static IObservationProcess Spot(string name, bool logSpot = false)
            {
                return Spot(new AssetId(name), logSpot);
            }
            public static IObservationProcess ArithmeticBasket(AssetId[] assets, double[] weights, bool averaging = true)
            {
                var underlyings = assets.Map(a => Spot(a));
                return new BasketFixingProcess(underlyings, weights, averaging);
            }
            public static IObservationProcess ArithmeticBasket(string[] assets, double[] weights, bool averaging = true)
            {
                return ArithmeticBasket(assets.Map(a => new AssetId(a)), weights, averaging);
            }
            public static IObservationProcess WorstOf(AssetId[] assets, double[] weights)
            {
                var underlyings = assets.Map(a => Spot(a));
                return new WorstOfProcess(underlyings, weights);
            }
            public static IObservationProcess WorstOf(string[] assets, double[] weights)
            {
                return WorstOf(assets.Map(a => new AssetId(a)), weights);
            }
            public static IObservationProcess Min(AssetId[] assets, double[] weights, DateTime start, Duration frequency)
            {
                var underlyings = assets.Map(a => Spot(a));
                return new WorstOfProcess(start, frequency, underlyings, weights);
            }
            public static IObservationProcess Min(string[] assets, double[] weights, DateTime start, Duration frequency)
            {
                return Min(assets.Map(a => new AssetId(a)), weights, start, frequency);
            }
            public static IObservationProcess Variance(AssetId asset, DateTime start, double varianceAnnualBasis = double.NaN)
            {
                return new VarianceProcess(Spot(asset, true), start, varianceAnnualBasis);
            }
            public static IObservationProcess Variance(string name, DateTime start, double varianceAnnualBasis = double.NaN)
            {
                return Variance(new AssetId(name), start, varianceAnnualBasis);
            }
        }

        public static class Rate
        {
            public static IObservationProcess Libor(Currency currency, Duration tenor)
            {
                return new LiborProcess(currency, tenor);
            }
            public static IObservationProcess Cms(Currency currency, Duration tenor)
            {
                return new CmsProcess(currency, tenor);
            }
        }
    }
    
    public abstract class FixingProcess<TFixing> : IObservationProcess
        where TFixing : IFixing
    {
        public IObservation Observation(DateTime date)
        {
            return Fixing(date);
        }
        public abstract TFixing Fixing(DateTime date);
    }

    public class EquitySpotProcess : FixingProcess<EquitySpot>
    {
        public EquitySpotProcess(AssetId asset, bool logSpot = false)
        {
            Asset = asset;
            LogSpot = logSpot;
        }
        public AssetId Asset { get; private set; }
        public bool LogSpot { get; private set; }
        public override EquitySpot Fixing(DateTime date)
        {
            return new EquitySpot(date, Asset, LogSpot);
        }
    }

    public class LiborProcess : FixingProcess<Libor>
    {
        public LiborProcess(Currency currency, Duration tenor)
        {
            Currency = currency;
            Tenor = tenor;
        }
        public Currency Currency { get; private set; }
        public Duration Tenor { get; private set; }
        public override Libor Fixing(DateTime date)
        {
            return new Libor(Currency, date, Tenor);
        }
    }

    public class CmsProcess : FixingProcess<Cms>
    {
        public CmsProcess(Currency currency, Duration tenor)
        {
            Currency = currency;
            Tenor = tenor;
        }
        public Currency Currency { get; private set; }
        public Duration Tenor { get; private set; }
        public override Cms Fixing(DateTime date)
        {
            return new Cms(Currency, date, Tenor);
        }
    }
}