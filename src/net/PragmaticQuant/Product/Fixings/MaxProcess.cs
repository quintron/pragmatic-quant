using System;
using System.Linq;
using PragmaticQuant.Core;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;

namespace PragmaticQuant.Product.Fixings
{
    public static class ObservationProcessUtils
    {
        public static DateTime[] Schedule(DateTime start, DateTime end, Duration frequency)
        {
            var schedule = ScheduleUtils.RawSchedule(start, end, frequency, true, false);
            schedule = schedule.Where(d => d < end && d > start).ToArray();
            schedule = BusinessConventions.ModifiedFollowing.Adjust(schedule, new SimpleCalendar());
            schedule = schedule.Where(d => d < end && d > start)
                               .Union(new[] { start, end })
                               .Distinct()
                               .OrderBy(d => d)
                               .ToArray();
            return schedule;
        }
    }

    public class MaxProcess : IObservationProcess
    {
        #region private fields
        private readonly IObservationProcess observationProcess;
        private readonly DateTime start;
        private readonly Duration frequency;
        #endregion
        public MaxProcess(IObservationProcess observationProcess, DateTime start, Duration frequency)
        {
            this.observationProcess = observationProcess;
            this.start = start;
            this.frequency = frequency;
        }
        public IObservation Observation(DateTime date)
        {
            if (date <= start)
                throw new Exception("MaxProcess : fixing date previous to start date");

            var schedule = ObservationProcessUtils.Schedule(start, date, frequency);
            var observations = schedule.Select(d => observationProcess.Observation(d)).Cast<IFixing>().ToArray();
            return new MaxFixingFunction(observations);
        }
    }

    public class MaxFixingFunction : IFixingFunction
    {
        #region private fields
        private readonly IFixing[] fixings;
        #endregion
        public MaxFixingFunction(IFixing[] fixings)
        {
            if (!EnumerableUtils.IsSorted(fixings.Map(f => f.Date)))
                throw new Exception("MaxFixing : fixings must be sorted !");
            this.fixings = fixings;
        }
        public DateTime Date
        {
            get { return fixings.Last().Date; }
        }
        public IObservation[] Observations
        {
            get { return fixings; }
        }
        public double Value(double[] fixingValues)
        {
            return fixingValues.Max();
        }
    }
    
    public class BasketFixingProcess : IObservationProcess
    {
        #region private fields
        private readonly IObservationProcess[] underlyings;
        private readonly double[] weights;
        private readonly bool averaging;
        #endregion
        public BasketFixingProcess(IObservationProcess[] underlyings, double[] weights, bool averaging)
        {
            if (underlyings.Length != weights.Length)
                throw new Exception("BasketFixingProcess : incompatible weights");

            this.underlyings = underlyings;
            this.weights = weights;
            this.averaging = averaging;
        }
        public IObservation Observation(DateTime date)
        {
            var fixings = underlyings.Map(u => u.Observation(date));
            return new BasketFixingFunction(fixings, weights, averaging);
        }
    }

    public class BasketFixingFunction : IFixingFunction
    {
        #region private fields
        private readonly IObservation[] observations;
        private readonly double[] weights;
        private readonly double normalization;
        #endregion
        public BasketFixingFunction(IObservation[] observations, double[] weights, bool averaging = true)
        {
            if (observations.Length != weights.Length)
                throw new Exception("BasketFixingFunction : incompatible weights");

            this.observations = observations;
            this.weights = weights;
            normalization = averaging ? 1.0 / observations.Length : 1.0;
        }
        public DateTime Date
        {
            get { return observations.Select(f=>f.Date).Max(); }
        }
        public IObservation[] Observations
        {
            get { return observations; }
        }
        public double Value(double[] fixingValues)
        {
            return normalization * fixingValues.DotProduct(weights);
        }
    }
    
    public class WorstOfProcess : IObservationProcess
    {
        #region private fields
        private readonly DateTime start;
        private readonly Duration frequency;
        private readonly IObservationProcess[] underlyings;
        private readonly double[] weights;
        #endregion
        public WorstOfProcess(DateTime start, Duration frequency, IObservationProcess[] underlyings, double[] weights)
        {
            if (underlyings.Length!=weights.Length)
                throw new Exception("WorstOfProcess : incompatible weights");

            this.start = start;
            this.frequency = frequency;
            this.underlyings = underlyings;
            this.weights = weights;
        }
        public WorstOfProcess(IObservationProcess[] underlyings, double[] weights)
            : this(DateTime.MaxValue, 0 * Duration.Day, underlyings, weights)
        {
        }

        public IObservation Observation(DateTime date)
        {
            if (date == start || start == DateTime.MaxValue)
                return new WorstOfFixingFunction(underlyings.Map(u => u.Observation(date)), weights);

            if (date < start)
                throw new Exception("WorstOfProcess : start must be previous to observation date ");

            var schedule = ObservationProcessUtils.Schedule(start, date, frequency);
            var fixings = schedule.Map(d => underlyings.Map(u => u.Observation(d)));
            return new WorstOfFixingFunction(fixings, weights);
        }
    }

    public class WorstOfFixingFunction : IFixingFunction
    {
        #region private fields
        private readonly IObservation[] allFixings;
        private readonly double[] weights;
        private readonly int nbDates;
        private readonly int nbUnderlyings;
        #endregion
        public WorstOfFixingFunction(IObservation[][] fixings, double[] weights)
        {
            this.weights = weights;
            allFixings = ArrayUtils.Append(fixings);
            Date = allFixings.Select(f => f.Date).Max();
            nbDates = fixings.Length;
            nbUnderlyings = fixings.First().Length;
            if (fixings.Any(f => f.Length!=nbUnderlyings))
                throw new Exception("WorstOfFixingFunction : invalid fixing array");
        }
        public WorstOfFixingFunction(IObservation[] fixings, double[] weights)
            : this(new[] { fixings }, weights) { }

        public IObservation[] Observations
        {
            get { return allFixings; }
        }
        public DateTime Date { get; private set; }
        public double Value(double[] fixingValues)
        {
            double worst = double.PositiveInfinity;
            int index = 0;
            for (int t = 0; t < nbDates; ++t)
            {
                for (int i = 0; i < nbUnderlyings; ++i)
                {
                    var val = fixingValues[index++] * weights[i];
                    if (val < worst) worst = val;
                }
            }
            return worst;
        }
    }

    public class AdditiveCliquetPerf : IFixingFunction
    {
        #region private fields
        private readonly IFixing[] fixings;
        private readonly double localYieldCap;
        private readonly double localYieldFloor;
        private readonly double localYieldStrike;
        #endregion
        public AdditiveCliquetPerf(IFixing[] fixings, double localYieldCap, double localYieldFloor, double localYieldStrike)
        {
            if (!EnumerableUtils.IsSorted(fixings.Map(f => f.Date)))
                throw new Exception("AdditiveCliquetPerf : fixings must be sorted !");
            this.fixings = fixings;
            this.localYieldCap = localYieldCap;
            this.localYieldFloor = localYieldFloor;
            this.localYieldStrike = localYieldStrike;
        }
        public DateTime Date
        {
            get { return fixings.Select(f=>f.Date).Max(); }
        }
        public IObservation[] Observations
        {
            get { return fixings; }
        }
        public double Value(double[] fixingValues)
        {
            var perf = 0.0;
            for (int i = 1; i < fixingValues.Length; i++)
            {
                var localYield = Math.Max(localYieldFloor, Math.Min(localYieldCap, fixingValues[i] / fixingValues[i - 1] - localYieldStrike));
                perf += localYield;
            }
            return perf;
        }
    }

    public class MultiplicativeCliquetPerf : IFixingFunction
    {
        #region private fields
        private readonly IFixing[] fixings;
        private readonly double localYieldCap;
        private readonly double localYieldFloor;
        #endregion
        public MultiplicativeCliquetPerf(IFixing[] fixings, double localYieldCap, double localYieldFloor)
        {
            if (!EnumerableUtils.IsSorted(fixings.Map(f => f.Date)))
                throw new Exception("MultiplicativeCliquetPerf : fixings must be sorted !");
            this.fixings = fixings;
            this.localYieldCap = localYieldCap;
            this.localYieldFloor = localYieldFloor;
        }
        public DateTime Date
        {
            get { return fixings.Select(f => f.Date).Max(); }
        }
        public IObservation[] Observations
        {
            get { return fixings; }
        }
        public double Value(double[] fixingValues)
        {
            var perf = 1.0;
            for (int i = 1; i < fixingValues.Length; i++)
            {
                var localYield = Math.Max(1.0 + localYieldFloor, Math.Min(1.0 + localYieldCap, fixingValues[i] / fixingValues[i - 1]));
                perf *= 1.0 + localYield;
            }
            return perf;
        }
    }

}