using System;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;

namespace PragmaticQuant.Product.Fixings
{
    public class VarianceProcess : IObservationProcess
    {
        #region private fields
        private readonly IObservationProcess fixingProcess;
        private readonly DateTime start;
        private readonly double varianceAnnualBasis;
        #endregion
        public VarianceProcess(IObservationProcess fixingProcess, DateTime start, double varianceAnnualBasis = double.NaN)
        {
            this.fixingProcess = fixingProcess;
            this.start = start;
            this.varianceAnnualBasis = varianceAnnualBasis;
        }

        public IObservation Observation(DateTime date)
        {
            if (date <= start)
                throw new Exception("VarianceProcess : fixing date previous to start date");

            var schedule = ObservationProcessUtils.Schedule(start, date, Duration.Day);
            var fixings = schedule.Select(d => fixingProcess.Observation(d)).ToArray();
            var dayAdj = double.IsNaN(varianceAnnualBasis) ? 1.0 : varianceAnnualBasis / fixings.Length;
            return new VariancePayoff(fixings, dayAdj);
        }
    }

    public class VariancePayoff : IFixingFunction
    {
        #region private fields
        private readonly IObservation[] varianceFixings;
        private readonly double varianceAdj;
        #endregion
        public VariancePayoff(IObservation[] varianceFixings, double varianceAdj = 1.0)
        {
            if (!EnumerableUtils.IsSorted(varianceFixings.Map(f => f.Date)))
                throw new Exception("VariancePayoff : fixings must be sorted !");
            this.varianceFixings = varianceFixings;
            this.varianceAdj = varianceAdj;
        }

        public DateTime Date
        {
            get { return varianceFixings.Last().Date; }
        }
        public IObservation[] Observations
        {
            get { return varianceFixings; }
        }
        public double Value(double[] fixings)
        {
            var variance = 0.0;
            for (int i = 1; i < fixings.Length; i++)
            {
                var ret = fixings[i] - fixings[i - 1];
                variance += ret * ret;
            }
            return variance * varianceAdj;
        }
    }

    public class LogVariancePayoff : IFixingFunction
    {
        #region private fields
        private readonly IFixing[] varianceFixings;
        private readonly double varianceAdj;
        #endregion
        public LogVariancePayoff(IFixing[] varianceFixings, double varianceAdj = 1.0)
        {
            if (!EnumerableUtils.IsSorted(varianceFixings.Map(f => f.Date)))
                throw new Exception("LogVariancePayoff : fixings must be sorted !");
            this.varianceFixings = varianceFixings;
            this.varianceAdj = varianceAdj;
        }

        public DateTime Date
        {
            get { return varianceFixings.Last().Date; }
        }
        public IObservation[] Observations
        {
            get { return varianceFixings; }
        }
        public double Value(double[] fixings)
        {
            var variance = 0.0;
            for (int i = 1; i < fixings.Length; i++)
            {
                var logReturn = Math.Log(fixings[i] / fixings[i - 1]);
                variance += logReturn * logReturn;
            }
            return variance * varianceAdj;
        }
    }
}