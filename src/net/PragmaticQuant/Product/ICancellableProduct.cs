using System;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.Product
{
    public interface ICancellableProduct : IProduct
    {
        ICouponDecomposable Underlying { get; }
        ICancellationItem[] CancellationItems { get; }
    }

    public static class CancellableUtils
    {
        /// <summary>
        /// Return underlying coupons whose paydate are within ]callDate, next call date ] 
        /// </summary>
        public static Coupon[] NextCancelledCoupons(this ICancellableProduct cancellable, DateTime callDate)
        {
            var callItem = cancellable.CancellationItem(callDate);
            var effectiveCallDate = callDate + callItem.Notice;

            var allCallDates = cancellable.CancellationItems.Map(it => it.Date);
            var callDateIndex = Array.BinarySearch(allCallDates, callDate);
            var effectiveNextCall = (callDateIndex == allCallDates.Length - 1)
                                    ? DateTime.MaxValue
                                    : allCallDates[callDateIndex + 1] + callItem.Notice;

            var underlyingCoupons = cancellable.Underlying.Decomposition();
            var result = underlyingCoupons.Where(cpn =>
            {
                var payDate = cpn.PaymentInfo.Date;
                return payDate >= effectiveCallDate && payDate < effectiveNextCall;
            }).ToArray();
            return result;
        }

        public static ICancellationItem CancellationItem(this ICancellableProduct cancellable, DateTime callDate)
        {
            return cancellable.CancellationItems.Single(item => item.Date == callDate);
        }

    }

    public interface ICancellationItem
    {
        /// <summary>
        /// Cancellation date
        /// </summary>
        DateTime Date { get; }

        /// <summary>
        /// Coupon cancellation notice : all coupons with payment date > calldate + notice will be cancelled
        /// </summary>
        Duration Notice { get; }
        
        /// <summary>
        /// Coupon paid if cancellation occured
        /// </summary>
        Coupon CancelFee { get; }
    }

    public class CallItem : ICancellationItem
    {
        public CallItem(DateTime date, Duration notice, Coupon cancelFee)
        {
            Date = date;
            Notice = notice;
            CancelFee = cancelFee;
        }
        public DateTime Date { get; private set; }
        public Duration Notice { get; private set; }
        public Coupon CancelFee { get; private set; }
    }

    public class TriggerItem : ICancellationItem
    {
        public TriggerItem(DateTime date, Duration notice, Coupon cancelFee, IFixingFunction trigger)
        {
            Date = date;
            Notice = notice;
            CancelFee = cancelFee;
            Trigger = trigger;

            if (trigger.Date>date)
                throw new Exception("TriggerItem : trigger incompatible with call date");
        }
        public DateTime Date { get; private set; }
        public Duration Notice { get; private set; }
        public Coupon CancelFee { get; private set; }
        public IFixingFunction Trigger { get; private set; }
    }

    public abstract class CancellableProduct<TCall> : ICancellableProduct where TCall : ICancellationItem
    {
        #region private fields
        private readonly TCall[] cancelItems;
        #endregion
        public CancellableProduct(ICouponDecomposable underlying, params TCall[] cancelItems)
        {
            Underlying = underlying;
            Financing = underlying.Financing;
            this.cancelItems = cancelItems.OrderBy(t => t.Date).ToArray();

            if (!cancelItems.All(t => t.CancelFee.Financing.Equals(underlying.Financing)))
                throw new Exception("CancellableProduct : cancellation fee financing incompatible with underlying");
        }

        public ICouponDecomposable Underlying { get; private set; }
        public ICancellationItem[] CancellationItems
        {
            get { return cancelItems.Cast<ICancellationItem>().ToArray(); }
        }
        public FinancingId Financing { get; private set; }
        public abstract TResult Accept<TResult>(IProductVisitor<TResult> visitor);
    }

    public class AutoCall : CancellableProduct<TriggerItem>
    {
        public AutoCall(ICouponDecomposable underlying, params TriggerItem[] triggers)
            :base(underlying, triggers)
        {}

        public override TResult Accept<TResult>(IProductVisitor<TResult> visitor)
        {
            return visitor.Visit(this);
        }
    }

    public class Callable : CancellableProduct<CallItem>
    {
        public Callable(ICouponDecomposable underlying, params CallItem[] callOptions)
            :base(underlying, callOptions)
        {}

        public override TResult Accept<TResult>(IProductVisitor<TResult> visitor)
        {
            return visitor.Visit(this);
        }
    }

}