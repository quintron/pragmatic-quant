using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.Product
{
    public interface ICouponDecomposable : IProduct
    {
        Coupon[] Decomposition();
    }
    
    public class CouponLinearCombination : ICouponDecomposable
    {
        #region private fields
        private readonly Coupon[] couponDecomposition;
        #endregion
        #region private methods
        private CouponLinearCombination(double[] weights, ICouponDecomposable[] decomposables)
        {
            Debug.Assert(weights.Length == decomposables.Length);
            var financings = decomposables.Select(d => d.Financing).Distinct().ToArray();
            if (financings.Count() > 1)
                throw new Exception("DecomposableLinearCombination : multiple financing are not allowed");
            Financing = financings.Single();
            Coupon[][] weightedCoupons = weights.ZipWith(decomposables, (w, d) =>
            {
                var coupons = d.Decomposition();

                if (DoubleUtils.EqualZero(w))
                    return new Coupon[0];
                
                if (DoubleUtils.MachineEquality(w, 1.0)) 
                    return coupons;

                return coupons.Map(cpn => new Coupon(cpn.CouponId, cpn.PaymentInfo, new WeightedFixingFunction(w, cpn.Payoff)));
            });
            couponDecomposition = EnumerableUtils.Append(weightedCoupons);
        }
        #endregion

        public static CouponLinearCombination Create(double[] weights, ICouponDecomposable[] decomposables)
        {
            Debug.Assert(weights.Length == decomposables.Length);
            var weightsList = new List<double>();
            var decompList = new List<ICouponDecomposable>();
            for (int i = 0; i < weights.Length; i++)
            {
                double weight = weights[i];
                ICouponDecomposable decomp = decomposables[i];
                decompList.Add(decomp);
                weightsList.Add(weight);
            }
            return new CouponLinearCombination(weightsList.ToArray(), decompList.ToArray());
        }
        public static CouponLinearCombination Create(params ICouponDecomposable[] decomposables)
        {
            return Create(ArrayUtils.Constant(decomposables.Length, 1.0), decomposables);
        }

        public ICouponDecomposable Add(ICouponDecomposable decomposable)
        {
            return Create(this, decomposable);
        }
        public ICouponDecomposable Add(double weight, ICouponDecomposable decomposable)
        {
            return Create(new[] {1.0, weight}, new[] {this, decomposable});
        }
        public ICouponDecomposable Mult(double weight)
        {
            return new CouponLinearCombination(new[] {weight}, new[] {this});
        }

        public FinancingId Financing { get; private set; }
        public TResult Accept<TResult>(IProductVisitor<TResult> visitor)
        {
            return visitor.Visit(this);
        }
        public Coupon[] Decomposition()
        {
            return couponDecomposition;
        }
    }

}