using System;
using PragmaticQuant.Basic;

namespace PragmaticQuant.Product
{
    public interface ICouponDescription
    {
        Coupon GetCoupon { get; }
    }

    public static class CouponDescriptions
    {
        public static class Equity
        {
            public static EquityOption VanillaOption(AssetId asset, DateTime maturity, Currency payCurrency, OptionType optionType, double strike)
            {
                return new EquityOption(asset, maturity, payCurrency, optionType, strike);
            }
            public static EquityOption Call(AssetId asset, DateTime maturity, Currency payCurrency, double strike)
            {
                return new EquityOption(asset, maturity, payCurrency, OptionType.Call, strike);
            }
            public static EquityOption Put(AssetId asset, DateTime maturity, Currency payCurrency, double strike)
            {
                return new EquityOption(asset, maturity, payCurrency, OptionType.Put, strike);
            }
            public static LogSwap LogSwap(AssetId asset, DateTime maturity, Currency payCurrency, double strike)
            {
                return new LogSwap(asset, maturity, payCurrency, strike);
            }
        }
    }
}