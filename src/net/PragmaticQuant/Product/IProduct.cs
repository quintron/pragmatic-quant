﻿using System;
using System.Collections.Generic;
using System.Linq;
using PragmaticQuant.Basic;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.Product
{
    public interface IProduct
    {
        FinancingId Financing { get; }

        TResult Accept<TResult>(IProductVisitor<TResult> visitor);
    }

    public interface IProductVisitor<out TResult>
    {
        TResult Visit(Coupon coupon);
        TResult Visit(ICouponDecomposable couponDecomposable);
        TResult Visit(AutoCall autocall);
        TResult Visit(Callable callable);
    }

    public static class ProductUtils
    {
        public static IObservation[] RetrieveCouponObservations(this IProduct product)
        {
            return product.Accept(new CouponObservationProductVisitor());
        }
        public static DateTime[] RetrieveEventDates(this IProduct product)
        {
            return product.Accept(new EventDateProductVisitor());
        }
        public static PaymentInfo[] RetrievePaymentInfos(this IProduct product)
        {
            return product.Accept(new PaymentProductVisitor());
        }

        #region private methods
        private class CouponObservationProductVisitor : IProductVisitor<IObservation[]>
        {
            #region private methods
            private IObservation[] Visit(ICancellableProduct cancellable)
            {
                var underlyingFixings = cancellable.Underlying.Accept(this);
                var callFees = cancellable.CancellationItems.Map(call => call.CancelFee);
                var callFeeFixings = callFees.Map(cpn => cpn.Accept(this));
                var mergedFixings = underlyingFixings.MergeWith(callFeeFixings);
                return mergedFixings.OrderBy(f => f.Date).ToArray();
            }
            #endregion
            public IObservation[] Visit(Coupon coupon)
            {
                return new[] { coupon.Payoff };
            }
            public IObservation[] Visit(ICouponDecomposable couponDecomposable)
            {
                var fixings = EnumerableUtils.Merge(couponDecomposable.Decomposition().Map(Visit));
                return fixings.OrderBy(f => f.Date).ToArray();
            }
            public IObservation[] Visit(AutoCall autocall)
            {
                var triggers = autocall.CancellationItems.Cast<TriggerItem>().ToArray();
                var triggerFixings = triggers.Map(t => t.Trigger);

                var cancellableFixings = Visit(autocall as ICancellableProduct);
                var mergedFixings = cancellableFixings.MergeWith(triggerFixings);
                return mergedFixings.OrderBy(f => f.Date).ToArray();
            }
            public IObservation[] Visit(Callable callable)
            {
                throw new NotImplementedException("Callable");
            }
        }

        private class EventDateProductVisitor : IProductVisitor<DateTime[]>
        {
            #region private methods
            private DateTime[] Visit(ICancellableProduct cancellable)
            {
                var underlyingDates = cancellable.Underlying.Accept(this);
                var callFeeDates = cancellable.CancellationItems.Map(call => call.CancelFee.Accept(this));
                
                var result = EnumerableUtils.Merge(callFeeDates);
                result = EnumerableUtils.Merge(underlyingDates, result);
                result = EnumerableUtils.Merge(result, cancellable.CancellationItems.Map(c=>c.Date));
                result = result.OrderBy(d => d).ToArray();
                return result;
            }
            #endregion
            public DateTime[] Visit(Coupon coupon)
            {
                var fixingsDates = EnumerableUtils.Merge(coupon.Payoff.Observations.Map(obs =>
                {
                    var function = obs as IFixingFunction;
                    if (function != null)
                    {
                        return function.Observations.Map(f => f.Date); //TODO add a check that observations are not IFixingFunction
                    }

                    return new[] {obs.Date};

                }));

                return fixingsDates.Union(new[] {coupon.PaymentInfo.Date})
                    .OrderBy(d => d).ToArray();
            }
            public DateTime[] Visit(ICouponDecomposable couponDecomposable)
            {
                Coupon[] coupons = couponDecomposable.Decomposition();
                var eventDates = new List<DateTime>(EnumerableUtils.Merge(coupons.Map(cpn => cpn.Accept(this))));
                eventDates.Sort();
                return eventDates.ToArray();
            }
            public DateTime[] Visit(AutoCall autocall)
            {
                return Visit(autocall as ICancellableProduct);
            }
            public DateTime[] Visit(Callable callable)
            {
                return Visit(callable as ICancellableProduct);
            }
        }

        private class PaymentProductVisitor : IProductVisitor<PaymentInfo[]>
        {
            #region private methods
            private PaymentInfo[] Visit(ICancellableProduct cancellable)
            {
                var underlyingPayments = cancellable.Underlying.Accept(this);
                var callFeePayments = cancellable.CancellationItems.Map(call => call.CancelFee.Accept(this));
                return callFeePayments.Aggregate(underlyingPayments, (prev, d) => prev.Union(d)
                                      .OrderBy(p=>p.Date).ToArray());
            }
            #endregion
            public PaymentInfo[] Visit(Coupon coupon)
            {
                return new[] {coupon.PaymentInfo};
            }
            public PaymentInfo[] Visit(ICouponDecomposable couponDecomposable)
            {
                var cpns = couponDecomposable.Decomposition();   
                return cpns.Aggregate(new PaymentInfo[0], (allPayments, cpn) => allPayments.Union(cpn.Accept(this))
                           .OrderBy(p=>p.Date)
                           .ToArray());
            }
            public PaymentInfo[] Visit(AutoCall autocall)
            {
                return Visit(autocall as ICancellableProduct);
            }
            public PaymentInfo[] Visit(Callable callable)
            {
                return Visit(callable as ICancellableProduct);
            }
        }
        #endregion
    }

}
