using System;
using System.Diagnostics;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets.Rate;
using PragmaticQuant.Product.Fixings;

namespace PragmaticQuant.Product
{
    [DebuggerDisplay("{Id}")]
    public class Swaption
    {
        public Swaption(Currency currency, DateOrDuration underlying, DateTime date, OptionType optionType, double strike)
        {
            Currency = currency;
            Underlying = underlying;
            Date = date;
            OptionType = optionType;
            Strike = strike;

            Id = string.Format("{2} {0}.{1}@{3} {4}", Currency, Underlying, OptionType, Strike.ToString("P"), Date.ToShortDateString());
        }

        public DateTime Date { get; private set; }
        public double Strike { get; private set; }
        public OptionType OptionType { get; private set; }
        public Currency Currency { get; private set; }
        public DateOrDuration Underlying { get; private set; }
        public string Id { get; private set; }
        
        public Coupon GetCoupon()
        {
            var convention = SwapConventions.For(Currency);
            var start = convention.FloatLegConvention.ScheduleBuilder.StartDate(Date);
            var swapMtm = new SwapMtm(Date, SwapSide.Payer, Strike, start, Underlying.ToDate(start), Currency, convention);
            var payoff = CapFloorPayoff.CreateOption(swapMtm, OptionType, 0.0);
            return new Coupon(Id, new PaymentInfo(Currency, Date), payoff);
        }
        public SwapRate UnderlyingRate()
        {
            var convention = SwapConventions.For(Currency);
            var start = convention.FloatLegConvention.ScheduleBuilder.StartDate(Date);
            return new SwapRate(Date, start, Underlying.ToDate(start), Currency, convention);
        }
    }
}