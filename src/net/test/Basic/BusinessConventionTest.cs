﻿using System;
using System.Globalization;
using System.Linq;
using NUnit.Framework;
using PragmaticQuant.Basic.Dates;

namespace test.Basic
{
    [TestFixture]
    public class BusinessConventionTest
    {
        [TestCase("2011-08-18", 1, "2011-09-19")]
        public void Following(string startDesc, int periodNbMonth, string refAdjustedDesc)
        {
            var start = DateTime.ParseExact(startDesc, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            var refAdjusted = DateTime.ParseExact(refAdjustedDesc, "yyyy-MM-dd", CultureInfo.InvariantCulture);

            var adjustedDate = BusinessConventions.Following.Adjust(new []{start.AddMonths(periodNbMonth)}, SimpleCalendar.Instance).First();
            Assert.AreEqual(refAdjusted, adjustedDate);
        }

        [TestCase("2011-06-30", 1, "2011-07-29")]
        [TestCase("2011-08-18", 1, "2011-09-19")]
        public void ModifiedFollowing(string startDesc, int periodNbMonth, string refAdjustedDesc)
        {
            var start = DateTime.ParseExact(startDesc, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            var refAdjusted = DateTime.ParseExact(refAdjustedDesc, "yyyy-MM-dd", CultureInfo.InvariantCulture);

            var adjustedDate = BusinessConventions.ModifiedFollowing.Adjust(new []{start.AddMonths(periodNbMonth)}, SimpleCalendar.Instance).First();
            Assert.AreEqual(refAdjusted, adjustedDate);
        }

        [TestCase("2011-08-18", 1, "2011-09-16")]
        public void Preceding(string startDesc, int periodNbMonth, string refAdjustedDesc)
        {
            var start = DateTime.ParseExact(startDesc, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            var refAdjusted = DateTime.ParseExact(refAdjustedDesc, "yyyy-MM-dd", CultureInfo.InvariantCulture);

            var adjustedDate = BusinessConventions.Preceding.Adjust(new[] {start.AddMonths(periodNbMonth)}, SimpleCalendar.Instance).First();
            Assert.AreEqual(refAdjusted, adjustedDate);
        }
        
        [TestCase("2012-04-30", 1, "2012-05-31")]
        [TestCase("2012-02-28", 1, "2012-03-28")] 
        public void EndOfMonth(string startDesc, int periodNbMonth, string refAdjustedDesc)
        {
            var start = DateTime.ParseExact(startDesc, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            var refAdjusted = DateTime.ParseExact(refAdjustedDesc, "yyyy-MM-dd", CultureInfo.InvariantCulture);

            var adjustedDate = BusinessConventions.EndOfMonth.Adjust(new[] {start, start.AddMonths(periodNbMonth) }, SimpleCalendar.Instance).Last();
            Assert.AreEqual(refAdjusted, adjustedDate);
        }
    }
}
