﻿using NUnit.Framework;
using PragmaticQuant.Basic;

namespace test.Basic
{
    [TestFixture]
    public class RegexUtilsTest
    {
        [TestCase("5", true)]
        [TestCase("5  ", true)]
        [TestCase(" -5", true)]
        [TestCase("0.1", true)]
        [TestCase("0,1", true)]
        [TestCase("0.1 ", true)]
        [TestCase("0,1 ", true)]
        [TestCase(" 0.1 ", true)]
        [TestCase(" 0,1 ", true)]
        [TestCase("5432155.1", true)]
        [TestCase("5432155,1", true)]
        [TestCase("555555.123848466", true)]
        [TestCase("-555555.123848466", true)]
        [TestCase("555555,123848466", true)]
        [TestCase("-555555,123848466", true)]

        [TestCase("a0.1 ", false)]
        [TestCase("a0,1 ", false)]
        [TestCase(" a0.1 ", false)]
        [TestCase(" a0,1 ", false)]
        [TestCase("- 555555,123848466", false)]
        public void NumberTest(string s, bool isNumber)
        {
            Assert.AreEqual(isNumber, RegexUtils.IsNumber(s));
        }
    }
}
