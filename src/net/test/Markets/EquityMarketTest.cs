﻿using System;
using NUnit.Framework;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets;
using PragmaticQuant.Markets.RawDatas;

namespace test.Markets
{
    [TestFixture]
    public class EquityMarketTest
    {
        [Test]
        public void FlatMktTest()
        {
            var refDate = new DateTime(2017, 08, 02);
            var rawMkt = new MarketDatas {RefDate = refDate};
            rawMkt.AddDiscountCurve(MarketDatasUtils.FlatCurve("USD", "Ois", 0.0));
            rawMkt.AddAsset(MarketDatasUtils.FlatAssetMarket("SP500", "USD", 3500.0, 0.18));
            
            var mkt = rawMkt.ToMarket();
            var assetMkt = mkt.AssetMarket(new AssetId("SP500"));
            
            var volSurface = assetMkt.VolSurface();
            Assert.AreEqual(0.18, volSurface.Volatility(refDate + 1 * Duration.Month, 3500.0 * 0.7));
            Assert.AreEqual(0.18, volSurface.Volatility(refDate + 20 * Duration.Year, 3500.0 * 0.7));
            Assert.AreEqual(0.18, volSurface.Volatility(refDate + 1 * Duration.Month, 3500.0 * 2.0));
            Assert.AreEqual(0.18, volSurface.Volatility(refDate + 20 * Duration.Year, 3500.0 * 2.0));

            var fwdCurve = assetMkt.ForwardCurve();
            Assert.AreEqual(3500.0, fwdCurve.Fwd(refDate + 1 * Duration.Month));
            Assert.AreEqual(3500.0, fwdCurve.Fwd(refDate + 1 * Duration.Year));
            Assert.AreEqual(3500.0, fwdCurve.Fwd(refDate + 5 * Duration.Year));
            Assert.AreEqual(3500.0, fwdCurve.Fwd(refDate + 20 * Duration.Year));
        }
    }

}
