﻿using System;
using NUnit.Framework;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets.RawDatas;
using PragmaticQuant.Markets.Scenarios;
using PragmaticQuant.Pricing;

namespace test.Markets
{
    [TestFixture]
    public class EquityVolWaveScenarioTest
    {
        [Test]
        public void Test()
        {
            var refDate = new DateTime(2017, 08, 02);
            var rawMkt = new MarketDatas { RefDate = refDate };
            rawMkt.AddDiscountCurve(MarketDatasUtils.FlatCurve("USD", "Ois", 0.0));
            rawMkt.AddAsset(MarketDatasUtils.FlatAssetMarket("SP500", "USD", 3500.0, 0.18));

            var mkt = new MarketConfig
            {
                MarketDatas = rawMkt
            }.BuildMarket();
            var volSurface = mkt.AssetMarket(new AssetId("SP500"))
                                .VolSurface();

            var wave = ParametricScenarios.EquityVolWaveShift("SP500", 13 * Duration.Month);
            var waveMkt = new MarketConfig
            {
                MarketDatas = rawMkt,
                MarketScenario = wave.Scenario(0.01)
            }.BuildMarket();
            var waveVolSurface = waveMkt.AssetMarket(new AssetId("SP500"))
                                        .VolSurface();

            double vol = waveVolSurface.Volatility(refDate + 1 * Duration.Month, 3500.0 * 0.7);
            double refVol = volSurface.Volatility(refDate + 1 * Duration.Month, 3500.0 * 0.7);
            Assert.AreEqual(0.0, vol - refVol);

            vol = waveVolSurface.Volatility(refDate + 11 * Duration.Month, 3500.0 * 0.7);
            refVol = volSurface.Volatility(refDate + 11 * Duration.Month, 3500.0 * 0.7);
            Assert.AreEqual(0.0, vol - refVol);

            vol = waveVolSurface.Volatility(refDate + 13 * Duration.Month, 3500.0 * 0.7);
            refVol = volSurface.Volatility(refDate + 13 * Duration.Month, 3500.0 * 0.7);
            Assert.AreEqual(0.01, vol - refVol, DoubleUtils.MachineEpsilon);

            vol = waveVolSurface.Volatility(refDate + 5 * Duration.Year, 3500.0 * 2.0);
            refVol = volSurface.Volatility(refDate + 5 * Duration.Year, 3500.0 * 2.0);
            Assert.AreEqual(0.01, vol - refVol, DoubleUtils.MachineEpsilon);

        }

    }
}
