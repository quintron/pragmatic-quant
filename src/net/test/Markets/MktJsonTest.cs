﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets.RawDatas;

namespace test.Markets
{
    [TestFixture]
    public class MktJsonTest
    {
        #region private methods
        private static DiscountCurveDatas TestCurve()
        {
            return DiscountCurveDatas.Create("EUR", "Ois",
                new []
                {
                    new DateOrDuration(3 * Duration.Month),
                    new DateOrDuration(Duration.Year),
                    new DateOrDuration(5 * Duration.Year),
                    new DateOrDuration(new DateTime(2060, 01, 01))
                },
                new[] {0.001, 0.008, 0.0123, 0.02});
        }
        private static RateVolCubeDatas TestRateVolCube()
        {
            var maturities = new DateOrDuration[]
            {
                Duration.Month,
                Duration.Year,
                5 * Duration.Year,
                10 * Duration.Year
            };
            var tenors = new[] { "6m", "2y", "5y", "20y" };
            var vols = new [,]
            {
                { 0.10, 0.3, 0.4, 0.42 },
                { 0.20, 0.4, 0.5, 0.5 },
                { 0.4, 0.5, 0.6, 0.6 },
                { 0.55, 0.6, 0.65, 0.65 },
            };
            return new RateVolCubeDatas
            {
                Currency = "EUR",
                AtmVolMatrix = new LabelledMatrix<DateOrDuration, string, double>(maturities, tenors, vols)
            };
        }
        private static AssetMarketDatas TestAssetMkt()
        {
            return new AssetMarketDatas
            {
                Name = "STOX5E",
                Currency = "EUR",
                Spot = 3553.69,
                RepoCurve = new RawMapDatas<DateOrDuration, double>(
                    new[] {new DateOrDuration(Duration.Year), new DateOrDuration(2 * Duration.Year), new DateOrDuration(3 * Duration.Year)},
                    new[] {0.02, 0.025, 0.03}),

                DividendForward = new RawMapDatas<DateTime, double>(
                    new[] {new DateTime(2013, 01, 23), new DateTime(2013, 02, 05)},
                    new[] {6.2, 6.2}),
                    
                VolatilityMatrix = new LabelledMatrix<DateOrDuration, double, double>(
                    new[] {new DateOrDuration(Duration.Month), new DateOrDuration(6 * Duration.Month), new DateOrDuration(1 * Duration.Year)},
                    new[] {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 1.0, 1.5},
                    new[,]
                    {
                        {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 1.0, 1.5},
                        {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 1.0, 1.5},
                        {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 1.0, 1.5}
                    })
            };
        }
        private static MarketDatas TestMkt()
        {
            return new MarketDatas
            {
                RefDate = new DateTime(2013, 01, 23),
                DiscountCurves = new List<DiscountCurveDatas> {TestCurve()},
                Assets = new List<AssetMarketDatas> {TestAssetMkt()},
                RateVolCubes = new List<RateVolCubeDatas> { TestRateVolCube()}
            };
        }
        #endregion
        
        [Test]
        public void ParseMarket()
        {
            MarketDatas mkt = TestMkt();
            var jsonMkt = MarketJsonParser.ToJson(mkt);
            MarketDatas mktFromJson = MarketJsonParser.ParseMarket(jsonMkt);
            Assert.AreEqual(mkt, mktFromJson);
        }

        [Test]
        public void ParseAssetMarket()
        {
            AssetMarketDatas assetMkt = TestAssetMkt();
            var jsonMkt = MarketJsonParser.ToJson(assetMkt);
            AssetMarketDatas assetMktFromJson = MarketJsonParser.ParseAssetMarket(jsonMkt);
            Assert.AreEqual(assetMkt, assetMktFromJson);
        }
        
        [Test]
        public void ParseDiscountCurve()
        {
            DiscountCurveDatas curve = TestCurve();
            var jsonCurve = MarketJsonParser.ToJson(curve);
            var curveFromJson = MarketJsonParser.ParseDiscountCurve(jsonCurve);
            Assert.AreEqual(curve, curveFromJson);
        }
    }
}
