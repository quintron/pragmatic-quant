﻿using System.IO;
using NUnit.Framework;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets;
using PragmaticQuant.Markets.RawDatas;

namespace test.Markets
{
    [TestFixture]
    public class RateMarketTest
    {
        [Test]
        public void RateVolConsistency()
        {
            string path = System.Reflection.Assembly.GetCallingAssembly().Location;
            var mktPath = Path.Combine(Path.GetDirectoryName(path), "mkt.txt");
            var mktJson = File.ReadAllText(mktPath);
            
            var mktDatas = MarketJsonParser.ParseMarket(mktJson);
            var mkt = mktDatas.ToMarket();

            var currency = mktDatas.RateVolCubes[0].Currency;
            var volMatrix = mktDatas.RateVolCubes[0].AtmVolMatrix;
            var volCube = mkt.RateMarket(Currency.Parse(currency)).VolCube();
            for(int i= 0; i < volMatrix.RowLabels.Length; i++)
            {
                var maturity = volMatrix.RowLabels[i].ToDate(mkt.RefDate);

                for (int j = 0; j < volMatrix.ColLabels.Length; j++)
                {
                    var tenor = Duration.Parse(volMatrix.ColLabels[j]);
                    var vol = volCube.Volatility(maturity, tenor, double.NaN);
                    var refVol = volMatrix.Values[i, j];
                    Assert.AreEqual(refVol, vol);
                }

                var leftFlatExtrapol = volCube.Volatility(maturity, Duration.Day, double.NaN);
                Assert.AreEqual(volMatrix.Values[i, 0], leftFlatExtrapol);

                var rightFlatExtrapol = volCube.Volatility(maturity, 1000 * Duration.Year, double.NaN);
                Assert.AreEqual(volMatrix.Values[i, volMatrix.ColLabels.Length - 1], rightFlatExtrapol);
            }            
        }

    }

}
