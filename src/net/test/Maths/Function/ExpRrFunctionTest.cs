﻿using System;
using NUnit.Framework;
using PragmaticQuant.Maths.Function;

namespace test.Maths.Function
{
    [TestFixture]
    public class ExpRrFunctionTest
    {
        [Test]
        public void LcSimplification()
        {
            var exp1 = RrFunctions.Exp(0.05);
            var exp2 = RrFunctions.Exp(0.05);
            var lc = 3.0 * exp1 + exp2;
            Assert.AreEqual(4.0 * RrFunctions.Exp(0.05), lc);
        }

        [Test]
        public void AffineExpIntegral()
        {
            var f = ExpRrFunction.Create(1.0, 1.0, 1.0);
            var primitive = f.Integral(3.14);
            
            Assert.AreEqual(0.0, primitive.Eval(3.14));

            var refPrim = ExpRrFunction.Create(1.0, 1.0, 1.0) - ExpRrFunction.Create(1.0, 1.0);
            refPrim -= refPrim.Eval(3.14);

            var rand = new Random(2017);
            for (int i = 0; i < 1000; i++)
            {
                var x = -10.0 + 20.0 * rand.NextDouble();
                Assert.AreEqual(refPrim.Eval(x), primitive.Eval(x));
            }
        }

        [Test]
        public void AffineIntegral()
        {
            var f = RrFunctions.Affine(0.01 , -30.0 * 0.01 ); // 0.01 * (X-30.0)
            var primitive = f.Integral(30.0); // 0.5 * 0.01 *(X-30.0)^2
            Assert.AreEqual(0.0, primitive.Eval(30.0));

            var refPrimitive = ExpRrFunction.Create(0.5 * 0.01, 0.0, 2.0) + RrFunctions.Affine(-30.0 * 0.01, 0.0);
            refPrimitive = refPrimitive - refPrimitive.Eval(30.0);
            Assert.AreEqual(0.0, refPrimitive.Eval(30.0));

            var rand = new Random(2017);
            for (int i = 0; i < 1000; i++)
            {
                var x = -10.0 + 20.0 * rand.NextDouble();
                var error = Math.Abs(refPrimitive.Eval(x) - primitive.Eval(x));
                Assert.LessOrEqual(error, 1.0e-15);
            }
        }

        [Test]
        public void Product1()
        {
            var f = RrFunctions.Affine(3.25, 0.0) * RrFunctions.Exp(5.0);
            var refF = ExpRrFunction.Create(3.25, 5.0, 1.0);
            Assert.AreEqual(refF, f);
        }

        [Test]
        public void Product2()
        {
            var f = RrFunctions.Affine(3.25, -3.25) * RrFunctions.Exp(5.0);
            var refF = ExpRrFunction.Create(3.25, 5.0, 1.0) - ExpRrFunction.Create(3.25, 5.0);
            var rand = new Random(2017);
            for (int i = 0; i < 1000; i++)
            {
                var x = -10.0 + 20.0 * rand.NextDouble();
                Assert.AreEqual(refF.Eval(x), f.Eval(x));
            }
        }

    }

}
