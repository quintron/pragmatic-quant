﻿using System;
using System.Collections;
using NUnit.Framework;
using PragmaticQuant.Core;
using PragmaticQuant.Basic;
using PragmaticQuant.Maths;
using PragmaticQuant.Maths.Function;

namespace test.Maths.Pde
{
    [TestFixture]
    public class BlackScholesPdeTest
    {
        #region private methods
        private static RegularGrid1D BuildGrid(RrFunction vol, double horizon, double nbDev, int size)
        {
            var stdDev = Math.Sqrt((vol * vol).Integral(0.0).Eval(horizon));
            return new RegularGrid1D(size, -stdDev * nbDev, stdDev * nbDev);
        }
        #endregion

        [Test, TestCaseSource("FwdTestDatas")]
        public void FwdTest(RrFunction vol, double maturity, double spotTol, double maxTol)
        {
            RegularGrid1D grid = BuildGrid(vol, maturity, 5.0, 251);

            var pdeCoeff = new TimeOnlyPdeCoeff(vol, RrFunctions.Zero, RrFunctions.Zero, RrFunctions.Zero);
            var stepOperator = new FiniteDiff1D(pdeCoeff, BoundaryCondition1D.Exponential());
            var scheme = PdeStepSolver.ThetaScheme(stepOperator, 0.5);

            var forward = grid.Exp(1.0);
            double cvx = Math.Exp(-0.5 * (vol * vol).Integral(0.0).Eval(maturity));
            forward.Value.Mult(cvx);

            var timeGrid = GridUtils.RegularGrid(0.0, maturity, (int) (50 * maturity));
            for (int timeIndex = timeGrid.Length - 2; timeIndex >= 0; timeIndex--)
            {
                var step = RealInterval.Compact(timeGrid[timeIndex], timeGrid[timeIndex + 1]);
                scheme.Backward(ref forward, step.Inf, step.Sup);
            }

            Assert.AreEqual(1.0, forward.LinearInterpolation(0.0), spotTol);
            
            var refFwd = grid.Exp(1.0);

            var error = forward.Value.Copy();
            error.Div(refFwd.Value);
            error.Add(-1.0);
            var maxError = error.Max();
            Assert.AreEqual(0.0, maxError, maxTol);
        }
        public static IEnumerable FwdTestDatas()
        {
            var cstVol = RrFunctions.Constant(0.30);
            yield return new object[] {cstVol, 1.0, 6.0e-7, 4.0e-4};
            yield return new object[] { cstVol, 5.0, 1.4e-5, 4.2e-3 };
        }
        
        [Test, TestCaseSource("BsVolDatas")]
        public void CallTest(RrFunction vol, double maturity, double strike)
        {
            RegularGrid1D grid = BuildGrid(vol, maturity, 5.0, 250);

            var pdeCoeff = new TimeOnlyPdeCoeff(vol, -0.5 * vol * vol, RrFunctions.Zero, RrFunctions.Zero); 
            var stepOperator = new FiniteDiff1D(pdeCoeff, BoundaryCondition1D.Exponential());
            var scheme =  PdeStepSolver.ThetaScheme(stepOperator, 0.5);

            var payoff = grid.Exp(1.0);  
            payoff.Value.Add(-strike); 
            payoff.Value.MaxWith(0.0);
            
            var timeGrid = GridUtils.RegularGrid(0.0, maturity, 50);
            for (int timeIndex = timeGrid.Length - 2; timeIndex >= 0; timeIndex--)
            {
                var step = RealInterval.Compact(timeGrid[timeIndex], timeGrid[timeIndex + 1]);
                scheme.Backward(ref payoff, step.Inf, step.Sup);
            }
            var price = payoff.LinearInterpolation(0.0);
            
            var refVol = Math.Sqrt((vol * vol).Integral(0.0).Eval(maturity) / maturity);
            var refPrice = BlackScholesOption.Price(1.0, strike, refVol, maturity, 1.0);
            var error = Math.Abs(price / refPrice - 1.0);
            Assert.AreEqual(0.0, error, 4.0e-4);
        }
        public static IEnumerable BsVolDatas()
        {
            var cstVol = RrFunctions.Constant(0.30);
            yield return new object[] { cstVol, 1.5, 0.5 };
            yield return new object[] {cstVol, 1.5, 1.0};
            yield return new object[] {cstVol, 1.5, 1.5};
        }

        [Test, TestCaseSource("VarianceDatas")]
        public void VarianceTest(RrFunction vol, double maturity)
        {
            RegularGrid1D grid = BuildGrid(vol, maturity, 5.0, 250);

            var pdeCoeff = new TimeOnlyPdeCoeff(vol, -0.5 * vol * vol, RrFunctions.Zero, vol * vol);
            var stepOperator = new FiniteDiff1D(pdeCoeff, BoundaryCondition1D.NoConvexity());
            var scheme = PdeStepSolver.ThetaScheme(stepOperator, 0.5);

            ISlice<RegularGrid1D, double[]> payoff = grid.Exp(0.0);
            payoff.Value.FillValue(0.0);

            var timeGrid = GridUtils.RegularGrid(0.0, maturity, 50);
            for (int timeIndex = timeGrid.Length - 2; timeIndex >= 0; timeIndex--)
            {
                var step = RealInterval.Compact(timeGrid[timeIndex], timeGrid[timeIndex + 1]);
                scheme.Backward(ref payoff, step.Inf, step.Sup);
            }
            var price = payoff.LinearInterpolation(0.0);
            var volVarSwap = Math.Sqrt(price / maturity);

            var refVolVarSwap = Math.Sqrt((vol * vol).Integral(0.0).Eval(maturity) / maturity);
            var error = Math.Abs(volVarSwap / refVolVarSwap - 1.0);
            Assert.AreEqual(0.0, error, 1.0e-13);
            
        }
        public static IEnumerable VarianceDatas()
        {
            var cstVol = RrFunctions.Constant(0.30);
            yield return new object[] {cstVol, 0.5};
            yield return new object[] {cstVol, 2.0};

            var vol = new StepFunction(new[] {0.0, 0.25, 1.0}, new[] {0.25, 0.28, 0.30}, double.NaN);
            yield return new object[] { vol, 0.5 };
            yield return new object[] { vol, 2.0 };
        }

    }

    /// <summary>
    ///  dP/dt + 0.5 * vol * vol * d2P/d2x + drift * dP/dx = rate * P + source
    /// </summary>
    public class TimeOnlyPdeCoeff : PdeCoeffSampler1D<RegularGrid1D>
    {
        #region private fields
        private readonly RrFunction squareVolIntegral;
        private readonly RrFunction driftIntegral;
        private readonly RrFunction rateIntegral;
        private readonly RrFunction sourceIntegral;
        #endregion
        public TimeOnlyPdeCoeff(RrFunction vol, RrFunction drift, RrFunction rate, RrFunction source)
        {
            squareVolIntegral = (vol * vol).Integral(0.0);
            driftIntegral = drift.Integral(0.0);
            rateIntegral = rate.Integral(0.0);
            sourceIntegral = source.Integral(0.0);
        }

        public override void FillCoeff(double start, double end, RegularGrid1D state, ref double[] d2X, ref double[] dX, ref double[] I)
        {
            var stepDiffusion = (squareVolIntegral.Eval(end) - squareVolIntegral.Eval(start)) / (end - start);
            d2X.FillValue(0.5 * stepDiffusion);
            
            var stepDrift = (driftIntegral.Eval(end) - driftIntegral.Eval(start)) / (end - start);
            dX.FillValue(stepDrift);

            var stepRate = (rateIntegral.Eval(end) - rateIntegral.Eval(start)) / (end - start);
            I.FillValue(-stepRate);
        }
        public override void FillSourceTerm(double start, double end, RegularGrid1D state, ref double[] sourceTerm)
        {
            var stepSource = (sourceIntegral.Eval(end) - sourceIntegral.Eval(start)) / (end - start);
            sourceTerm.FillValue(stepSource);
        }
    }

}
