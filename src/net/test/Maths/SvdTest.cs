﻿using System;
using System.Collections;
using NUnit.Framework;
using PragmaticQuant.Core;
using PragmaticQuant.Maths;

namespace test.Maths
{
    [TestFixture]
    public class SvdTest
    {
        [Test, TestCaseSource("Matrix")]
        public void DecompositionTest(double[,] m, double error)
        {
            var svd = new Svd(m);
            var mRecomposed = svd.U.Copy();
            mRecomposed = mRecomposed.Prod(MatrixUtils.Diagonal(svd.W));
            mRecomposed = mRecomposed.Prod(svd.V.Tranpose());

            for(int i=0; i<m.GetLength(0) ; i++)
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    var err = Math.Abs(m[i, j] - mRecomposed[i, j]);
                    Assert.AreEqual(0.0, err, error);
                }
        }
        
        private static IEnumerable Matrix()
        {
            var m1 = new[,]
            {
                {1.0, 0.5, 0.5, 0.5},
                {0.5, 1.0, 0.5, 0.5},
                {0.5, 0.5, 1.0, 0.5},
                {0.5, 0.5, 0.5, 1.0}
            };
            yield return new object[] {m1, 1.0e-15};

            var m2 = new double[10000, 3];
            var rand = new Random(15);
            for (int i = 0; i < m2.GetLength(0); i++)
            {
                var gauss1 = NormalDistribution.FastCumulativeInverse(rand.NextDouble());
                var gauss2 = NormalDistribution.FastCumulativeInverse(rand.NextDouble());

                m2[i, 0] = gauss1;
                m2[i, 1] = gauss2;
                m2[i, 2] = 0.5 * (gauss1 + gauss2);
            }
            yield return new object[] { m2, 5.0e-13 };
        }

        
        [Test, TestCaseSource("SymMatrix")]
        public void SqrtTest(double[,] m, double error)
        {
            var sqrt = m.EigenValueSquareRoot();
            var mRecomposed = sqrt.Prod(sqrt.Tranpose());

            for (int i = 0; i < m.GetLength(0); i++)
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    var err = Math.Abs(m[i, j] - mRecomposed[i, j]);
                    Assert.AreEqual(0.0, err, error);
                }
        }

        private static IEnumerable SymMatrix()
        {
            var m1 = new[,]
            {
                {1.0, 0.5, 0.5, 0.5},
                {0.5, 1.0, 0.5, 0.5},
                {0.5, 0.5, 1.0, 0.5},
                {0.5, 0.5, 0.5, 1.0}
            };
            yield return new object[] { m1, 1.0e-15 };

            var m2 = new[,]
            {
                {1.0, 0.9999, 0.9999, 0.9999},
                {0.9999, 1.0, 0.9999, 0.9999},
                {0.9999, 0.9999, 1.0, 0.9999},
                {0.9999, 0.9999, 0.9999, 1.0}
            };
            yield return new object[] { m2, 1.5e-15 };

            var m3 = new[,]
            {
                {1.0, 1.0, 1.0, 1.0},
                {1.0, 1.0, 1.0, 1.0},
                {1.0, 1.0, 1.0, 1.0},
                {1.0, 1.0, 1.0, 1.0}
            };
            yield return new object[] { m3, 1.0e-15 };

            var m4 = new[,]
            {
                {1.0, 0.5, 1.0, 0.5},
                {0.5, 1.0, 0.5, 0.5},
                {1.0, 0.5, 1.0, 0.5},
                {0.5, 0.5, 0.5, 1.0}
            };
            yield return new object[] { m4, 1.0e-15 };
        } 
    }
}
