﻿using System;
using NUnit.Framework;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets;
using PragmaticQuant.Model.Equity.Dividends;

namespace test.Model
{
    [TestFixture]
    public class AffineDivStrippingTest
    {
        [Test]
        public void Test()
        {
            var refDate = DateTime.Parse("07/06/2009");
            var time = TimeMeasure.Act365(refDate);
            var assetCurve = DiscountCurve.Flat(null, refDate, 0.04);
            var divStripper = new AffineDivModelStripping(3.14, assetCurve, time);

            var divDates = EnumerableUtils.For(1, 60, i => refDate + i * Duration.Month);
            var fwdDivs = new RawMapDatas<DateTime, double>(divDates, ArrayUtils.Constant(divDates.Length, 0.05 / 12.0));
            
            //Check consistency
            var affineDivs = divStripper.StripAffineDiv(fwdDivs, t => 1.0 - Math.Exp(-t / 2.0));
            var affineRestrippedFwds = divStripper.StripForwardDivs(affineDivs);
            for (int i = 0; i < affineDivs.Length; i++)
            {
                Assert.AreEqual(fwdDivs.Values[i], affineRestrippedFwds.Values[i]);
            }
        }

        [Test]
        public void FullCashTest()
        {
            var refDate = DateTime.Parse("07/06/2009");
            var time = TimeMeasure.Act365(refDate);
            var assetCurve = DiscountCurve.Flat(null, refDate, 0.04);
            var divStripper = new AffineDivModelStripping(3.14, assetCurve, time);

            var divDates = EnumerableUtils.For(1, 60, i => refDate + i * Duration.Month);
            var fwdDivs = new RawMapDatas<DateTime, double>(divDates, ArrayUtils.Constant(divDates.Length, 0.05 / 12.0));

            //Check full cash consistency
            var fullCash = divStripper.StripAffineDiv(fwdDivs, t => 0.0);
            for (int i = 0; i < fullCash.Length; i++)
            {
                Assert.AreEqual(fwdDivs.Values[i], fullCash[i].Cash);
                Assert.AreEqual(0.0, fullCash[i].Yield);
            }
        }

        [Test]
        public void FullYieldTest()
        {
            var refDate = DateTime.Parse("07/06/2009");
            var time = TimeMeasure.Act365(refDate);
            var assetCurve = DiscountCurve.Flat(null, refDate, 0.04);
            var divStripper = new AffineDivModelStripping(3.14, assetCurve, time);

            var divDates = EnumerableUtils.For(1, 60, i => refDate + i * Duration.Month);
            var fwdDivs = new RawMapDatas<DateTime, double>(divDates, ArrayUtils.Constant(divDates.Length, 0.05 / 12.0));
            
            //Check full yield consistency
            var fullYield = divStripper.StripAffineDiv(fwdDivs, t => 1.0);
            var fyRestrippedFwds = divStripper.StripForwardDivs(fullYield);
            for (int i = 0; i < fullYield.Length; i++)
            {
                Assert.AreEqual(fwdDivs.Values[i], fyRestrippedFwds.Values[i]);
                Assert.AreEqual(0.0, fullYield[i].Cash);
            }
            
        }
    }
}
