﻿using System;
using NUnit.Framework;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets;
using PragmaticQuant.Model.Equity;
using PragmaticQuant.Model.Equity.Dividends;

namespace test.Model
{
    [TestFixture]
    public class BlackDivImpliedVolTest
    {
        [Test, TestCaseSource(typeof (BsDivModelTestDatas), "AssetMarket")]
        public void SampleStrike(AssetMarket assetMkt, DiscountCurve zcCurve)
        {
            DateTime maturity = assetMkt.RefDate + 3 * Duration.Year;
            var fwd = assetMkt.ForwardCurve().Fwd(maturity);
            double t = assetMkt.Time[maturity];
            var affineDiv = EquityModelUtils.AffineDivModel(d => 0.2, assetMkt);
            var pricer = BlackScholesWithDividendOption.Build(assetMkt.Spot, affineDiv, zcCurve, assetMkt.Time);
            
            var vols = GridUtils.RegularGrid(0.05, 1.0, 50);
            var moneynesses = GridUtils.RegularGrid(-10.0, 10.0, 51);
            foreach (double vol in vols)
            {
                var stdDev = vol * Math.Sqrt(t);
                foreach (double m in moneynesses)
                {
                    var strike = fwd * Math.Exp(stdDev * m);
                    var price = pricer.Price(t, strike, vol, m > 0 ? 1 : -1);
                    var impliedVol = pricer.ImpliedVol(t, strike, price, m > 0 ? 1 : -1);

                    var errRelative = (impliedVol - vol) / vol;
                    Assert.AreEqual(0.0, Math.Abs(errRelative), 2.0e-11);
                }
            }
            
        }

        [Test, TestCaseSource(typeof(BsDivModelTestDatas), "AssetMarket")]
        public void CalibrationTest(AssetMarket assetMkt, DiscountCurve zcCurve)
        {
            var maturities = new [] {3 * Duration.Month, 6 * Duration.Month, Duration.Year, 2 * Duration.Year, 3 * Duration.Year, 4 * Duration.Year, 5 * Duration.Year};
            var vols = new[] {0.20, 0.15, 0.20, 0.15, 0.20, 0.18, 0.20};
            
            var calibDates = assetMkt.Time[maturities.Map(p => assetMkt.RefDate + p)];
            var strikes = ArrayUtils.Constant(calibDates.Length, assetMkt.Spot);
            var affineDiv = EquityModelUtils.AffineDivModel(d => 0.2, assetMkt);
            var pricer = BlackScholesWithDividendOption.Build(assetMkt.Spot, affineDiv, zcCurve, assetMkt.Time);
            var targetPrices = EnumerableUtils.For(0, calibDates.Length, 
                i => pricer.Price(calibDates[i], strikes[i], vols[i], 1.0));

            var calibVols = pricer.CalibrateVol(calibDates, targetPrices, assetMkt.Spot, 1.0);

        }

    }
}
