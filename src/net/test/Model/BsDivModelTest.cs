﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using PragmaticQuant.Core.Sobol;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets;
using PragmaticQuant.Maths;
using PragmaticQuant.Model;
using PragmaticQuant.Model.Equity.BlackScholes;
using PragmaticQuant.Model.Rate;
using PragmaticQuant.Pricing;
using PragmaticQuant.Product;
using PragmaticQuant.DslPayoff;

namespace test.Model
{

    [TestFixture]
    public class BsDivModelTest
    {
        #region private fields
        private static IProduct ForwardLeg(DateTime[] fwdDates)
        {
            var legDesc = fwdDates.Map(d =>
            {
                var legParam = new Dictionary<string, object>
                {
                    {"fixingDate", d},
                    {"stock", "asset(Stoxx50)"}
                };
                return new DslCouponDescription("FwdLeg", d, Currency.Eur, "stock@fixingDate", legParam);
            });
            var legCpns = new DslPayoffFactory().BuildCoupons(legDesc);
            return CouponLinearCombination.Create(legCpns);
        }
        #endregion
        
        [Test, TestCaseSource(typeof(BsDivModelTestDatas), "AssetMarket")]
        public void CheckZeroVolForward(AssetMarket assetMkt, DiscountCurve zcCurve)
        {
            var convs = new DefaultMarketConventions(new Dictionary<AssetId, Currency> {{assetMkt.Asset, zcCurve.Financing.Currency}});
            var market = new Market(new[] { zcCurve }, new[] { assetMkt }, convs);
            var affineDiv = new RawMapDatas<DateOrDuration, double>(new[] {new DateOrDuration(50 * Duration.Year)}, new[] {0.2});
            var zeroVol = new RawMapDatas<DateOrDuration, double>(new[] {new DateOrDuration(50 * Duration.Year)}, new[] {0.0});
            var blackScholesDesc = new BlackScholesModelDescription(assetMkt.Asset, assetMkt.AssetCurrency, affineDiv, zeroVol);
            var detRates = new DeterministicRateDescription(assetMkt.AssetCurrency);
            var blackScholesModel = ModelFactory.Instance.Build(new ModelDescription(new RiskFactorCorrelation(), blackScholesDesc, detRates), market);
            var mcConfig = new MonteCarloConfig(1, false, null, RandomGenerators.GaussianSobol(SobolDirection.Kuo3));
            
            var fwdDates = new[] {Duration.Month, 6 * Duration.Month, Duration.Year, 2 * Duration.Year, 5 * Duration.Year}
                                .Map(d => assetMkt.RefDate + d);
            
            IProduct fwdLeg = ForwardLeg(fwdDates);
            var pricingResult = McPricers.McWithDetails(mcConfig).Price(fwdLeg, blackScholesModel, market);
            PriceDetails priceDetails = pricingResult.GetDetails<PriceDetails>();
            double[] fwds = priceDetails.Details.Map(kv => kv.Item3.Value);

            var assetFwdCurve = assetMkt.ForwardCurve();
            double[] refFwds = fwdDates.Map(d => assetFwdCurve.Fwd(d) * zcCurve.Zc(d));

            foreach (var i  in Enumerable.Range(0, fwdDates.Length))
            {
                var err = Math.Abs(fwds[i] / refFwds[i] - 1.0);
                Assert.LessOrEqual(err, 20.0 * DoubleUtils.MachineEpsilon);
            }
        }

    }

    public class BsDivModelTestDatas
    {
        public static IEnumerable AssetMarket
        {
            get
            {
                var refDate = DateTime.Parse("07/06/2009");
                var asset = new AssetId("Stoxx50");
                var time = TimeMeasure.Act365(refDate);

                var zcCurve = DiscountCurve.Flat(FinancingId.Ois(Currency.Eur), refDate, 0.01);
                var assetCurve = DiscountCurve.Flat(FinancingId.AssetCollat(Currency.Eur, asset), refDate, 0.04);
                
                var divDates = EnumerableUtils.For(1, 60, i => refDate + i * Duration.Month);
                var fwdDivs = new RawMapDatas<DateTime, double>(divDates, ArrayUtils.Constant(divDates.Length, 0.05 / 12.0));

                var assetMarket = new AssetMarket(asset, time, 1.0, assetCurve, fwdDivs, null);
                yield return new object[] { assetMarket, zcCurve };
            }
        }
    }
    
}
