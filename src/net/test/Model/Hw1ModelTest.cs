﻿using System;
using System.Collections;
using System.Linq;
using NUnit.Framework;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Maths.Integration;
using PragmaticQuant.Model.Rate.HullWhite;
using PragmaticQuant.MonteCarlo;

namespace test.Model
{
    [TestFixture]
    public class Hw1ModelTest
    {
        [Test, TestCaseSource(typeof (Hw1ModelTestDatas), "Zc")]
        public void Zc(double lambda, Duration zcStart, Duration zcDuration, int quadratureNbPoints, double precision)
        {
            var sigma = new StepFunction(new[] {0.0, 1.0, 2.0}, new[] {0.007, 0.004, 0.0065}, 0.0);
            var refDate = DateTime.Parse("07/06/2009");

            var hw1 = HwnModel.Hw1(TimeMeasure.Act365(refDate), Currency.Eur, lambda, sigma);
            var hw1Zc = new HwnModelZcRepresentation(hw1);

            var zcDate = refDate + zcStart;
            var zc = hw1Zc.Zc(zcDate, zcDate + zcDuration, 1.0);

            var drift = hw1.DrivingOrnsteinProcess(new PaymentInfo(Currency.Eur, refDate))
                           .Drifts.First();
            var stdDev = Math.Sqrt(drift.Eval(hw1.Time[zcDate]));

            double[] x, w;
            GaussHermite.GetQuadrature(quadratureNbPoints, out x, out w);
            double zcQuad = x.Select((t, i) => w[i] * zc.Eval(new[] {stdDev * t})).Sum();

            var error = Math.Abs(zcQuad - 1.0);
            Assert.LessOrEqual(error, precision);
        }

        [Test, TestCaseSource(typeof (Hw1ModelTestDatas), "ProbaMeasure")]
        public void ProbaMeasure(double lambda, RrFunction sigma, Duration probaMaturity, Duration simulationMaturity,
            int quadratureNbPoints, double precision)
        {
            var refDate = DateTime.Parse("07/06/2009");
            var hw1 = HwnModel.Hw1(TimeMeasure.Act365(refDate), Currency.Eur, lambda, sigma);
            var hw1Model = new PragmaticQuant.Model.Model(null, hw1);

            var probaMeasure = new PaymentInfo(hw1.Currency, refDate + probaMaturity);
            var t = refDate + simulationMaturity;

            var pathGenFactory = new HwnModelPathGeneratorFactory();
            var hw1PathGen = pathGenFactory.Build(hw1Model, probaMeasure, new[] { t });
            var gaussFactor = pathGenFactory.GaussianFactors(hw1Model, probaMeasure);
            var gaussFactorPath = GaussianFactorPath.Build(gaussFactor, hw1PathGen.AllSimulatedDates, hw1PathGen.Dates);

            var hw1Zc = new HwnModelZcRepresentation(hw1);
            var numeraireZc = hw1Zc.Zc(t, probaMeasure.Date, 1.0);

            Assert.AreEqual(hw1PathGen.AllSimulatedDates.Length, 1);

            double[] x, w;
            GaussHermite.GetQuadrature(quadratureNbPoints, out x, out w);

            double flow = 0.0;
            var dw = new double[1][];
            dw[0] = new double[1];
            for (int i = 0; i < x.Length; i++)
            {
                gaussFactorPath.GaussianFactorIncrements(ref dw, new[] {x[i]});
                var ornstein = hw1PathGen.Path(dw).GetProcessValue(0);
                flow += w[i] * 1.0 / numeraireZc.Eval(ornstein);
            }

            var error = Math.Abs(flow - 1.0);
            Assert.LessOrEqual(error, precision);
        }
    }

    internal class Hw1ModelTestDatas
    {
        public static IEnumerable Zc
        {
            get
            {
                yield return new object[] {0.1, 10 * Duration.Year, 30 * Duration.Year, 15, DoubleUtils.MachineEpsilon};
                yield return new object[] {0.01, 30 * Duration.Year, 10 * Duration.Year, 15, 2 * DoubleUtils.MachineEpsilon};
                yield return new object[] {0.0, 30 * Duration.Year, 30 * Duration.Year, 15, DoubleUtils.MachineEpsilon};
            }
        }
        public static IEnumerable ProbaMeasure
        {
            get
            {
                yield return new object[] { 0.0, new StepFunction(new[] { 0.0 }, new[] { 0.01 }, 0.0), 30 * Duration.Year, 10 * Duration.Year, 15, 1.0e-15 };
                yield return new object[] {0.001, new StepFunction(new[] { 0.0 }, new[] { 0.01 }, 0.0), 30 * Duration.Year, 10 * Duration.Year, 15, 2.5e-13};
                yield return new object[] { 0.1, new StepFunction(new[] { 0.0 }, new[] { 0.015 }, 0.0), 30 * Duration.Year, 10 * Duration.Year, 15, 1.0e-15 };
                yield return new object[] {0.1, new StepFunction(new[] {0.0}, new[] {0.015}, 0.0), 30 * Duration.Year, 10 * Duration.Year, 15, 1.0e-15};
                yield return new object[] {0.1, new StepFunction(new[] {0.0, 5.0}, new[] {0.015, 0.010}, 0.0), 30 * Duration.Year, 10 * Duration.Year, 15, 1.0e-15};
                yield return new object[] {0.01, new StepFunction(new[] {0.0, 1.0, 2.0}, new[] {0.007, 0.004, 0.0065}, 0.0), 30 * Duration.Year, 10 * Duration.Year, 15, 1.6e-15};
            }
        }
    }
}
