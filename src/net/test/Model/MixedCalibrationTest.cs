﻿using System;
using NUnit.Framework;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets;
using PragmaticQuant.Markets.RawDatas;
using PragmaticQuant.Model.Equity.Bergomi;
using PragmaticQuant.Model.Equity.BergomiMixedLv;
using System.IO;
using PragmaticQuant.Basic;
using PragmaticQuant.Model;
using PragmaticQuant.Pricing;

namespace test.Model
{
    [TestFixture]
    public class MixedCalibrationTest
    {
        [Test]
        public void MixedLvVanillaRepricing()
        {
            string mktDir = Path.GetDirectoryName(System.Reflection.Assembly.GetCallingAssembly().Location);
            var mktPath = Path.Combine(mktDir, "mkt.txt");
            var jsonMkt = File.ReadAllText(mktPath);
            var mktDatas = MarketJsonParser.ParseMarket(jsonMkt);

            var mixedCalibrationDescription = BergomiMixedLvCalibDesc.Create("SP500",
                                                    RawMapDatasUtils.Constant(1.0, (DateOrDuration) (50 * Duration.Year)),
                                                    new StochasticVolParams(5.35, 0.35, 0.2435, 1.90, -0.3219, -0.41, -0.65),
                                                    5000);
            var pricingConfig = new PricingConfig
            {
                Market = new MarketConfig { MarketDatas = mktDatas },
                Model = mixedCalibrationDescription,
                NumericalMethod = new MonteCarloConfig(50000, true)
            };

            var mkt = mktDatas.ToMarket();
            var equityMarket = mkt.AssetMarket(new AssetId("SP500"));
            var spot = equityMarket.Spot;

            var maturities = new DateOrDuration[] { 6 * Duration.Month, Duration.Year, 2 * Duration.Year, 5 * Duration.Year};
            var strikes = new[] {spot * 0.90, spot, spot * 1.10};

            var vols = VanillaViewer.EquitySmile(pricingConfig, "SP500", maturities, strikes);
            var volSurface = equityMarket.VolSurface();

            for (int m = 0; m < maturities.Length; m++)
            {
                for (int k = 0; k < strikes.Length; k++)
                {
                    var refVol = volSurface.Volatility(maturities[m].ToDate(mkt.RefDate), strikes[k]);
                    var err = Math.Abs(vols[m, k] - refVol);
                    Assert.LessOrEqual(err, 0.0017);
                }
            }
        }

    }
}
