﻿using System;
using System.Collections;
using NUnit.Framework;
using PragmaticQuant.Model;
using PragmaticQuant.Model.Equity;

namespace test.Model
{
    [TestFixture]
    public class SmileLawUtilTest
    {
        [Test, TestCaseSource("SmileDatas")]
        public void QuantileTest(Func<double, double> vol, double maturity, double[] probas)
        {
            foreach (var proba in probas)
            {
                var q = SmileLawUtils.ComputeQuantile(1.0, maturity, vol, proba);
                var probaResult = SmileLawUtils.Cumulative(1.0, maturity, vol, q);
                var error = Math.Abs(probaResult / proba - 1.0);
                Assert.AreEqual(0.0, error, 1.3e-8);
            }
        }

        public static IEnumerable SmileDatas 
        {
            get
            {
                yield return new object[] {SabrFormulas.Smile(0.20, 0.55 / 2.0, 0.40), 1.0, new[] {1.0e-6, 1.0 - 1.0e-6}};
                yield return new object[] { SabrFormulas.Smile(0.20, 0.55, 0.40), 0.25, new[] { 1.0e-6, 1.0 - 1.0e-6 } };
            }
        }


    }
}
