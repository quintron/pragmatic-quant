﻿using System;
using System.Linq;
using NUnit.Framework;
using PragmaticQuant.Core.Sobol;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets;
using PragmaticQuant.Maths;
using PragmaticQuant.Maths.Function;
using PragmaticQuant.Model;
using PragmaticQuant.Model.Rate.HullWhite;
using PragmaticQuant.Pricing;
using PragmaticQuant.Product;
using PragmaticQuant.Product.Fixings;

namespace test.MonteCarlo
{
    [TestFixture]
    public class Hw1FixedLegTest
    {
        #region private methods
        private static Market Market()
        {
            var refDate = DateTime.Parse("07/06/2009");
            var time = TimeMeasure.Act365(refDate);
            var pillars = new[] {refDate + Duration.Year, refDate + 2 * Duration.Year, refDate + 3 * Duration.Year, refDate + 5 * Duration.Year};
            var zcRates = new[] { 0.0010, 0.003, 0.005, 0.008};
           
            DiscountCurve discountCurve = DiscountCurve.LinearRateInterpol(
                FinancingId.Ois(Currency.Eur), new RawMapDatas<DateTime, double>(pillars, zcRates), time);
            var market = new Market(new[] {discountCurve}, new AssetMarket[0], new DefaultMarketConventions());
            return market;
        }
        private static ICouponDecomposable FixedLeg(DateTime refDate)
        {
            const int mat = 10;
            var dates = Enumerable.Range(0, mat).Select(i => refDate + (i + 1) * Duration.Year);
            var coupons = dates.Map(d => new Coupon(string.Format("FixedCoupon {0}",d), new PaymentInfo(Currency.Eur, d),
                new GenericFixingFunction(f => 1.0)));
            return CouponLinearCombination.Create(coupons); 
        }
        #endregion

        [Test]
        public void Test()
        {
            var market = Market();

            const double lambda = 0.01;
            var sigma = new StepFunction(new[] {0.0, 1.0, 2.0}, new[] {0.007, 0.004, 0.0065}, 0.0);
            var hw1 = HwnModel.Hw1(TimeMeasure.Act365(market.RefDate), Currency.Eur, lambda, sigma);
            var hw1Model = new PragmaticQuant.Model.Model(null, hw1); 
            var mcConfig = new MonteCarloConfig(20000, false, null, RandomGenerators.GaussianSobol(SobolDirection.JoeKuoD5));
            var mcPricer = McPricers.McWithDetails(mcConfig);
            
            var fixedLeg = FixedLeg(market.RefDate);
            var mcPriceResult = (PricingResult)mcPricer.Price(fixedLeg, hw1Model, market);
            PriceDetails priceDetails = mcPriceResult.GetDetails<PriceDetails>();

            var mcCoupons = priceDetails.Details.Map(p => p.Item3.Value);
            var refCoupons = priceDetails.Details.Map(pi => market.DiscountCurve(pi.Item2.Financing).Zc(pi.Item2.Date));

            var errAbs = Math.Abs(mcCoupons.Sum() - refCoupons.Sum());
            Assert.LessOrEqual(errAbs, 7.0e-5);

            var errRel = Math.Abs(mcCoupons.Sum() / refCoupons.Sum() - 1.0);
            Assert.LessOrEqual(errRel, 8.0e-6);
        }
    }
}
