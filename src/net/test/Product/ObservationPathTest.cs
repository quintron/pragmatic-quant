﻿using System;
using NUnit.Framework;
using PragmaticQuant.Basic;
using PragmaticQuant.MonteCarlo.Product;
using PragmaticQuant.Product.Fixings;

namespace test.Product
{
    [TestFixture]
    public class ObservationPathTest
    {
        [Test]
        public void PathEvaluator()
        {
            var sp500 = ObservationProcesses.Equity.Spot("SP500");
            var sx5E = ObservationProcesses.Equity.Spot("STOXX50");
            var constatDates = new[] {new DateTime(2017, 01, 11), new DateTime(2018, 01, 11)};
            var wofFixings = constatDates.Map(d => new[] {sp500.Observation(d), sx5E.Observation(d)});
            var wof = new WorstOfFixingFunction(wofFixings, new[] {1.0, 1.0});
            const double strike = 0.6;
            var putWof = new CapFloorPayoff(wof, double.PositiveInfinity, 0.0, strike, -1.0);

            var putwofEvaluator = ObservationPathEvaluator.Build(putWof);
            Func<double[][], double> putwofEval = fixingPath =>
            {
                var putwofValue = new double[1];
                putwofEvaluator.FillObservations(ref putwofValue, fixingPath);
                return putwofValue[0];
            };

            Assert.AreEqual(strike - 0.4, putwofEval(new[] { new[] { 0.4, 0.5 }, new[] { 1.0, 0.8 } }));
            Assert.AreEqual(strike - 0.1, putwofEval(new[] { new[] { 0.7, 0.5 }, new[] { 1.0, 0.1 } }));
            Assert.AreEqual(0.0, putwofEval(new[] { new[] { 0.7, 0.6 }, new[] { 2.0, 0.8 } }));
        }
    }
}
