using System.Linq;
using NUnit.Framework;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Product.Fixings;

namespace test.Product
{

    public class ObservationScheduleTest
    {
        [TestCase("2015/04/30", "2020/02/02", "1m")]
        [TestCase("2015/04/30", "2020/02/02", "1d")]
        [TestCase("2015/05/02", "2020/02/02", "1d")]
        [TestCase("2015/05/02", "2020/02/02", "1m")]
        public void CheckStartEnd(string start, string end, string perio)
        {
            var startDate = DateAndDurationConverter.ConvertDate(start);
            var endDate = DateAndDurationConverter.ConvertDate(end);
            var period = DateAndDurationConverter.ConvertDuration(perio);

            var schedule = ObservationProcessUtils.Schedule(startDate, endDate, period);
            Assert.AreEqual(startDate, schedule.First());
            Assert.AreEqual(endDate, schedule.Last());
            Assert.IsTrue(!schedule.Any(d => d < startDate || d > endDate));
        }
    }
}