using System;
using System.Collections.Generic;
using NUnit.Framework;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Basic.Structure;
using PragmaticQuant.Markets.RawDatas;
using PragmaticQuant.Model;
using PragmaticQuant.Model.Equity.BlackScholes;
using PragmaticQuant.Pricing;
using PragmaticQuant.Product;
using PragmaticQuant.Product.Fixings;

namespace test.Product
{
    [TestFixture]
    public class PastFixingTest
    {
        //[Test]
        public void PastLegTest()
        {
            //var refDate = new DateTime(2015, 06, 07);
            //var fixingValues = new Dictionary<IFixing, double>
            //{
            //    {new Cms(Currency.Eur, refDate, 10 * Duration.Year), 0.025},
            //    {new Libor(Currency.Usd, refDate, 3 * Duration.Month), 0.01},
            //    {new EquitySpot(refDate, new AssetId("STOX5E")), 3500.0},
            //    {new Cms(Currency.Eur, refDate + Duration.Year, 10 * Duration.Year), 0.015},
            //    {new Libor(Currency.Usd, refDate + Duration.Year, 3 * Duration.Month), 0.02},
            //    {new EquitySpot(refDate + Duration.Year, new AssetId("STOX5E")), 3800.0},
            //};

            //var payDate = new DateTime(2017, 06, 07);
            //var payInfo = new PaymentInfo(Currency.Eur, payDate);
            //var cpns = fixingValues.Keys.Map(f => new Coupon(f.ToString(), payInfo, new GenericFixingFunction(x => x[0], f)));
            //var leg = CouponLinearCombination.Create(cpns);


            //var mkt = new MarketConfig
            //{
            //    Market = new RawMarket
            //    {
            //        RefDate = payDate,
            //        DiscountCurves = new[] { new DiscountCurveDatas { Currency = "EUR"} },
            //        Assets = new[] {new AssetMarketDatas {Name = "STOX5E", Spot = 1.0, Currency = "EUR"}},
            //        HistoricalFixings = new LabelledMatrix<DateTime, string, double>(fixingValues.Map(f=>f.Key.Date),   )
            //    }
            //};

            //var pricingConfig = new PricingConfig
            //{
            //    Market = mkt,
            //    Model = new BlackScholesModelCalibDesc("STOX5E", RawMapDatasUtils.Constant(1.0, (DateOrDuration) (0 * Duration.Year)), new DateOrDuration[] { Duration.Month }),
            //    NumericalMethod = new MonteCarloConfig(1, false),
            //    Product = leg
            //};

            //var priceResult = new PriceComputationTask().Price(pricingConfig);

            //Assert.IsTrue(priceResult != null);

        }
    }

}