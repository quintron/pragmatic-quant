﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.MonteCarlo;
using PragmaticQuant.MonteCarlo.Product;
using PragmaticQuant.Product;
using PragmaticQuant.Product.Fixings;
using PragmaticQuant.DslPayoff;

namespace test.Product
{

    [TestFixture]
    public class ProductPathFlowTest
    {
        #region private methods
        private static void CheckPathFlow(IProductPathFlow productPathFlow, double[][] fixingPath, double[] expectedFlowValue)
        {
            var pathFlows = productPathFlow.NewPathFlow();

            var numeraireFlows = new PathFlows<double[], PaymentInfo[]>(
                                        productPathFlow.Payments.Map(ps => ArrayUtils.Constant(ps.Length, 1.0)),
                                        productPathFlow.Payments);
            productPathFlow.ComputePathFlows(ref pathFlows,
                new PathFlows<double[], IObservation[]>(fixingPath, productPathFlow.Fixings),
                numeraireFlows);

            Assert.AreEqual(expectedFlowValue.Length, pathFlows.Flows.Length);
            for (int i = 0; i < expectedFlowValue.Length; i++)
            {
                Assert.AreEqual(expectedFlowValue[i], pathFlows.Flows[i]);
            }
        }
        #endregion

        [Test]
        public void PdiTest()
        {
            const double strike = 0.80;
            const double ki = 0.30;
            DateTime date = new DateTime(2017, 08, 11);
            Currency currency = Currency.Usd;
            var sp500 = ObservationProcesses.Equity.Spot("SP500");
            var eurostox = ObservationProcesses.Equity.Spot("STOXX50");
            var worst = new WorstOfProcess(new DateTime(2017, 01, 11), Duration.Month,
                                           new[] { eurostox, sp500 }, new[] { 1.0, 1.0 });

            var cpnDesc = new DslCouponDescription("PDI", date, currency,
                " Max(0.0, 1.0 - Index@FixingDate/Strike ) * (KI_Index@FixingDate < KI ? 1.0 : 0.0 )");
            cpnDesc.Payoff.Parameters.Add("Strike", strike);
            cpnDesc.Payoff.Parameters.Add("Index", sp500);
            cpnDesc.Payoff.Parameters.Add("KI_Index", worst);
            cpnDesc.Payoff.Parameters.Add("KI", ki);
            cpnDesc.Payoff.Parameters.Add("FixingDate", date);

            var pdi = new DslPayoffFactory().BuildCoupons(cpnDesc).Single();
            Assert.AreEqual(1.0 - 0.7 / 0.80, pdi.Payoff.Value(new[] { 0.7, 0.29 }));
            Assert.AreEqual(0.0, pdi.Payoff.Value(new[] { 0.7, 0.31 }));

            IProductPathFlow pdiPathFlow = ProductPathFlowFactory.ProductPathFlow(pdi);
            
            double[][] fixingPath1 = 
            {
                new[] {1.0, 0.9},
                new[] {1.0, 0.8},
                new[] {1.0, 0.7},
                new[] {1.0, 0.6},
                new[] {1.0, 0.5},
                new[] {1.0, 0.3},
                new[] {1.0, 0.2},
                new[] {0.7, 0.5}
            };
            double[] expectedFlowValue1 = { Math.Max(0.0, 1.0 - 0.7 / 0.80) };
            CheckPathFlow(pdiPathFlow, fixingPath1, expectedFlowValue1);

            double[][] fixingPath2 = 
            {
                new[] {1.0, 0.9},
                new[] {1.0, 0.8},
                new[] {1.0, 0.7},
                new[] {1.0, 0.6},
                new[] {1.0, 0.5},
                new[] {1.0, 0.31},
                new[] {1.0, 0.31},
                new[] {0.7, 0.5}
            };
            double[] expectedFlowValue2 = { 0.0 };
            CheckPathFlow(pdiPathFlow, fixingPath2, expectedFlowValue2);
        }

        [Test]
        public void AutocallTest()
        {
            var terminalCoupon = new Coupon("TerminalCoupon", Currency.Eur, new DateTime(2025, 08, 12), new GenericFixingFunction(x => 3.0));

            var triggerItems = new List<TriggerItem>();
            var underlying = ObservationProcesses.Equity.Spot("SP500");

            var triggerDate1 = new DateTime(2020, 08, 12);
            var fee1 = new Coupon("Fee1", Currency.Eur, triggerDate1, new GenericFixingFunction(x => 1.0));
            var trigger1 = new GenericFixingFunction(u => (u[0] > 1.1) ? 1.0 : 0.0, underlying.Observation(triggerDate1));
            triggerItems.Add(new TriggerItem(triggerDate1, 0 * Duration.Year, fee1, trigger1));

            var triggerDate2 = new DateTime(2021, 08, 12);
            var fee2 = new Coupon("Fee2", Currency.Eur, triggerDate2, new GenericFixingFunction(x => 2.0));
            var trigger2 = new GenericFixingFunction(u => (u[0] > 1.1) ? 1.0 : 0.0, underlying.Observation(triggerDate2));
            triggerItems.Add(new TriggerItem(triggerDate2, 0 * Duration.Year, fee2, trigger2));
            
            var autocall = new AutoCall(terminalCoupon, triggerItems.ToArray());
            IProductPathFlow autocallPathFlow = ProductPathFlowFactory.ProductPathFlow(autocall);

            double[][] path1 = { new[] { 1.0 }, new[] { 1.0 } };
            double[] expectedFlowValue1 = { 3.0, 0.0, 0.0 };
            CheckPathFlow(autocallPathFlow, path1, expectedFlowValue1);

            double[][] path2 = { new[] { 1.2 }, new[] { 1.0 } };
            double[] expectedFlowValue2 = { 0.0, 1.0, 0.0 };
            CheckPathFlow(autocallPathFlow, path2, expectedFlowValue2);

            double[][] path3 = { new[] { 1.0 }, new[] { 1.2 } };
            double[] expectedFlowValue3 = { 0.0, 0.0, 2.0 };
            CheckPathFlow(autocallPathFlow, path3, expectedFlowValue3);

        }

    }

}
