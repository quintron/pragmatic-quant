﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using PragmaticQuant.Basic;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.Markets.Rate;
using PragmaticQuant.MonteCarlo.Product;
using PragmaticQuant.Product.Fixings;
using System;
using System.Collections.Generic;

namespace benchmark
{
    public class SwapPathBenchmark
    {
        private ObservationPathEvaluator swapEval;
        private double[][] fixings;
        private int nbObservations;

        [GlobalSetup]
        public void SetUp()
        {
            var start = new DateTime(2018, 3, 31);
            var end = start + NbYears * Duration.Year;

            var couponPayoffs = new List<IObservation>();

            var conv = SwapConventions.For(Currency.Usd);
            var fixedLeg = conv.FixedLegConvention.BuildLeg(start, end);
            for (int i = 0; i < fixedLeg.Schedule.Length; i++)
            {
                var cpn = fixedLeg.DayFracs[i] * 0.05;
                var fixedCpn = new WeightedFixingFunction(-1.0, new GenericFixingFunction(x => cpn));
                couponPayoffs.Add(fixedCpn);
            }
            var floatLeg = conv.FloatLegConvention.BuildLeg(start, end);
            for (int i = 0; i < fixedLeg.Schedule.Length; i++)
            {
                var libor = new Libor(Currency.Usd, floatLeg.Schedule[i].Fixing, 3 * Duration.Month);
                var dcf = fixedLeg.DayFracs[i];
                var floatCpn = new GenericFixingFunction(l => l[0] * dcf, libor);
                couponPayoffs.Add(floatCpn);
            }            
            swapEval = ObservationPathEvaluator.Build(couponPayoffs.ToArray());

            nbObservations = swapEval.Observations.Length;
            fixings = new double[swapEval.Fixings.Length][];
            var rand = new Random(42);
            for (int i = 0; i < fixings.Length; i++)
                fixings[i] = new[] { rand.NextDouble() };
        }

        [Params(10, 20, 30)]
        public int NbYears { get; set; }

        [Benchmark]
        public double[] SwapEval()
        {
            var cpnValues = new double[nbObservations];
            swapEval.FillObservations(ref cpnValues, fixings);
            return cpnValues;
        }
    }
}
