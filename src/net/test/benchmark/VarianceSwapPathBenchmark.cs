﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Attributes.Jobs;
using BenchmarkDotNet.Running;
using PragmaticQuant.Basic.Dates;
using PragmaticQuant.MonteCarlo.Product;
using PragmaticQuant.Product.Fixings;
using System;

namespace benchmark
{
    //[LegacyJitX86Job, LegacyJitX64Job, RyuJitX64Job]
    //[ClrJob, MonoJob, CoreJob]
    public class VarianceSwapPathBenchmark
    {
        private ObservationPathEvaluator varianceEval;
        private double[][] fixings;

        private IFixingFunction variance;
        private double[] flatFixings;

        [GlobalSetup]
        public void SetUp()
        {
            var start = new DateTime(2018, 3, 31);
            var end = start + NbYears * Duration.Year;
            variance = (IFixingFunction) ObservationProcesses.Equity.Variance("SP500", start)
                                                                    .Observation(end);
            varianceEval = ObservationPathEvaluator.Build(variance);

            fixings = new double[varianceEval.Fixings.Length][];
            flatFixings = new double[varianceEval.Fixings.Length];
            var rand = new Random(42);
            for (int i = 0; i < fixings.Length; i++)
            {
                var val = rand.NextDouble();
                fixings[i] = new[] { val };
                flatFixings[i] = val;
            }                
        }

        [Params(1, 4, 8)]       
        public int NbYears { get; set; }

        [Benchmark]
        public double[] VarianceSwapEval()
        {
            var varianceValue = new double[1];
            varianceEval.FillObservations(ref varianceValue, fixings);
            return varianceValue;
        }
        [Benchmark]
        public double[] VarianceSwapEvalRef()
        {
            var inputFixings = new double[varianceEval.Fixings.Length];
            for (int i = 0; i < fixings.Length; i++)
            {
                inputFixings[i] = fixings[i][0];
            }
            return new[] { variance.Value(inputFixings) };
        }
        [Benchmark]
        public double VarianceSwapEvalMin()
        {
            return variance.Value(flatFixings);
        }
    }
}
