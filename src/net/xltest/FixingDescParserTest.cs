﻿using NUnit.Framework;
using PragmaticQuant.Xl;

namespace xltest
{
    [TestFixture]
    public class FixingDescParserTest
    {
        [Test]
        public void Test()
        {
            var fixing1 = FixingDescParser.ParseFixing("10y10y - 5ySp500");
            var fixing2 = FixingDescParser.ParseFixing("0,5 *(1y10y - 1y2y) + 0.5 *(2y10y - 2y2y)");
        }
    }
}
