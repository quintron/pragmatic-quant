"""
"""

from os import path as os_path
from sys import path
import clr

PACKAGE_PATH = os_path.dirname(__file__)
if not PACKAGE_PATH in path:
    path.insert(0, PACKAGE_PATH)

clr.AddReference('System.Collections')
clr.AddReference('PragmaticQuant')
