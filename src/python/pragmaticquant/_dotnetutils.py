from datetime import date, datetime

# Pythonnet import of dot net types
from System import Double, DateTime, Array
from PragmaticQuant.Basic import Currency
from PragmaticQuant.Basic.Dates import DateOrDuration
from PragmaticQuant.Basic.Dates import DateAndDurationConverter as dateOrDurationUtils
from PragmaticQuant.Basic.Structure import RawMapDatas, LabelledMatrix


def date_conversion(d):
    try:
        return DateTime(d.year, d.month, d.day, d.hour, d.minute, d.second, d.microsecond)
    except:
        try:
            return DateTime(d.year, d.month, d.day)
        except:
            return dateOrDurationUtils.ConvertDate(d)


def python_date_conversion(d):    
    if isinstance(d, datetime) or isinstance(d, date):
        return d

    if isinstance(d, str):
        dotnet_date = date_or_duration(d)
        return python_date_conversion(dotnet_date)

    if isinstance(d, DateOrDuration) and d.IsDate:
        return python_date_conversion(d.Date)

    return datetime(d.Year, d.Month, d.Day, d.Hour, d.Minute, d.Second, d.Millisecond)


def date_or_duration(d):
    try:
        return dateOrDurationUtils.ConvertDateOrDuration(date_conversion(d))
    except:
        return dateOrDurationUtils.ConvertDateOrDuration(d)


def duration(d):
    return dateOrDurationUtils.ConvertDuration(d)


def currency(c):
    return Currency.Parse(c)


def double_array_conversion(m):

    nbRows = len(m)
    if nbRows == 0:
        return Array.CreateInstance(Double, 0, 0)

    nbCols = len(m[0])
    if nbCols == 0:
        return Array.CreateInstance(Double, nbRows, 0)

    array = Array.CreateInstance(Double, nbRows, nbCols)
    for i in range(0, nbRows):
        row = m[i]
        for j in range(0, nbCols):
            array[i, j] = row[j]
    return array


def python_array2d_conversion(m):
    nbRows = m.GetLength(0)
    nbCols = m.GetLength(1)
    array = []
    for i in range(0, nbRows):
        row = []
        for j in range(0, nbCols):
            row.append(m[i, j])
        array.append(row)
    return array


def python_array_conversion(m):
    array = []
    for i in range(0, m.Length):
        array.append(m[i])
    return array


def rawcurve(datas):
    pillars = [date_or_duration(k) for (k, v) in datas.items()]
    vals = [v for (k, v) in datas.items()]
    return RawMapDatas[DateOrDuration, Double](pillars, vals)


def dico_from_rawcurve(raw_curve):
    pillars = []
    for d in raw_curve.Pillars:
        try:
            pillars.append(python_date_conversion(d))
        except:
            pillars.append(str(d))

    return dict(zip(pillars, raw_curve.Values))
