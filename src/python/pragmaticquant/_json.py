"""
Json utils for serialization
"""
import json
from datetime import date, datetime
from _utils import flatten

BASIC_TYPES = [bool, int, float, str]

def as_jsonable(obj):
    if obj is None:
        return None

    if type(obj) in BASIC_TYPES:
        return obj

    if isinstance(obj, date) | isinstance(obj, datetime):
        return obj.isoformat()

    if isinstance(obj, dict):
        obj_dict = {}
        for key, val in obj.items():
            obj_dict[as_jsonable(key)] = as_jsonable(val)
        return obj_dict

    if isinstance(obj, list):
        return [as_jsonable(o) for o in obj]

    try:
        return obj.as_jsonable()
    except:
        raise Exception('object is not jsonable')

class Jsonable(object):

    def as_jsonable(self):
        d = { '__classname__' : type(self).__name__ }
        d.update(self.__dict__)
        return as_jsonable(d)

    def __repr__(self):
        return json.dumps(self.as_jsonable(), indent=4)

def class_from_jsonable(obj, types):

    if obj is None:
        return None

    if type(obj) in BASIC_TYPES:
        return obj

    clsname =  obj.get('__classname__', None)
    if clsname is None:
        raise Exception('Missing __classname__, unable to find class type')

    class_type = next(t for t in flatten(types) if t.__name__==clsname)
    class_instance = class_type.__new__(class_type)

    from_json_method = getattr(class_instance, "from_jsonable", None)
    if callable(from_json_method):
        return from_json_method(obj)

    class_instance.__dict__.update(obj)
    return class_instance
