from collections import Iterable


def _flatten(items, ignore_types=(str, bytes)):
    for x in items:
        if isinstance(x, Iterable) and not isinstance(x, ignore_types):
            yield from _flatten(x, ignore_types)
        else:
            yield x


def flatten(items, ignore_types=(str, bytes)):
    if items is None:
        return []
    return list(_flatten([items], ignore_types))


def single_or_flat(items):
    flat = flatten(items)
    if len(flat) == 1:
        return flat[0]
    else:
        return flat


def single_or_flat_output(f):
    def single_or_flat_wrapper(*args, **kwargs):
        r = f(*args, **kwargs)
        return single_or_flat(r)
    return single_or_flat_wrapper
