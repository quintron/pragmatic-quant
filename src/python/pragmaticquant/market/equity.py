import json
from .._dotnetutils import python_date_conversion, date_or_duration, date_conversion
from .._dotnetutils import double_array_conversion, python_array_conversion
from .._dotnetutils import dico_from_rawcurve, rawcurve
from .._json import Jsonable

# Pythonnet import of dot net types
from System import Double, DateTime, Array
from PragmaticQuant.Basic.Dates import DateOrDuration
from PragmaticQuant.Basic.Structure import RawMapDatas, LabelledMatrix
from PragmaticQuant.Markets.RawDatas import AssetMarketDatas


class VolMatrix(Jsonable):

    def __init__(self, *, strikes, vols_by_pillars=None):
        self.strikes = strikes
        self.maturities = []
        self.vols = []

        if vols_by_pillars is None:
            vols_by_pillars = {}

        for (maturity, vol_slice) in vols_by_pillars.items():
            if len(vol_slice) != len(strikes):
                raise Exception("VolMatrix : vols size incompatible with strikes")
            self.maturities.append(maturity)
            self.vols.append(vol_slice)

    def __setitem__(self, maturity, vols):

        if isinstance(vols, float):
            vols = [float(vols)] * len(self.strikes)

        if len(vols) != len(self.strikes):
            raise Exception("VolMatrix : vols size incompatible with strikes")

        self.maturities.append(maturity)
        self.vols.append(vols)
        return

    @staticmethod
    def from_dotnet_volmatrix(voldatas):
        vols = voldatas.Values
        vols_by_pillars = {}
        for row in range(0, len(voldatas.RowLabels)):
            vol_slice = [vols[row, k] for k in range(0, len(voldatas.ColLabels))]
            d = voldatas.RowLabels[row]
            if d.IsDate:
                pillar = python_date_conversion(d)
            else:
                pillar = str(d.Duration)
            vols_by_pillars[pillar] = vol_slice

        return VolMatrix(strikes=python_array_conversion(voldatas.ColLabels),
                         vols_by_pillars=vols_by_pillars)

    def to_dotnet_volmatrix(self):
        pillarsArray = Array[DateOrDuration]([date_or_duration(d) for d in self.maturities])
        strikeArray = Array[Double](self.strikes)
        volMatrix = double_array_conversion(self.vols)
        return LabelledMatrix[DateOrDuration, Double, Double](pillarsArray, strikeArray, volMatrix)

    @staticmethod
    def from_jsonable(obj):
        vol_matrix = VolMatrix(strikes=obj['strikes'])
        vol_matrix.maturities = obj['maturities']
        vol_matrix.vols = obj['vols']
        return vol_matrix

class AssetMarket(Jsonable):

    def __init__(self, name: str, *,
                 currency: str = '',
                 spot: float = float('nan'),
                 repos=None,
                 divs=None,
                 volmatrix=None) -> None:
        self.name = name
        self.currency = currency
        self.spot = spot

        self.repos = {}
        if not repos is None:
            self.repos = repos

        self.divs = {}
        if not divs is None:
            self.divs = divs

        if volmatrix is None:
            self.volmatrix = VolMatrix(strikes=[spot])
        else:
            self.volmatrix = volmatrix

    @staticmethod
    def from_dotnet_assetmarket(datas: AssetMarketDatas):

        repos = dico_from_rawcurve(datas.RepoCurve)
        divs = dico_from_rawcurve(datas.DividendForward)
        return AssetMarket(datas.Name,
                           currency=datas.Currency,
                           spot=datas.Spot,
                           repos=repos,
                           divs=divs,
                           volmatrix=VolMatrix.from_dotnet_volmatrix(datas.VolatilityMatrix))

    def to_dotnet_assetmarket(self):
        mkt = AssetMarketDatas()
        mkt.Name = self.name
        mkt.Currency = self.currency
        mkt.Spot = self.spot
        mkt.RepoCurve = rawcurve(self.repos)
        mkt.VolatilityMatrix = self.volmatrix.to_dotnet_volmatrix()

        if not self.divs is None:
            divdates = [date_conversion(k) for (k, v) in self.divs.items()]
            fwds = [v for (k, v) in self.divs.items()]
            mkt.DividendForward = RawMapDatas[DateTime, Double](divdates, fwds)
        return mkt

    @staticmethod
    def from_jsonable(obj):
        return AssetMarket(str(obj['name']),
                           currency=str(obj['currency']),
                           spot=float(obj['spot']),
                           repos=obj['repos'],
                           divs=obj['divs'],
                           volmatrix=VolMatrix.from_jsonable(obj['volmatrix']))
