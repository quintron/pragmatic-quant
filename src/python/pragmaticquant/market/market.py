import json
from datetime import datetime
from .._utils import flatten, single_or_flat, single_or_flat_output
from .._dotnetutils import python_date_conversion, date_conversion, date_or_duration, duration, currency
from .._json import Jsonable, class_from_jsonable
from .equity import AssetMarket
from .rate import DiscountCurve, RateVolMatrixDatas

# Pythonnet import of dot net types
from System import Double, DateTime, Array
from System.Collections.Generic import List
from PragmaticQuant.Basic.Structure import RawMapDatas, LabelledMatrix
from PragmaticQuant.Markets.RawDatas import AssetMarketDatas, DiscountCurveDatas, RateVolCubeDatas, MarketJsonParser
from PragmaticQuant.Markets.RawDatas import MarketDatas as RawMarket
from PragmaticQuant.Markets import MarketUtils, AssetMarketUtils
from PragmaticQuant.Markets import MarketDatasInterpolation as mktinterpoler
from PragmaticQuant.Pricing import MarketConfig, MarketConfigUtils


class MarketDatas(Jsonable):

    @staticmethod
    def __discount_tag(currency: str, financing: str):
        return '%s|%s' % (currency.lower(), financing.lower())

    def __init__(self, refdate, *, 
                 discount_curves=None,
                 ratevol_matrices=None, 
                 asset_markets=None):
        self.refdate = python_date_conversion(refdate)

        self.discount_curves = {}
        if discount_curves is not None:
            for curve in discount_curves:
                curve_tag = self.__discount_tag(curve.currency, curve.financing)
                self.discount_curves[curve_tag] = curve

        self.ratevol_matrices = {}
        if ratevol_matrices is not None:
            for ratevol_matrix in ratevol_matrices:
                cur = ratevol_matrix.currency
                self.ratevol_matrices[cur] = ratevol_matrix

        if asset_markets is None:
            self.asset_markets = {}
        else:
            self.asset_markets = dict((assetmkt.name, assetmkt) for assetmkt in asset_markets)

    @staticmethod
    def from_json(json_datas: str):
        rawmarket = MarketJsonParser.ParseMarket(json_datas)

        asset_markets = []
        for assetMkt in rawmarket.Assets:
            asset_markets.append(AssetMarket.from_dotnet_assetmarket(assetMkt))
        refdate = python_date_conversion(rawmarket.RefDate)
        mkt_datas = MarketDatas(refdate, asset_markets=asset_markets)
        for curve in rawmarket.DiscountCurves:
            discount_curve = DiscountCurve.from_dotnet_curve(curve)
            mkt_datas.set_discount_curve(discount_curve)       
        if rawmarket.RateVolCubes is not None:
            for ratevoldatas in rawmarket.RateVolCubes:
                ratevol_matrix = RateVolMatrixDatas.from_dotnet_volmatrix(ratevoldatas)
                mkt_datas.set_ratevol_matrix(ratevol_matrix)
        return mkt_datas

    @staticmethod
    def load_json(path: str):
        with open(path) as file:
            json_datas = file.read()
            return MarketDatas.from_json(json_datas)

    def discount_curve(self, currency: str, financing: str = 'ois'):
        return self.discount_curves[self.__discount_tag(currency, financing)]

    def set_discount_curve(self, curve: DiscountCurve):
        curve_tag = self.__discount_tag(curve.currency, curve.financing)
        self.discount_curves[curve_tag] = curve
        return

    def add_discount_curve(self, currency: str, financing: str = 'ois', *, rates=None):
        curve = DiscountCurve(currency, financing, rates)
        self.set_discount_curve(curve)
        return

    def ratevol_matrix(self, currency: str):
        return self.ratevol_matrices[currency]

    def set_ratevol_matrix(self, ratevol_matrix: RateVolMatrixDatas):
        self.ratevol_matrices[ratevol_matrix.currency] = ratevol_matrix
        return

    def add_ratevol_matrix(self, currency: str, *, tenors, atmvols_by_pillars=None):
        ratevol_matrix = RateVolMatrixDatas(currency,
                                            tenors=tenors,
                                            atmvols_by_pillars=atmvols_by_pillars)
        self.set_ratevol_matrix(ratevol_matrix)
        return

    def asset_market(self, name: str):
        return self.asset_markets[name]

    def set_asset_market(self, asset_mkt: AssetMarket):
        self.asset_markets[asset_mkt.name] = asset_mkt

    def add_asset_market(self, name: str):
        self.set_asset_market(AssetMarket(name))

    def to_dotnet_rawmarket(self):
        mkt = RawMarket()
        mkt.RefDate = date_conversion(self.refdate)

        curves = List[DiscountCurveDatas]()
        for _, curve in self.discount_curves.items():
            curves.Add(curve.to_dotnet_curve())
        mkt.DiscountCurves = curves 

        rate_vols = List[RateVolCubeDatas]()
        for _, ratevolmatrix in self.ratevol_matrices.items():
            rate_vols.Add(ratevolmatrix.to_dotnet_ratevolcube())
        mkt.RateVolCubes = rate_vols

        assets = List[AssetMarketDatas]()
        for assetMkt in self.asset_markets.values():
            assets.Add(assetMkt.to_dotnet_assetmarket())
        mkt.Assets = assets

        return mkt

    def to_json(self)-> str:
        dotnet_mkt = self.to_dotnet_rawmarket()
        return MarketJsonParser.ToJson(dotnet_mkt)

    @staticmethod
    def from_jsonable(obj):
        discount_fromjsonable = map(lambda o: class_from_jsonable(o, DiscountCurve), 
                                    obj['discount_curves'].values())
        asset_fromjsonable = [AssetMarket.from_jsonable(o)
                              for o in obj['asset_markets'].values()]
        return MarketDatas(obj['refdate'],
                           discount_curves=discount_fromjsonable,
                           asset_markets=asset_fromjsonable)


def _build_market_config(datas, scenario=None):
    config = MarketConfig()
    if scenario is not None:
        config.MarketScenario = scenario.to_dotnet_scenario()
    try:
        config.MarketDatas = datas.to_dotnet_rawmarket()
    except:
        config.Market = datas
    return config


class Market(object):
    """Market : TODO DOC
    """

    def __init__(self, mkt_datas: MarketDatas, scenario=None):
        mkt_config = _build_market_config(mkt_datas, scenario)
        self.__dotnet_mkt = MarketConfigUtils.BuildMarket(mkt_config)

    def __discountcurve(self, curve_currency):
        return self.__dotnet_mkt.OisDiscountCurve(currency(curve_currency))

    def __ratemarket(self, rate_currency):
        return self.__dotnet_mkt.RateMarket(currency(rate_currency))

    def __borforwardcurve(self, rate_currency, tenor):
        return self.__ratemarket(rate_currency).BorForwardCurve(duration(tenor))

    def __swaprateforwardcurve(self, rate_currency):
        return self.__ratemarket(rate_currency).SwapRateCurve()

    def __ratevolcube(self, rate_currency):
        return self.__ratemarket(rate_currency).VolCube()

    def __assetmarket(self, key):
        assetid = MarketUtils.AssetIdFromName(self.__dotnet_mkt, key)
        return self.__dotnet_mkt.AssetMarket(assetid)

    def __equityfwdcurve(self, key):
        return self.__assetmarket(key).ForwardCurve()

    def __volsurface(self, key):
        return self.__assetmarket(key).VolSurface()

    def __logswapcurve(self, key):
        return AssetMarketUtils.LogSwapVolCurve(self.__assetmarket(key))

    def __maturity_dates(self, maturity):
        maturities = flatten([maturity])
        return [date_or_duration(m).ToDate(self.__dotnet_mkt.RefDate) 
                for m in maturities]

    def refdate(self):
        """ Return market ref date
        """
        return python_date_conversion(self.__dotnet_mkt.RefDate)

    @single_or_flat_output
    def discount(self, currency, maturities):
        discount_curve = self.__discountcurve(currency)
        return [discount_curve.Zc(d) for d in self.__maturity_dates(maturities)]

    @single_or_flat_output
    def forward_bor(self, currency, maturities, tenor):
        fwd_curve = self.__borforwardcurve(currency, tenor)
        return [fwd_curve.Fwd(d) for d in self.__maturity_dates(maturities)]

    @single_or_flat_output
    def forward_swaprate(self, currency, maturities, tenor_or_end):
        fwd_curve = self.__swaprateforwardcurve(currency)
        rate_end = date_or_duration(tenor_or_end)
        return [fwd_curve.Fwd(d, rate_end) for d in self.__maturity_dates(maturities)]

    @single_or_flat_output
    def ratevol(self, currency, maturities, tenor_or_end, strike):
        ratevol_cube = self.__ratevolcube(currency)
        maturity_dates = self.__maturity_dates(maturities)
        rate_end = date_or_duration(tenor_or_end)
        return [ratevol_cube.Volatility(t, rate_end, strike) for t in maturity_dates]

    @single_or_flat_output
    def assetspot(self, assetid):
        return [self.__assetmarket(a).Spot for a in flatten(assetid)]

    @single_or_flat_output
    def assetforward(self, assetid, maturities):
        fwd_curve = self.__equityfwdcurve(assetid)
        return [fwd_curve.Fwd(d) for d in self.__maturity_dates(maturities)]

    def assetvol(self, assetid, maturities, strikes):
        mats = [date_or_duration(m) for m in flatten([maturities])]
        ref_date = self.__dotnet_mkt.RefDate
        mat_as_date = [d.ToDate(ref_date) for d in mats]

        vol_surface = self.__volsurface(assetid)
        vols = []
        for m in mat_as_date:
            smile = [vol_surface.Volatility(m, k) for k in flatten([strikes])]
            if len(smile) == 1:
                vols.append(smile[0])
            else:
                vols.append(smile)

        if len(vols) == 1:
            return single_or_flat(vols[0])
        else:
            return vols

    @single_or_flat_output
    def assetlogswapvol(self, assetid, maturities):
        logswap_curve = self.__logswapcurve(assetid)
        vols = [logswap_curve.Volatility(d)
                for d in self.__maturity_dates(maturities)]
        return vols
