import json
from .._dotnetutils import rawcurve, dico_from_rawcurve, date_or_duration, python_date_conversion
from .._dotnetutils import python_array_conversion, double_array_conversion
from .._json import Jsonable
# Pythonnet import of dot net types
from System import Double, String, DateTime, Array
from PragmaticQuant.Basic.Dates import DateOrDuration
from PragmaticQuant.Basic.Structure import LabelledMatrix
from PragmaticQuant.Markets.RawDatas import DiscountCurveDatas, RateVolCubeDatas


class DiscountCurve(Jsonable):

    def __init__(self, currency: str, financing: str = 'ois', rates=None):
        self.currency = currency
        self.financing = financing
        self.rates = rates if rates is not None else {}

    @staticmethod
    def from_dotnet_curve(dotnet_discount):
        return DiscountCurve(dotnet_discount.Currency,
                             dotnet_discount.Financing,
                             dico_from_rawcurve(dotnet_discount.ZcRates))

    def to_dotnet_curve(self) ->DiscountCurveDatas:
        dotnet_discount = DiscountCurveDatas()
        dotnet_discount.Currency = self.currency
        dotnet_discount.ZcRates = rawcurve(self.rates)
        dotnet_discount.Financing = self.financing
        return dotnet_discount


class RateVolMatrixDatas(Jsonable):

    def __init__(self, currency: str, *, tenors, atmvols_by_pillars=None):
        self.currency = currency
        self.tenors = tenors
        self.maturities = []
        self.atmvols = []

        if atmvols_by_pillars is None:
            atmvols_by_pillars = {}

        for (maturity, vol_slice) in atmvols_by_pillars.items():
            if len(vol_slice) != len(tenors):
                raise Exception("RateVolMatrixDatas : vols size incompatible with tenors")
            self.maturities.append(maturity)
            self.atmvols.append(vol_slice)

    def __setitem__(self, maturity, vols):

        if isinstance(vols, float):
            vols = [float(vols)] * len(self.tenors)

        if len(vols) != len(self.tenors):
            raise Exception("RateVolMatrixDatas : vols size incompatible with strikes")

        self.maturities.append(maturity)
        self.atmvols.append(vols)
        return

    @staticmethod
    def from_dotnet_volmatrix(ratevoldatas):
        voldatas = ratevoldatas.AtmVolMatrix
        vols = voldatas.Values
        vols_by_pillars = {}
        for row in range(0, len(voldatas.RowLabels)):
            vol_slice = [vols[row, k] for k in range(0, len(voldatas.ColLabels))]
            d = voldatas.RowLabels[row]
            if d.IsDate:
                pillar = python_date_conversion(d)
            else:
                pillar = str(d.Duration)
            vols_by_pillars[pillar] = vol_slice

        return RateVolMatrixDatas(ratevoldatas.Currency, 
                                  tenors=python_array_conversion(voldatas.ColLabels),
                                  atmvols_by_pillars=vols_by_pillars)

    def to_dotnet_ratevolcube(self):
        pillarsArray = Array[DateOrDuration]([date_or_duration(d) for d in self.maturities])
        tenorsArray = Array[String](self.tenors)
        volMatrix = double_array_conversion(self.atmvols)
        atm_vol_matrix = LabelledMatrix[DateOrDuration, String, Double](pillarsArray, tenorsArray, volMatrix)

        rate_vol_cube = RateVolCubeDatas()
        rate_vol_cube.Currency = self.currency
        rate_vol_cube.AtmVolMatrix = atm_vol_matrix
        return rate_vol_cube

    @staticmethod
    def from_jsonable(obj):
        ratevol_matrix = RateVolMatrixDatas(str(obj['currency']), tenors=obj['tenors'])
        ratevol_matrix.maturities = obj['maturities']
        ratevol_matrix.atmvols = obj['atmvols']
        return ratevol_matrix
