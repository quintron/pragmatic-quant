import numpy as np
from ._json import Jsonable, class_from_jsonable
from ._dotnetutils import rawcurve, double_array_conversion, currency, date_or_duration
from ._utils import flatten

# Pythonnet import of dot net types
from System import String, Double, Array
from System.Collections.Generic import Dictionary
from PragmaticQuant.Basic.Structure import LabelledMatrix
from PragmaticQuant.Model.Equity.LocalVolatility import LocalVolModelCalibDesc, LocalVolSampleConfig
from PragmaticQuant.Model.Equity.Bergomi import Bergomi2FModelCalibDesc, StochasticVolParams
from PragmaticQuant.Model.Equity.BergomiMixedLv import BergomiMixedLvCalibDesc
from PragmaticQuant.Model.Rate.HullWhite import HwnModelCalibDesc
from PragmaticQuant.Model.Rate import SwaptionSmilePoint


class EquityModel(Jsonable):
    def __init__(self, underlyings, divmodel, correls=None):
        self.underlyings = flatten(underlyings)

        if divmodel is None:
            self.divmodel = {'50y': 1.0}
        else:
            self.divmodel = divmodel

        if correls is None:
            self.correls = np.identity(len(self.underlyings)).tolist()
        elif isinstance(correls, float):
            if correls > 1.0 or correls < -1.0:
                raise Exception('Correlation should be contains in [-1.0, 1.0]')
            nb_underlyings = len(self.underlyings)
            cst_correl = np.full((nb_underlyings, nb_underlyings), float(correls))
            np.fill_diagonal(cst_correl, 1.0)
            self.correls = cst_correl.tolist()
        else:
            self.correls = correls


class LocalVolModel(EquityModel):
    def __init__(self, underlyings, divmodel=None, correls=None):
        super().__init__(underlyings, divmodel, correls)

    def _calib_description(self):
        lv_config = LocalVolSampleConfig(1.0e-6, 251, True)
        correl_structure = LabelledMatrix[String, String, Double](self.underlyings,
                                                                  self.underlyings,
                                                                  double_array_conversion(self.correls))
        raw_div = rawcurve(self.divmodel)
        divmodel_by_udl = [raw_div for u in self.underlyings]
        return LocalVolModelCalibDesc(self.underlyings, divmodel_by_udl,
                                      correl_structure, lv_config)


def localvolmodel(underlyings, *, divmodel=None, correls=None):
    return LocalVolModel(underlyings, divmodel, correls)


class StochVolParams(Jsonable):
    def __init__(self, k1: float, k2: float, theta: float, nu: float,
                 rho_xy: float, rho_sx: float, rho_sy: float):
        self.k1 = k1
        self.k2 = k2
        self.theta = theta
        self.nu = nu
        self.rho_xy = rho_xy
        self.rho_sx = rho_sx
        self.rho_sy = rho_sy

    def _dotnet_params(self):
        return StochasticVolParams(self.k1, self.k2, self.theta,
                                   self.nu, self.rho_xy,
                                   self.rho_sx, self.rho_sy)


def stochvol_params(*, k1: float, k2: float, theta: float, nu: float,
                    rho_xy: float, rho_sx: float, rho_sy: float):
    return StochVolParams(k1, k2, theta, nu, rho_xy, rho_sx, rho_sy)


class BergomiModel(EquityModel):
    def __init__(self, underlyings, divmodel, stochvol_params):
        super().__init__(underlyings, divmodel)
        self.stochvol_params = stochvol_params
        if len(self.underlyings) != 1:
            raise Exception('BergomiModel allow only single underlying (yet)')

    def _calib_description(self):
        return Bergomi2FModelCalibDesc(self.underlyings[0],
                                       rawcurve(self.divmodel),
                                       self.stochvol_params._dotnet_params())

    @staticmethod
    def from_jsonable(obj):
        return BergomiModel(obj['underlyings'],
                            obj['divmodel'],
                            class_from_jsonable(obj['stochvol_params'], StochVolParams))


def bergomi2Fmodel(underlying, *, divmodel=None, stochvol_params):
    return BergomiModel(underlying, divmodel, stochvol_params)


class MixedLvBergomiModel(EquityModel):
    def __init__(self, underlyings, divmodel, stochvol_params, nb_calib_particle = 10000):
        super().__init__(underlyings, divmodel)
        self.stochvol_params = stochvol_params
        self.nb_calib_particle = int(nb_calib_particle)
        if len(self.underlyings) != 1:
            raise Exception('MixedBergomiModel allow only single underlying (yet)')

    def _calib_description(self):
        return BergomiMixedLvCalibDesc.Create(self.underlyings[0],
                                              rawcurve(self.divmodel),
                                              self.stochvol_params._dotnet_params(),
                                              int(self.nb_calib_particle))

    @staticmethod
    def from_jsonable(obj):
        return MixedLvBergomiModel(obj['underlyings'],
                                   obj['divmodel'],
                                   class_from_jsonable(obj['stochvol_params'], StochVolParams),
                                   int(obj['nb_calib_particle']))


def mixedlv_bergomi_model(underlying, *, divmodel=None, stochvol_params, nb_calib_particle = 10000):
    return MixedLvBergomiModel(underlying, divmodel, stochvol_params, nb_calib_particle)


class HwnModel(Jsonable):
    def __init__(self, currency: str,
                 mean_reversions,
                 factors,
                 factor_vol_ratios,
                 factor_correlations,
                 calibration_rates):
        self.currency = currency
        self.mean_reversions = mean_reversions
        self.factors = factors
        self.factor_vol_ratios = factor_vol_ratios
        self.factor_correlations = factor_correlations
        self.calibration_rates = calibration_rates

        assert len(mean_reversions) > 0
        for _, factor in factors.items():
            assert len(factor) == len(mean_reversions)

    def _calib_description(self):

        factors = Dictionary[String, Array[Double]]()
        for name, factor in self.factors.items():
            factors.Add(name, factor)
        calibration_pattern = []
        for maturity, underlying in self.calibration_rates.items():
            swaption = SwaptionSmilePoint(currency(self.currency),
                                          date_or_duration(maturity),
                                          date_or_duration(underlying),
                                          0.0, True)
            calibration_pattern.append(swaption)

        return HwnModelCalibDesc(currency(self.currency),
                                 self.mean_reversions,
                                 factors,
                                 self.factor_vol_ratios,
                                 double_array_conversion(self.factor_correlations),
                                 calibration_pattern, None)


def hw1Fmodel(currency: str, mean_reversion: float, calibration_pattern):
    return HwnModel(currency, [mean_reversion], 
                    {'Level': [1.0]}, [1.0], [[1.0]], 
                    calibration_pattern)


MODEL_CLASSES = [LocalVolModel, BergomiModel, MixedLvBergomiModel, HwnModel]


def model_from_jsonable(obj):
    return class_from_jsonable(obj, MODEL_CLASSES)
