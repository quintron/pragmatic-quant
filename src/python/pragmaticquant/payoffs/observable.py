from .._utils import flatten
from .._dotnetutils import date_conversion, duration, currency
from .._json import Jsonable, as_jsonable, class_from_jsonable
# Pythonnet import of dot net types
from PragmaticQuant.Product.Fixings import ObservationProcesses


class ObservableProcess(Jsonable):

    def to_dotnet_process(self):
        return NotImplemented


class EquitySpotProcess(ObservableProcess):

    def __init__(self, underlying: str):
        self.underlying = underlying

    def to_dotnet_process(self):
        return ObservationProcesses.Equity.Spot(self.underlying, False)  


def equity_spot(underlying: str):
    return EquitySpotProcess(underlying)


class EquityMinSpotProcess(ObservableProcess):

    def __init__(self, underlying: str, start_date, frequency):
        self.underlying = underlying
        self.start_date = start_date
        self.frequency = frequency

    def to_dotnet_process(self):
        return ObservationProcesses.Equity.Min([self.underlying], [1.0],
                                           date_conversion(self.start_date),
                                           duration(self.frequency))


def equity_min_spot(underlying: str, start_date, frequency):
    return EquityMinSpotProcess(underlying, start_date, frequency)


class EquityArithmeticBasket(ObservableProcess):

    def __init__(self, underlyings, underlyings_ref_values):
        self.underlyings = flatten(underlyings)
        self.underlyings_ref_values = flatten(underlyings_ref_values)
        if len(self.underlyings) != len(self.underlyings_ref_values):
            raise Exception('underlyings and underlyings_ref_values should have same size')

    def to_dotnet_process(self):
        return ObservationProcesses.Equity.ArithmeticBasket(self.underlyings,
                                                            [1.0 / v for v in self.underlyings_ref_values])


def equity_arithmetic_basket(underlyings, underlyings_ref_values):
    return EquityArithmeticBasket(underlyings, underlyings_ref_values)


class EquityWorstOf(ObservableProcess):

    def __init__(self, underlyings, underlyings_ref_values):
        self.underlyings = flatten(underlyings)
        self.underlyings_ref_values = flatten(underlyings_ref_values)
        if len(self.underlyings) != len(self.underlyings_ref_values):
            raise Exception('underlyings and underlyings_ref_values should have same size')

    def to_dotnet_process(self):
        return ObservationProcesses.Equity.WorstOf(self.underlyings,
                                                   [1.0 / v for v in self.underlyings_ref_values])


def equity_worst_of(underlyings, underlyings_ref_values):
    return EquityWorstOf(underlyings, underlyings_ref_values)


class EquityMinWorstOf(ObservableProcess):

    def __init__(self, underlyings, underlyings_ref_values, start_date, frequency):
        self.underlyings = flatten(underlyings)
        self.underlyings_ref_values = flatten(underlyings_ref_values)
        if len(self.underlyings) != len(self.underlyings_ref_values):
            raise Exception('underlyings and underlyings_ref_values should have same size')
        self.start_date = start_date
        self.frequency = frequency

    def to_dotnet_process(self):
        return ObservationProcesses.Equity.Min(self.underlyings,
                                               [1.0 / v for v in self.underlyings_ref_values],
                                               date_conversion(self.start_date),
                                               duration(self.frequency))


def equity_min_worst_of(underlyings, underlyings_ref_values, start_date, frequency):
    return EquityMinWorstOf(underlyings, underlyings_ref_values, start_date, frequency)


class EquityVarianceProcess(ObservableProcess):

    def __init__(self, underlying: str, start_date, variance_annual_basis=None):
        self.underlying = underlying
        self.start_date = start_date
        if variance_annual_basis is None:
            self.variance_annual_basis = float('Nan')
        else:
            self.variance_annual_basis = variance_annual_basis

    def to_dotnet_process(self):
        return ObservationProcesses.Equity.Variance(self.underlying, date_conversion(self.start_date),
                                                    self.variance_annual_basis)

    @staticmethod
    def from_jsonable(obj):
        return EquityVarianceProcess(str(obj['underlying']), 
                                     obj['start_date'], 
                                     obj['variance_annual_basis'])


def equity_variance(underlying: str, start_date, variance_annual_basis: float = None):
    return EquityVarianceProcess(underlying, start_date, variance_annual_basis)


class LiborProcess(ObservableProcess):

    def __init__(self, currency: str, tenor: str):
        self.currency = currency
        self.tenor = tenor

    def to_dotnet_process(self):
        return ObservationProcesses.Rate.Libor(currency(self.currency), duration(self.tenor))


def rate_libor(currency: str, tenor: str):
    return LiborProcess(currency, tenor)


class CmsProcess(ObservableProcess):

    def __init__(self, currency: str, tenor: str):
        self.currency = currency
        self.tenor = tenor

    def to_dotnet_process(self):
        return ObservationProcesses.Rate.Cms(currency(self.currency), 
                                             duration(self.tenor))


def rate_cms(currency: str, tenor: str):
    return CmsProcess(currency, tenor)


PROCESS_CLASSES = [
    EquitySpotProcess,
    EquityMinSpotProcess,
    EquityArithmeticBasket,
    EquityWorstOf,
    EquityMinWorstOf,
    EquityVarianceProcess,
    LiborProcess,
    CmsProcess
]


def observableprocess_from_jsonable(obj):
    return class_from_jsonable(obj, PROCESS_CLASSES)


__all__ = ['equity_spot',
           'equity_min_spot',
           'equity_arithmetic_basket',
           'equity_worst_of',
           'equity_min_worst_of',
           'equity_variance',
           'rate_libor',
           'rate_cms']
