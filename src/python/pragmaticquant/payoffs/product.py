from datetime import date, datetime
from .._dotnetutils import date_conversion, python_date_conversion, currency, duration
from .._json import Jsonable, class_from_jsonable
from .observable import ObservableProcess, PROCESS_CLASSES
# Pythonnet import of dot net types
from System import String, Object
from System.Collections.Generic import Dictionary
from PragmaticQuant.Basic import AssetId
from PragmaticQuant.Product import CouponDescriptions, CouponLinearCombination
from PragmaticQuant.Product import CallItem, TriggerItem, Callable, AutoCall
from PragmaticQuant.DslPayoff import DslPayoffDescription, DslCouponDescription, DslPayoffFactory


def _dotnet_params(parameters):
    params = Dictionary[String, Object]()
    for k, v in parameters.items():
        if isinstance(v, datetime) or isinstance(v, date):
            params.Add(k, date_conversion(v))
        elif isinstance(v, ObservableProcess):
            params.Add(k, v.to_dotnet_process())
        else:
            params.Add(k, v)
    return params


def _params_from_jsonable(obj):
    params = {}
    for key, value in obj.items():
        params[key] = class_from_jsonable(value, PROCESS_CLASSES)
    return params


class DslCoupon(Jsonable):

    def __init__(self, paydate, paycurrency, 
                 *, script: str, id: str='',
                 **kwargs):
        self.id = str(id)
        self.paydate = python_date_conversion(paydate)
        self.paycurrency = paycurrency
        self.script = script
        self.parameters = kwargs

    def dsl_description(self):
        return DslCouponDescription(str(self.id),
                                    date_conversion(self.paydate),
                                    currency(self.paycurrency),
                                    str(self.script),
                                    _dotnet_params(self.parameters))

    def compile(self):
        product = Product(self.id)
        product.add_coupon(self)
        return product.compile()

    @staticmethod
    def from_jsonable(obj):
        return DslCoupon(obj['paydate'], obj['paycurrency'],
                         script=obj['script'], id=str(obj['id']),
                         **_params_from_jsonable(obj['parameters']))


class CallabilityOption(Jsonable):

    def __init__(self, calldate, callfee=0.0, *, notice='7d'):
        self.calldate = python_date_conversion(calldate)
        self.notice = notice
        self.callfee = callfee


class TriggerOption(Jsonable):

    def __init__(self, calldate, callfee=0.0, *, notice='7d', trigger_script, **kwargs):
        self.calldate = python_date_conversion(calldate)
        self.notice = notice
        self.callfee = callfee
        self.trigger_script = trigger_script
        self.parameters = kwargs

    @staticmethod
    def from_jsonable(obj):
        return TriggerOption(obj['calldate'],
                             obj['callfee'],
                             notice=obj['notice'],
                             trigger_script=obj['trigger_script'],
                             **_params_from_jsonable(obj['parameters']))


class Product(Jsonable):

    def __init__(self, name: str):
        self.name = name
        self.coupons = []
        self.coupon_weights = []
        self.options = []
        self.triggers = []

    def _default_currency(self):
        currencies = set([cpn.paycurrency for cpn in self.coupons])
        if len(currencies) != 1:
            return None
        else:
            return currencies.pop()

    def _fee_coupon_desc(self, option, default_currency):
        if isinstance(option.callfee, float):
            if default_currency:
                
                if isinstance(option, CallabilityOption):
                    fee_id = str.format('{name}_CallFee', name=self.name)
                elif isinstance(option, TriggerOption):
                    fee_id = str.format('{name}_TriggerFee', name=self.name)                    

                return DslCoupon(option.calldate, default_currency,                                 
                                 script=float(option.callfee),
                                 id=fee_id
                                 ).dsl_description()
            else:
                raise Exception('Unable to find a default currency for callfee')
        return option.callfee.dsl_description()

    def add_coupon(self, coupon, weight: float=1.0):
        self.coupons.append(coupon)
        self.coupon_weights.append(weight)
        return

    def add_dslcoupon(self, paydate, paycurrency,
                      *, script,
                         weight: float=1.0,
                         id: str='',
                      **kwargs):
        dsl_cpn = DslCoupon(paydate, paycurrency, script=script, id=id, **kwargs)
        self.add_coupon(dsl_cpn)

    def add_callability_option(self, calldate, callfee=0.0, *, notice='7d'):
        self.options.append(CallabilityOption(calldate, callfee, notice=notice))
        return

    def add_trigger_option(self, calldate, callfee=0.0, *, notice='7d', trigger_script, **kwargs):
        trigger = TriggerOption(calldate, callfee,
                                notice=notice,
                                trigger_script=trigger_script,
                                **kwargs)
        self.triggers.append(trigger)
        return

    def __dotnet_dsl_cpns(self):              
        cpn_descs = [cpn.dsl_description() for cpn in self.coupons]
        dotnet_cpns = DslPayoffFactory().BuildCoupons(cpn_descs)
        return CouponLinearCombination.Create(self.coupon_weights, dotnet_cpns)

    def compile(self):
        underlying = self.__dotnet_dsl_cpns()

        isCallable = len(self.options) > 0
        isTrigger = len(self.triggers) > 0

        if not (isCallable or isTrigger):
            return underlying

        if isCallable & isTrigger:
            raise Exception('Mixed cancellation option and trigger are not yet allowed')

        default_currency = self._default_currency()
        payoff_factory = DslPayoffFactory()

        if isCallable:
            cancelFee_descs = [self._fee_coupon_desc(opt, default_currency) for opt in self.options]
            cancelFees = payoff_factory.BuildCoupons(cancelFee_descs)
            callItems = [CallItem(date_conversion(opt.calldate), duration(opt.notice), fee) for opt, fee in zip(self.options, cancelFees)]
            return Callable(underlying, callItems)

        if isTrigger:
            triggerFee_descs = [self._fee_coupon_desc(opt, default_currency) for opt in self.triggers]
            triggerFees = payoff_factory.BuildCoupons(triggerFee_descs)
            triggerFuncs = payoff_factory.Build([DslPayoffDescription(opt.trigger_script, _dotnet_params(opt.parameters)) for opt in self.triggers])
            triggersItems = []
            for i in range(0, len(self.triggers)):
                opt = self.triggers[i]
                trig = TriggerItem(date_conversion(opt.calldate), duration(opt.notice), triggerFees[i], triggerFuncs[i])
                triggersItems.append(trig)

            return AutoCall(underlying, triggersItems)

    @staticmethod
    def from_jsonable(obj):
        product = Product(obj['name'])
        product.coupon_weights = obj['coupon_weights']
        product.coupons = list(map(lambda o: class_from_jsonable(o, DslCoupon), obj['coupons']))
        product.options = list(map(lambda o: class_from_jsonable(o, CallabilityOption), obj['options']))
        product.triggers = list(map(lambda o: class_from_jsonable(o, TriggerOption), obj['triggers']))
        return product
