from ._utils import flatten
from ._json import Jsonable, class_from_jsonable
from ._dotnetutils import duration, date_or_duration, python_array2d_conversion, python_date_conversion
from .market import MarketDatas, _build_market_config 
from .model import model_from_jsonable
from .scenarios import scenario_from_jsonable
from .payoffs import Product

# Pythonnet import of dot net types
from PragmaticQuant.Basic.Dates import SimulationSchedules
from PragmaticQuant.Model import MonteCarloConfig, BmcStrategyConfig
from PragmaticQuant.Pricing import PriceableConfig as Priceable
from PragmaticQuant.Pricing import PricingConfig as DotNetPricingConfig
from PragmaticQuant.Pricing import PriceComputationTask, RiskAnalysisTask, IRiskAnalysis
from PragmaticQuant.Pricing import RiskAnalysisDetails, PricingResultExtensions, PriceDetails
from PragmaticQuant.Pricing import VanillaViewer


class MonteCarloAlgo(Jsonable):

    def __init__(self, nbpaths: int, *,
                 multithreading: bool = True,
                 nbsteps: int = 100,
                 minstep: str = '6h',
                 maxstep: str = '10d',
                 lsmc_nbpaths: int = None):
        self.nbpaths = nbpaths
        self.multithreading = multithreading
        self.nbsteps = nbsteps
        self.minstep = minstep
        self.maxstep = maxstep
        if lsmc_nbpaths is None:
            self.lsmc_nbpaths = max(1000, int(nbpaths / 2))
        else:
            self.lsmc_nbpaths = lsmc_nbpaths

    def _dotnet_algoconfig(self):
        lsmc_strategy = BmcStrategyConfig(int(self.lsmc_nbpaths))
        simulScheduleBuilder = SimulationSchedules.ScheduleBuilder(duration(self.maxstep), self.nbsteps, duration(self.minstep))
        return MonteCarloConfig(self.nbpaths, self.multithreading, simulScheduleBuilder, lsmc_strategy)


def algo_from_jsonable(obj):    
    return class_from_jsonable(obj, MonteCarloAlgo)


class PricingConfig(Jsonable):
    def __init__(self, *,
                 algorithm: MonteCarloAlgo,
                 model,
                 market,
                 scenario=None):
        self.algorithm = algorithm
        self.model = model
        self.market = market
        self.scenario = scenario

    def _dotnet_config(self):
        config = DotNetPricingConfig()
        config.NumericalMethod = self.algorithm._dotnet_algoconfig()
        config.Model = self.model._calib_description()
        config.Market = _build_market_config(self.market, self.scenario)
        return config

    @staticmethod
    def from_jsonable(obj):
        return PricingConfig(algorithm=algo_from_jsonable(obj['algorithm']),
                             model=model_from_jsonable(obj['model']),
                             market=MarketDatas.from_jsonable(obj['market']),
                             scenario=scenario_from_jsonable(obj['scenario']))


class PriceableConfig(Jsonable):

    def __init__(self, *,
                 product,
                 pricing_config):
        self.product = product
        self.pricing_config = pricing_config

    def _dotnet_priceable(self):
        priceable_config = Priceable()
        priceable_config.PricingConfig = self.pricing_config._dotnet_config()
        priceable_config.Product = self.product.compile()        
        return priceable_config

    @staticmethod
    def from_jsonable(obj):
        return PriceableConfig(product=Product.from_jsonable(obj['product']),
                               pricing_config=PricingConfig.from_jsonable(obj['pricing_config']))


def compute_price(pricing_config, *risks):
    priceonly = (risks is None) or (len(risks) == 0)
    if priceonly:
        task = PriceComputationTask()
    else:
        dotnet_risks = [r.to_dotnet_risk() for r in flatten(risks)]
        task = RiskAnalysisTask(dotnet_risks)

    try:
        result = task.Price(pricing_config)
    except TypeError:
        result = task.Price(pricing_config._dotnet_priceable())

    price = result.Price

    if priceonly:
        price_details = PricingResultExtensions.GetDetails[PriceDetails](result)
        details = []
        for d in price_details.Details:
            cpn_name = d.Item1
            cpn_paydate = python_date_conversion(d.Item2.Date)
            cpn_price = d.Item3.Value
            cpn_currency = d.Item3.Currency.Name
            details.append((cpn_name, cpn_paydate, cpn_price, cpn_currency))
    else:
        risk_details = PricingResultExtensions.GetDetails[RiskAnalysisDetails](result)
        details = dict([(t.Item1, t.Item2) for t in risk_details.RiskResults])

    return (price.Value, price.Currency.Name, details)


def get_risk_profile_task(risk_analysis, scenario, shocks):
    from pandas import DataFrame
    scenarios = [(float(shock), scenario(float(shock))) for shock in shocks]

    def risk_profile_task(priceable_config):
        base_scenario = priceable_config.pricing_config.scenario
        results = []
        for shock, scenario in scenarios:

            if base_scenario is None:
                priceable_config.pricing_config.scenario = scenario
            else:
                priceable_config.pricing_config.scenario = base_scenario | scenario

            price, cur, ars = compute_price(priceable_config, risk_analysis)
            results.append([scenario, shock, 'Price', price])
            for (ar, val) in ars.items():
                results.append([scenario, shock, ar, val])
            print('Pricing Done for shock= %s' % str(shock))
        priceable_config.pricing_config.scenario = base_scenario
        return DataFrame(results, columns=['Scenario', 'Shock', 'Risk', 'Value'])

    return risk_profile_task


def get_equitysmile_viewer_task(underlying: str, maturities, strikes):

    flat_mats = flatten([maturities])
    flat_strikes = flatten([strikes])
    mats = [date_or_duration(d) for d in flat_mats]

    def equitysmile_task(pricing_config):
        smile_array = VanillaViewer.EquitySmile(pricing_config._dotnet_config(),
                                                underlying, mats, flat_strikes)
        smile = []
        for i in range(0, len(flat_mats)):
            smile.append([smile_array[i, j] for j in range(len(flat_strikes))])
        return smile

    return equitysmile_task
