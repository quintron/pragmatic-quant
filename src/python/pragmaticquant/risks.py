from ._json import Jsonable, class_from_jsonable
from ._utils import flatten, single_or_flat_output
from ._dotnetutils import date_or_duration

# Pythonnet import of dot net types
from PragmaticQuant.Pricing import RiskAnalysis


class Risk(Jsonable):

    def __init__(self, name):
        self.name = name

    def to_dotnet_risk(self):
        return NotImplemented


class EquityVega(Risk):

    def __init__(self, asset: str):
        self.asset = asset
        super(EquityVega, self).__init__('EquityVega')

    def to_dotnet_risk(self):
        return RiskAnalysis.Equity.Vega(self.asset)


class EquityVegaWave(Risk):

    def __init__(self, asset: str, buckets):
        self.asset = asset
        self.buckets = buckets
        super(EquityVegaWave, self).__init__('EquityVegaWave')

    def to_dotnet_risk(self):
        wave_buckets = [date_or_duration(d) for d in self.buckets]
        return RiskAnalysis.Equity.VegaWave(self.asset, wave_buckets)


class EquityDelta(Risk):

    def __init__(self, asset: str):
        self.asset = asset
        super(EquityDelta, self).__init__('EquityDelta')

    def to_dotnet_risk(self):
        return RiskAnalysis.Equity.Delta(self.asset)


class EquityDeltaRepo(Risk):

    def __init__(self, asset: str):
        self.asset = asset
        super(EquityDeltaRepo, self).__init__('EquityDeltaRepo')

    def to_dotnet_risk(self):
        return RiskAnalysis.Equity.DeltaRepo(self.asset)


class RateDelta(Risk):

    def __init__(self, currency: str):
        self.currency = currency
        super(RateDelta, self).__init__('RateDelta')

    def to_dotnet_risk(self):
        return RiskAnalysis.Rate.Delta(self.currency)


class RateVega(Risk):

    def __init__(self, currency: str):
        self.currency = currency
        super(RateVega, self).__init__('RateVega')

    def to_dotnet_risk(self):
        return RiskAnalysis.Rate.Vega(self.currency)


RISK_CLASSES = [
    EquityVega,
    EquityVegaWave,
    EquityDelta,
    EquityDeltaRepo,
    RateDelta,
    RateVega
]


def risk_from_jsonable(obj):
    return class_from_jsonable(obj, RISK_CLASSES)


@single_or_flat_output
def equity_vega(*assets):
    return [EquityVega(a) for a in flatten(assets)]


@single_or_flat_output
def equity_vega_wave(assets, buckets):
    return [EquityVegaWave(a, buckets) for a in flatten(assets)]


@single_or_flat_output
def equity_delta(*assets):
    return [EquityDelta(a) for a in flatten(assets)]


@single_or_flat_output
def equity_deltarepo(*assets):
    return [EquityDeltaRepo(a) for a in flatten(assets)]


@single_or_flat_output
def rate_delta(*currencies):
    return [RateDelta(c) for c in flatten(currencies)]


@single_or_flat_output
def rate_vega(*currencies):
    return [RateVega(c) for c in flatten(currencies)]
