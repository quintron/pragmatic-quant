from ._utils import flatten, single_or_flat_output
from ._json import Jsonable, class_from_jsonable
# Pythonnet import of dot net types
from PragmaticQuant.Markets.Scenarios import ParametricScenarios, MarketScenario

class Scenario(Jsonable):

    def to_dotnet_scenario(self):
        return NotImplemented

    def __or__(self, right_scenario):
        return CompoundedScenario(self, right_scenario)

class CompoundedScenario(Scenario):
    """
    Compounded scenario
    """
    def __init__(self, *scenarios):
        self.scenarios = flatten(scenarios)

    def to_dotnet_scenario(self):

        if self.scenarios is None or len(self.scenarios) == 0:
            return MarketScenario.Identity()

        compounded_sc = self.scenarios[0].to_dotnet_scenario()
        for sc in self.scenarios[1:]:
            compounded_sc = compounded_sc.CompoundLeft(sc.to_dotnet_scenario())
        return compounded_sc

    @staticmethod
    def from_jsonable(obj):
        return [class_from_jsonable(sc_obj, BASIC_SCENARIO_CLASSES) for sc_obj in obj['scenarios']]

class EquitySpotScenario(Scenario):
    """
    Relative shock of asset spot.
    """
    def __init__(self, assets, relative_shock: float, sticky_strike: bool = True):
        self.assets = flatten(assets)
        self.relative_shock = relative_shock
        self.sticky_strike = sticky_strike

    def to_dotnet_scenario(self):
        param_scenario = ParametricScenarios.EquitySpotShift(self.assets, self.sticky_strike)
        return param_scenario.Scenario(self.relative_shock)


class EquityVolScenario(Scenario):
    """
    Shock of asset vol.
    """
    def __init__(self, asset: str, shock: float):
        self.asset = asset
        self.shock = shock

    def to_dotnet_scenario(self):
        param_scenario = ParametricScenarios.EquityVolShift(self.asset)
        return param_scenario.Scenario(self.shock)


class EquityRepoScenario(Scenario):
    """
    Shock of asset repo
    """
    def __init__(self, asset: str, shock: float):
        self.asset = asset
        self.shock = shock

    def to_dotnet_scenario(self):
        param_scenario = ParametricScenarios.EquityRepoShift(self.asset)
        return param_scenario.Scenario(self.shock)


class RateScenario(Scenario):
    """
    Shock of rate curves
    """
    def __init__(self, currency: str, shock: float):
        self.currency = currency
        self.shock = shock

    def to_dotnet_scenario(self):
        param_scenario = ParametricScenarios.RateShift(self.currency)
        return param_scenario.Scenario(self.shock)


BASIC_SCENARIO_CLASSES = [
    EquitySpotScenario,
    EquityVolScenario,
    EquityRepoScenario,
    RateScenario
]

SCENARIO_CLASSES = [CompoundedScenario] + BASIC_SCENARIO_CLASSES


def scenario_from_jsonable(obj):
    return class_from_jsonable(obj, SCENARIO_CLASSES)


def equity_spot(assets, sticky_strike=False):
    @single_or_flat_output
    def get_scenarios(*shocks):
        return [EquitySpotScenario(assets, s, sticky_strike) for s in flatten(shocks)]
    return get_scenarios


def equity_vol(asset):
    @single_or_flat_output
    def get_scenarios(*shocks):
        return [EquityVolScenario(asset, s) for s in flatten(shocks)]
    return get_scenarios


def equity_repo(asset):
    @single_or_flat_output
    def get_scenarios(*shocks):
        return [EquityRepoScenario(asset, s) for s in flatten(shocks)]
    return get_scenarios


def rate(currency):
    @single_or_flat_output
    def get_scenarios(*shocks):
        return [RateScenario(currency, s) for s in flatten(shocks)]
    return get_scenarios
