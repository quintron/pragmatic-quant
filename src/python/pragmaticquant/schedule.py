from ._json import Jsonable
from ._dotnetutils import currency as get_currency
from ._dotnetutils import date_conversion, duration, python_date_conversion
# Pythonnet import of dot net types
from PragmaticQuant.Basic.Dates import SwapScheduleBuilder, BusinessConventions, SimpleCalendar, DayCountFrac
from PragmaticQuant.Markets.Rate import SwapScheduleConventions


def day_count(start, end, basis: str):
    dotnet_daycount = DayCountFrac.Parse(basis)
    return dotnet_daycount.Count(date_conversion(start), date_conversion(end))


class CouponSchedule(Jsonable):
    def __init__(self, start, end, pay, fixing):
        self.start = start
        self.end = end
        self.pay = pay
        self.fixing = fixing


def swap_schedule(start, end, periodicity, *,
                  currency=None,
                  fixing_shift=None,
                  business_convention=None):
    if currency is not None:
        if (fixing_shift is None) & (business_convention is None):
            schedule_builder = SwapScheduleConventions.For(get_currency(currency))
        else:
            raise Exception('When specifying a  currency, no override of fixing_shift or business_convention are allowed')
    else:
        if fixing_shift is None:
            fixing_shift = 2
        if business_convention is None:
            business_convention = 'EndOfMonth'
        schedule_builder = SwapScheduleBuilder(SimpleCalendar.Instance,
                                               int(fixing_shift),
                                               BusinessConventions.For(business_convention))
    dotnet_cpn_schedules = schedule_builder.GetSchedule(date_conversion(start),
                                                        date_conversion(end),
                                                        duration(periodicity))
    cpn_schedules = []
    for dotnet_cpn in dotnet_cpn_schedules:
        cpn = CouponSchedule(python_date_conversion(dotnet_cpn.Start),
                             python_date_conversion(dotnet_cpn.End),
                             python_date_conversion(dotnet_cpn.Pay),
                             python_date_conversion(dotnet_cpn.Fixing))
        cpn_schedules.append(cpn)
    return cpn_schedules
