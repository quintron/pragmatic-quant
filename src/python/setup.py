import os.path
import shutil
from setuptools import setup

setup(name='pragmaticquant',
      version='1.0',
      description='Derivatives pricing library',
      author='Quintron',
      packages=['pragmaticquant', 'pragmaticquant.market', 'pragmaticquant.payoffs' ],
      package_dir={'pragmaticquant': 'pragmaticquant'},
      package_data={'pragmaticquant': ['*.dll', '*.pdb']},
      data_files=[('', ['requirements.txt', 'Install.bat'])])
