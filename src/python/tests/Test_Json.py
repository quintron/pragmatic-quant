import unittest, json, datetime
from os import path as os_path
try:
    import context
except:
    pass
from pragmaticquant.market import rate, equity
from pragmaticquant import model, market, scenarios, _json
from pragmaticquant.pricing import PricingConfig, PriceableConfig, MonteCarloAlgo, compute_price
from pragmaticquant.model import LocalVolModel, BergomiModel, stochvol_params, model_from_jsonable


class JsonTest(unittest.TestCase):
    def __pricingconfig(self):
        json_path = os_path.join(os_path.dirname(__file__), 'mkt.json')
        mkt_datas = market.MarketDatas.load_json(json_path)
        return PricingConfig(algorithm=MonteCarloAlgo(20000),
                             model=model.localvolmodel('SP500'),
                             market=mkt_datas)

    def test_pricingconfig(self):
        config = self.__pricingconfig()
        json_config = json.dumps(config.as_jsonable())
        config_from_json = PricingConfig.from_jsonable(json.loads(json_config))                
        self.assertIsNotNone(config_from_json)

    def test_ratecurve(self):
        rates = {datetime.datetime(2017, 5, 23, 17) : 0.01, '1y' : 0.015}
        curve = rate.DiscountCurve('USD', 'ois', rates)
        json_curve = json.dumps(curve.as_jsonable())
        curve_from_json = _json.class_from_jsonable(json.loads(json_curve), [rate.DiscountCurve])

        self.assertEqual(curve.currency, curve_from_json.currency)
        self.assertEqual(curve.financing, curve_from_json.financing)
        self.assertEqual(len(curve.rates), len(curve_from_json.rates))
        self.assertEqual(curve.rates['1y'], curve_from_json.rates['1y'])

    def test_market(self):
        mkt = self.__pricingconfig().market
        json_mkt = json.dumps(mkt.as_jsonable())
        mkt_from_json = market.MarketDatas.from_jsonable(json.loads(json_mkt))

        asset_mkt = mkt.asset_market('SP500')
        assetmkt_from_json = mkt_from_json.asset_market('SP500')
        self.assertEqual(asset_mkt.spot, assetmkt_from_json.spot)
        self.assertDictEqual(asset_mkt.repos, assetmkt_from_json.repos)
        #TODO test other stuffs
    
    def test_model(self):
        lv_model = LocalVolModel('SP500', {'0y' : 0.0, '5y' : 1.0})
        json_lv_model = json.dumps(lv_model.as_jsonable())
        lv_model_from_json = model_from_jsonable(json.loads(json_lv_model))
        self.assertEqual(lv_model.underlyings, lv_model_from_json.underlyings)
        self.assertDictEqual(lv_model.divmodel, lv_model_from_json.divmodel)

        lv_model = LocalVolModel(['SP500', 'STOX5E'], {'0y' : 0.0, '5y' : 1.0}, [[1.0, 0.70],[0.70, 1.0]])
        json_lv_model = json.dumps(lv_model.as_jsonable())
        lv_model_from_json = model_from_jsonable(json.loads(json_lv_model))
        self.assertListEqual(lv_model.underlyings, lv_model_from_json.underlyings)
        self.assertDictEqual(lv_model.divmodel, lv_model_from_json.divmodel)
        self.assertListEqual(lv_model.correls, lv_model_from_json.correls)

        stoch_vol_params = stochvol_params(k1=5.30, k2=0.3, nu=1.90, rho_sx=-0.58, rho_sy=-0.5, rho_xy=0.2774, theta=0.2171)
        bergomi_model = BergomiModel('SP500', 
                                     {'0y' : 0.0, '5y' : 1.0},
                                     stoch_vol_params)
        json_bergomi_model = json.dumps(bergomi_model.as_jsonable())
        bergomi_model_from_json = model_from_jsonable(json.loads(json_bergomi_model)) 
        self.assertEqual(bergomi_model.underlyings, bergomi_model_from_json.underlyings)
        self.assertDictEqual(bergomi_model.divmodel, bergomi_model_from_json.divmodel)
        self.assertEqual(stoch_vol_params.nu, bergomi_model_from_json.stochvol_params.nu)

    def test_scenario(self):
        spot_sc = scenarios.EquitySpotScenario(['SP500', 'STOX5E_X'], -0.5, True)
        json_spot_sc = json.dumps(spot_sc.as_jsonable())
        spot_sc_from_json = scenarios.scenario_from_jsonable(json.loads(json_spot_sc))

        self.assertListEqual(spot_sc.assets, spot_sc_from_json.assets)
        self.assertEqual(spot_sc.relative_shock, spot_sc_from_json.relative_shock)
        self.assertEqual(spot_sc.sticky_strike, spot_sc_from_json.sticky_strike)


if __name__ == '__main__':
    unittest.main()
