import unittest
try:
    import context
except:
    pass
from datetime import datetime, timedelta
from pragmaticquant.market import MarketDatas, Market
from pragmaticquant import scenarios


class MarketTest(unittest.TestCase):

    @staticmethod
    def __market_sample():
        #Build market
        refdate = datetime(2017, 2, 2) + timedelta(hours=17)
        mkt_datas = MarketDatas(refdate)

        #Build discount
        mkt_datas.add_discount_curve('USD')
        curve = mkt_datas.discount_curve('USD')
        curve.rates.update({"6m": 0.005, "1y": 0.01, "5y": 0.02, "30y": 0.025})

        mkt_datas.add_ratevol_matrix('USD', 
                                     tenors=['3m', '5y', '10y'], 
                                     atmvols_by_pillars = {'1y' : [0.1, 0.4, 0.5],
                                                           '5y' : [0.5, 0.6, 0.7]})

        #Build asset market
        mkt_datas.add_asset_market('SP500')
        assetMkt = mkt_datas.asset_market('SP500')
        assetMkt.currency = 'USD'
        assetMkt.spot = 3500.0
        assetMkt.repos = {"6m": 0.001, "1y": 0.005 }
        assetMkt.divs = dict((datetime(2017 + int(m / 12), 1 + m % 12, 2) + timedelta(minutes = 1), 3500 * 0.03 / 12 ) for m in range(2, 5*12))

        assetMkt.volmatrix.strikes = [0.80*3500, 1.0*3500, 1.2*3500, 2.0 * 3500]
        assetMkt.volmatrix['3m'] = [0.2893, 0.20, 0.1271, 0.1271]
        assetMkt.volmatrix['6m'] = [0.2589, 0.20, 0.1519, 0.1519]
        assetMkt.volmatrix[datetime(2035, 1, 1)] = [0.2064, 0.20, 0.1947, 0.1947]

        return mkt_datas

    def test_market(self):
        mkt_datas = self.__market_sample()
        mkt = Market(mkt_datas)
        self.assertTrue(mkt.refdate() == mkt_datas.refdate)
        self.assertTrue(mkt.discount('USD', '0d') == 1.0, 'market discount failed')
        self.assertTrue(mkt.discount('USD', '2y') == 0.9753132498503071, 'market discount failed')
        self.assertEqual(mkt.ratevol('USD', '5y', '3m', 0.0), 0.5, 'market rate vol test failed')
        self.assertEqual(mkt.ratevol('USD', '10y', '5y', 0.0), 0.7, 'market rate vol test failed')
        self.assertTrue(mkt.assetspot("SP500") == 3500.0, "market spot test failed")
        self.assertTrue(mkt.assetforward("SP500", "0h") == 3500.0, "market fwd failed")
        self.assertTrue(mkt.assetforward("SP500", "26m") == 3330.482553936972, "market fwd failed")
        self.assertTrue(mkt.assetvol("SP500", "3m", 3500.0) == 0.20, "market vol test failed")
        self.assertTrue(mkt.assetvol("SP500", "6m", 3500.0) == 0.20, "market vol test failed")
        return

    def test_json(self):

        mkt_datas = self.__market_sample()
        mkt = Market(mkt_datas)

        json = mkt_datas.to_json()
        reversed_mkt_datas = MarketDatas.from_json(json)
        reversed_mkt = Market(reversed_mkt_datas)

        self.assertTrue(mkt.refdate() == reversed_mkt.refdate())

        #Compare discount curve
        mats = ['15d', '3m', '6m', '9m', '1y', '2y', '3y', '5y', '8y', '10y', '70y']
        zcs = mkt.discount('USD', mats)
        reversed_zcs = reversed_mkt.discount('USD', mats)
        for i in range(0, len(zcs)):
            self.assertTrue(zcs[i] == reversed_zcs[i])

        #Compare equity fwd curve
        self.assertTrue(mkt.assetspot('SP500') == reversed_mkt.assetspot('SP500'))
        fwds_mats = ['15d', '3m', '6m', '9m', '1y', '2y', '3y', '5y', '8y', '10y', '70y']
        fwds = mkt.assetforward('SP500', fwds_mats)
        reversed_fwds = reversed_mkt.assetforward('SP500', fwds_mats)
        for i in range(0, len(fwds)):
            self.assertTrue(fwds[i] == reversed_fwds[i])

        #Compare equity vol surface
        spot = mkt.assetspot('SP500')
        vols_strikes = [spot * m for m in [0.5, 0.9, 1.0, 1.1, 2.0]]
        for t in fwds_mats:
            vols = mkt.assetvol('SP500', t, vols_strikes)
            reversed_vols = reversed_mkt.assetvol('SP500', t, vols_strikes)
            for i in range(0, len(vols_strikes)):
                self.assertTrue(vols[i] == reversed_vols[i])

        return

    def test_Scenario(self):  # TODO create a specific scenario test class 

        mkt_datas = self.__market_sample()
        spot_shift = scenarios.equity_spot('SP500')
        shifted_mkt = Market(mkt_datas, spot_shift(0.20))
        self.assertTrue(shifted_mkt.assetspot("SP500") == 3500.0 * 1.20, "market spot test succeed")
        return


if __name__ == '__main__':
    unittest.main()
