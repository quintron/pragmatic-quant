import unittest, json
from datetime import date
try:
    import context
except:
    pass
from pragmaticquant import market, scenarios, payoffs
from pragmaticquant._json import class_from_jsonable


class ProductTest(unittest.TestCase):
    def test_Init(self):
        """
        Ensure Product initialisation.
        """             
        prod = payoffs.Product('EquityNote')
        put_redemption = payoffs.DslCoupon('02/17/2017', 'USD',
                                           script="Max(0.0, 1.0 - Index@FixingDate/Strike )",
                                           Index='Asset(SP500)',
                                           FixingDate=date(2017, 2, 17),
                                           Strike=1.0 )
        prod.add_coupon(put_redemption)
        prod.add_trigger_option('01/17/2017', 1.05, notice ='0d',
                                trigger_script="(Index@FixingDate > KO) ? 1.0 : 0.0",
                                Index='Asset(SP500)',
                                FixingDate='01/17/2017',
                                KO=1.1)
                                   
        dotnet_prod = prod.compile()
        self.assertIsNotNone(dotnet_prod, 'product compilation failed')

    def test_json(self):
        prod = payoffs.Product('EquityNote')
        put_redemption = payoffs.DslCoupon('02/17/2017', 'USD',
                                           script="Max(0.0, 1.0 - Index@FixingDate/Strike )",
                                           Index=payoffs.equity_spot('SP500'),
                                           FixingDate=date(2017, 2, 17),
                                           Strike=1.0)
        prod.add_coupon(put_redemption)
        prod.add_trigger_option('01/17/2017', 1.05, notice='0d',
                                trigger_script="(Index@FixingDate > KO) ? 1.0 : 0.0",
                                Index=payoffs.equity_spot('SP500'),
                                FixingDate='01/17/2017',
                                KO=1.1)

        json_prod = json.dumps(prod.as_jsonable())
        prod_from_json = class_from_jsonable(json.loads(json_prod), payoffs.Product)
        self.assertIsNotNone(prod_from_json)

        compiled_prod_from_json = prod_from_json.compile()
        self.assertIsNotNone(compiled_prod_from_json)


if __name__ == '__main__':
    unittest.main()
