import unittest, math
from os import path as os_path
from dateutil.relativedelta import relativedelta
try:
    import context
except:
    pass
from pragmaticquant import payoffs, model, market
from pragmaticquant.pricing import PricingConfig, PriceableConfig, MonteCarloAlgo, compute_price
from pragmaticquant.risks import equity_vega, equity_delta


class PricingTest(unittest.TestCase):

    def __pricingconfig(self):
        json_path = os_path.join(os_path.dirname(__file__), 'mkt.json')
        mkt_datas = market.MarketDatas.load_json(json_path)

        return PricingConfig(algorithm=MonteCarloAlgo(20000),
                             model=model.localvolmodel('SP500'),
                             market=mkt_datas)

    def __calloption(self, refdate):
        paydate = refdate + relativedelta(years=1)
        call = payoffs.Product('Call')
        call.add_dslcoupon(paydate, 'USD',
                           script='Max(0.0, Underlying@FixingDate - K)',
                           Underlying=payoffs.equity_spot('SP500'),
                           FixingDate=paydate,
                           K=1.0)
        return call

    def test_callprice(self):
        config = self.__pricingconfig()
        call =self.__calloption(config.market.refdate)
        call_config = PriceableConfig(product=call, pricing_config=config)

        price, _, _ = compute_price(call_config)
        self.assertTrue(math.fabs(price - 0.08418257580368321) < 1.0e-9, 'call price regression failed')

    def test_callrisk(self):
        config = self.__pricingconfig()
        call =self.__calloption(config.market.refdate)
        call_config = PriceableConfig(product=call, pricing_config=config)

        ars = [equity_vega('SP500'), equity_delta('SP500')]
        _, _, arRes = compute_price(call_config, ars)

        vega = arRes["Vega(SP500)"]
        self.assertTrue(math.fabs(vega-0.3929060941556174) < 1.0e-7, 'call vega regression failed')

        delta = arRes["Delta(SP500)"]
        self.assertTrue(math.fabs(delta-0.5354137055048829) < 1.0e-7, 'call delta regression failed')

    def test_variance(self):
        config = self.__pricingconfig()
        paydate = config.market.refdate + relativedelta(years=1)
        variance_cpn = payoffs.DslCoupon(paydate, 'USD',
                                         script='Variance@Paydate',
                                         Paydate=paydate,
                                         Variance=payoffs.equity_variance('SP500', config.market.refdate + relativedelta(days=1)))

        price, _, _ = compute_price(pricing_config=PriceableConfig(product=variance_cpn, pricing_config=config))
        self.assertTrue(math.fabs(price - 0.05568920539040319) < 1.0e-9, 'var swap price regression failed')

        mkt = market.Market(config.market)
        mkt_logswapvol = 100.0 * mkt.assetlogswapvol('SP500', paydate)
        model_logswapvol = 100.0 * math.sqrt(price / mkt.discount('USD', paydate))
        self.assertTrue(math.fabs(model_logswapvol - mkt_logswapvol) < 0.05, 'var swap price check failed')

if __name__ == '__main__':
    unittest.main()
