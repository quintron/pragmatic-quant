import unittest, os, json
from datetime import date
from dateutil.relativedelta import relativedelta
try: 
    import context
except: 
    pass
from pragmaticquant import market, schedule

class RateSwapTest(unittest.TestCase):

    def test_reconstitued_swap(self):
        json_path = os.path.join(os.path.dirname(__file__), 'mkt.json')
        mkt_datas = market.MarketDatas.load_json(json_path)
        mkt = market.Market(mkt_datas)

        start = mkt.refdate() + relativedelta(days=2)
        end = mkt.refdate() + relativedelta(years=20)

        floatleg = 0.0 
        for cpn in schedule.swap_schedule(start, end, '6m'):
            theta = schedule.day_count(cpn.start, cpn.end, 'act/360')
            zc = mkt.discount('EUR', cpn.pay)
            floatleg += theta * mkt.forward_bor('EUR', cpn.fixing, '6m') * zc

        bpv = 0.0 
        for cpn in schedule.swap_schedule(start, end, '1y'):
            theta = schedule.day_count(cpn.start, cpn.end, '30/360')
            zc = mkt.discount('EUR', cpn.pay)
            bpv += theta * zc

        swaprate = floatleg / bpv
        ref_swaprate = mkt.forward_swaprate('EUR', mkt.refdate(), end)

        self.assertEqual(swaprate, ref_swaprate)

if __name__ == '__main__':
    unittest.main()
