import unittest
try:
    import context
except:
    pass
from pragmaticquant._utils import flatten


class UtilsTest(unittest.TestCase):
    def test_flatten(self):        
        self.assertListEqual(flatten(None), [])
        self.assertListEqual(flatten([]), [])
        self.assertListEqual(flatten('toto'), ['toto'])
        self.assertListEqual(flatten(['toto']), ['toto'])
        self.assertListEqual(flatten(5.0), [5.0])
        self.assertListEqual(flatten([5.0]), [5.0])
        self.assertListEqual(flatten(['toto', ['a', 'b']]), ['toto', 'a', 'b'])
        self.assertListEqual(flatten([['toto', ['a', 'b']]]), ['toto', 'a', 'b'])
        self.assertListEqual(flatten([['toto', 'a'], 'b']), ['toto', 'a', 'b'])
        self.assertListEqual(flatten(['toto', [['a', 'b']]]), ['toto', 'a', 'b'])

if __name__ == '__main__':
    unittest.main()
