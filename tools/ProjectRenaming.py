import os
import fileinput

TOSEARCH = ['Pragmatic', 'pragmatic-', 'pragmatic']
TOREPLACE =''

def replace_text_file(fileToSearch, textToSearch, textToReplace):
	with fileinput.FileInput(fileToSearch, inplace=True) as file:
		for line in file:
			print(line.replace(textToSearch, textToReplace), end='')

current_dir = os.path.abspath(os.path.join(__file__, os.pardir, 'src'))

dotnet_exts = ['.cs', '.sln', '.csproj', '.dna']
python_exts =['.py', '.pyproj', '.sln']
file_extensions = dotnet_exts + python_exts

for relpath, dirs, files in os.walk(current_dir):	
	fulldir = os.path.join(current_dir, relpath)
	for file in files:
		filename, file_extension = os.path.splitext(file)
		if file_extension in file_extensions:		
			fullpath = os.path.join(fulldir, file)			
			for tosearch in TOSEARCH:
				replace_text_file(fullpath, tosearch, TOREPLACE)
			
			pattern_in_path=[tosearch for tosearch in TOSEARCH if (tosearch in filename)]
			if len(pattern_in_path)>0 : #REPLACE FILE NAME
				newpath = os.path.join(fulldir, file.replace(pattern_in_path[0], TOREPLACE))
				os.rename(fullpath, newpath)
	
	shortdir = fulldir.replace(current_dir, '')
	pattern_in_dir =[tosearch for tosearch in TOSEARCH if (tosearch in shortdir)]
	if(len(pattern_in_dir)): #REPLACE DIR NAME
		newdir =  shortdir.replace(pattern_in_dir[0], TOREPLACE)
		newdir = current_dir + newdir		
		os.rename(fulldir, newdir)

input("Press Enter to continue...")